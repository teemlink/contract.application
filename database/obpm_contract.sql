/*
Navicat MySQL Data Transfer

Source Server         : 192.168.88.231
Source Server Version : 50716
Source Host           : 192.168.88.231:3307
Source Database       : obpm_contract

Target Server Type    : MYSQL
Target Server Version : 50716
File Encoding         : 65001

Date: 2023-11-15 16:48:18
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tlk_agreement_approval
-- ----------------------------
DROP TABLE IF EXISTS `tlk_agreement_approval`;
CREATE TABLE `tlk_agreement_approval` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_contract_name` varchar(100) DEFAULT NULL,
  `ITEM_contract_number` varchar(100) DEFAULT NULL,
  `ITEM_apply_dept` varchar(100) DEFAULT NULL,
  `ITEM_apply_dept_name` varchar(100) DEFAULT NULL,
  `ITEM_dept_head` varchar(100) DEFAULT NULL,
  `ITEM_apply_date` datetime DEFAULT NULL,
  `ITEM_apply_person` longtext,
  `ITEM_apply_person_name` varchar(100) DEFAULT NULL,
  `ITEM_attend_dept` longtext,
  `ITEM_re_project_name` varchar(100) DEFAULT NULL,
  `ITEM_re_project_id` varchar(100) DEFAULT NULL,
  `ITEM_re_contract_name` varchar(100) DEFAULT NULL,
  `ITEM_re_contract_id` varchar(100) DEFAULT NULL,
  `ITEM_contract_amount` decimal(22,10) DEFAULT NULL,
  `ITEM_start_time` datetime DEFAULT NULL,
  `ITEM_end_time` datetime DEFAULT NULL,
  `ITEM_term` longtext,
  `ITEM_contract_description` longtext,
  `ITEM_counterpart` varchar(100) DEFAULT NULL,
  `ITEM_counterpart_number` varchar(100) DEFAULT NULL,
  `ITEM_counterpart_legal_person` varchar(100) DEFAULT NULL,
  `ITEM_counterpart_type` varchar(100) DEFAULT NULL,
  `ITEM_counterpart_address` varchar(100) DEFAULT NULL,
  `ITEM_bank` varchar(100) DEFAULT NULL,
  `ITEM_bank_number` varchar(100) DEFAULT NULL,
  `ITEM_sign_leader` varchar(100) DEFAULT NULL,
  `ITEM_contact_info` varchar(100) DEFAULT NULL,
  `ITEM_is_use_template` varchar(100) DEFAULT NULL,
  `ITEM_upload` longtext,
  `ITEM_annex` longtext,
  `ITEM_re_dept_person` varchar(100) DEFAULT NULL,
  `ITEM_re_dept_attitude` varchar(100) DEFAULT NULL,
  `ITEM_re_dept_date` datetime DEFAULT NULL,
  `ITEM_re_dept_head` varchar(100) DEFAULT NULL,
  `ITEM_re_dept_head_attitude` varchar(100) DEFAULT NULL,
  `ITEM_re_dept_head_date` datetime DEFAULT NULL,
  `ITEM_finance_person` varchar(100) DEFAULT NULL,
  `ITEM_finance_person_attitude` varchar(100) DEFAULT NULL,
  `ITEM_finance_person_date` datetime DEFAULT NULL,
  `ITEM_finance_head` varchar(100) DEFAULT NULL,
  `ITEM_finance_head_attitude` varchar(100) DEFAULT NULL,
  `ITEM_finance_head_date` datetime DEFAULT NULL,
  `ITEM_legal_person` varchar(100) DEFAULT NULL,
  `ITEM_legal_attitude` varchar(100) DEFAULT NULL,
  `ITEM_legal_date` datetime DEFAULT NULL,
  `ITEM_legal_head` varchar(100) DEFAULT NULL,
  `ITEM_legal_head_attitude` varchar(100) DEFAULT NULL,
  `ITEM_legal_head_date` datetime DEFAULT NULL,
  `ITEM_leader` varchar(100) DEFAULT NULL,
  `ITEM_leader_attitude` varchar(100) DEFAULT NULL,
  `ITEM_leader_date` datetime DEFAULT NULL,
  `ITEM_manager` varchar(100) DEFAULT NULL,
  `ITEM_manager_attitude` varchar(100) DEFAULT NULL,
  `ITEM_manager_date` datetime DEFAULT NULL,
  `ITEM_legal_rep` varchar(100) DEFAULT NULL,
  `ITEM_legal_rep_attitude` varchar(100) DEFAULT NULL,
  `ITEM_legal_rep_date` datetime DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  `ITEM_expiration_description` longtext,
  `ITEM_identification` varchar(100) DEFAULT NULL,
  `ITEM_center_director` varchar(100) DEFAULT NULL,
  `ITEM_center_director_attitude` varchar(100) DEFAULT NULL,
  `ITEM_center_director_date` datetime DEFAULT NULL,
  `ITEM_zg_leader` varchar(100) DEFAULT NULL,
  `ITEM_zg_leader_attitude` varchar(100) DEFAULT NULL,
  `ITEM_zg_leader_date` datetime DEFAULT NULL,
  `ITEM_apply_person_date` datetime DEFAULT NULL,
  `ITEM_dept_head_date` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_agreement_approval
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_agreement_contract_form
-- ----------------------------
DROP TABLE IF EXISTS `tlk_agreement_contract_form`;
CREATE TABLE `tlk_agreement_contract_form` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_contract_name` varchar(100) DEFAULT NULL,
  `ITEM_contract_number` varchar(100) DEFAULT NULL,
  `ITEM_contract_type` varchar(100) DEFAULT NULL,
  `ITEM_signing_date` datetime DEFAULT NULL,
  `ITEM_effective_date` datetime DEFAULT NULL,
  `ITEM_expiring_date` datetime DEFAULT NULL,
  `ITEM_contract_entity` varchar(100) DEFAULT NULL,
  `ITEM_contract_status` varchar(100) DEFAULT NULL,
  `ITEM_contract_amount` decimal(22,10) DEFAULT NULL,
  `ITEM_head` longtext,
  `ITEM_dept` longtext,
  `ITEM_re_project_name` varchar(100) DEFAULT NULL,
  `ITEM_re_project_id` varchar(100) DEFAULT NULL,
  `ITEM_re_contract_name` varchar(100) DEFAULT NULL,
  `ITEM_re_contract_id` varchar(100) DEFAULT NULL,
  `ITEM_contract_description` longtext,
  `ITEM_term` longtext,
  `ITEM_file` longtext,
  `ITEM_apply_person` varchar(100) DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  `ITEM_contract_ref` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_agreement_contract_form
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_collection_record
-- ----------------------------
DROP TABLE IF EXISTS `tlk_collection_record`;
CREATE TABLE `tlk_collection_record` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_RECEIVED_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ITEM_RECEIVED_DATE` datetime DEFAULT NULL,
  `ITEM_CONTRACT` varchar(100) DEFAULT NULL,
  `ITEM_CONTRACT_NUMBER` varchar(100) DEFAULT NULL,
  `ITEM_UNIT` varchar(100) DEFAULT NULL,
  `ITEM_COUNTERPART` varchar(100) DEFAULT NULL,
  `ITEM_RELATEID` varchar(100) DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  `ITEM_REGISTRANT` longtext,
  `ITEM_REGISTRATION_DATE` datetime DEFAULT NULL,
  `ITEM_BANK` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_collection_record
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_contract_change
-- ----------------------------
DROP TABLE IF EXISTS `tlk_contract_change`;
CREATE TABLE `tlk_contract_change` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_TITLE` varchar(100) DEFAULT NULL,
  `ITEM_APPLY_PERSON` longtext,
  `ITEM_APPLY_DEPT` varchar(100) DEFAULT NULL,
  `ITEM_APPLY_DATE` datetime DEFAULT NULL,
  `ITEM_CONTRACT_NAME` varchar(100) DEFAULT NULL,
  `ITEM_CONTRACT_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ITEM_CONTRACT_NUMBER` varchar(100) DEFAULT NULL,
  `ITEM_PAID_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ITEM_UNPAID_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ITEM_CHANGE_TYPE` varchar(100) DEFAULT NULL,
  `ITEM_OLD_CONTRACT` longtext,
  `ITEM_NEW_CONTRACT` longtext,
  `ITEM_CHANGE_REASON` longtext,
  `ITEM_PROTOCOL` longtext,
  `ITEM_ANNEX` longtext,
  `ID` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_contract_change
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_contract_close
-- ----------------------------
DROP TABLE IF EXISTS `tlk_contract_close`;
CREATE TABLE `tlk_contract_close` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_CONTRACT_NAME` varchar(100) DEFAULT NULL,
  `ITEM_CONTRACT_NUMBER` varchar(100) DEFAULT NULL,
  `ITEM_SIGNING_DATE` datetime DEFAULT NULL,
  `ITEM_CONTRACT_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ITEM_COUNTERPART` varchar(100) DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_contract_close
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_contract_detail
-- ----------------------------
DROP TABLE IF EXISTS `tlk_contract_detail`;
CREATE TABLE `tlk_contract_detail` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_OBJECT` varchar(100) DEFAULT NULL,
  `ITEM_UNIT` varchar(100) DEFAULT NULL,
  `ITEM_QUANTITY` decimal(22,10) DEFAULT NULL,
  `ITEM_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ITEM_PRICE` decimal(22,10) DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_contract_detail
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_contract_payment_after
-- ----------------------------
DROP TABLE IF EXISTS `tlk_contract_payment_after`;
CREATE TABLE `tlk_contract_payment_after` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_PAYMENT_TYPE` varchar(100) DEFAULT NULL,
  `ITEM_PAYMENT_TERMS` varchar(100) DEFAULT NULL,
  `ITEM_PAYMENT_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ITEM_PAYMENT_RATIO` decimal(22,10) DEFAULT NULL,
  `ITEM_PAYMENT_DATE` datetime DEFAULT NULL,
  `ITEM_DESCRIPTION` longtext,
  `ID` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_contract_payment_after
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_contract_payment_before
-- ----------------------------
DROP TABLE IF EXISTS `tlk_contract_payment_before`;
CREATE TABLE `tlk_contract_payment_before` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_PAYMENT_TYPE` varchar(100) DEFAULT NULL,
  `ITEM_PAYMENT_TERMS` varchar(100) DEFAULT NULL,
  `ITEM_PAYMENT_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ITEM_PAYMENT_RATIO` decimal(22,10) DEFAULT NULL,
  `ITEM_PAYMENT_DATE` datetime DEFAULT NULL,
  `ITEM_DESCRIPTION` longtext,
  `ID` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_contract_payment_before
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_contract_template
-- ----------------------------
DROP TABLE IF EXISTS `tlk_contract_template`;
CREATE TABLE `tlk_contract_template` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_TEMPLATE_NAME` varchar(100) DEFAULT NULL,
  `ITEM_CREATER` longtext,
  `ITEM_VERSION` varchar(100) DEFAULT NULL,
  `ITEM_CATEGORY` varchar(100) DEFAULT NULL,
  `ITEM_TEMPLATE` longtext,
  `ID` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_contract_template
-- ----------------------------
INSERT INTO `tlk_contract_template` VALUES (null, '2023-11-08 07:57:48', '基础设置/合同模板/contract_template', null, null, null, '37wipIeJTuR7qrkH3Nf', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '$$$test', '2023-11-08 07:54:16', '__mSryjVglOxq2vz1msZZ', '', null, '\0', '2', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '37wipIeJTuR7qrkH3Nf', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '3-视频类-视频制作及宣传（我方受托）-V1.0', '37wipIeJTuR7qrkH3Nf', '1.0', '7ZxloQDLgHRNRZcYXR2--__j12Xtf91UsiT5EvWUnn', '[{\"id\":\"3-视频类-视频制作及宣传（我方受托）-V1.0-uu345KbfUyAddbiA18I\",\"name\":\"3-视频类-视频制作及宣传（我方受托）-V1.0.docx\",\"path\":\"\\/uploads\\/item\\/2023\\/11\\/3-视频类-视频制作及宣传（我方受托）-V1.0-uu345KbfUyAddbiA18I.docx\",\"url\":\"\\/obpm\\/\\/uploads\\/item\\/2023\\/11\\/3-视频类-视频制作及宣传（我方受托）-V1.0-uu345KbfUyAddbiA18I.docx\",\"fileType\":\".docx\",\"uploader\":\"钱辉\",\"uploadTime\":\"2023-11-08 15:57:44\",\"userName\":\"钱辉\",\"time\":\"2023-11-08 15:57:44\",\"size\":\"47106\",\"isHasCompare\":false,\"showName\":\"3-视频类-视频制作及宣传（我方受托）-V1.0.docx\",\"isDelete\":false,\"isallowcopy\":\"true\",\"isallowdownload\":\"true\",\"isallowprint\":\"true\",\"openWaterMark\":false,\"watermark\":\"\",\"newversionscript\":true,\"versioncomparescript\":true,\"downloadscript\":true,\"deletescript\":true,\"renamescript\":true,\"previewscript\":true,\"editscript\":true,\"uid\":1699430264860,\"status\":\"success\",\"isEdit\":false}]', '2aIEKDlkbTXEJZGkKJL--__mSryjVglOxq2vz1msZZ');
INSERT INTO `tlk_contract_template` VALUES (null, '2023-11-08 08:01:39', '基础设置/合同模板/contract_template', null, null, null, '37wipIeJTuR7qrkH3Nf', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '$$$test', '2023-11-08 08:01:14', '__mSryjVglOxq2vz1msZZ', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '37wipIeJTuR7qrkH3Nf', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '9-采购服务类-口译服务协议-V1.0', '37wipIeJTuR7qrkH3Nf', '1.0', '1rR9gaptaxpMesNGVjc--__j12Xtf91UsiT5EvWUnn', '[{\"id\":\"9-采购服务类-口译服务协议-V1.0-06ChxEfQ0xosmKmfy7v\",\"name\":\"9-采购服务类-口译服务协议-V1.0.doc\",\"path\":\"\\/uploads\\/item\\/2023\\/11\\/9-采购服务类-口译服务协议-V1.0-06ChxEfQ0xosmKmfy7v.doc\",\"url\":\"\\/obpm\\/\\/uploads\\/item\\/2023\\/11\\/9-采购服务类-口译服务协议-V1.0-06ChxEfQ0xosmKmfy7v.doc\",\"fileType\":\".doc\",\"uploader\":\"钱辉\",\"uploadTime\":\"2023-11-08 16:01:17\",\"userName\":\"钱辉\",\"time\":\"2023-11-08 16:01:17\",\"size\":\"72704\",\"isHasCompare\":false,\"showName\":\"9-采购服务类-口译服务协议-V1.0.doc\",\"isDelete\":false,\"isallowcopy\":\"true\",\"isallowdownload\":\"true\",\"isallowprint\":\"true\",\"openWaterMark\":false,\"watermark\":\"\",\"newversionscript\":true,\"versioncomparescript\":true,\"downloadscript\":true,\"deletescript\":true,\"renamescript\":true,\"previewscript\":true,\"editscript\":true,\"uid\":1699430477494,\"status\":\"success\",\"isEdit\":false}]', '2u3edowFLDp04JAMAl4--__mSryjVglOxq2vz1msZZ');
INSERT INTO `tlk_contract_template` VALUES (null, '2023-10-16 05:31:46', '基础设置/合同模板/contract_template', null, null, null, 'hnu7lc0tv0okrCNI0y0', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', '$$$test', '2023-10-16 05:29:56', '__mSryjVglOxq2vz1msZZ', '', null, '\0', '2', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'hnu7lc0tv0okrCNI0y0', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '演播室技术服务协议', 'hnu7lc0tv0okrCNI0y0', '1.0', 'KF4zNdsfSbozLen0A3I--__j12Xtf91UsiT5EvWUnn', '[{\"id\":\"演播室技术服务协议-io3P9tSXnfcrcv4JY8B\",\"name\":\"演播室技术服务协议.docx\",\"path\":\"\\/uploads\\/item\\/2023\\/10\\/演播室技术服务协议-io3P9tSXnfcrcv4JY8B.docx\",\"url\":\"\\/obpm\\/\\/uploads\\/item\\/2023\\/10\\/演播室技术服务协议-io3P9tSXnfcrcv4JY8B.docx\",\"fileType\":\".docx\",\"uploader\":\"吕静璇\",\"uploadTime\":\"2023-10-16 13:30:29\",\"userName\":\"吕静璇\",\"time\":\"2023-10-16 13:30:29\",\"size\":\"18079\",\"isHasCompare\":false,\"showName\":\"演播室技术服务协议.docx\",\"isDelete\":false,\"isallowcopy\":\"true\",\"isallowdownload\":\"true\",\"isallowprint\":\"true\",\"openWaterMark\":false,\"watermark\":\"\",\"newversionscript\":true,\"versioncomparescript\":true,\"downloadscript\":true,\"deletescript\":true,\"renamescript\":true,\"previewscript\":true,\"editscript\":true,\"uid\":1697434229135,\"status\":\"success\",\"isEdit\":false}]', '6qXVdvsuoj3UzvAB5xt--__mSryjVglOxq2vz1msZZ');
INSERT INTO `tlk_contract_template` VALUES (null, '2023-11-08 08:01:09', '基础设置/合同模板/contract_template', null, null, null, '37wipIeJTuR7qrkH3Nf', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '$$$test', '2023-11-08 08:00:28', '__mSryjVglOxq2vz1msZZ', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '37wipIeJTuR7qrkH3Nf', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '8-采购服务类-演出服务协议-V1.0', '37wipIeJTuR7qrkH3Nf', '1.0', '1rR9gaptaxpMesNGVjc--__j12Xtf91UsiT5EvWUnn', '[{\"id\":\"8-采购服务类-演出服务协议-V1.0-pmdXW27oK0lUy825YGj\",\"name\":\"8-采购服务类-演出服务协议-V1.0.docx\",\"path\":\"\\/uploads\\/item\\/2023\\/11\\/8-采购服务类-演出服务协议-V1.0-pmdXW27oK0lUy825YGj.docx\",\"url\":\"\\/obpm\\/\\/uploads\\/item\\/2023\\/11\\/8-采购服务类-演出服务协议-V1.0-pmdXW27oK0lUy825YGj.docx\",\"fileType\":\".docx\",\"uploader\":\"钱辉\",\"uploadTime\":\"2023-11-08 16:00:33\",\"userName\":\"钱辉\",\"time\":\"2023-11-08 16:00:33\",\"size\":\"51363\",\"isHasCompare\":false,\"showName\":\"8-采购服务类-演出服务协议-V1.0.docx\",\"isDelete\":false,\"isallowcopy\":\"true\",\"isallowdownload\":\"true\",\"isallowprint\":\"true\",\"openWaterMark\":false,\"watermark\":\"\",\"newversionscript\":true,\"versioncomparescript\":true,\"downloadscript\":true,\"deletescript\":true,\"renamescript\":true,\"previewscript\":true,\"editscript\":true,\"uid\":1699430433438,\"status\":\"success\",\"isEdit\":false}]', '9zsR4FPHKHMsXZ6gLys--__mSryjVglOxq2vz1msZZ');
INSERT INTO `tlk_contract_template` VALUES (null, '2023-11-08 07:58:26', '基础设置/合同模板/contract_template', null, null, null, '37wipIeJTuR7qrkH3Nf', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '$$$test', '2023-11-08 07:58:02', '__mSryjVglOxq2vz1msZZ', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '37wipIeJTuR7qrkH3Nf', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '2-直播类-直播宣传推广服务合同（我方受托）-V1.0', '37wipIeJTuR7qrkH3Nf', '1.0', '7ZxloQDLgHRNRZcYXR2--__j12Xtf91UsiT5EvWUnn', '[{\"id\":\"2-直播类-直播宣传推广服务合同（我方受托）-V1.0-MZCDtGR2BTkAeF3EzAI\",\"name\":\"2-直播类-直播宣传推广服务合同（我方受托）-V1.0.docx\",\"path\":\"\\/uploads\\/item\\/2023\\/11\\/2-直播类-直播宣传推广服务合同（我方受托）-V1.0-MZCDtGR2BTkAeF3EzAI.docx\",\"url\":\"\\/obpm\\/\\/uploads\\/item\\/2023\\/11\\/2-直播类-直播宣传推广服务合同（我方受托）-V1.0-MZCDtGR2BTkAeF3EzAI.docx\",\"fileType\":\".docx\",\"uploader\":\"钱辉\",\"uploadTime\":\"2023-11-08 15:58:06\",\"userName\":\"钱辉\",\"time\":\"2023-11-08 15:58:06\",\"size\":\"49537\",\"isHasCompare\":false,\"showName\":\"2-直播类-直播宣传推广服务合同（我方受托）-V1.0.docx\",\"isDelete\":false,\"isallowcopy\":\"true\",\"isallowdownload\":\"true\",\"isallowprint\":\"true\",\"openWaterMark\":false,\"watermark\":\"\",\"newversionscript\":true,\"versioncomparescript\":true,\"downloadscript\":true,\"deletescript\":true,\"renamescript\":true,\"previewscript\":true,\"editscript\":true,\"uid\":1699430286880,\"status\":\"success\",\"isEdit\":false}]', 'Eq3o61RIFbPTBUOEEqh--__mSryjVglOxq2vz1msZZ');
INSERT INTO `tlk_contract_template` VALUES (null, '2023-11-15 01:20:50', '基础设置/合同模板/contract_template', null, null, null, 'Ro6tebUrjHw0LKx5ceU', 'yGzfIz8OZvGaoQDBJlb', 'yGzfIz8OZvGaoQDBJlb', '$$$test', '2023-11-15 01:18:24', '__mSryjVglOxq2vz1msZZ', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'Ro6tebUrjHw0LKx5ceU', '1i9X1xRMM6BRD9GMjj9', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, 'A类客户销售合同', 'Ro6tebUrjHw0LKx5ceU', '', 'pqY6DHw2ivlxcja1N5a--__j12Xtf91UsiT5EvWUnn', '[{\"id\":\"新建 DOCX 文档-uifv7gokCLL7TIkJ6B5\",\"name\":\"新建 DOCX 文档.docx\",\"path\":\"\\/uploads\\/item\\/2023\\/11\\/新建 DOCX 文档-uifv7gokCLL7TIkJ6B5.docx\",\"url\":\"\\/obpm\\/\\/uploads\\/item\\/2023\\/11\\/新建 DOCX 文档-uifv7gokCLL7TIkJ6B5.docx\",\"fileType\":\".docx\",\"uploader\":\"测试用户\",\"uploadTime\":\"2023-11-15 09:19:49\",\"userName\":\"测试用户\",\"time\":\"2023-11-15 09:19:49\",\"size\":\"0\",\"isHasCompare\":true,\"showName\":\"新建 DOCX 文档.docx\",\"isDelete\":false,\"isallowcopy\":\"true\",\"isallowdownload\":\"true\",\"isallowprint\":\"true\",\"openWaterMark\":false,\"watermark\":\"\",\"newversionscript\":true,\"versioncomparescript\":true,\"downloadscript\":true,\"deletescript\":true,\"renamescript\":true,\"previewscript\":true,\"editscript\":true,\"uid\":1700011189806,\"status\":\"success\",\"isEdit\":false}]', 'GDBGScj0MAEhWD0osx9--__mSryjVglOxq2vz1msZZ');
INSERT INTO `tlk_contract_template` VALUES (null, '2023-11-08 07:54:09', '基础设置/合同模板/contract_template', null, null, null, '37wipIeJTuR7qrkH3Nf', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '$$$test', '2023-11-08 07:53:25', '__mSryjVglOxq2vz1msZZ', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '37wipIeJTuR7qrkH3Nf', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '1-广告类-一般广告发布合同（我方受托）-V1.0', '37wipIeJTuR7qrkH3Nf', '1.0', '7ZxloQDLgHRNRZcYXR2--__j12Xtf91UsiT5EvWUnn', '[{\"id\":\"1-广告类-一般广告发布合同（我方受托）-V1.0-AYmby5CKiABT9kjrGT9\",\"name\":\"1-广告类-一般广告发布合同（我方受托）-V1.0.doc\",\"path\":\"\\/uploads\\/item\\/2023\\/11\\/1-广告类-一般广告发布合同（我方受托）-V1.0-AYmby5CKiABT9kjrGT9.doc\",\"url\":\"\\/obpm\\/\\/uploads\\/item\\/2023\\/11\\/1-广告类-一般广告发布合同（我方受托）-V1.0-AYmby5CKiABT9kjrGT9.doc\",\"fileType\":\".doc\",\"uploader\":\"钱辉\",\"uploadTime\":\"2023-11-08 15:53:37\",\"userName\":\"钱辉\",\"time\":\"2023-11-08 15:53:37\",\"size\":\"76800\",\"isHasCompare\":false,\"showName\":\"1-广告类-一般广告发布合同（我方受托）-V1.0.doc\",\"isDelete\":false,\"isallowcopy\":\"true\",\"isallowdownload\":\"true\",\"isallowprint\":\"true\",\"openWaterMark\":false,\"watermark\":\"\",\"newversionscript\":true,\"versioncomparescript\":true,\"downloadscript\":true,\"deletescript\":true,\"renamescript\":true,\"previewscript\":true,\"editscript\":true,\"uid\":1699430017318,\"status\":\"success\",\"isEdit\":false}]', 'HEoXi9eOhSvvM8G2Z1f--__mSryjVglOxq2vz1msZZ');
INSERT INTO `tlk_contract_template` VALUES (null, '2023-11-08 08:00:23', '基础设置/合同模板/contract_template', null, null, null, '37wipIeJTuR7qrkH3Nf', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '$$$test', '2023-11-08 08:00:04', '__mSryjVglOxq2vz1msZZ', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '37wipIeJTuR7qrkH3Nf', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '7-采购服务类-演员演出统筹服务协议-V1.0', '37wipIeJTuR7qrkH3Nf', '1.0', '1rR9gaptaxpMesNGVjc--__j12Xtf91UsiT5EvWUnn', '[{\"id\":\"7-采购服务类-演员演出统筹服务协议-V1.0-PZ0cfG0KOfYbdAARyJn\",\"name\":\"7-采购服务类-演员演出统筹服务协议-V1.0.doc\",\"path\":\"\\/uploads\\/item\\/2023\\/11\\/7-采购服务类-演员演出统筹服务协议-V1.0-PZ0cfG0KOfYbdAARyJn.doc\",\"url\":\"\\/obpm\\/\\/uploads\\/item\\/2023\\/11\\/7-采购服务类-演员演出统筹服务协议-V1.0-PZ0cfG0KOfYbdAARyJn.doc\",\"fileType\":\".doc\",\"uploader\":\"钱辉\",\"uploadTime\":\"2023-11-08 16:00:08\",\"userName\":\"钱辉\",\"time\":\"2023-11-08 16:00:08\",\"size\":\"567808\",\"isHasCompare\":false,\"showName\":\"7-采购服务类-演员演出统筹服务协议-V1.0.doc\",\"isDelete\":false,\"isallowcopy\":\"true\",\"isallowdownload\":\"true\",\"isallowprint\":\"true\",\"openWaterMark\":false,\"watermark\":\"\",\"newversionscript\":true,\"versioncomparescript\":true,\"downloadscript\":true,\"deletescript\":true,\"renamescript\":true,\"previewscript\":true,\"editscript\":true,\"uid\":1699430408609,\"status\":\"success\",\"isEdit\":false}]', 'jzhSyxOt0A0sPTz4HxO--__mSryjVglOxq2vz1msZZ');
INSERT INTO `tlk_contract_template` VALUES (null, '2023-10-16 05:32:37', '基础设置/合同模板/contract_template', null, null, null, 'hnu7lc0tv0okrCNI0y0', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', '$$$test', '2023-10-16 05:32:01', '__mSryjVglOxq2vz1msZZ', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'hnu7lc0tv0okrCNI0y0', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '设备采购合同', 'hnu7lc0tv0okrCNI0y0', '1.0', 'Mrk7iOSqs2uphPBFXPE--__j12Xtf91UsiT5EvWUnn', '[{\"id\":\"设备（货物）采购合同模版-1a2XoDD1G0nno6zbBwz\",\"name\":\"设备（货物）采购合同模版.doc\",\"path\":\"\\/uploads\\/item\\/2023\\/10\\/设备（货物）采购合同模版-1a2XoDD1G0nno6zbBwz.doc\",\"url\":\"\\/obpm\\/\\/uploads\\/item\\/2023\\/10\\/设备（货物）采购合同模版-1a2XoDD1G0nno6zbBwz.doc\",\"fileType\":\".doc\",\"uploader\":\"吕静璇\",\"uploadTime\":\"2023-10-16 13:32:11\",\"userName\":\"吕静璇\",\"time\":\"2023-10-16 13:32:11\",\"size\":\"49664\",\"isHasCompare\":false,\"showName\":\"设备（货物）采购合同模版.doc\",\"isDelete\":false,\"isallowcopy\":\"true\",\"isallowdownload\":\"true\",\"isallowprint\":\"true\",\"openWaterMark\":false,\"watermark\":\"\",\"newversionscript\":true,\"versioncomparescript\":true,\"downloadscript\":true,\"deletescript\":true,\"renamescript\":true,\"previewscript\":true,\"editscript\":true,\"uid\":1697434331565,\"status\":\"success\",\"isEdit\":false}]', 'SckyOHvu1LcFVEBbZGU--__mSryjVglOxq2vz1msZZ');
INSERT INTO `tlk_contract_template` VALUES (null, '2023-11-08 07:58:59', '基础设置/合同模板/contract_template', null, null, null, '37wipIeJTuR7qrkH3Nf', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '$$$test', '2023-11-08 07:58:36', '__mSryjVglOxq2vz1msZZ', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '37wipIeJTuR7qrkH3Nf', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '4-采购服务类-技术服务合作协议（…注意替换相应技术服务名称）-V1.0', '37wipIeJTuR7qrkH3Nf', '1.0', '1rR9gaptaxpMesNGVjc--__j12Xtf91UsiT5EvWUnn', '[{\"id\":\"4-采购服务类-技术服务合作协议（\\u2026注意替换相应技术服务名称）-V1.0-sNsjYhgQHiXyX5zt89i\",\"name\":\"4-采购服务类-技术服务合作协议（\\u2026注意替换相应技术服务名称）-V1.0.doc\",\"path\":\"\\/uploads\\/item\\/2023\\/11\\/4-采购服务类-技术服务合作协议（\\u2026注意替换相应技术服务名称）-V1.0-sNsjYhgQHiXyX5zt89i.doc\",\"url\":\"\\/obpm\\/\\/uploads\\/item\\/2023\\/11\\/4-采购服务类-技术服务合作协议（\\u2026注意替换相应技术服务名称）-V1.0-sNsjYhgQHiXyX5zt89i.doc\",\"fileType\":\".doc\",\"uploader\":\"钱辉\",\"uploadTime\":\"2023-11-08 15:58:43\",\"userName\":\"钱辉\",\"time\":\"2023-11-08 15:58:43\",\"size\":\"68096\",\"isHasCompare\":false,\"showName\":\"4-采购服务类-技术服务合作协议（\\u2026注意替换相应技术服务名称）-V1.0.doc\",\"isDelete\":false,\"isallowcopy\":\"true\",\"isallowdownload\":\"true\",\"isallowprint\":\"true\",\"openWaterMark\":false,\"watermark\":\"\",\"newversionscript\":true,\"versioncomparescript\":true,\"downloadscript\":true,\"deletescript\":true,\"renamescript\":true,\"previewscript\":true,\"editscript\":true,\"uid\":1699430323404,\"status\":\"success\",\"isEdit\":false}]', 'SfARSGYsfPGqYzGkOMc--__mSryjVglOxq2vz1msZZ');
INSERT INTO `tlk_contract_template` VALUES (null, '2023-11-08 07:59:59', '基础设置/合同模板/contract_template', null, null, null, '37wipIeJTuR7qrkH3Nf', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '$$$test', '2023-11-08 07:59:31', '__mSryjVglOxq2vz1msZZ', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '37wipIeJTuR7qrkH3Nf', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '6-采购服务类-导播摄像服务合作协议-V1.0', '37wipIeJTuR7qrkH3Nf', '1.0', '1rR9gaptaxpMesNGVjc--__j12Xtf91UsiT5EvWUnn', '[{\"id\":\"6-采购服务类-导播摄像服务合作协议-V1.0-jwBQi7ycIbqzJgsxIV5\",\"name\":\"6-采购服务类-导播摄像服务合作协议-V1.0.docx\",\"path\":\"\\/uploads\\/item\\/2023\\/11\\/6-采购服务类-导播摄像服务合作协议-V1.0-jwBQi7ycIbqzJgsxIV5.docx\",\"url\":\"\\/obpm\\/\\/uploads\\/item\\/2023\\/11\\/6-采购服务类-导播摄像服务合作协议-V1.0-jwBQi7ycIbqzJgsxIV5.docx\",\"fileType\":\".docx\",\"uploader\":\"钱辉\",\"uploadTime\":\"2023-11-08 15:59:42\",\"userName\":\"钱辉\",\"time\":\"2023-11-08 15:59:42\",\"size\":\"45801\",\"isHasCompare\":false,\"showName\":\"6-采购服务类-导播摄像服务合作协议-V1.0.docx\",\"isDelete\":false,\"isallowcopy\":\"true\",\"isallowdownload\":\"true\",\"isallowprint\":\"true\",\"openWaterMark\":false,\"watermark\":\"\",\"newversionscript\":true,\"versioncomparescript\":true,\"downloadscript\":true,\"deletescript\":true,\"renamescript\":true,\"previewscript\":true,\"editscript\":true,\"uid\":1699430382333,\"status\":\"success\",\"isEdit\":false}]', 'TQKnO2gYfiqWe2eKay3--__mSryjVglOxq2vz1msZZ');
INSERT INTO `tlk_contract_template` VALUES (null, '2023-11-08 07:59:27', '基础设置/合同模板/contract_template', null, null, null, '37wipIeJTuR7qrkH3Nf', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '$$$test', '2023-11-08 07:59:03', '__mSryjVglOxq2vz1msZZ', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '37wipIeJTuR7qrkH3Nf', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '5-采购服务类-视频后期制作委托协议-V1.0', '37wipIeJTuR7qrkH3Nf', '1.0', '1rR9gaptaxpMesNGVjc--__j12Xtf91UsiT5EvWUnn', '[{\"id\":\"5-采购服务类-视频后期制作委托协议-V1.0-CgF0y9wOqATGQxr1g6E\",\"name\":\"5-采购服务类-视频后期制作委托协议-V1.0.doc\",\"path\":\"\\/uploads\\/item\\/2023\\/11\\/5-采购服务类-视频后期制作委托协议-V1.0-CgF0y9wOqATGQxr1g6E.doc\",\"url\":\"\\/obpm\\/\\/uploads\\/item\\/2023\\/11\\/5-采购服务类-视频后期制作委托协议-V1.0-CgF0y9wOqATGQxr1g6E.doc\",\"fileType\":\".doc\",\"uploader\":\"钱辉\",\"uploadTime\":\"2023-11-08 15:59:07\",\"userName\":\"钱辉\",\"time\":\"2023-11-08 15:59:07\",\"size\":\"570368\",\"isHasCompare\":false,\"showName\":\"5-采购服务类-视频后期制作委托协议-V1.0.doc\",\"isDelete\":false,\"isallowcopy\":\"true\",\"isallowdownload\":\"true\",\"isallowprint\":\"true\",\"openWaterMark\":false,\"watermark\":\"\",\"newversionscript\":true,\"versioncomparescript\":true,\"downloadscript\":true,\"deletescript\":true,\"renamescript\":true,\"previewscript\":true,\"editscript\":true,\"uid\":1699430347729,\"status\":\"success\",\"isEdit\":false}]', 'y6RZkSsUwFqyyVPKAj2--__mSryjVglOxq2vz1msZZ');

-- ----------------------------
-- Table structure for tlk_contract_template_title
-- ----------------------------
DROP TABLE IF EXISTS `tlk_contract_template_title`;
CREATE TABLE `tlk_contract_template_title` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_MENU_NAME` varchar(100) DEFAULT NULL,
  `ITEM_SUPERIOR_MENU_NAME` varchar(100) DEFAULT NULL,
  `ITEM_SORT` decimal(22,10) DEFAULT NULL,
  `ITEM_SUPERIOR_MENU_ID` varchar(100) DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_contract_template_title
-- ----------------------------
INSERT INTO `tlk_contract_template_title` VALUES (null, '2023-10-31 02:12:18', '基础设置/合同模板/contract_template_title', null, null, null, '37wipIeJTuR7qrkH3Nf', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '$$$test', '2023-10-31 02:11:50', '__j12Xtf91UsiT5EvWUnn', '', null, '\0', '3', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '37wipIeJTuR7qrkH3Nf', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '支出类', '', '2.0000000000', '', '1rR9gaptaxpMesNGVjc--__j12Xtf91UsiT5EvWUnn');
INSERT INTO `tlk_contract_template_title` VALUES (null, '2023-10-31 02:11:50', '基础设置/合同模板/contract_template_title', null, null, null, '37wipIeJTuR7qrkH3Nf', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '$$$test', '2023-10-31 02:11:35', '__j12Xtf91UsiT5EvWUnn', '', null, '\0', '2', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '37wipIeJTuR7qrkH3Nf', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '收入类', '', '1.0000000000', '', '7ZxloQDLgHRNRZcYXR2--__j12Xtf91UsiT5EvWUnn');
INSERT INTO `tlk_contract_template_title` VALUES (null, '2023-11-15 01:17:55', '基础设置/合同模板/contract_template_title', null, null, null, 'Ro6tebUrjHw0LKx5ceU', 'yGzfIz8OZvGaoQDBJlb', 'yGzfIz8OZvGaoQDBJlb', '$$$test', '2023-11-15 01:17:46', '__j12Xtf91UsiT5EvWUnn', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'Ro6tebUrjHw0LKx5ceU', '1i9X1xRMM6BRD9GMjj9', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '销售合同A', '销售合同', '0.0000000000', 'pqY6DHw2ivlxcja1N5a--__j12Xtf91UsiT5EvWUnn', 'gY4js5jQCcZ2sXcxK1r--__j12Xtf91UsiT5EvWUnn');
INSERT INTO `tlk_contract_template_title` VALUES (null, '2023-09-07 06:37:24', '基础设置/合同模板/contract_template_title', null, null, null, 'hnu7lc0tv0okrCNI0y0', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', '$$$test', '2023-09-07 06:37:10', '__j12Xtf91UsiT5EvWUnn', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'hnu7lc0tv0okrCNI0y0', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '收入类', '', '0.0000000000', '', 'KF4zNdsfSbozLen0A3I--__j12Xtf91UsiT5EvWUnn');
INSERT INTO `tlk_contract_template_title` VALUES (null, '2023-10-31 02:12:28', '基础设置/合同模板/contract_template_title', null, null, null, '37wipIeJTuR7qrkH3Nf', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '$$$test', '2023-10-31 02:12:18', '__j12Xtf91UsiT5EvWUnn', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '37wipIeJTuR7qrkH3Nf', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '其它类', '', '3.0000000000', '', 'leacBB3LyUqG6z17Q97--__j12Xtf91UsiT5EvWUnn');
INSERT INTO `tlk_contract_template_title` VALUES (null, '2023-09-07 06:37:30', '基础设置/合同模板/contract_template_title', null, null, null, 'hnu7lc0tv0okrCNI0y0', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', '$$$test', '2023-09-07 06:37:24', '__j12Xtf91UsiT5EvWUnn', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'hnu7lc0tv0okrCNI0y0', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '支出类', '', '0.0000000000', '', 'Mrk7iOSqs2uphPBFXPE--__j12Xtf91UsiT5EvWUnn');
INSERT INTO `tlk_contract_template_title` VALUES (null, '2023-11-15 01:17:39', '基础设置/合同模板/contract_template_title', null, null, null, 'Ro6tebUrjHw0LKx5ceU', 'yGzfIz8OZvGaoQDBJlb', 'yGzfIz8OZvGaoQDBJlb', '$$$test', '2023-11-15 01:17:18', '__j12Xtf91UsiT5EvWUnn', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'Ro6tebUrjHw0LKx5ceU', '1i9X1xRMM6BRD9GMjj9', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '销售合同', '', '0.0000000000', '', 'pqY6DHw2ivlxcja1N5a--__j12Xtf91UsiT5EvWUnn');
INSERT INTO `tlk_contract_template_title` VALUES (null, '2023-10-16 03:22:09', '基础设置/合同模板/contract_template_title', null, null, null, 'nSk90pVKgErBwhDxsqF', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', '$$$test', '2023-10-16 03:21:55', '__j12Xtf91UsiT5EvWUnn', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'nSk90pVKgErBwhDxsqF', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '其它类', '', '0.0000000000', '', 'SnztWzp0rXp0SgGZCKE--__j12Xtf91UsiT5EvWUnn');
INSERT INTO `tlk_contract_template_title` VALUES (null, '2023-11-15 01:17:46', '基础设置/合同模板/contract_template_title', null, null, null, 'Ro6tebUrjHw0LKx5ceU', 'yGzfIz8OZvGaoQDBJlb', 'yGzfIz8OZvGaoQDBJlb', '$$$test', '2023-11-15 01:17:39', '__j12Xtf91UsiT5EvWUnn', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'Ro6tebUrjHw0LKx5ceU', '1i9X1xRMM6BRD9GMjj9', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '采购合同', '', '0.0000000000', '', 'wjTfsTffEjPnCPaileX--__j12Xtf91UsiT5EvWUnn');

-- ----------------------------
-- Table structure for tlk_contract_withdraw
-- ----------------------------
DROP TABLE IF EXISTS `tlk_contract_withdraw`;
CREATE TABLE `tlk_contract_withdraw` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_REASON` longtext,
  `ITEM_RELATEID` varchar(100) DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_contract_withdraw
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_counter
-- ----------------------------
DROP TABLE IF EXISTS `tlk_counter`;
CREATE TABLE `tlk_counter` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_NAME` varchar(100) DEFAULT NULL,
  `ITEM_COUNTER` decimal(22,10) DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_counter
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_counterpart_detail
-- ----------------------------
DROP TABLE IF EXISTS `tlk_counterpart_detail`;
CREATE TABLE `tlk_counterpart_detail` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  `ITEM_COUNTERPART` varchar(100) DEFAULT NULL,
  `ITEM_COUNTERPART_NUMBER` varchar(100) DEFAULT NULL,
  `ITEM_COUNTERPART_LEGAL_PERSON` varchar(100) DEFAULT NULL,
  `ITEM_COUNTERPART_TYPE` varchar(100) DEFAULT NULL,
  `ITEM_COUNTERPART_ADDRESS` varchar(100) DEFAULT NULL,
  `ITEM_BANK` varchar(100) DEFAULT NULL,
  `ITEM_BANK_NUMBER` varchar(100) DEFAULT NULL,
  `ITEM_SIGN_LEADER` varchar(100) DEFAULT NULL,
  `ITEM_CONTACT_INFO` varchar(100) DEFAULT NULL,
  `ITEM_IDENTIFICATION` varchar(100) DEFAULT NULL,
  `ITEM_INVOICE_HEADER` varchar(100) DEFAULT NULL,
  `ITEM_INVOICE_TYPE` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_counterpart_detail
-- ----------------------------
INSERT INTO `tlk_counterpart_detail` VALUES ('NvO1Izago3LhU2EZmyC--__7NuSLfcmZ3ILpeCe13O', '2023-11-08 07:51:45', '合同起草/支出类合同/counterpart_detail', null, null, null, '37wipIeJTuR7qrkH3Nf', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '$$$test', '2023-11-08 07:51:45', '__eMGGuhtm2iy4vrBbVfP', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '37wipIeJTuR7qrkH3Nf', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '9siqW6GVbwHjl7b2MAK--__eMGGuhtm2iy4vrBbVfP', '测试2', 'XDF2311080003', '3223', '客户', '233232', '23232', '332', '2232', '2332', '32332', '测试2', '');
INSERT INTO `tlk_counterpart_detail` VALUES ('CkVebZwxkH2nMWXtMcv--__7NuSLfcmZ3ILpeCe13O', '2023-11-06 06:53:12', '合同起草/支出类合同/counterpart_detail', null, null, null, 'WlrAo0DK6YyyoGCLzBr', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', '$$$test', '2023-11-06 06:53:12', '__eMGGuhtm2iy4vrBbVfP', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'WlrAo0DK6YyyoGCLzBr', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, 'BoRCsAW9MNgaXu6syAJ--__eMGGuhtm2iy4vrBbVfP', '广州市天翎网络科技有限公司', 'XDF2311060001', '周志军', '供应商', '广州市越秀区环市东路403号1802-05房', '广州市天翎网络科技有限公司', '11007036795501', '吴继华', '17301020963', '91440104793450545K', '广州市天翎网络科技有限公司', '增值税普通发票');
INSERT INTO `tlk_counterpart_detail` VALUES ('ox6O9gIpU6QfZw3T8Af--__7NuSLfcmZ3ILpeCe13O', '2023-11-07 10:22:31', '合同起草/支出类合同/counterpart_detail', null, null, null, 'Mxspdb6zQc7sibnXuG0', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', '$$$test', '2023-11-07 10:22:31', '__eMGGuhtm2iy4vrBbVfP', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'Mxspdb6zQc7sibnXuG0', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, 'dgVVTkncTE1ZMyto7Vx--__eMGGuhtm2iy4vrBbVfP', '广州市天翎网络科技有限公司', 'XDF2311060001', '周志军', '供应商', '广州市越秀区环市东路403号1802-05房', '广州市天翎网络科技有限公司', '11007036795501', '吴继华', '17301020963', '91440104793450545K', '广州市天翎网络科技有限公司', '增值税普通发票');
INSERT INTO `tlk_counterpart_detail` VALUES ('1wyAZXduj1dqO7xHC8n--__7NuSLfcmZ3ILpeCe13O', '2023-11-07 09:46:44', '合同起草/支出类合同/counterpart_detail', null, null, null, 'WlrAo0DK6YyyoGCLzBr', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', '$$$test', '2023-11-07 09:46:44', '__eMGGuhtm2iy4vrBbVfP', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'WlrAo0DK6YyyoGCLzBr', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, 'ElscS1GrR8PnfT9j1kb--__eMGGuhtm2iy4vrBbVfP', '广州市天翎网络科技有限公司', 'XDF2311060001', '周志军', '供应商', '广州市越秀区环市东路403号1802-05房', '广州市天翎网络科技有限公司', '11007036795501', '吴继华', '17301020963', '91440104793450545K', '广州市天翎网络科技有限公司', '增值税普通发票');
INSERT INTO `tlk_counterpart_detail` VALUES ('NvO1Izago3LhU2EZmyC--__7NuSLfcmZ3ILpeCe13O', '2023-11-08 07:51:45', '合同起草/支出类合同/counterpart_detail', null, null, null, '37wipIeJTuR7qrkH3Nf', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '$$$test', '2023-11-08 07:51:45', '__eMGGuhtm2iy4vrBbVfP', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '37wipIeJTuR7qrkH3Nf', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, 'FmqpP0iODmuxlIpXYZE--__eMGGuhtm2iy4vrBbVfP', '广州市天翎网络科技有限公司', 'XDF2311080002', '周志军', '客户', '广州市越秀区环市东路403号广州国际电子大厦    18楼1802-05', '平安银行广州分行', '11007036795501', '钟维玲', '13544520390', '2323323232323232', '广州市天翎网络科技有限公司', '增值税普通发票');
INSERT INTO `tlk_counterpart_detail` VALUES ('M8vYVlZr7mbTnySfpRe--__7NuSLfcmZ3ILpeCe13O', '2023-11-06 11:12:40', '合同起草/支出类合同/counterpart_detail', null, null, null, 'WlrAo0DK6YyyoGCLzBr', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', '$$$test', '2023-11-06 11:12:40', '__eMGGuhtm2iy4vrBbVfP', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'WlrAo0DK6YyyoGCLzBr', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, 'jlmzawrIjjyVQ7hX8Ja--__eMGGuhtm2iy4vrBbVfP', '广州市天翎网络科技有限公司', 'XDF2311060001', '周志军', '供应商', '广州市越秀区环市东路403号1802-05房', '广州市天翎网络科技有限公司', '11007036795501', '吴继华', '17301020963', '91440104793450545K', '广州市天翎网络科技有限公司', '增值税普通发票');
INSERT INTO `tlk_counterpart_detail` VALUES ('mdlSpsuJ2IHItfaAc8g--__7NuSLfcmZ3ILpeCe13O', '2023-11-06 06:38:23', '合同起草/支出类合同/counterpart_detail', null, null, null, 'WlrAo0DK6YyyoGCLzBr', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', '$$$test', '2023-11-06 06:38:23', '__eMGGuhtm2iy4vrBbVfP', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'WlrAo0DK6YyyoGCLzBr', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, 'KZWP8pT4qA54MhekkzU--__eMGGuhtm2iy4vrBbVfP', '广州市天翎网络科技有限公司', 'XDF2311060001', '周志军', '供应商', '广州市越秀区环市东路403号1802-05房', '广州市天翎网络科技有限公司', '11007036795501', '吴继华', '17301020963', '91440104793450545K', '广州市天翎网络科技有限公司', '增值税普通发票');
INSERT INTO `tlk_counterpart_detail` VALUES ('fm9TbnyMRatuyI7UoMl--__4WlF2OuywICW02IsUW8', '2023-11-09 06:03:55', '合同起草/支出类合同/counterpart_detail', null, null, null, 'lYjxYqrytPMOTiM6KLp', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '$$$test', '2023-11-09 06:03:55', '__eMGGuhtm2iy4vrBbVfP', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'lYjxYqrytPMOTiM6KLp', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, 'LrvlEUv49gU7mEge6kT--__eMGGuhtm2iy4vrBbVfP', '收入合同', 'XDF2311090003', '21122121', '客户', '1221212', '12222121', '21212122', '12121', '11221', '121212121', '收入合同', '');
INSERT INTO `tlk_counterpart_detail` VALUES ('KqOT95JfyyMjED96VEu--__7NuSLfcmZ3ILpeCe13O', '2023-11-06 06:29:30', '合同起草/支出类合同/counterpart_detail', null, null, null, 'WlrAo0DK6YyyoGCLzBr', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', '$$$test', '2023-11-06 06:29:30', '__eMGGuhtm2iy4vrBbVfP', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'WlrAo0DK6YyyoGCLzBr', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, 'O0Wrzp5ve2zGkG2RWTB--__eMGGuhtm2iy4vrBbVfP', '广州市天翎网络科技有限公司', 'XDF2311060001', '周志军', '供应商', '广州市越秀区环市东路403号1802-05房', '广州市天翎网络科技有限公司', '11007036795501', '吴继华', '17301020963', '91440104793450545K', '广州市天翎网络科技有限公司', '增值税普通发票');
INSERT INTO `tlk_counterpart_detail` VALUES ('gC7DZ9kPANuCEkH0yne--__7NuSLfcmZ3ILpeCe13O', '2023-10-26 08:08:44', '合同起草/支出类合同/counterpart_detail', null, null, null, 'cuUclVWNtvwD0nR9g1Z', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_6kZnRiHLEbuSM8L1eix', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_6kZnRiHLEbuSM8L1eix', '$$$test', '2023-10-26 08:08:44', '__eMGGuhtm2iy4vrBbVfP', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'cuUclVWNtvwD0nR9g1Z', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, 'sYBty3GMzg1KXuw3EuN--__eMGGuhtm2iy4vrBbVfP', '捷成华视网聚（常州）文化有限公司', 'XDF2310090001', '郑羌', '供应商', '江苏武进经济开发区祥云路6号', '北京银行北清路支行', '20000034302000016710112', '路涛', '13910253575', '9132041208699632XU', null, null);
INSERT INTO `tlk_counterpart_detail` VALUES ('EtRxI1hmgaBw7Kr4L4D--__jvpXgGxXUFqKjojY4Qy', '2023-11-09 06:04:04', '合同起草/支出类合同/counterpart_detail', null, null, null, 'lYjxYqrytPMOTiM6KLp', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '$$$test', '2023-11-09 06:04:04', '__eMGGuhtm2iy4vrBbVfP', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'lYjxYqrytPMOTiM6KLp', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, 'TKOQ5enSIkeLkDasWpm--__eMGGuhtm2iy4vrBbVfP', '广州市天翎网络科技有限公司', 'XDF2311090002', '周志军', '供应商', '广州市越秀区环市东路403号广州国际电子大厦18楼1802-05', '平安银行广州分行', '11007036795501', '钟维玲 ', '13544520390', '1221122', '广州市天翎网络科技有限公司', '增值税普通发票');
INSERT INTO `tlk_counterpart_detail` VALUES ('iBG5kWafqj9dt7kZQFq--__7NuSLfcmZ3ILpeCe13O', '2023-11-06 05:50:29', '合同起草/支出类合同/counterpart_detail', null, null, null, 'Mxspdb6zQc7sibnXuG0', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', '$$$test', '2023-11-06 05:50:29', '__eMGGuhtm2iy4vrBbVfP', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'Mxspdb6zQc7sibnXuG0', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, 'u53HTKBB87tfhjxrcBv--__eMGGuhtm2iy4vrBbVfP', '广州市天翎网络科技有限公司', 'XDF2311060001', '周志军', '供应商', '广州市越秀区环市东路403号1802-05房', '广州市天翎网络科技有限公司', '11007036795501', '吴继华', '17301020963', '91440104793450545K', '广州市天翎网络科技有限公司', '增值税普通发票');
INSERT INTO `tlk_counterpart_detail` VALUES ('gC7DZ9kPANuCEkH0yne--__7NuSLfcmZ3ILpeCe13O', '2023-10-26 08:08:44', '合同起草/支出类合同/counterpart_detail', null, null, null, 'cuUclVWNtvwD0nR9g1Z', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_6kZnRiHLEbuSM8L1eix', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_6kZnRiHLEbuSM8L1eix', '$$$test', '2023-10-26 08:08:44', '__eMGGuhtm2iy4vrBbVfP', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'cuUclVWNtvwD0nR9g1Z', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, 'vawbmY0tc1kx7Nlz4I7--__eMGGuhtm2iy4vrBbVfP', '优购文化传媒（北京）有限公司', 'XDF2308280001', '高巍', '客户', '北京市大兴区亦庄经济技术开发区同济南路19号', '交通银行股份有限公司北京自贸试验区支行', '1100 6077 7018 1700 87940', '罗佳睿', '15201100817', '91110302576870367Y', null, null);
INSERT INTO `tlk_counterpart_detail` VALUES ('h3RPw5RL61b6l6waPQQ--__7NuSLfcmZ3ILpeCe13O', '2023-11-08 02:13:29', '合同起草/支出类合同/counterpart_detail', null, null, null, '37wipIeJTuR7qrkH3Nf', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '$$$test', '2023-11-08 02:13:29', '__eMGGuhtm2iy4vrBbVfP', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '37wipIeJTuR7qrkH3Nf', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, 'WwdSwcJN3SKknwHnl9r--__eMGGuhtm2iy4vrBbVfP', '广州市天翎网络科技有限公司', 'XDF2311080002', '周志军', '客户', '广州市越秀区环市东路403号广州国际电子大厦    18楼1802-05', '平安银行广州分行', '11007036795501', '钟维玲', '13544520390', '2323323232323232', '广州市天翎网络科技有限公司', '增值税普通发票');
INSERT INTO `tlk_counterpart_detail` VALUES ('9meSkBipjgrJJwL1loG--__7NuSLfcmZ3ILpeCe13O', '2023-11-06 07:11:38', '合同起草/支出类合同/counterpart_detail', null, null, null, 'WlrAo0DK6YyyoGCLzBr', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', '$$$test', '2023-11-06 07:11:38', '__eMGGuhtm2iy4vrBbVfP', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'WlrAo0DK6YyyoGCLzBr', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, 'ZPOyNE4dXucniNPJYkr--__eMGGuhtm2iy4vrBbVfP', '广州市天翎网络科技有限公司', 'XDF2311060001', '周志军', '供应商', '广州市越秀区环市东路403号1802-05房', '广州市天翎网络科技有限公司', '11007036795501', '吴继华', '17301020963', '91440104793450545K', '广州市天翎网络科技有限公司', '增值税普通发票');

-- ----------------------------
-- Table structure for tlk_counterpart_management
-- ----------------------------
DROP TABLE IF EXISTS `tlk_counterpart_management`;
CREATE TABLE `tlk_counterpart_management` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_counterpart` varchar(100) DEFAULT NULL,
  `ITEM_counterpart_number` varchar(100) DEFAULT NULL,
  `ITEM_counterpart_sname` varchar(100) DEFAULT NULL,
  `ITEM_counterpart_type` varchar(100) DEFAULT NULL,
  `ITEM_status` varchar(100) DEFAULT NULL,
  `ITEM_head` varchar(100) DEFAULT NULL,
  `ITEM_head_telephone` varchar(100) DEFAULT NULL,
  `ITEM_cp_introduce` longtext,
  `ITEM_legal_representative` varchar(100) DEFAULT NULL,
  `ITEM_registration_info` varchar(100) DEFAULT NULL,
  `ITEM_registration_number` varchar(100) DEFAULT NULL,
  `ITEM_registration_amount` varchar(100) DEFAULT NULL,
  `ITEM_registration_date` datetime DEFAULT NULL,
  `ITEM_unit_status` varchar(100) DEFAULT NULL,
  `ITEM_unit_telephone` varchar(100) DEFAULT NULL,
  `ITEM_unit_email` varchar(100) DEFAULT NULL,
  `ITEM_fax` varchar(100) DEFAULT NULL,
  `ITEM_contact_address` varchar(100) DEFAULT NULL,
  `ITEM_settlement_type` varchar(100) DEFAULT NULL,
  `ITEM_bank` varchar(100) DEFAULT NULL,
  `ITEM_bank_number` varchar(100) DEFAULT NULL,
  `ITEM_currency` varchar(100) DEFAULT NULL,
  `ITEM_invoice_type` varchar(100) DEFAULT NULL,
  `ITEM_invoice_header` varchar(100) DEFAULT NULL,
  `ITEM_identification` varchar(100) DEFAULT NULL,
  `ITEM_telephone` varchar(100) DEFAULT NULL,
  `ITEM_address` varchar(100) DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_counterpart_management
-- ----------------------------
INSERT INTO `tlk_counterpart_management` VALUES (null, '2023-11-15 08:34:44', '基础设置/相对方管理/counterpart_management', 'YQOXj5pPsc3Jgt8JWZN--__GD8U7ledIDdqSex1aw1-WKqk3fMO6z2yCET2l1x', 'fVyiUPWErazp2FCoLQt', '2023-11-15 08:34:43', 'Ro6tebUrjHw0LKx5ceU', 'yGzfIz8OZvGaoQDBJlb', 'yGzfIz8OZvGaoQDBJlb', '$$$test', '2023-11-15 01:13:55', '__GD8U7ledIDdqSex1aw1', '', 'Ro6tebUrjHw0LKx5ceU', '\0', '2', '__M4oRwEVzSB1oI1D0faN', '1048576', '通过', '', '80', 'Ro6tebUrjHw0LKx5ceU', '1i9X1xRMM6BRD9GMjj9', '{\"1689326041173\":[]}', '{\"1689326050220\":[]}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"通过\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', '[]', '[]', null, null, null, null, '北京新媒体集团', 'XDF2311150001', '新媒体', '客户', null, '张三', '1388888888', '多家子公司的媒体集团', '李四', '', '', '', null, '', '81566666', '', '', '北京市XX区XX路8号', '银行转账', '', '', '人民币', '', '北京新媒体', '1000000001', '', '', 'YQOXj5pPsc3Jgt8JWZN--__GD8U7ledIDdqSex1aw1');

-- ----------------------------
-- Table structure for tlk_court_details
-- ----------------------------
DROP TABLE IF EXISTS `tlk_court_details`;
CREATE TABLE `tlk_court_details` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_START_DATE` datetime DEFAULT NULL,
  `ITEM_NOTES` longtext,
  `ID` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_court_details
-- ----------------------------
INSERT INTO `tlk_court_details` VALUES ('qXLIeK2lbbKjcKdVHVo--__TPksi5U5JPY35O51oMu', '2023-09-06 09:00:21', '台账管理/诉讼台账/court_details', null, null, null, null, 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', '$$$test', '2023-08-28 05:57:22', '__TT9kaMtyxJJfAlmqBXV', '', null, '\0', '2', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '0VFlDyTyT0gylwIP52Z', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '2023-08-28 00:00:00', 'aa', 'csgGqImcw1NFqadadLP--__TT9kaMtyxJJfAlmqBXV');
INSERT INTO `tlk_court_details` VALUES ('yujcX84oXqbggnrV9vy--__TPksi5U5JPY35O51oMu', '2023-08-29 02:23:31', '台账管理/诉讼台账/court_details', null, null, null, '__oP0irhWXGA2oZRusW1d', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s', '$$$test', '2023-08-29 02:23:10', '__TT9kaMtyxJJfAlmqBXV', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '__oP0irhWXGA2oZRusW1d', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '2023-08-29 00:00:00', 'asfafaf安徽发货就卡刷卡顺丰哈开始发货就卡死', 'KwZzEoeGH9X116JiYh9--__TT9kaMtyxJJfAlmqBXV');

-- ----------------------------
-- Table structure for tlk_expense_contract_approval
-- ----------------------------
DROP TABLE IF EXISTS `tlk_expense_contract_approval`;
CREATE TABLE `tlk_expense_contract_approval` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_contract_name` varchar(100) DEFAULT NULL,
  `ITEM_contract_number` varchar(100) DEFAULT NULL,
  `ITEM_apply_dept` varchar(100) DEFAULT NULL,
  `ITEM_apply_dept_name` varchar(100) DEFAULT NULL,
  `ITEM_dept_head` varchar(100) DEFAULT NULL,
  `ITEM_apply_date` datetime DEFAULT NULL,
  `ITEM_apply_person` longtext,
  `ITEM_apply_person_name` varchar(100) DEFAULT NULL,
  `ITEM_attend_dept` longtext,
  `ITEM_re_project_name` varchar(100) DEFAULT NULL,
  `ITEM_re_project_id` varchar(100) DEFAULT NULL,
  `ITEM_procure_type` varchar(100) DEFAULT NULL,
  `ITEM_re_contract_name` varchar(100) DEFAULT NULL,
  `ITEM_re_contract_id` varchar(100) DEFAULT NULL,
  `ITEM_re_contract_amount` decimal(22,10) DEFAULT NULL,
  `ITEM_contract_amount` decimal(22,10) DEFAULT NULL,
  `ITEM_deliver_date` datetime DEFAULT NULL,
  `ITEM_start_time` datetime DEFAULT NULL,
  `ITEM_end_time` datetime DEFAULT NULL,
  `ITEM_term` longtext,
  `ITEM_contract_description` longtext,
  `ITEM_is_use_template` varchar(100) DEFAULT NULL,
  `ITEM_upload` longtext,
  `ITEM_annex` longtext,
  `ITEM_re_dept_person` varchar(100) DEFAULT NULL,
  `ITEM_re_dept_attitude` varchar(100) DEFAULT NULL,
  `ITEM_re_dept_date` datetime DEFAULT NULL,
  `ITEM_re_dept_head` varchar(100) DEFAULT NULL,
  `ITEM_re_dept_head_attitude` varchar(100) DEFAULT NULL,
  `ITEM_re_dept_head_date` datetime DEFAULT NULL,
  `ITEM_finance_person` varchar(100) DEFAULT NULL,
  `ITEM_finance_person_attitude` varchar(100) DEFAULT NULL,
  `ITEM_finance_person_date` datetime DEFAULT NULL,
  `ITEM_finance_head` varchar(100) DEFAULT NULL,
  `ITEM_finance_head_attitude` varchar(100) DEFAULT NULL,
  `ITEM_finance_head_date` datetime DEFAULT NULL,
  `ITEM_legal_person` varchar(100) DEFAULT NULL,
  `ITEM_legal_attitude` varchar(100) DEFAULT NULL,
  `ITEM_legal_date` datetime DEFAULT NULL,
  `ITEM_legal_head` varchar(100) DEFAULT NULL,
  `ITEM_legal_head_attitude` varchar(100) DEFAULT NULL,
  `ITEM_legal_head_date` datetime DEFAULT NULL,
  `ITEM_leader` varchar(100) DEFAULT NULL,
  `ITEM_leader_attitude` varchar(100) DEFAULT NULL,
  `ITEM_leader_date` datetime DEFAULT NULL,
  `ITEM_manager` varchar(100) DEFAULT NULL,
  `ITEM_manager_attitude` varchar(100) DEFAULT NULL,
  `ITEM_manager_date` datetime DEFAULT NULL,
  `ITEM_legal_rep` varchar(100) DEFAULT NULL,
  `ITEM_legal_rep_attitude` varchar(100) DEFAULT NULL,
  `ITEM_legal_rep_date` datetime DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  `ITEM_expiration_description` longtext,
  `ITEM_counterpart` varchar(100) DEFAULT NULL,
  `ITEM_apply_person_date` datetime DEFAULT NULL,
  `ITEM_dept_head_date` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_expense_contract_approval
-- ----------------------------
INSERT INTO `tlk_expense_contract_approval` VALUES (null, '2023-11-07 09:47:13', '合同起草/支出类合同/expense_contract_approval', null, null, null, 'WlrAo0DK6YyyoGCLzBr', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', '$$$test', '2023-11-07 09:46:19', '__7NuSLfcmZ3ILpeCe13O', '__bjuatsuk2Hyiet4Z5k6,__vbHLPkSRLphAB2bSquz,__eMGGuhtm2iy4vrBbVfP', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'WlrAo0DK6YyyoGCLzBr', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '测试', 'ZC2311070008', 'YQPXWBU7xxJlbQtgDmh', '运营拓展部', '齐若凡', '2023-11-07 00:00:00', 'WlrAo0DK6YyyoGCLzBr', '万宝蔚', '', '', '', '竞争性谈判', '', '', '0.0000000000', '10000.0000000000', null, '2023-11-07 00:00:00', '2023-11-27 00:00:00', 'cs', '', '是', '[{\"id\":\"演播室技术服务协议-io3P9tSXnfcrcv4JY8B\",\"name\":\"演播室技术服务协议.docx\",\"path\":\"\\/uploads\\/item\\/2023\\/10\\/演播室技术服务协议-io3P9tSXnfcrcv4JY8B.docx\",\"url\":\"\\/obpm\\/\\/uploads\\/item\\/2023\\/10\\/演播室技术服务协议-io3P9tSXnfcrcv4JY8B.docx\",\"fileType\":\".docx\",\"uploader\":\"吕静璇\",\"uploadTime\":\"2023-10-16 13:30:29\",\"userName\":\"吕静璇\",\"time\":\"2023-10-16 13:30:29\",\"size\":\"18079\",\"isHasCompare\":false,\"showName\":\"演播室技术服务协议.docx\",\"isDelete\":false,\"isallowcopy\":\"true\",\"isallowdownload\":\"true\",\"isallowprint\":\"true\",\"openWaterMark\":false,\"watermark\":\"\",\"newversionscript\":true,\"versioncomparescript\":true,\"downloadscript\":true,\"deletescript\":true,\"renamescript\":true,\"previewscript\":true,\"editscript\":true,\"uid\":1697434229135,\"status\":\"success\",\"isEdit\":false}]', '', '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, '1wyAZXduj1dqO7xHC8n--__7NuSLfcmZ3ILpeCe13O', '', '广州市天翎网络科技有限公司', null, null);
INSERT INTO `tlk_expense_contract_approval` VALUES (null, '2023-08-25 02:14:40', '合同起草/支出类合同/expense_contract_approval', '', null, null, '0VFlDyTyT0gylwIP52Z', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', '$$$test', '2023-08-25 01:40:00', '__7NuSLfcmZ3ILpeCe13O', '__bjuatsuk2Hyiet4Z5k6,__vbHLPkSRLphAB2bSquz', '0VFlDyTyT0gylwIP52Z', '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '0VFlDyTyT0gylwIP52Z', 'QnkpCDiSJqeIHrbbY9X', '{\"1689675721656\":[]}', '{\"1689675721656\":[]}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, 'aaaa', 'ZC2308250002', 'horJPFRbb5VHH3aHY3Q', '法务', 'shaw', '2023-08-25 00:00:00', '0VFlDyTyT0gylwIP52Z', 'shaw', '', '', '', '三方比价', '', '', '0.0000000000', '1000000.0000000000', null, '2023-08-30 00:00:00', '2023-09-28 00:00:00', '1111', '', '是', '', '', '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, '736MecIL7G6obbD01Gs--__7NuSLfcmZ3ILpeCe13O', '', null, null, null);
INSERT INTO `tlk_expense_contract_approval` VALUES (null, '2023-11-13 07:52:16', '合同起草/支出类合同/expense_contract_approval', '7Q0G8h9g587tohUGy0z--__7NuSLfcmZ3ILpeCe13O-1S7qeEgsRGpRsH396O8', 'Mxspdb6zQc7sibnXuG0', '2023-11-13 07:52:16', 'Mxspdb6zQc7sibnXuG0', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', '$$$test', '2023-11-10 06:47:31', '__7NuSLfcmZ3ILpeCe13O', '__bjuatsuk2Hyiet4Z5k6,__vbHLPkSRLphAB2bSquz,__eMGGuhtm2iy4vrBbVfP', 'Mxspdb6zQc7sibnXuG0', '\0', '2', '__M4oRwEVzSB1oI1D0faN', '256', '财务初审', '姚丹', '80', 'Mxspdb6zQc7sibnXuG0', 'QnkpCDiSJqeIHrbbY9X', '{\"1690256013713\":[\"hVS4izsK5fGJSNsfAdu\"]}', '{}', '[{\"instanceId\":\"7Q0G8h9g587tohUGy0z--__7NuSLfcmZ3ILpeCe13O-1S7qeEgsRGpRsH396O8\",\"flowName\":\"支出合同审批流程\",\"flowId\":\"__uIAg4VJq29YlVvCwn4g\",\"nodes\":[{\"nodeId\":\"1690256013713\",\"stateLabel\":\"财务初审\",\"state\":0,\"auditors\":[{\"id\":\"hVS4izsK5fGJSNsfAdu\",\"name\":\"姚丹\",\"userCode\":\"yaodan\"}],\"coAuditors\":[]}]}]', '[{\"instanceId\":\"7Q0G8h9g587tohUGy0z--__7NuSLfcmZ3ILpeCe13O-1S7qeEgsRGpRsH396O8\",\"prevAuditNode\":\"申请人\"}]', '[{\"instanceId\":\"7Q0G8h9g587tohUGy0z--__7NuSLfcmZ3ILpeCe13O-1S7qeEgsRGpRsH396O8\",\"prevAuditUser\":\"吴继华\"}]', null, null, null, null, '测试', 'ZC2311100015', 'horJPFRbb5VHH3aHY3Q', '法务', '', '2023-11-10 00:00:00', 'Mxspdb6zQc7sibnXuG0', '吴继华', '', '', '', '询价', '', '', '0.0000000000', '10000.0000000000', null, '2023-11-10 00:00:00', '2023-11-30 00:00:00', '测试', '', '是', '[{\"id\":\"设备（货物）采购合同模版-zE1sYvTlwk9kYjBdAtn\",\"name\":\"设备（货物）采购合同模版.doc\",\"path\":\"\\/uploads\\/item\\/2023\\/10\\/设备（货物）采购合同模版-zE1sYvTlwk9kYjBdAtn.doc\",\"url\":\"\\/obpm\\/\\/uploads\\/item\\/2023\\/10\\/设备（货物）采购合同模版-zE1sYvTlwk9kYjBdAtn.doc\",\"fileType\":\".doc\",\"uploader\":\"吕静璇\",\"uploadTime\":\"2023-10-16 13:32:11\",\"userName\":\"吕静璇\",\"time\":\"2023-10-16 13:32:11\",\"size\":\"49664\",\"isHasCompare\":false,\"showName\":\"设备（货物）采购合同模版.doc\",\"isDelete\":false,\"isallowcopy\":\"true\",\"isallowdownload\":\"true\",\"isallowprint\":\"true\",\"openWaterMark\":false,\"watermark\":\"\",\"newversionscript\":true,\"versioncomparescript\":true,\"downloadscript\":true,\"deletescript\":true,\"renamescript\":true,\"previewscript\":true,\"editscript\":true,\"uid\":1697434331565,\"status\":\"success\",\"isEdit\":false}]', '', '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, '7Q0G8h9g587tohUGy0z--__7NuSLfcmZ3ILpeCe13O', '', '', '2023-11-13 00:00:00', null);
INSERT INTO `tlk_expense_contract_approval` VALUES (null, '2023-11-10 04:23:48', '合同起草/支出类合同/expense_contract_approval', 'i1iNKJtnTvtGxo2DBkt--__7NuSLfcmZ3ILpeCe13O-JMiceLty0wVuAqZiACQ', 'QghJDaDCt5fckOMCWAe', '2023-11-10 04:23:48', 'Mxspdb6zQc7sibnXuG0', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', '$$$test', '2023-11-10 03:45:24', '__7NuSLfcmZ3ILpeCe13O', '__bjuatsuk2Hyiet4Z5k6,__vbHLPkSRLphAB2bSquz,__eMGGuhtm2iy4vrBbVfP', 'Mxspdb6zQc7sibnXuG0', '\0', '13', '__M4oRwEVzSB1oI1D0faN', '1048576', '结束', '', '80', 'Mxspdb6zQc7sibnXuG0', 'QnkpCDiSJqeIHrbbY9X', '{\"1689675733215\":[]}', '{\"1690256013713\":[]}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"结束\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', '[]', '[]', null, null, null, null, '223232', 'ZC2311100008', 'horJPFRbb5VHH3aHY3Q', '法务', '', '2023-11-10 00:00:00', 'Mxspdb6zQc7sibnXuG0', '吴继华', '', '', '', '竞争性谈判', '', '', '0.0000000000', '323233323.0000000000', null, '2023-11-19 00:00:00', '2023-11-27 00:00:00', '323232', '', '是', '[{\"id\":\"演播室技术服务协议-io3P9tSXnfcrcv4JY8B\",\"name\":\"演播室技术服务协议.docx\",\"path\":\"\\/uploads\\/item\\/2023\\/10\\/演播室技术服务协议-io3P9tSXnfcrcv4JY8B.docx\",\"url\":\"\\/obpm\\/\\/uploads\\/item\\/2023\\/10\\/演播室技术服务协议-io3P9tSXnfcrcv4JY8B.docx\",\"fileType\":\".docx\",\"uploader\":\"吕静璇\",\"uploadTime\":\"2023-10-16 13:30:29\",\"userName\":\"吕静璇\",\"time\":\"2023-10-16 13:30:29\",\"size\":\"18079\",\"isHasCompare\":true,\"showName\":\"演播室技术服务协议.docx\",\"isDelete\":false,\"isallowcopy\":\"true\",\"isallowdownload\":\"true\",\"isallowprint\":\"true\",\"openWaterMark\":false,\"watermark\":\"\",\"newversionscript\":true,\"versioncomparescript\":true,\"downloadscript\":true,\"deletescript\":true,\"renamescript\":true,\"previewscript\":true,\"editscript\":true,\"uid\":1697434229135,\"status\":\"success\",\"isEdit\":false}]', '', '', '', null, '', '', null, '姚丹', '同意', '2023-11-10 00:00:00', '张兴玉', '同意', '2023-11-10 00:00:00', '吕静璇', '同意', '2023-11-10 00:00:00', '', '', null, '宗昊', '同意', '2023-11-10 00:00:00', '王毅', '同意', '2023-11-10 00:00:00', '宗昊', '同意', '2023-11-10 00:00:00', 'i1iNKJtnTvtGxo2DBkt--__7NuSLfcmZ3ILpeCe13O', '', '', '2023-11-10 00:00:00', null);
INSERT INTO `tlk_expense_contract_approval` VALUES (null, '2023-11-08 01:43:10', '合同起草/支出类合同/expense_contract_approval', '', null, null, 'Rnj8E66kUoXuFk1aJly', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_0SGMMIc0GQhlhtbKzpY_HxAPsLoWqGcM9VArPJH', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_0SGMMIc0GQhlhtbKzpY_HxAPsLoWqGcM9VArPJH', '$$$test', '2023-11-08 01:42:39', '__7NuSLfcmZ3ILpeCe13O', '__bjuatsuk2Hyiet4Z5k6,__vbHLPkSRLphAB2bSquz,__eMGGuhtm2iy4vrBbVfP', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'Rnj8E66kUoXuFk1aJly', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '233233232', 'ZC2311080001', 'HxAPsLoWqGcM9VArPJH', '内容安全', '张晓峰', '2023-11-08 00:00:00', 'Rnj8E66kUoXuFk1aJly', '马玥', '', '', '', '三方比价', '', '', '0.0000000000', '50000.0000000000', '2023-11-16 00:00:00', '2023-11-19 00:00:00', '2023-11-21 00:00:00', '2332323', '', '', '', '', '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, 'IkMDHOS6jTztTOllpHz--__7NuSLfcmZ3ILpeCe13O', '', '', null, null);
INSERT INTO `tlk_expense_contract_approval` VALUES (null, '2023-08-25 08:01:05', '合同起草/支出类合同/expense_contract_approval', null, null, null, '4ZTy2kciL50s5wZdnEV', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s', '$$$test', '2023-08-25 07:51:15', '__7NuSLfcmZ3ILpeCe13O', '__bjuatsuk2Hyiet4Z5k6,__vbHLPkSRLphAB2bSquz', null, '\0', '2', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '4ZTy2kciL50s5wZdnEV', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '测试', 'ZC2308250004', 'Z42xU8LmniRouZ93E7s', '北京时间有限公司', '', '2023-08-25 00:00:00', '4ZTy2kciL50s5wZdnEV', 'aa', '', '', '', '三方比价', '', '', '0.0000000000', '10000.0000000000', null, '2023-08-25 00:00:00', '2023-08-31 00:00:00', 'cs', '', '否', '[{\"id\":\"合同模板 (1)-BvWmAysjgxd8Vyh0JSs\",\"name\":\"合同模板 (1).docx\",\"path\":\"\\/uploads\\/item\\/2023\\/8\\/合同模板 (1)-BvWmAysjgxd8Vyh0JSs.docx\",\"url\":\"\\/obpm\\/\\/uploads\\/item\\/2023\\/8\\/合同模板 (1)-BvWmAysjgxd8Vyh0JSs.docx\",\"fileType\":\".docx\",\"uploader\":\"aa\",\"uploadTime\":\"2023-08-25 15:52:39\",\"userName\":\"aa\",\"time\":\"2023-08-25 15:52:39\",\"size\":\"22297\",\"isHasCompare\":false,\"showName\":\"合同模板 (1).docx\",\"isDelete\":false,\"isallowcopy\":\"true\",\"isallowdownload\":\"true\",\"isallowprint\":\"true\",\"openWaterMark\":false,\"watermark\":\"\",\"newversionscript\":false,\"versioncomparescript\":false,\"downloadscript\":true,\"deletescript\":true,\"renamescript\":true,\"previewscript\":true,\"editscript\":true,\"isEdit\":false,\"uid\":1692949959014,\"status\":\"success\"}]', '[{\"id\":\"秦越志远表单流程-7ra5yyc6Iszc2KM9FMY\",\"name\":\"秦越志远表单流程.docx\",\"path\":\"\\/uploads\\/item\\/2023\\/8\\/秦越志远表单流程-7ra5yyc6Iszc2KM9FMY.docx\",\"url\":\"\\/obpm\\/\\/uploads\\/item\\/2023\\/8\\/秦越志远表单流程-7ra5yyc6Iszc2KM9FMY.docx\",\"fileType\":\".docx\",\"uploader\":\"aa\",\"uploadTime\":\"2023-08-25 16:00:47\",\"userName\":\"aa\",\"time\":\"2023-08-25 16:00:47\",\"size\":\"26341758\",\"isHasCompare\":false,\"showName\":\"秦越志远表单流程.docx\",\"isDelete\":false,\"isallowcopy\":\"true\",\"isallowdownload\":\"true\",\"isallowprint\":\"true\",\"openWaterMark\":false,\"watermark\":\"\",\"newversionscript\":true,\"versioncomparescript\":true,\"downloadscript\":true,\"deletescript\":true,\"renamescript\":true,\"previewscript\":true,\"editscript\":true,\"uid\":1692950447171,\"status\":\"success\",\"isEdit\":false},{\"id\":\"activity流程引擎数据表结构-JxCrz0g60EJko395HB9\",\"name\":\"activity流程引擎数据表结构.docx\",\"path\":\"\\/uploads\\/item\\/2023\\/8\\/activity流程引擎数据表结构-JxCrz0g60EJko395HB9.docx\",\"url\":\"\\/obpm\\/\\/uploads\\/item\\/2023\\/8\\/activity流程引擎数据表结构-JxCrz0g60EJko395HB9.docx\",\"fileType\":\".docx\",\"uploader\":\"aa\",\"uploadTime\":\"2023-08-25 15:51:41\",\"userName\":\"aa\",\"time\":\"2023-08-25 15:51:41\",\"size\":\"0\",\"isHasCompare\":false,\"showName\":\"activity流程引擎数据表结构.docx\",\"isDelete\":false,\"isallowcopy\":\"true\",\"isallowdownload\":\"true\",\"isallowprint\":\"true\",\"openWaterMark\":false,\"watermark\":\"\",\"newversionscript\":true,\"versioncomparescript\":true,\"downloadscript\":true,\"deletescript\":true,\"renamescript\":true,\"previewscript\":true,\"editscript\":true,\"uid\":1692949901925,\"status\":\"success\",\"isEdit\":false},{\"id\":\"合同模板 (1)-bSeNoSG2TYAm5F6Wjuq\",\"name\":\"合同模板 (1).docx\",\"path\":\"\\/uploads\\/item\\/2023\\/8\\/合同模板 (1)-bSeNoSG2TYAm5F6Wjuq.docx\",\"url\":\"\\/obpm\\/\\/uploads\\/item\\/2023\\/8\\/合同模板 (1)-bSeNoSG2TYAm5F6Wjuq.docx\",\"fileType\":\".docx\",\"uploader\":\"aa\",\"uploadTime\":\"2023-08-25 15:51:41\",\"userName\":\"aa\",\"time\":\"2023-08-25 15:51:41\",\"size\":\"22297\",\"isHasCompare\":false,\"showName\":\"合同模板 (1).docx\",\"isDelete\":false,\"isallowcopy\":\"true\",\"isallowdownload\":\"true\",\"isallowprint\":\"true\",\"openWaterMark\":false,\"watermark\":\"\",\"newversionscript\":true,\"versioncomparescript\":true,\"downloadscript\":true,\"deletescript\":true,\"renamescript\":true,\"previewscript\":true,\"editscript\":true,\"uid\":1692949901882,\"status\":\"success\",\"isEdit\":false},{\"id\":\"旧系统数据量统计-XggtsuPet4k2hdzHQir\",\"name\":\"旧系统数据量统计.xlsx\",\"path\":\"\\/uploads\\/item\\/2023\\/8\\/旧系统数据量统计-XggtsuPet4k2hdzHQir.xlsx\",\"url\":\"\\/obpm\\/\\/uploads\\/item\\/2023\\/8\\/旧系统数据量统计-XggtsuPet4k2hdzHQir.xlsx\",\"fileType\":\".xlsx\",\"uploader\":\"aa\",\"uploadTime\":\"2023-08-25 15:51:41\",\"userName\":\"aa\",\"time\":\"2023-08-25 15:51:41\",\"size\":\"112836\",\"isHasCompare\":false,\"showName\":\"旧系统数据量统计.xlsx\",\"isDelete\":false,\"isallowcopy\":\"true\",\"isallowdownload\":\"true\",\"isallowprint\":\"true\",\"openWaterMark\":false,\"watermark\":\"\",\"newversionscript\":true,\"versioncomparescript\":true,\"downloadscript\":true,\"deletescript\":true,\"renamescript\":true,\"previewscript\":true,\"editscript\":true,\"uid\":1692949901873,\"status\":\"success\",\"isEdit\":false},{\"id\":\"秦越志远方案书-ELFoRTdl0F2zbAv4ciG\",\"name\":\"秦越志远方案书.docx\",\"path\":\"\\/uploads\\/item\\/2023\\/8\\/秦越志远方案书-ELFoRTdl0F2zbAv4ciG.docx\",\"url\":\"\\/obpm\\/\\/uploads\\/item\\/2023\\/8\\/秦越志远方案书-ELFoRTdl0F2zbAv4ciG.docx\",\"fileType\":\".docx\",\"uploader\":\"aa\",\"uploadTime\":\"2023-08-25 15:51:41\",\"userName\":\"aa\",\"time\":\"2023-08-25 15:51:41\",\"size\":\"222046\",\"isHasCompare\":false,\"showName\":\"秦越志远方案书.docx\",\"isDelete\":false,\"isallowcopy\":\"true\",\"isallowdownload\":\"true\",\"isallowprint\":\"true\",\"openWaterMark\":false,\"watermark\":\"\",\"newversionscript\":true,\"versioncomparescript\":true,\"downloadscript\":true,\"deletescript\":true,\"renamescript\":true,\"previewscript\":true,\"editscript\":true,\"uid\":1692949901869,\"status\":\"success\",\"isEdit\":false},{\"id\":\"项目导入-eHcTU1QjFq2NmTVEvKt\",\"name\":\"项目导入.xlsx\",\"path\":\"\\/uploads\\/item\\/2023\\/8\\/项目导入-eHcTU1QjFq2NmTVEvKt.xlsx\",\"url\":\"\\/obpm\\/\\/uploads\\/item\\/2023\\/8\\/项目导入-eHcTU1QjFq2NmTVEvKt.xlsx\",\"fileType\":\".xlsx\",\"uploader\":\"aa\",\"uploadTime\":\"2023-08-25 15:51:41\",\"userName\":\"aa\",\"time\":\"2023-08-25 15:51:41\",\"size\":\"9385\",\"isHasCompare\":false,\"showName\":\"项目导入.xlsx\",\"isDelete\":false,\"isallowcopy\":\"true\",\"isallowdownload\":\"true\",\"isallowprint\":\"true\",\"openWaterMark\":false,\"watermark\":\"\",\"newversionscript\":true,\"versioncomparescript\":true,\"downloadscript\":true,\"deletescript\":true,\"renamescript\":true,\"previewscript\":true,\"editscript\":true,\"uid\":1692949901851,\"status\":\"success\",\"isEdit\":false},{\"id\":\"myapps启动步骤文档-yz0JqBYKDXlQ4Ent9HH\",\"name\":\"myapps启动步骤文档.docx\",\"path\":\"\\/uploads\\/item\\/2023\\/8\\/myapps启动步骤文档-yz0JqBYKDXlQ4Ent9HH.docx\",\"url\":\"\\/obpm\\/\\/uploads\\/item\\/2023\\/8\\/myapps启动步骤文档-yz0JqBYKDXlQ4Ent9HH.docx\",\"fileType\":\".docx\",\"uploader\":\"aa\",\"uploadTime\":\"2023-08-25 15:51:41\",\"userName\":\"aa\",\"time\":\"2023-08-25 15:51:41\",\"size\":\"12024\",\"isHasCompare\":false,\"showName\":\"myapps启动步骤文档.docx\",\"isDelete\":false,\"isallowcopy\":\"true\",\"isallowdownload\":\"true\",\"isallowprint\":\"true\",\"openWaterMark\":false,\"watermark\":\"\",\"newversionscript\":true,\"versioncomparescript\":true,\"downloadscript\":true,\"deletescript\":true,\"renamescript\":true,\"previewscript\":true,\"editscript\":true,\"uid\":1692949901715,\"status\":\"success\",\"isEdit\":false}]', '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, 'yMOiqgzjPlL1KYxyUdR--__7NuSLfcmZ3ILpeCe13O', '', null, null, null);

-- ----------------------------
-- Table structure for tlk_expense_contract_form
-- ----------------------------
DROP TABLE IF EXISTS `tlk_expense_contract_form`;
CREATE TABLE `tlk_expense_contract_form` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_contract_name` varchar(100) DEFAULT NULL,
  `ITEM_contract_number` varchar(100) DEFAULT NULL,
  `ITEM_contract_type` varchar(100) DEFAULT NULL,
  `ITEM_signing_date` datetime DEFAULT NULL,
  `ITEM_effective_date` datetime DEFAULT NULL,
  `ITEM_expiring_date` datetime DEFAULT NULL,
  `ITEM_head` longtext,
  `ITEM_dept` longtext,
  `ITEM_contract_amount` decimal(22,10) DEFAULT NULL,
  `ITEM_receipt_amount` decimal(22,10) DEFAULT NULL,
  `ITEM_paid_amount` decimal(22,10) DEFAULT NULL,
  `ITEM_unpaid_amount` decimal(22,10) DEFAULT NULL,
  `ITEM_contract_status` varchar(100) DEFAULT NULL,
  `ITEM_re_contract_name` varchar(100) DEFAULT NULL,
  `ITEM_re_contract_id` varchar(100) DEFAULT NULL,
  `ITEM_re_project_name` varchar(100) DEFAULT NULL,
  `ITEM_re_project_id` varchar(100) DEFAULT NULL,
  `ITEM_counterpart` varchar(100) DEFAULT NULL,
  `ITEM_counterpart_legal_person` varchar(100) DEFAULT NULL,
  `ITEM_contacts` varchar(100) DEFAULT NULL,
  `ITEM_contact_info` varchar(100) DEFAULT NULL,
  `ITEM_counterpart_number` varchar(100) DEFAULT NULL,
  `ITEM_counterpart_type` varchar(100) DEFAULT NULL,
  `ITEM_address` varchar(100) DEFAULT NULL,
  `ITEM_bank` varchar(100) DEFAULT NULL,
  `ITEM_bank_number` varchar(100) DEFAULT NULL,
  `ITEM_file` longtext,
  `ITEM_apply_person` varchar(100) DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  `ITEM_contract_ref` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_expense_contract_form
-- ----------------------------
INSERT INTO `tlk_expense_contract_form` VALUES (null, '2023-11-09 06:33:46', '合同执行/合同卡片/expense_contract_form', null, null, null, 'lYjxYqrytPMOTiM6KLp', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '$$$test', '2023-11-09 06:04:03', '__jvpXgGxXUFqKjojY4Qy', '__KodzsMDr3aXCy87cPrb,__JcZ5aDwRM1YEQUCqBtw,__4QOsDYSOr9kvmHJJfQ6,__NgXWRPuKStY5nygCkmL,__eMGGuhtm2iy4vrBbVfP,__MPD9VyFPfx9pMhwwoeu', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'lYjxYqrytPMOTiM6KLp', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '支出合同测试1-1109', 'ZC2311090002', '支出合同', '2023-11-09 00:00:00', '2023-11-07 00:00:00', '2023-11-24 00:00:00', 'Rnj8E66kUoXuFk1aJly', 'HxAPsLoWqGcM9VArPJH', '10000.0000000000', '0.0000000000', '10000.0000000000', '0.0000000000', '履行中', '', '', '', '', '广州市天翎网络科技有限公司', '', '', '', '', '', '', '', '', '', 'Rnj8E66kUoXuFk1aJly', 'EtRxI1hmgaBw7Kr4L4D--__jvpXgGxXUFqKjojY4Qy', '2023-BJSJ-JSAQZX-NRAQ-006');

-- ----------------------------
-- Table structure for tlk_expense_contract_form_object
-- ----------------------------
DROP TABLE IF EXISTS `tlk_expense_contract_form_object`;
CREATE TABLE `tlk_expense_contract_form_object` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_OBJECT` varchar(100) DEFAULT NULL,
  `ITEM_UNIT` varchar(100) DEFAULT NULL,
  `ITEM_QUANTITY` decimal(22,10) DEFAULT NULL,
  `ITEM_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_expense_contract_form_object
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_expense_contract_form_payment
-- ----------------------------
DROP TABLE IF EXISTS `tlk_expense_contract_form_payment`;
CREATE TABLE `tlk_expense_contract_form_payment` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_PAYMENT_TERMS` varchar(100) DEFAULT NULL,
  `ITEM_SETTLEMENT_TYPE` varchar(100) DEFAULT NULL,
  `ITEM_PAYMENT_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ITEM_PAYMENT_RATIO` decimal(22,10) DEFAULT NULL,
  `ITEM_PAYMENT_STATUS` varchar(100) DEFAULT NULL,
  `ITEM_DESCRIPTION` longtext,
  `ID` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_expense_contract_form_payment
-- ----------------------------
INSERT INTO `tlk_expense_contract_form_payment` VALUES ('EtRxI1hmgaBw7Kr4L4D--__jvpXgGxXUFqKjojY4Qy', '2023-11-09 06:04:03', '合同执行/合同卡片/expense_contract_form_payment', null, null, null, 'lYjxYqrytPMOTiM6KLp', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '$$$test', '2023-11-09 06:04:03', '__4QOsDYSOr9kvmHJJfQ6', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'lYjxYqrytPMOTiM6KLp', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '', '', '0.0000000000', '0.0000000000', '', '', 'OPpYpF3Jji01jXR1bHD--__4QOsDYSOr9kvmHJJfQ6');
INSERT INTO `tlk_expense_contract_form_payment` VALUES ('EtRxI1hmgaBw7Kr4L4D--__jvpXgGxXUFqKjojY4Qy', '2023-11-09 06:04:03', '合同执行/合同卡片/expense_contract_form_payment', null, null, null, 'lYjxYqrytPMOTiM6KLp', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '$$$test', '2023-11-09 06:04:03', '__4QOsDYSOr9kvmHJJfQ6', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'lYjxYqrytPMOTiM6KLp', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '付款条件', '', '10000.0000000000', '100.0000000000', '', '', 'UfZAZjadLnL1h0p5ZgB--__4QOsDYSOr9kvmHJJfQ6');

-- ----------------------------
-- Table structure for tlk_expense_contract_object
-- ----------------------------
DROP TABLE IF EXISTS `tlk_expense_contract_object`;
CREATE TABLE `tlk_expense_contract_object` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_OBJECT` varchar(100) DEFAULT NULL,
  `ITEM_UNIT` varchar(100) DEFAULT NULL,
  `ITEM_QUANTITY` decimal(22,10) DEFAULT NULL,
  `ITEM_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_expense_contract_object
-- ----------------------------
INSERT INTO `tlk_expense_contract_object` VALUES ('JOtQ92Uw182j9FesoSl--__7NuSLfcmZ3ILpeCe13O', '2023-08-28 06:49:19', '合同起草/支出类合同/expense_contract_object', null, null, null, 'gWsc9qqUYEnVavXC5dM', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_GWAFbb3QNDDsNavToBH', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_GWAFbb3QNDDsNavToBH', '$$$test', '2023-08-28 06:49:15', '__vbHLPkSRLphAB2bSquz', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'gWsc9qqUYEnVavXC5dM', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '1', '1', '1.0000000000', '1.0000000000', '4sXptQEjwAZIs4Nh6pt--__vbHLPkSRLphAB2bSquz');
INSERT INTO `tlk_expense_contract_object` VALUES ('JOtQ92Uw182j9FesoSl--__7NuSLfcmZ3ILpeCe13O', '2023-08-28 06:50:37', '合同起草/支出类合同/expense_contract_object', null, null, null, 'gWsc9qqUYEnVavXC5dM', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_GWAFbb3QNDDsNavToBH', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_GWAFbb3QNDDsNavToBH', '$$$test', '2023-08-28 06:50:33', '__vbHLPkSRLphAB2bSquz', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'gWsc9qqUYEnVavXC5dM', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '3', '33', '33.0000000000', '3.0000000000', '5nakPYTyDdtjq743EtK--__vbHLPkSRLphAB2bSquz');
INSERT INTO `tlk_expense_contract_object` VALUES ('LX8OvpCzKNIXHdNJyrn--__7NuSLfcmZ3ILpeCe13O', '2023-08-28 03:38:53', '合同起草/支出类合同/expense_contract_object', null, null, null, 'gWsc9qqUYEnVavXC5dM', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_GWAFbb3QNDDsNavToBH', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_GWAFbb3QNDDsNavToBH', '$$$test', '2023-08-28 03:38:04', '__vbHLPkSRLphAB2bSquz', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'gWsc9qqUYEnVavXC5dM', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '33', '333', '33.0000000000', '33.0000000000', '5rRNpujKVfWqMRN1ljd--__vbHLPkSRLphAB2bSquz');
INSERT INTO `tlk_expense_contract_object` VALUES ('9meSkBipjgrJJwL1loG--__7NuSLfcmZ3ILpeCe13O', '2023-11-06 07:12:15', '合同起草/支出类合同/expense_contract_object', null, null, null, 'WlrAo0DK6YyyoGCLzBr', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', '$$$test', '2023-11-06 07:11:55', '__vbHLPkSRLphAB2bSquz', '', null, '\0', '-1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'WlrAo0DK6YyyoGCLzBr', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, 'cs', '1', '1.0000000000', '10000.0000000000', 'ewmyEc7uOr6mTCiv8tX--__vbHLPkSRLphAB2bSquz');
INSERT INTO `tlk_expense_contract_object` VALUES ('yMOiqgzjPlL1KYxyUdR--__7NuSLfcmZ3ILpeCe13O', '2023-08-25 07:52:59', '合同起草/支出类合同/expense_contract_object', null, null, null, '4ZTy2kciL50s5wZdnEV', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s', '$$$test', '2023-08-25 07:52:53', '__vbHLPkSRLphAB2bSquz', '', null, '\0', '2', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '4ZTy2kciL50s5wZdnEV', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '10000', '', '10000.0000000000', '0.0000000000', 'FEgNDOcaEq4tnKYh0Wb--__vbHLPkSRLphAB2bSquz');
INSERT INTO `tlk_expense_contract_object` VALUES ('qaaJ36uqOGbuyBPXprm--__7NuSLfcmZ3ILpeCe13O', '2023-08-28 01:52:14', '合同起草/支出类合同/expense_contract_object', null, null, null, 'oKdPiM3Z0BhZbUERl5a', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_yHPdjVfJiEd95u7Eij9', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_yHPdjVfJiEd95u7Eij9', '$$$test', '2023-08-28 01:52:08', '__vbHLPkSRLphAB2bSquz', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'oKdPiM3Z0BhZbUERl5a', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '1', '1', '1.0000000000', '1.0000000000', 'JCwG0SkuMdptBrp6SPR--__vbHLPkSRLphAB2bSquz');
INSERT INTO `tlk_expense_contract_object` VALUES ('Ou6SqPS11BolkS5FbJm--__7NuSLfcmZ3ILpeCe13O', '2023-08-28 06:52:38', '合同起草/支出类合同/expense_contract_object', null, null, null, 'gWsc9qqUYEnVavXC5dM', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_GWAFbb3QNDDsNavToBH', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_GWAFbb3QNDDsNavToBH', '$$$test', '2023-08-28 06:52:37', '__vbHLPkSRLphAB2bSquz', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'gWsc9qqUYEnVavXC5dM', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '', '', '0.0000000000', '0.0000000000', 'laoVhTqykICQtOg4qC1--__vbHLPkSRLphAB2bSquz');
INSERT INTO `tlk_expense_contract_object` VALUES ('736MecIL7G6obbD01Gs--__7NuSLfcmZ3ILpeCe13O', '2023-08-25 02:14:40', '合同起草/支出类合同/expense_contract_object', null, null, null, '0VFlDyTyT0gylwIP52Z', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', '$$$test', '2023-08-25 02:14:08', '__vbHLPkSRLphAB2bSquz', '', null, '\0', '2', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '0VFlDyTyT0gylwIP52Z', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, 'aaa', 'aaa', '10.0000000000', '10000.0000000000', 'PU2kVfuUn7bs7AKGAe5--__vbHLPkSRLphAB2bSquz');
INSERT INTO `tlk_expense_contract_object` VALUES ('JOtQ92Uw182j9FesoSl--__7NuSLfcmZ3ILpeCe13O', '2023-08-28 06:49:46', '合同起草/支出类合同/expense_contract_object', null, null, null, 'gWsc9qqUYEnVavXC5dM', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_GWAFbb3QNDDsNavToBH', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_GWAFbb3QNDDsNavToBH', '$$$test', '2023-08-28 06:49:42', '__vbHLPkSRLphAB2bSquz', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'gWsc9qqUYEnVavXC5dM', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '2', '2', '2.0000000000', '2.0000000000', 'UflaAFKvOrxngBWEFEg--__vbHLPkSRLphAB2bSquz');
INSERT INTO `tlk_expense_contract_object` VALUES ('1wyAZXduj1dqO7xHC8n--__7NuSLfcmZ3ILpeCe13O', '2023-11-07 09:47:12', '合同起草/支出类合同/expense_contract_object', null, null, null, 'WlrAo0DK6YyyoGCLzBr', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', '$$$test', '2023-11-07 09:46:58', '__vbHLPkSRLphAB2bSquz', '', null, '\0', '2', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'WlrAo0DK6YyyoGCLzBr', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, 'cs', '1', '1.0000000000', '10000.0000000000', 'YFMMXzetbgBsSh11iPI--__vbHLPkSRLphAB2bSquz');

-- ----------------------------
-- Table structure for tlk_expense_contract_payment
-- ----------------------------
DROP TABLE IF EXISTS `tlk_expense_contract_payment`;
CREATE TABLE `tlk_expense_contract_payment` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_PAYMENT_TYPE` varchar(100) DEFAULT NULL,
  `ITEM_PAYMENT_TERMS` varchar(100) DEFAULT NULL,
  `ITEM_PAYMENT_RATIO` decimal(22,10) DEFAULT NULL,
  `ITEM_PAYMENT_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ITEM_DESCRIPTION` longtext,
  `ID` varchar(100) NOT NULL,
  `ITEM_SETTLEMENT` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_expense_contract_payment
-- ----------------------------
INSERT INTO `tlk_expense_contract_payment` VALUES ('mdlSpsuJ2IHItfaAc8g--__7NuSLfcmZ3ILpeCe13O', '2023-11-06 06:39:06', '合同起草/支出类合同/expense_contract_payment', null, null, null, 'WlrAo0DK6YyyoGCLzBr', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', '$$$test', '2023-11-06 06:38:26', '__bjuatsuk2Hyiet4Z5k6', '', null, '\0', '-1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'WlrAo0DK6YyyoGCLzBr', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '首付款', '112212', '50.0000000000', '50000.0000000000', '', '0CHXQDWqQehzO3RH91M--__bjuatsuk2Hyiet4Z5k6', '');
INSERT INTO `tlk_expense_contract_payment` VALUES ('KqOT95JfyyMjED96VEu--__7NuSLfcmZ3ILpeCe13O', '2023-11-06 06:34:44', '合同起草/支出类合同/expense_contract_payment', null, null, null, 'WlrAo0DK6YyyoGCLzBr', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', '$$$test', '2023-11-06 06:31:15', '__bjuatsuk2Hyiet4Z5k6', '', null, '\0', '0', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'WlrAo0DK6YyyoGCLzBr', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '', '', '0.0000000000', '0.0000000000', '', '4MiaXzrrLJ0yCyVsqCX--__bjuatsuk2Hyiet4Z5k6', '');
INSERT INTO `tlk_expense_contract_payment` VALUES ('i1iNKJtnTvtGxo2DBkt--__7NuSLfcmZ3ILpeCe13O', '2023-11-10 03:48:39', '合同起草/支出类合同/expense_contract_payment', null, null, null, 'Mxspdb6zQc7sibnXuG0', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', '$$$test', '2023-11-10 03:45:52', '__bjuatsuk2Hyiet4Z5k6', '', null, '\0', '0', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'Mxspdb6zQc7sibnXuG0', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '', '', '0.0000000000', '0.0000000000', '', '5gAYxeWq9jd2d0oFFOF--__bjuatsuk2Hyiet4Z5k6', '');
INSERT INTO `tlk_expense_contract_payment` VALUES ('CkVebZwxkH2nMWXtMcv--__7NuSLfcmZ3ILpeCe13O', '2023-11-06 06:53:58', '合同起草/支出类合同/expense_contract_payment', null, null, null, 'WlrAo0DK6YyyoGCLzBr', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', '$$$test', '2023-11-06 06:53:14', '__bjuatsuk2Hyiet4Z5k6', '', null, '\0', '2', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'WlrAo0DK6YyyoGCLzBr', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '首付款', '签订合同', '50.0000000000', '50000.0000000000', '', '8XrCvje1gQKl4BtKOq6--__bjuatsuk2Hyiet4Z5k6', '');
INSERT INTO `tlk_expense_contract_payment` VALUES ('h3RPw5RL61b6l6waPQQ--__7NuSLfcmZ3ILpeCe13O', '2023-11-08 02:14:10', '合同起草/支出类合同/expense_contract_payment', null, null, null, '37wipIeJTuR7qrkH3Nf', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '$$$test', '2023-11-08 02:13:44', '__bjuatsuk2Hyiet4Z5k6', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '37wipIeJTuR7qrkH3Nf', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '', '', '0.0000000000', '0.0000000000', '', 'aqY3hIxpqGqsTll1Zks--__bjuatsuk2Hyiet4Z5k6', '');
INSERT INTO `tlk_expense_contract_payment` VALUES ('mdlSpsuJ2IHItfaAc8g--__7NuSLfcmZ3ILpeCe13O', '2023-11-06 06:39:06', '合同起草/支出类合同/expense_contract_payment', null, null, null, 'WlrAo0DK6YyyoGCLzBr', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', '$$$test', '2023-11-06 06:38:54', '__bjuatsuk2Hyiet4Z5k6', '', null, '\0', '0', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'WlrAo0DK6YyyoGCLzBr', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '', '', '0.0000000000', '0.0000000000', '', 'C7pfaHPYaMiBIu7iLth--__bjuatsuk2Hyiet4Z5k6', '');
INSERT INTO `tlk_expense_contract_payment` VALUES ('Xr7IniKfUTs0qijRDFv--__7NuSLfcmZ3ILpeCe13O', '2023-10-23 06:50:06', '合同起草/支出类合同/expense_contract_payment', null, null, null, 'nSk90pVKgErBwhDxsqF', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', '$$$test', '2023-10-23 06:49:56', '__bjuatsuk2Hyiet4Z5k6', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'nSk90pVKgErBwhDxsqF', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '据实结算', '', '100.0000000000', '0.0000000000', '', 'CueQdWHheujRss7JOsD--__bjuatsuk2Hyiet4Z5k6', '按季度结算');
INSERT INTO `tlk_expense_contract_payment` VALUES ('mdlSpsuJ2IHItfaAc8g--__7NuSLfcmZ3ILpeCe13O', '2023-11-06 06:39:06', '合同起草/支出类合同/expense_contract_payment', null, null, null, 'WlrAo0DK6YyyoGCLzBr', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', '$$$test', '2023-11-06 06:38:41', '__bjuatsuk2Hyiet4Z5k6', '', null, '\0', '0', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'WlrAo0DK6YyyoGCLzBr', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '', '', '0.0000000000', '0.0000000000', '', 'HLdRW2iIGJXoNTU1nno--__bjuatsuk2Hyiet4Z5k6', '');
INSERT INTO `tlk_expense_contract_payment` VALUES ('vK6pB6oN1l0tOI0EEi5--__7NuSLfcmZ3ILpeCe13O', '2023-09-05 07:33:31', '合同起草/支出类合同/expense_contract_payment', null, null, null, 'MfokklYeqit6DecvrOu', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_6kZnRiHLEbuSM8L1eix', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_6kZnRiHLEbuSM8L1eix', '$$$test', '2023-09-05 07:33:22', '__bjuatsuk2Hyiet4Z5k6', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'MfokklYeqit6DecvrOu', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '一次性付款', '测试', '100.0000000000', '10000.0000000000', '', 'HUeYfCPS6Q7QPHAO8ri--__bjuatsuk2Hyiet4Z5k6', null);
INSERT INTO `tlk_expense_contract_payment` VALUES ('i1iNKJtnTvtGxo2DBkt--__7NuSLfcmZ3ILpeCe13O', '2023-11-10 03:48:39', '合同起草/支出类合同/expense_contract_payment', null, null, null, 'Mxspdb6zQc7sibnXuG0', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', '$$$test', '2023-11-10 03:45:44', '__bjuatsuk2Hyiet4Z5k6', '', null, '\0', '-1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'Mxspdb6zQc7sibnXuG0', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '一次性付款', '323', '100.0000000000', '3323.0000000000', '', 'inFf43HR7JDjeaR0lXh--__bjuatsuk2Hyiet4Z5k6', '');
INSERT INTO `tlk_expense_contract_payment` VALUES ('KqOT95JfyyMjED96VEu--__7NuSLfcmZ3ILpeCe13O', '2023-11-06 06:34:44', '合同起草/支出类合同/expense_contract_payment', null, null, null, 'WlrAo0DK6YyyoGCLzBr', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', '$$$test', '2023-11-06 06:29:36', '__bjuatsuk2Hyiet4Z5k6', '', null, '\0', '-1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'WlrAo0DK6YyyoGCLzBr', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '首付款', '签订合同10个工作日', '50.0000000000', '50000.0000000000', '', 'iXwfaiNu2ZAoXnms61t--__bjuatsuk2Hyiet4Z5k6', '');
INSERT INTO `tlk_expense_contract_payment` VALUES ('CkVebZwxkH2nMWXtMcv--__7NuSLfcmZ3ILpeCe13O', '2023-11-06 06:53:58', '合同起草/支出类合同/expense_contract_payment', null, null, null, 'WlrAo0DK6YyyoGCLzBr', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', '$$$test', '2023-11-06 06:53:34', '__bjuatsuk2Hyiet4Z5k6', '', null, '\0', '2', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'WlrAo0DK6YyyoGCLzBr', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '尾款', '验收', '50.0000000000', '50000.0000000000', '', 'jffzAImatzRWG966wGA--__bjuatsuk2Hyiet4Z5k6', '');
INSERT INTO `tlk_expense_contract_payment` VALUES ('JOtQ92Uw182j9FesoSl--__7NuSLfcmZ3ILpeCe13O', '2023-08-28 06:49:54', '合同起草/支出类合同/expense_contract_payment', null, null, null, 'gWsc9qqUYEnVavXC5dM', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_GWAFbb3QNDDsNavToBH', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_GWAFbb3QNDDsNavToBH', '$$$test', '2023-08-28 06:49:50', '__bjuatsuk2Hyiet4Z5k6', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'gWsc9qqUYEnVavXC5dM', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '首付款', '1', '1.0000000000', '1.0000000000', '1', 'JSvp5Y5sWZZ46VcRjwI--__bjuatsuk2Hyiet4Z5k6', null);
INSERT INTO `tlk_expense_contract_payment` VALUES ('736MecIL7G6obbD01Gs--__7NuSLfcmZ3ILpeCe13O', '2023-08-25 02:14:40', '合同起草/支出类合同/expense_contract_payment', null, null, null, '0VFlDyTyT0gylwIP52Z', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', '$$$test', '2023-08-25 02:13:46', '__bjuatsuk2Hyiet4Z5k6', '', null, '\0', '2', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '0VFlDyTyT0gylwIP52Z', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '首付款', '', '0.0000000000', '0.0000000000', '', 'KGVd5EbUJsvrWc1RA0v--__bjuatsuk2Hyiet4Z5k6', null);
INSERT INTO `tlk_expense_contract_payment` VALUES ('yMOiqgzjPlL1KYxyUdR--__7NuSLfcmZ3ILpeCe13O', '2023-08-25 07:52:59', '合同起草/支出类合同/expense_contract_payment', null, null, null, '4ZTy2kciL50s5wZdnEV', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s', '$$$test', '2023-08-25 07:52:43', '__bjuatsuk2Hyiet4Z5k6', '', null, '\0', '2', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '4ZTy2kciL50s5wZdnEV', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '首付款', '', '0.0000000000', '100000.0000000000', '', 'Mu72utW7NifIPWIQCbY--__bjuatsuk2Hyiet4Z5k6', null);
INSERT INTO `tlk_expense_contract_payment` VALUES ('1wyAZXduj1dqO7xHC8n--__7NuSLfcmZ3ILpeCe13O', '2023-11-07 09:47:12', '合同起草/支出类合同/expense_contract_payment', null, null, null, 'WlrAo0DK6YyyoGCLzBr', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', '$$$test', '2023-11-07 09:46:47', '__bjuatsuk2Hyiet4Z5k6', '', null, '\0', '2', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'WlrAo0DK6YyyoGCLzBr', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '一次性付款', 'cs', '100.0000000000', '10000.0000000000', '', 'Ojhm7n3ag9NEcS2ZYr5--__bjuatsuk2Hyiet4Z5k6', '');
INSERT INTO `tlk_expense_contract_payment` VALUES ('h3RPw5RL61b6l6waPQQ--__7NuSLfcmZ3ILpeCe13O', '2023-11-08 02:14:10', '合同起草/支出类合同/expense_contract_payment', null, null, null, '37wipIeJTuR7qrkH3Nf', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '$$$test', '2023-11-08 02:13:31', '__bjuatsuk2Hyiet4Z5k6', '', null, '\0', '2', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '37wipIeJTuR7qrkH3Nf', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '一次性付款', '232323', '100.0000000000', '50000.0000000000', '', 'pR8nGzdgrq3QxMajYqY--__bjuatsuk2Hyiet4Z5k6', '');
INSERT INTO `tlk_expense_contract_payment` VALUES ('CkVebZwxkH2nMWXtMcv--__7NuSLfcmZ3ILpeCe13O', '2023-11-06 06:53:58', '合同起草/支出类合同/expense_contract_payment', null, null, null, 'WlrAo0DK6YyyoGCLzBr', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', '$$$test', '2023-11-06 06:53:50', '__bjuatsuk2Hyiet4Z5k6', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'WlrAo0DK6YyyoGCLzBr', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '', '', '0.0000000000', '0.0000000000', '', 'PZp7WATopN6YyujjRuJ--__bjuatsuk2Hyiet4Z5k6', '');
INSERT INTO `tlk_expense_contract_payment` VALUES ('qaaJ36uqOGbuyBPXprm--__7NuSLfcmZ3ILpeCe13O', '2023-08-28 01:51:00', '合同起草/支出类合同/expense_contract_payment', null, null, null, 'oKdPiM3Z0BhZbUERl5a', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_yHPdjVfJiEd95u7Eij9', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_yHPdjVfJiEd95u7Eij9', '$$$test', '2023-08-28 01:50:55', '__bjuatsuk2Hyiet4Z5k6', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'oKdPiM3Z0BhZbUERl5a', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '首付款', '1', '1.0000000000', '1.0000000000', '1', 'qVMcDv11vdR8xgSJN4i--__bjuatsuk2Hyiet4Z5k6', null);
INSERT INTO `tlk_expense_contract_payment` VALUES ('Ou6SqPS11BolkS5FbJm--__7NuSLfcmZ3ILpeCe13O', '2023-08-28 06:52:24', '合同起草/支出类合同/expense_contract_payment', null, null, null, 'gWsc9qqUYEnVavXC5dM', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_GWAFbb3QNDDsNavToBH', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_GWAFbb3QNDDsNavToBH', '$$$test', '2023-08-28 06:52:23', '__bjuatsuk2Hyiet4Z5k6', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'gWsc9qqUYEnVavXC5dM', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '', '', '0.0000000000', '0.0000000000', '', 'tpgN63Ap1UjJz3GudXT--__bjuatsuk2Hyiet4Z5k6', null);
INSERT INTO `tlk_expense_contract_payment` VALUES ('GZfYDHAh5LP6q1W8i3i--__7NuSLfcmZ3ILpeCe13O', '2023-08-28 06:48:13', '合同起草/支出类合同/expense_contract_payment', null, null, null, 'gWsc9qqUYEnVavXC5dM', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_GWAFbb3QNDDsNavToBH', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_GWAFbb3QNDDsNavToBH', '$$$test', '2023-08-28 06:48:08', '__bjuatsuk2Hyiet4Z5k6', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'gWsc9qqUYEnVavXC5dM', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '首付款', '1', '11.0000000000', '1.0000000000', '1', 'Ut7vYL4tMRvBDUYoT2Q--__bjuatsuk2Hyiet4Z5k6', null);
INSERT INTO `tlk_expense_contract_payment` VALUES ('mdlSpsuJ2IHItfaAc8g--__7NuSLfcmZ3ILpeCe13O', '2023-11-06 06:39:06', '合同起草/支出类合同/expense_contract_payment', null, null, null, 'WlrAo0DK6YyyoGCLzBr', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', '$$$test', '2023-11-06 06:38:40', '__bjuatsuk2Hyiet4Z5k6', '', null, '\0', '-1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'WlrAo0DK6YyyoGCLzBr', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '尾款', '23233', '50.0000000000', '50000.0000000000', '', 'uXPmix4TPHAwVHPQJnt--__bjuatsuk2Hyiet4Z5k6', '');
INSERT INTO `tlk_expense_contract_payment` VALUES ('9meSkBipjgrJJwL1loG--__7NuSLfcmZ3ILpeCe13O', '2023-11-06 07:12:15', '合同起草/支出类合同/expense_contract_payment', null, null, null, 'WlrAo0DK6YyyoGCLzBr', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', '$$$test', '2023-11-06 07:11:40', '__bjuatsuk2Hyiet4Z5k6', '', null, '\0', '-1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'WlrAo0DK6YyyoGCLzBr', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '一次性付款', 'cs', '100.0000000000', '10000.0000000000', '', 'zbH38hozks6WpsBbv81--__bjuatsuk2Hyiet4Z5k6', '');
INSERT INTO `tlk_expense_contract_payment` VALUES ('KqOT95JfyyMjED96VEu--__7NuSLfcmZ3ILpeCe13O', '2023-11-06 06:34:44', '合同起草/支出类合同/expense_contract_payment', null, null, null, 'WlrAo0DK6YyyoGCLzBr', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', '$$$test', '2023-11-06 06:30:05', '__bjuatsuk2Hyiet4Z5k6', '', null, '\0', '-1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'WlrAo0DK6YyyoGCLzBr', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '尾款', '验收后10个工作日', '50.0000000000', '50000.0000000000', '', 'ZvHaFaqZYPy3BTtX3Zl--__bjuatsuk2Hyiet4Z5k6', '');

-- ----------------------------
-- Table structure for tlk_general_contract_approval
-- ----------------------------
DROP TABLE IF EXISTS `tlk_general_contract_approval`;
CREATE TABLE `tlk_general_contract_approval` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_contract_name` varchar(100) DEFAULT NULL,
  `ITEM_contract_number` varchar(100) DEFAULT NULL,
  `ITEM_apply_dept` varchar(100) DEFAULT NULL,
  `ITEM_apply_dept_name` varchar(100) DEFAULT NULL,
  `ITEM_dept_head` varchar(100) DEFAULT NULL,
  `ITEM_apply_date` datetime DEFAULT NULL,
  `ITEM_apply_person` longtext,
  `ITEM_apply_person_name` varchar(100) DEFAULT NULL,
  `ITEM_attend_dept` longtext,
  `ITEM_re_project_name` varchar(100) DEFAULT NULL,
  `ITEM_re_project_id` varchar(100) DEFAULT NULL,
  `ITEM_re_contract_name` varchar(100) DEFAULT NULL,
  `ITEM_re_contract_id` varchar(100) DEFAULT NULL,
  `ITEM_contract_amount` decimal(22,10) DEFAULT NULL,
  `ITEM_deliver_date` datetime DEFAULT NULL,
  `ITEM_start_time` datetime DEFAULT NULL,
  `ITEM_end_time` datetime DEFAULT NULL,
  `ITEM_term` longtext,
  `ITEM_contract_description` longtext,
  `ITEM_counterpart` varchar(100) DEFAULT NULL,
  `ITEM_counterpart_number` varchar(100) DEFAULT NULL,
  `ITEM_counterpart_legal_person` varchar(100) DEFAULT NULL,
  `ITEM_counterpart_type` varchar(100) DEFAULT NULL,
  `ITEM_counterpart_address` varchar(100) DEFAULT NULL,
  `ITEM_bank` varchar(100) DEFAULT NULL,
  `ITEM_bank_number` varchar(100) DEFAULT NULL,
  `ITEM_sign_leader` varchar(100) DEFAULT NULL,
  `ITEM_contact_info` varchar(100) DEFAULT NULL,
  `ITEM_is_use_template` varchar(100) DEFAULT NULL,
  `ITEM_upload` longtext,
  `ITEM_annex` longtext,
  `ITEM_re_dept_person` varchar(100) DEFAULT NULL,
  `ITEM_re_dept_attitude` varchar(100) DEFAULT NULL,
  `ITEM_re_dept_date` datetime DEFAULT NULL,
  `ITEM_re_dept_head` varchar(100) DEFAULT NULL,
  `ITEM_re_dept_head_attitude` varchar(100) DEFAULT NULL,
  `ITEM_re_dept_head_date` datetime DEFAULT NULL,
  `ITEM_finance_person` varchar(100) DEFAULT NULL,
  `ITEM_finance_person_attitude` varchar(100) DEFAULT NULL,
  `ITEM_finance_person_date` datetime DEFAULT NULL,
  `ITEM_finance_head` varchar(100) DEFAULT NULL,
  `ITEM_finance_head_attitude` varchar(100) DEFAULT NULL,
  `ITEM_finance_head_date` datetime DEFAULT NULL,
  `ITEM_legal_person` varchar(100) DEFAULT NULL,
  `ITEM_legal_attitude` varchar(100) DEFAULT NULL,
  `ITEM_legal_date` datetime DEFAULT NULL,
  `ITEM_legal_head` varchar(100) DEFAULT NULL,
  `ITEM_legal_head_attitude` varchar(100) DEFAULT NULL,
  `ITEM_legal_head_date` datetime DEFAULT NULL,
  `ITEM_leader` varchar(100) DEFAULT NULL,
  `ITEM_leader_attitude` varchar(100) DEFAULT NULL,
  `ITEM_leader_date` datetime DEFAULT NULL,
  `ITEM_manager` varchar(100) DEFAULT NULL,
  `ITEM_manager_attitude` varchar(100) DEFAULT NULL,
  `ITEM_manager_date` datetime DEFAULT NULL,
  `ITEM_legal_rep` varchar(100) DEFAULT NULL,
  `ITEM_legal_rep_attitude` varchar(100) DEFAULT NULL,
  `ITEM_legal_rep_date` datetime DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  `ITEM_expiration_description` longtext,
  `ITEM_identification` varchar(100) DEFAULT NULL,
  `ITEM_center_director` varchar(100) DEFAULT NULL,
  `ITEM_center_director_attitude` varchar(100) DEFAULT NULL,
  `ITEM_center_director_date` datetime DEFAULT NULL,
  `ITEM_zg_leader` varchar(100) DEFAULT NULL,
  `ITEM_zg_leader_attitude` varchar(100) DEFAULT NULL,
  `ITEM_zg_leader_date` datetime DEFAULT NULL,
  `ITEM_apply_person_date` datetime DEFAULT NULL,
  `ITEM_dept_head_date` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_general_contract_approval
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_general_contract_form
-- ----------------------------
DROP TABLE IF EXISTS `tlk_general_contract_form`;
CREATE TABLE `tlk_general_contract_form` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_contract_name` varchar(100) DEFAULT NULL,
  `ITEM_contract_number` varchar(100) DEFAULT NULL,
  `ITEM_contract_type` varchar(100) DEFAULT NULL,
  `ITEM_signing_date` datetime DEFAULT NULL,
  `ITEM_effective_date` datetime DEFAULT NULL,
  `ITEM_expiring_date` datetime DEFAULT NULL,
  `ITEM_contract_entity` varchar(100) DEFAULT NULL,
  `ITEM_contract_status` varchar(100) DEFAULT NULL,
  `ITEM_contract_amount` decimal(22,10) DEFAULT NULL,
  `ITEM_head` longtext,
  `ITEM_dept` longtext,
  `ITEM_re_project_name` varchar(100) DEFAULT NULL,
  `ITEM_re_project_id` varchar(100) DEFAULT NULL,
  `ITEM_re_contract_name` varchar(100) DEFAULT NULL,
  `ITEM_re_contract_id` varchar(100) DEFAULT NULL,
  `ITEM_contract_description` longtext,
  `ITEM_term` longtext,
  `ITEM_file` longtext,
  `ITEM_apply_person` varchar(100) DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  `ITEM_contract_ref` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_general_contract_form
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_invoice_form
-- ----------------------------
DROP TABLE IF EXISTS `tlk_invoice_form`;
CREATE TABLE `tlk_invoice_form` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_CONTRACT` varchar(100) DEFAULT NULL,
  `ITEM_CONTRACT_NUMBER` varchar(100) DEFAULT NULL,
  `ITEM_COUNTERPART` varchar(100) DEFAULT NULL,
  `ITEM_COUNTERPART_NUMBER` varchar(100) DEFAULT NULL,
  `ITEM_INVOICE_HEADER` varchar(100) DEFAULT NULL,
  `ITEM_IDENTIFICATION` varchar(100) DEFAULT NULL,
  `ITEM_INVOICE_NUMBER` varchar(100) DEFAULT NULL,
  `ITEM_INVOICE_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ITEM_INVOICE_DATE` datetime DEFAULT NULL,
  `ITEM_INVOICE_POINT` decimal(22,10) DEFAULT NULL,
  `ITEM_INVOICE_CONTENT` varchar(100) DEFAULT NULL,
  `ITEM_RECEIPT_DATE` datetime DEFAULT NULL,
  `ITEM_INVOICE_STATUS` varchar(100) DEFAULT NULL,
  `ITEM_INVOICE_TYPE` varchar(100) DEFAULT NULL,
  `ITEM_RECEIPT_UNIT` varchar(100) DEFAULT NULL,
  `ITEM_INVOICING_UNIT` varchar(100) DEFAULT NULL,
  `ITEM_RELATEID` varchar(100) DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  `ITEM_REGISTRANT` longtext,
  `ITEM_REGISTRATION_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_invoice_form
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_invoice_manager
-- ----------------------------
DROP TABLE IF EXISTS `tlk_invoice_manager`;
CREATE TABLE `tlk_invoice_manager` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_invoice_manager
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_invoicing_application
-- ----------------------------
DROP TABLE IF EXISTS `tlk_invoicing_application`;
CREATE TABLE `tlk_invoicing_application` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_INVOICE_STATUS` varchar(100) DEFAULT NULL,
  `ITEM_INVOICING_DATE` datetime DEFAULT NULL,
  `ITEM_INVOICE_NUMBER` varchar(100) DEFAULT NULL,
  `ITEM_APPLY_PERSON` longtext,
  `ITEM_APPLY_PERSON_NAME` varchar(100) DEFAULT NULL,
  `ITEM_APPLY_DEPT` varchar(100) DEFAULT NULL,
  `ITEM_APPLY_DEPT_NAME` varchar(100) DEFAULT NULL,
  `ITEM_APPLY_DATE` datetime DEFAULT NULL,
  `ITEM_CONTRACT_NAME` varchar(100) DEFAULT NULL,
  `ITEM_CONTRACT_NUMBER` varchar(100) DEFAULT NULL,
  `ITEM_CONTRACT_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ITEM_INVOICED_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ITEM_NOT_INVOICED_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ITEM_OBJECT` varchar(100) DEFAULT NULL,
  `ITEM_INVOICE_TYPE` varchar(100) DEFAULT NULL,
  `ITEM_INVOICE_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ITEM_INVOICE_AMOUNT_CN` varchar(100) DEFAULT NULL,
  `ITEM_INVOICE_CONTENT` varchar(100) DEFAULT NULL,
  `ITEM_INVOICE_HEADER` varchar(100) DEFAULT NULL,
  `ITEM_BANK` varchar(100) DEFAULT NULL,
  `ITEM_BANK_NUMBER` varchar(100) DEFAULT NULL,
  `ITEM_IDENTIFICATION` varchar(100) DEFAULT NULL,
  `ITEM_ADDRESS` varchar(100) DEFAULT NULL,
  `ITEM_TELEPHONE` varchar(100) DEFAULT NULL,
  `ITEM_DEPT_HEAD` varchar(100) DEFAULT NULL,
  `ITEM_LEADER` varchar(100) DEFAULT NULL,
  `ITEM_FINANCE_PERSON` varchar(100) DEFAULT NULL,
  `ITEM_FINANCE_HEAD` varchar(100) DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  `ITEM_APPLY_DATE_SHOW` varchar(100) DEFAULT NULL,
  `ITEM_PAYER` varchar(100) DEFAULT NULL,
  `ITEM_DEPT_HEAD_DATE` datetime DEFAULT NULL,
  `ITEM_FINANCE_PERSON_DATE` datetime DEFAULT NULL,
  `ITEM_FINANCE_PERSON2` varchar(100) DEFAULT NULL,
  `ITEM_FINANCE_PERSON_DATE2` datetime DEFAULT NULL,
  `ITEM_LEADER_DATE` datetime DEFAULT NULL,
  `ITEM_FINANCE_HEAD_DATE` datetime DEFAULT NULL,
  `ITEM_FINANCE_MANAGER` varchar(100) DEFAULT NULL,
  `ITEM_FINANCE_MANAGER_DATE` datetime DEFAULT NULL,
  `ITEM_MANAGER` varchar(100) DEFAULT NULL,
  `ITEM_MANAGER_DATE` datetime DEFAULT NULL,
  `ITEM_CHAIRMAN` varchar(100) DEFAULT NULL,
  `ITEM_CHAIRMAN_DATE` datetime DEFAULT NULL,
  `ITEM_RELATEID` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_invoicing_application
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_invoicing_withdraw
-- ----------------------------
DROP TABLE IF EXISTS `tlk_invoicing_withdraw`;
CREATE TABLE `tlk_invoicing_withdraw` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_REASON` longtext,
  `ITEM_RELATEID` varchar(100) DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_invoicing_withdraw
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_litigation_ledger
-- ----------------------------
DROP TABLE IF EXISTS `tlk_litigation_ledger`;
CREATE TABLE `tlk_litigation_ledger` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_litigation_ledger
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_my_contract
-- ----------------------------
DROP TABLE IF EXISTS `tlk_my_contract`;
CREATE TABLE `tlk_my_contract` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_my_contract
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_other_disputes
-- ----------------------------
DROP TABLE IF EXISTS `tlk_other_disputes`;
CREATE TABLE `tlk_other_disputes` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_BRIEF_CONTENT` longtext,
  `ITEM_NOTES` longtext,
  `ITEM_ANNEX` longtext,
  `ID` varchar(100) NOT NULL,
  `ITEM_HANDLING_PERSON` longtext,
  `ITEM_FROM` varchar(100) DEFAULT NULL,
  `ITEM_DEPT` longtext,
  `ITEM_RECEIVING_TIME` datetime DEFAULT NULL,
  `ITEM_OBLIGEE` longtext,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_other_disputes
-- ----------------------------
INSERT INTO `tlk_other_disputes` VALUES (null, '2023-08-29 02:32:30', '台账管理/诉讼台账/other_disputes', null, null, null, '__oP0irhWXGA2oZRusW1d', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s', '$$$test', '2023-08-29 02:30:10', '__qwo0CY05xSdyRB6uI3n', '', null, '\0', '3', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '__oP0irhWXGA2oZRusW1d', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '333', '33', '[{\"id\":\"版权维权类诉讼-sKHol5WeJs7qUbE04QW\",\"name\":\"版权维权类诉讼.xlsx\",\"path\":\"\\/uploads\\/item\\/2023\\/8\\/版权维权类诉讼-sKHol5WeJs7qUbE04QW.xlsx\",\"url\":\"\\/obpm\\/\\/uploads\\/item\\/2023\\/8\\/版权维权类诉讼-sKHol5WeJs7qUbE04QW.xlsx\",\"fileType\":\".xlsx\",\"uploader\":\"系统管理员\",\"uploadTime\":\"2023-08-29 10:32:27\",\"userName\":\"系统管理员\",\"time\":\"2023-08-29 10:32:27\",\"size\":\"4031\",\"isHasCompare\":false,\"showName\":\"版权维权类诉讼.xlsx\",\"isDelete\":false,\"isallowcopy\":\"true\",\"isallowdownload\":\"true\",\"isallowprint\":\"true\",\"openWaterMark\":false,\"watermark\":\"\",\"newversionscript\":true,\"versioncomparescript\":true,\"downloadscript\":true,\"deletescript\":true,\"renamescript\":true,\"previewscript\":true,\"editscript\":true,\"uid\":1693276347733,\"status\":\"success\",\"isEdit\":false}]', 'PvuBzgZ82m44vCwEr2n--__qwo0CY05xSdyRB6uI3n', 'a3UGQV6L2idFJORswya', '33', '9bQfJ8WkDrU9R2y9nBm', '2023-08-29 00:00:00', 'a3UGQV6L2idFJORswya');
INSERT INTO `tlk_other_disputes` VALUES (null, '2023-08-28 05:58:22', '台账管理/诉讼台账/other_disputes', null, null, null, '0VFlDyTyT0gylwIP52Z', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', '$$$test', '2023-08-28 05:57:53', '__qwo0CY05xSdyRB6uI3n', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '0VFlDyTyT0gylwIP52Z', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '11', '', '', 'rX3k8r63BlzJUohc54r--__qwo0CY05xSdyRB6uI3n', 'a3UGQV6L2idFJORswya', '11', '29sgTuB5EkBjAiBg84W', '2023-08-28 00:00:00', 'a3UGQV6L2idFJORswya');

-- ----------------------------
-- Table structure for tlk_payment_application
-- ----------------------------
DROP TABLE IF EXISTS `tlk_payment_application`;
CREATE TABLE `tlk_payment_application` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_PAYMENT_STATUS` varchar(100) DEFAULT NULL,
  `ITEM_PAYMENT_NUMBER` varchar(100) DEFAULT NULL,
  `ITEM_APPLY_PERSON` longtext,
  `ITEM_APPLY_PERSON_NAME` varchar(100) DEFAULT NULL,
  `ITEM_APPLY_DEPT` varchar(100) DEFAULT NULL,
  `ITEM_APPLY_DEPT_NAME` varchar(100) DEFAULT NULL,
  `ITEM_APPLY_DATE` datetime DEFAULT NULL,
  `ITEM_CONTRACT_NAME` varchar(100) DEFAULT NULL,
  `ITEM_CONTRACT_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ITEM_CONTRACT_NUMBER` varchar(100) DEFAULT NULL,
  `ITEM_PAID_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ITEM_UNPAID_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ITEM_HEAD` longtext,
  `ITEM_PAYMENT_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ITEM_PAYMENT_AMOUNT_CN` varchar(100) DEFAULT NULL,
  `ITEM_INVOICE_RECORD` varchar(100) DEFAULT NULL,
  `ITEM_PAYMENT_DATE` datetime DEFAULT NULL,
  `ITEM_COUNTERPART` varchar(100) DEFAULT NULL,
  `ITEM_BANK` varchar(100) DEFAULT NULL,
  `ITEM_BANK_NUMBER` varchar(100) DEFAULT NULL,
  `ITEM_TELEPHONE` varchar(100) DEFAULT NULL,
  `ITEM_DEPT_HEAD` varchar(100) DEFAULT NULL,
  `ITEM_FINANCE_PERSON` varchar(100) DEFAULT NULL,
  `ITEM_LEADER` varchar(100) DEFAULT NULL,
  `ITEM_FINANCE_HEAD` varchar(100) DEFAULT NULL,
  `ITEM_MANAGER` varchar(100) DEFAULT NULL,
  `ITEM_CHAIRMAN` varchar(100) DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  `ITEM_APPLY_DATE_SHOW` varchar(100) DEFAULT NULL,
  `ITEM_PAYMENT_PURPOSE` varchar(100) DEFAULT NULL,
  `ITEM_DEPT_HEAD_DATE` datetime DEFAULT NULL,
  `ITEM_FINANCE_PERSON_DATE` datetime DEFAULT NULL,
  `ITEM_FINANCE_PERSON2` varchar(100) DEFAULT NULL,
  `ITEM_FINANCE_PERSON_DATE2` datetime DEFAULT NULL,
  `ITEM_LEADER_DATE` datetime DEFAULT NULL,
  `ITEM_FINANCE_HEAD_DATE` datetime DEFAULT NULL,
  `ITEM_FINANCE_MANAGER` varchar(100) DEFAULT NULL,
  `ITEM_FINANCE_MANAGER_DATE` datetime DEFAULT NULL,
  `ITEM_MANAGER_DATE` datetime DEFAULT NULL,
  `ITEM_CHAIRMAN_DATE` datetime DEFAULT NULL,
  `ITEM_RELATEID` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_payment_application
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_payment_withdraw
-- ----------------------------
DROP TABLE IF EXISTS `tlk_payment_withdraw`;
CREATE TABLE `tlk_payment_withdraw` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_REASON` longtext,
  `ID` varchar(100) NOT NULL,
  `ITEM_RELATEID` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_payment_withdraw
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_project_management
-- ----------------------------
DROP TABLE IF EXISTS `tlk_project_management`;
CREATE TABLE `tlk_project_management` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_PROJECT_NAME` varchar(100) DEFAULT NULL,
  `ITEM_PROJECT_NUMBER` varchar(100) DEFAULT NULL,
  `ITEM_DEPT` varchar(100) DEFAULT NULL,
  `ITEM_PROJECT_HEAD` longtext,
  `ITEM_PROJECT_INTRODUCE` longtext,
  `ITEM_BUDGET_REVENUE` decimal(22,10) DEFAULT NULL,
  `ITEM_BUDGET_EXPENSE` decimal(22,10) DEFAULT NULL,
  `ITEM_STARTTIME` datetime DEFAULT NULL,
  `ITEM_ENDTIME` datetime DEFAULT NULL,
  `ITEM_SHARE` longtext,
  `ID` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_project_management
-- ----------------------------
INSERT INTO `tlk_project_management` VALUES (null, '2023-08-28 06:40:46', '基础设置/项目管理/project_management', null, null, null, '6zKKV9YUfRbQCaDaDwF', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_Wg8vhQqQ1RmlJ2AqBE4_XFSJvjbFgV9IZvFmoLy', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_Wg8vhQqQ1RmlJ2AqBE4_XFSJvjbFgV9IZvFmoLy', '$$$test', '2023-08-28 06:40:35', '__9xLa19mFr63PMS7EYud', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '6zKKV9YUfRbQCaDaDwF', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '测试', 'XM2308280002', 'XFSJvjbFgV9IZvFmoLy', '6zKKV9YUfRbQCaDaDwF', '', '10000.0000000000', '1000.0000000000', null, null, '', 'BAdrFMAJoiCgLzUI8qv--__9xLa19mFr63PMS7EYud');
INSERT INTO `tlk_project_management` VALUES (null, '2023-10-07 06:55:43', '基础设置/项目管理/project_management', null, null, null, 'UXzo63pIOMHnkEn9wGt', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_IVaLClkZID618w3OzDG_6eC7WhjR16nb18veaaZ', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_IVaLClkZID618w3OzDG_6eC7WhjR16nb18veaaZ', '$$$test', '2023-10-07 06:50:06', '__9xLa19mFr63PMS7EYud', '', null, '\0', '2', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'UXzo63pIOMHnkEn9wGt', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '2023年北京人艺短视频拍摄制作委托协议', 'XM2310070001', '6eC7WhjR16nb18veaaZ', 'UXzo63pIOMHnkEn9wGt', '负责北京人艺2023年全年短视频拍摄不少于35条', '350000.0000000000', '262500.0000000000', '2023-03-15 00:00:00', '2023-12-31 00:00:00', '', 'ERD4gwynWZh4kPkLW1M--__9xLa19mFr63PMS7EYud');
INSERT INTO `tlk_project_management` VALUES (null, '2023-10-08 03:01:18', '基础设置/项目管理/project_management', null, null, null, '9MnvWeuJ3hdu3yrkyT7', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', '$$$test', '2023-10-08 02:58:34', '__9xLa19mFr63PMS7EYud', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '9MnvWeuJ3hdu3yrkyT7', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '捷成华视网聚与北京IPTV内容合作协议', 'XM2310080001', 'YQPXWBU7xxJlbQtgDmh', '9MnvWeuJ3hdu3yrkyT7', '对方为内容提供商，为我方提供淘系列节目内容', null, null, '2023-08-01 00:00:00', '2026-05-31 00:00:00', '', 'h2qnzNYRz6T739ZGPeQ--__9xLa19mFr63PMS7EYud');
INSERT INTO `tlk_project_management` VALUES (null, '2023-08-29 05:45:52', '基础设置/项目管理/project_management', null, null, null, '__oP0irhWXGA2oZRusW1d', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s', '$$$test', '2023-08-29 05:45:51', '__9xLa19mFr63PMS7EYud', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '__oP0irhWXGA2oZRusW1d', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, 'ccc', 'XM2308290006', 'Z42xU8LmniRouZ93E7s', '__oP0irhWXGA2oZRusW1d', 'ccc', '333.0000000000', '333.0000000000', '2023-08-29 00:00:00', '2023-08-29 00:00:00', null, 'H7YAniYrxUWksMXlfPb--__9xLa19mFr63PMS7EYud');
INSERT INTO `tlk_project_management` VALUES (null, '2023-08-31 08:29:53', '基础设置/项目管理/project_management', null, null, null, '9cagMGQba7zhagaFV8r', 'hPKRWJcyToX0kYbTiQ2_NjwtzNUPWIwUxPbaH2T', 'hPKRWJcyToX0kYbTiQ2_NjwtzNUPWIwUxPbaH2T', '$$$test', '2023-08-31 08:28:41', '__9xLa19mFr63PMS7EYud', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '9cagMGQba7zhagaFV8r', 'KXsU3iF6YPBxYPWpRLa', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '北京市舆论引导技术服务委托项目', 'XM2308310001', 'NjwtzNUPWIwUxPbaH2T', '9cagMGQba7zhagaFV8r', '北京市舆论引导技术服务委托', '60000.0000000000', '0.0000000000', '2023-07-01 00:00:00', '2023-12-31 00:00:00', '', 'iHBBbOOs2KIfYqU16X1--__9xLa19mFr63PMS7EYud');
INSERT INTO `tlk_project_management` VALUES (null, '2023-08-22 10:27:48', '基础设置/项目管理/project_management', null, null, null, 'yEAg5BNT1NEnT5xb7dD', 'AWzpejpJq4vRIvXlU8r_GWAFbb3QNDDsNavToBH', 'AWzpejpJq4vRIvXlU8r_GWAFbb3QNDDsNavToBH', '$$$test', '2023-08-22 10:27:48', '__9xLa19mFr63PMS7EYud', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'yEAg5BNT1NEnT5xb7dD', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '项目D', 'XM2308220003', 'GWAFbb3QNDDsNavToBH', 'yEAg5BNT1NEnT5xb7dD', '测试2', '20000.0000000000', '2000.0000000000', '2023-08-01 00:00:00', '2023-08-07 00:00:00', null, 'JuiDxXJFDrr23NIlfji--__9xLa19mFr63PMS7EYud');
INSERT INTO `tlk_project_management` VALUES (null, '2023-08-25 09:43:40', '基础设置/项目管理/project_management', null, null, null, 'bCEHt1D0J8lg3TC7p9h', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_gIyQOGfjWUUsSHz7TDh', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_gIyQOGfjWUUsSHz7TDh', '$$$test', '2023-08-25 09:43:26', '__9xLa19mFr63PMS7EYud', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'bCEHt1D0J8lg3TC7p9h', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, 'aa', 'XM2308250001', 'gIyQOGfjWUUsSHz7TDh', 'bCEHt1D0J8lg3TC7p9h', '', '111.0000000000', '11.0000000000', null, null, '', 'l8CfrFaMkLqP9YNI5XU--__9xLa19mFr63PMS7EYud');
INSERT INTO `tlk_project_management` VALUES (null, '2023-08-30 07:50:25', '基础设置/项目管理/project_management', null, null, null, 'UXzo63pIOMHnkEn9wGt', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_IVaLClkZID618w3OzDG_6eC7WhjR16nb18veaaZ', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_IVaLClkZID618w3OzDG_6eC7WhjR16nb18veaaZ', '$$$test', '2023-08-30 06:46:45', '__9xLa19mFr63PMS7EYud', '', null, '\0', '3', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'UXzo63pIOMHnkEn9wGt', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '2023年端午文化节暨第二届张堪文化节活动项目', 'XM2308300001', '6eC7WhjR16nb18veaaZ', 'UXzo63pIOMHnkEn9wGt', '北京时间承接顺义区宣传部2023年端午文化节暨第二届张堪文化节活动执行，包含场地制作、演出执行、宣传推广等工作', '579440.0000000000', '400000.0000000000', '2023-06-01 00:00:00', '2023-06-30 00:00:00', '', 'lIEkzpC4LtKtuGOcFJt--__9xLa19mFr63PMS7EYud');
INSERT INTO `tlk_project_management` VALUES (null, '2023-08-28 09:41:00', '基础设置/项目管理/project_management', null, null, null, 'gWsc9qqUYEnVavXC5dM', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_GWAFbb3QNDDsNavToBH', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_GWAFbb3QNDDsNavToBH', '$$$test', '2023-08-28 09:40:54', '__9xLa19mFr63PMS7EYud', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'gWsc9qqUYEnVavXC5dM', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, 'bb', 'XM2308280005', 'GWAFbb3QNDDsNavToBH', 'gWsc9qqUYEnVavXC5dM', '', '33.0000000000', '33.0000000000', null, null, '', 'mrFcIaOkkWMl1AMJhsc--__9xLa19mFr63PMS7EYud');
INSERT INTO `tlk_project_management` VALUES (null, '2023-10-09 02:12:03', '基础设置/项目管理/project_management', null, null, null, '9MnvWeuJ3hdu3yrkyT7', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', '$$$test', '2023-10-09 02:08:45', '__9xLa19mFr63PMS7EYud', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '9MnvWeuJ3hdu3yrkyT7', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '北京新媒体（集团）与捷成华视网聚（常州）文化有限公司内容合作协议', 'XM2310090001', 'YQPXWBU7xxJlbQtgDmh', '9MnvWeuJ3hdu3yrkyT7', '对方为IPTV内容提供商，分成模式。', '0.0000000000', '0.0000000000', '2023-08-01 00:00:00', '2026-05-31 00:00:00', '', 'nwl73kRkIOxWz481BFO--__9xLa19mFr63PMS7EYud');
INSERT INTO `tlk_project_management` VALUES (null, '2023-08-29 05:43:14', '基础设置/项目管理/project_management', null, null, null, '__oP0irhWXGA2oZRusW1d', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s', '$$$test', '2023-08-29 02:39:27', '__9xLa19mFr63PMS7EYud', '', null, '\0', '2', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '__oP0irhWXGA2oZRusW1d', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, 'bb', 'XM2308290003', 'Z42xU8LmniRouZ93E7s', '__oP0irhWXGA2oZRusW1d', 'bb', '111.0000000000', '111.0000000000', '2023-08-29 00:00:00', '2023-08-31 00:00:00', '', 'oaEnZxHlrQijUBHdynY--__9xLa19mFr63PMS7EYud');
INSERT INTO `tlk_project_management` VALUES (null, '2023-08-25 10:02:31', '基础设置/项目管理/project_management', null, null, null, 'bCEHt1D0J8lg3TC7p9h', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_gIyQOGfjWUUsSHz7TDh', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_gIyQOGfjWUUsSHz7TDh', '$$$test', '2023-08-25 09:44:24', '__9xLa19mFr63PMS7EYud', '', null, '\0', '2', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'bCEHt1D0J8lg3TC7p9h', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, 'bb', 'XM2308250002', 'gIyQOGfjWUUsSHz7TDh', 'bCEHt1D0J8lg3TC7p9h', '', '11144.0000000000', '222.0000000000', null, null, 'a3UGQV6L2idFJORswya', 'Og8d2qFQXgVAyGYeUL2--__9xLa19mFr63PMS7EYud');
INSERT INTO `tlk_project_management` VALUES (null, '2023-08-28 09:40:34', '基础设置/项目管理/project_management', null, null, null, 'gWsc9qqUYEnVavXC5dM', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_GWAFbb3QNDDsNavToBH', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_GWAFbb3QNDDsNavToBH', '$$$test', '2023-08-28 09:40:12', '__9xLa19mFr63PMS7EYud', '', null, '\0', '2', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'gWsc9qqUYEnVavXC5dM', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, 'aa', 'XM2308280003', 'GWAFbb3QNDDsNavToBH', 'gWsc9qqUYEnVavXC5dM', '', '22.0000000000', '22.0000000000', null, null, '', 'ov0G6bOQeIpVNOqOckd--__9xLa19mFr63PMS7EYud');
INSERT INTO `tlk_project_management` VALUES (null, '2023-08-30 07:53:30', '基础设置/项目管理/project_management', null, null, null, '9cagMGQba7zhagaFV8r', 'hPKRWJcyToX0kYbTiQ2_NjwtzNUPWIwUxPbaH2T', 'hPKRWJcyToX0kYbTiQ2_NjwtzNUPWIwUxPbaH2T', '$$$test', '2023-08-30 07:52:20', '__9xLa19mFr63PMS7EYud', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '9cagMGQba7zhagaFV8r', 'KXsU3iF6YPBxYPWpRLa', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '法律咨询H5改造开发服务项目', 'XM2308300001', 'NjwtzNUPWIwUxPbaH2T', '9cagMGQba7zhagaFV8r', '法律咨询H5改造开发服务项目', '0.0000000000', '36000.0000000000', '2023-08-16 00:00:00', '2023-08-31 00:00:00', '', 'QBLvl2yM4EhjzbCF3L3--__9xLa19mFr63PMS7EYud');
INSERT INTO `tlk_project_management` VALUES (null, '2023-08-29 05:43:28', '基础设置/项目管理/project_management', null, null, null, '__oP0irhWXGA2oZRusW1d', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s', '$$$test', '2023-08-29 02:38:53', '__9xLa19mFr63PMS7EYud', '', null, '\0', '3', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '__oP0irhWXGA2oZRusW1d', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, 'aaa', 'XM2308290001', 'Z42xU8LmniRouZ93E7s', '__oP0irhWXGA2oZRusW1d', '111', '1111.0000000000', '11111.0000000000', '2023-08-29 00:00:00', '2023-08-31 00:00:00', '', 'QRWjo7eCLMelhnxvBt2--__9xLa19mFr63PMS7EYud');
INSERT INTO `tlk_project_management` VALUES (null, '2023-08-22 10:27:48', '基础设置/项目管理/project_management', null, null, null, 'yEAg5BNT1NEnT5xb7dD', 'AWzpejpJq4vRIvXlU8r_GWAFbb3QNDDsNavToBH', 'AWzpejpJq4vRIvXlU8r_GWAFbb3QNDDsNavToBH', '$$$test', '2023-08-22 10:27:47', '__9xLa19mFr63PMS7EYud', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'yEAg5BNT1NEnT5xb7dD', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '项目C', 'XM2308220002', 'GWAFbb3QNDDsNavToBH', 'yEAg5BNT1NEnT5xb7dD', '测试1', '10000.0000000000', '1000.0000000000', '2023-07-20 00:00:00', '2023-07-30 00:00:00', null, 'UainAMVfnYP0fZ4adKf--__9xLa19mFr63PMS7EYud');
INSERT INTO `tlk_project_management` VALUES (null, '2023-08-25 10:00:50', '基础设置/项目管理/project_management', null, null, null, 'bCEHt1D0J8lg3TC7p9h', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_gIyQOGfjWUUsSHz7TDh', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_gIyQOGfjWUUsSHz7TDh', '$$$test', '2023-08-25 10:00:41', '__9xLa19mFr63PMS7EYud', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'bCEHt1D0J8lg3TC7p9h', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, 'cc', 'XM2308250004', 'gIyQOGfjWUUsSHz7TDh', 'bCEHt1D0J8lg3TC7p9h', '', '33.0000000000', '33.0000000000', null, null, '', 'WI1t4p2LTdtayaldKEA--__9xLa19mFr63PMS7EYud');
INSERT INTO `tlk_project_management` VALUES (null, '2023-10-07 06:59:11', '基础设置/项目管理/project_management', null, null, null, 'to1jk20TgizISD5GXN4', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_KXqgCEyc0D9xv9INFn5_SGeFvmFSWMsyQu24EEX', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_KXqgCEyc0D9xv9INFn5_SGeFvmFSWMsyQu24EEX', '$$$test', '2023-10-07 06:58:14', '__9xLa19mFr63PMS7EYud', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'to1jk20TgizISD5GXN4', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '测试1234', 'XM2310070002', 'SGeFvmFSWMsyQu24EEX', 'to1jk20TgizISD5GXN4', '1234', '1234.0000000000', '1234.0000000000', '2023-10-08 00:00:00', '2024-10-31 00:00:00', '', 'Xm7XM2TneHQOcPmXsWJ--__9xLa19mFr63PMS7EYud');

-- ----------------------------
-- Table structure for tlk_project_share
-- ----------------------------
DROP TABLE IF EXISTS `tlk_project_share`;
CREATE TABLE `tlk_project_share` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_SHARE` longtext,
  `ITEM_SHAREINFO` longtext,
  `ID` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_project_share
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_protect_litigation
-- ----------------------------
DROP TABLE IF EXISTS `tlk_protect_litigation`;
CREATE TABLE `tlk_protect_litigation` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_CASE_NUMBER` varchar(100) DEFAULT NULL,
  `ITEM_CASE_NAME` varchar(100) DEFAULT NULL,
  `ITEM_MEDIATE_NUMBER` varchar(100) DEFAULT NULL,
  `ITEM_ROLE` varchar(100) DEFAULT NULL,
  `ITEM_CASE_STATUS` varchar(100) DEFAULT NULL,
  `ITEM_CASE_ORIGIN` varchar(100) DEFAULT NULL,
  `ITEM_CASE_ORIGIN2` varchar(100) DEFAULT NULL,
  `ITEM_RECEIPT_DATE` datetime DEFAULT NULL,
  `ITEM_CLOSE_DATE` datetime DEFAULT NULL,
  `ITEM_CLOSE_TYPE` varchar(100) DEFAULT NULL,
  `ITEM_LITIGATION_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ITEM_LITIGATION_COST` decimal(22,10) DEFAULT NULL,
  `ITEM_CLOSE_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ITEM_AGENT` varchar(100) DEFAULT NULL,
  `ITEM_HANDLING_PERSON` varchar(100) DEFAULT NULL,
  `ITEM_VOUCHER_NO` varchar(100) DEFAULT NULL,
  `ITEM_CLOSE` varchar(100) DEFAULT NULL,
  `ITEM_FILE` longtext,
  `ITEM_NOTES` longtext,
  `ID` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_protect_litigation
-- ----------------------------
INSERT INTO `tlk_protect_litigation` VALUES (null, '2023-09-06 09:00:30', '台账管理/诉讼台账/protect_litigation', null, null, null, null, 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', 'AWzpejpJq4vRIvXlU8r_n8i9yKz2zGegRCPur5Z_horJPFRbb5VHH3aHY3Q', '$$$test', '2023-08-28 05:57:08', '__TPksi5U5JPY35O51oMu', '__TT9kaMtyxJJfAlmqBXV', null, '\0', '3', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '0VFlDyTyT0gylwIP52Z', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, 'aa', 'aa', 'aa', '被告', '已执行', '网络侵权责任纠纷', '', '2023-08-28 00:00:00', null, '', '222.0000000000', '22.0000000000', '22.0000000000', '22', '22', '22', '22', '', '22', 'qXLIeK2lbbKjcKdVHVo--__TPksi5U5JPY35O51oMu');
INSERT INTO `tlk_protect_litigation` VALUES (null, '2023-08-29 02:28:58', '台账管理/诉讼台账/protect_litigation', null, null, null, '__oP0irhWXGA2oZRusW1d', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s', '$$$test', '2023-08-29 02:21:53', '__TPksi5U5JPY35O51oMu', '__TT9kaMtyxJJfAlmqBXV', null, '\0', '2', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '__oP0irhWXGA2oZRusW1d', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '222', '22', '22', '其他', '一审', '网络侵权责任纠纷', '', '2023-08-29 00:00:00', '2023-08-31 00:00:00', '调解', '11.0000000000', '11.0000000000', '11111.0000000000', '11', '11', '11', '11', '', '1111', 'yujcX84oXqbggnrV9vy--__TPksi5U5JPY35O51oMu');

-- ----------------------------
-- Table structure for tlk_revenue_contract_approval
-- ----------------------------
DROP TABLE IF EXISTS `tlk_revenue_contract_approval`;
CREATE TABLE `tlk_revenue_contract_approval` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_contract_name` varchar(100) DEFAULT NULL,
  `ITEM_contract_number` varchar(100) DEFAULT NULL,
  `ITEM_apply_dept` varchar(100) DEFAULT NULL,
  `ITEM_apply_dept_name` varchar(100) DEFAULT NULL,
  `ITEM_dept_head` varchar(100) DEFAULT NULL,
  `ITEM_apply_date` datetime DEFAULT NULL,
  `ITEM_apply_person` longtext,
  `ITEM_apply_person_name` varchar(100) DEFAULT NULL,
  `ITEM_attend_dept` longtext,
  `ITEM_re_project_name` varchar(100) DEFAULT NULL,
  `ITEM_re_project_id` varchar(100) DEFAULT NULL,
  `ITEM_re_contract_name` varchar(100) DEFAULT NULL,
  `ITEM_re_contract_id` varchar(100) DEFAULT NULL,
  `ITEM_contract_amount` decimal(22,10) DEFAULT NULL,
  `ITEM_start_time` datetime DEFAULT NULL,
  `ITEM_end_time` datetime DEFAULT NULL,
  `ITEM_term` longtext,
  `ITEM_contract_description` longtext,
  `ITEM_counterpart` varchar(100) DEFAULT NULL,
  `ITEM_counterpart_number` varchar(100) DEFAULT NULL,
  `ITEM_counterpart_legal_person` varchar(100) DEFAULT NULL,
  `ITEM_counterpart_type` varchar(100) DEFAULT NULL,
  `ITEM_counterpart_address` varchar(100) DEFAULT NULL,
  `ITEM_bank` varchar(100) DEFAULT NULL,
  `ITEM_bank_number` varchar(100) DEFAULT NULL,
  `ITEM_sign_leader` varchar(100) DEFAULT NULL,
  `ITEM_contact_info` varchar(100) DEFAULT NULL,
  `ITEM_is_use_template` varchar(100) DEFAULT NULL,
  `ITEM_upload` longtext,
  `ITEM_annex` longtext,
  `ITEM_re_dept_person` varchar(100) DEFAULT NULL,
  `ITEM_re_dept_attitude` varchar(100) DEFAULT NULL,
  `ITEM_re_dept_date` datetime DEFAULT NULL,
  `ITEM_re_dept_head` varchar(100) DEFAULT NULL,
  `ITEM_re_dept_head_attitude` varchar(100) DEFAULT NULL,
  `ITEM_re_dept_head_date` datetime DEFAULT NULL,
  `ITEM_finance_person` varchar(100) DEFAULT NULL,
  `ITEM_finance_person_attitude` varchar(100) DEFAULT NULL,
  `ITEM_finance_person_date` datetime DEFAULT NULL,
  `ITEM_finance_head` varchar(100) DEFAULT NULL,
  `ITEM_finance_head_attitude` varchar(100) DEFAULT NULL,
  `ITEM_finance_head_date` datetime DEFAULT NULL,
  `ITEM_legal_person` varchar(100) DEFAULT NULL,
  `ITEM_legal_attitude` varchar(100) DEFAULT NULL,
  `ITEM_legal_date` datetime DEFAULT NULL,
  `ITEM_legal_head` varchar(100) DEFAULT NULL,
  `ITEM_legal_head_attitude` varchar(100) DEFAULT NULL,
  `ITEM_legal_head_date` datetime DEFAULT NULL,
  `ITEM_leader` varchar(100) DEFAULT NULL,
  `ITEM_leader_attitude` varchar(100) DEFAULT NULL,
  `ITEM_leader_date` datetime DEFAULT NULL,
  `ITEM_manager` varchar(100) DEFAULT NULL,
  `ITEM_manager_attitude` varchar(100) DEFAULT NULL,
  `ITEM_manager_date` datetime DEFAULT NULL,
  `ITEM_legal_rep` varchar(100) DEFAULT NULL,
  `ITEM_legal_rep_attitude` varchar(100) DEFAULT NULL,
  `ITEM_legal_rep_date` datetime DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  `ITEM_expiration_description` longtext,
  `ITEM_identification` varchar(100) DEFAULT NULL,
  `ITEM_center_director` varchar(100) DEFAULT NULL,
  `ITEM_center_director_attitude` varchar(100) DEFAULT NULL,
  `ITEM_center_director_date` datetime DEFAULT NULL,
  `ITEM_zg_leader` varchar(100) DEFAULT NULL,
  `ITEM_zg_leader_attitude` varchar(100) DEFAULT NULL,
  `ITEM_zg_leader_date` datetime DEFAULT NULL,
  `ITEM_apply_person_date` datetime DEFAULT NULL,
  `ITEM_dept_head_date` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_revenue_contract_approval
-- ----------------------------
INSERT INTO `tlk_revenue_contract_approval` VALUES (null, '2023-10-24 06:13:03', '合同起草/收入类合同/revenue_contract_approval', null, null, null, 'OzdSAQBjeXp51fcn9wB', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_WZL33QFxAxQ3ThBxsd0', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_WZL33QFxAxQ3ThBxsd0', '$$$test', '2023-10-24 06:11:56', '__1HnnpsDdzfkNvwYDmRk', '__inZ4Rf1o4l1zhdJIkAk,__b6zlIys9xCcLYu4pnuW', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'OzdSAQBjeXp51fcn9wB', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '收入类合同', 'SR2310240001', 'WZL33QFxAxQ3ThBxsd0', '媒体电商部', '郑宇', '2023-10-24 00:00:00', 'OzdSAQBjeXp51fcn9wB', '陈兰', '', '', '', '', '', '10000.0000000000', '2023-10-27 00:00:00', '2023-10-31 00:00:00', '11212121', '', '优购文化传媒（北京）有限公司', 'XDF2308280001', '高巍', '客户', '北京市大兴区亦庄经济技术开发区同济南路19号', '交通银行股份有限公司北京自贸试验区支行', '1100 6077 7018 1700 87940', '罗佳睿', '15201100817', '是', '[{\"id\":\"演播室技术服务协议-io3P9tSXnfcrcv4JY8B\",\"name\":\"演播室技术服务协议.docx\",\"path\":\"\\/uploads\\/item\\/2023\\/10\\/演播室技术服务协议-io3P9tSXnfcrcv4JY8B.docx\",\"url\":\"\\/obpm\\/\\/uploads\\/item\\/2023\\/10\\/演播室技术服务协议-io3P9tSXnfcrcv4JY8B.docx\",\"fileType\":\".docx\",\"uploader\":\"吕静璇\",\"uploadTime\":\"2023-10-16 13:30:29\",\"userName\":\"吕静璇\",\"time\":\"2023-10-16 13:30:29\",\"size\":\"18079\",\"isHasCompare\":false,\"showName\":\"演播室技术服务协议.docx\",\"isDelete\":false,\"isallowcopy\":\"true\",\"isallowdownload\":\"true\",\"isallowprint\":\"true\",\"openWaterMark\":false,\"watermark\":\"\",\"newversionscript\":true,\"versioncomparescript\":true,\"downloadscript\":true,\"deletescript\":true,\"renamescript\":true,\"previewscript\":true,\"editscript\":true,\"uid\":1697434229135,\"status\":\"success\",\"isEdit\":false}]', '', '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, 'LxHVrTWjkgj21tzSWtN--__1HnnpsDdzfkNvwYDmRk', '', '', '', '', null, '', '', null, null, null);

-- ----------------------------
-- Table structure for tlk_revenue_contract_form
-- ----------------------------
DROP TABLE IF EXISTS `tlk_revenue_contract_form`;
CREATE TABLE `tlk_revenue_contract_form` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_contract_name` varchar(100) DEFAULT NULL,
  `ITEM_contract_number` varchar(100) DEFAULT NULL,
  `ITEM_contract_type` varchar(100) DEFAULT NULL,
  `ITEM_signing_date` datetime DEFAULT NULL,
  `ITEM_effective_date` datetime DEFAULT NULL,
  `ITEM_expiring_date` datetime DEFAULT NULL,
  `ITEM_contract_entity` varchar(100) DEFAULT NULL,
  `ITEM_contract_status` varchar(100) DEFAULT NULL,
  `ITEM_contract_amount` decimal(22,10) DEFAULT NULL,
  `ITEM_invoiced_amount` decimal(22,10) DEFAULT NULL,
  `ITEM_received_amount` decimal(22,10) DEFAULT NULL,
  `ITEM_collect_amount` decimal(22,10) DEFAULT NULL,
  `ITEM_head` longtext,
  `ITEM_dept` longtext,
  `ITEM_re_project_name` varchar(100) DEFAULT NULL,
  `ITEM_re_project_id` varchar(100) DEFAULT NULL,
  `ITEM_re_contract_name` varchar(100) DEFAULT NULL,
  `ITEM_re_contract_id` varchar(100) DEFAULT NULL,
  `ITEM_contract_description` longtext,
  `ITEM_term` longtext,
  `ITEM_counterpart` varchar(100) DEFAULT NULL,
  `ITEM_counterpart_legal_person` varchar(100) DEFAULT NULL,
  `ITEM_counterpart_number` varchar(100) DEFAULT NULL,
  `ITEM_contacts` varchar(100) DEFAULT NULL,
  `ITEM_contact_info` varchar(100) DEFAULT NULL,
  `ITEM_address` varchar(100) DEFAULT NULL,
  `ITEM_bank` varchar(100) DEFAULT NULL,
  `ITEM_bank_number` varchar(100) DEFAULT NULL,
  `ITEM_file` longtext,
  `ITEM_apply_person` varchar(100) DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  `ITEM_contract_ref` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_revenue_contract_form
-- ----------------------------
INSERT INTO `tlk_revenue_contract_form` VALUES (null, '2023-11-09 06:32:40', '合同执行/合同卡片/revenue_contract_form', null, null, null, 'lYjxYqrytPMOTiM6KLp', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '$$$test', '2023-11-09 06:03:55', '__4WlF2OuywICW02IsUW8', '__Y50eV6qmxDkqlFJlMaC,__eMGGuhtm2iy4vrBbVfP,__lnRHl7hm2H6mtcpH327,__2mEMWhShJw6mjoCQJot,__MPD9VyFPfx9pMhwwoeu,__kgtrW5PYTckYCxVWeCw', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'lYjxYqrytPMOTiM6KLp', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '收入类合同-1109', 'SR2311090001', '收入合同', '2023-11-09 00:00:00', '2023-11-02 00:00:00', '2023-11-28 00:00:00', '收入合同', '履行中', '100000.0000000000', '10000.0000000000', '0.0000000000', '100000.0000000000', 'Rnj8E66kUoXuFk1aJly', 'HxAPsLoWqGcM9VArPJH', '', '', '', '', '', '条款1\n条款2', '收入合同', '', '', '', '', '', '', '', '', 'Rnj8E66kUoXuFk1aJly', 'fm9TbnyMRatuyI7UoMl--__4WlF2OuywICW02IsUW8', '2023-BJSJ-JSAQZX-NRAQ-005');

-- ----------------------------
-- Table structure for tlk_revenue_contract_form_object
-- ----------------------------
DROP TABLE IF EXISTS `tlk_revenue_contract_form_object`;
CREATE TABLE `tlk_revenue_contract_form_object` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_OBJECT` varchar(100) DEFAULT NULL,
  `ITEM_UNIT` varchar(100) DEFAULT NULL,
  `ITEM_QUANTITY` decimal(22,10) DEFAULT NULL,
  `ITEM_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_revenue_contract_form_object
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_revenue_contract_form_payment
-- ----------------------------
DROP TABLE IF EXISTS `tlk_revenue_contract_form_payment`;
CREATE TABLE `tlk_revenue_contract_form_payment` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_PAYMENT_TYPE` varchar(100) DEFAULT NULL,
  `ITEM_COLLECTION_TERMS` varchar(100) DEFAULT NULL,
  `ITEM_COLLECTION_RATIO` decimal(22,10) DEFAULT NULL,
  `ITEM_COLLECTION_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ITEM_COLLECTION_STATUS` varchar(100) DEFAULT NULL,
  `ITEM_INVOICE_NUMBER` varchar(100) DEFAULT NULL,
  `ITEM_DESCRIPTION` longtext,
  `ID` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_revenue_contract_form_payment
-- ----------------------------
INSERT INTO `tlk_revenue_contract_form_payment` VALUES ('fm9TbnyMRatuyI7UoMl--__4WlF2OuywICW02IsUW8', '2023-11-09 06:03:55', '合同执行/合同卡片/revenue_contract_form_payment', null, null, null, 'lYjxYqrytPMOTiM6KLp', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '$$$test', '2023-11-09 06:03:55', '__kgtrW5PYTckYCxVWeCw', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'lYjxYqrytPMOTiM6KLp', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '', '', '0.0000000000', '0.0000000000', '', '', '', '8IfcTdAjicDJMYUUMgo--__kgtrW5PYTckYCxVWeCw');
INSERT INTO `tlk_revenue_contract_form_payment` VALUES ('fm9TbnyMRatuyI7UoMl--__4WlF2OuywICW02IsUW8', '2023-11-09 06:03:55', '合同执行/合同卡片/revenue_contract_form_payment', null, null, null, 'lYjxYqrytPMOTiM6KLp', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_tvwbDrtXPYldJwebSPj_gIyQOGfjWUUsSHz7TDh', '$$$test', '2023-11-09 06:03:55', '__kgtrW5PYTckYCxVWeCw', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'lYjxYqrytPMOTiM6KLp', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '一次性收款', '工作干完', '100.0000000000', '10000.0000000000', '', '', '', 'KAR28pPSgy0r6HdHD7R--__kgtrW5PYTckYCxVWeCw');

-- ----------------------------
-- Table structure for tlk_revenue_contract_object
-- ----------------------------
DROP TABLE IF EXISTS `tlk_revenue_contract_object`;
CREATE TABLE `tlk_revenue_contract_object` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_OBJECT` varchar(100) DEFAULT NULL,
  `ITEM_UNIT` varchar(100) DEFAULT NULL,
  `ITEM_QUANTITY` decimal(22,10) DEFAULT NULL,
  `ITEM_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_revenue_contract_object
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_revenue_contract_payment
-- ----------------------------
DROP TABLE IF EXISTS `tlk_revenue_contract_payment`;
CREATE TABLE `tlk_revenue_contract_payment` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_PAYMENT_TYPE` varchar(100) DEFAULT NULL,
  `ITEM_PAYMENT_TERMS` varchar(100) DEFAULT NULL,
  `ITEM_PAYMENT_RATIO` decimal(22,10) DEFAULT NULL,
  `ITEM_PAYMENT_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ITEM_DESCRIPTION` longtext,
  `ID` varchar(100) NOT NULL,
  `ITEM_SETTLEMENT` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_revenue_contract_payment
-- ----------------------------
INSERT INTO `tlk_revenue_contract_payment` VALUES ('cHh2PiEQoafBYrjwXw4--__1HnnpsDdzfkNvwYDmRk', '2023-08-30 08:01:19', '合同起草/收入类合同/revenue_contract_payment', null, null, null, 'UXzo63pIOMHnkEn9wGt', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_IVaLClkZID618w3OzDG_6eC7WhjR16nb18veaaZ', '5fbaLCe8DIbIP2BdQoc_Z42xU8LmniRouZ93E7s_IVaLClkZID618w3OzDG_6eC7WhjR16nb18veaaZ', '$$$test', '2023-08-30 07:59:53', '__inZ4Rf1o4l1zhdJIkAk', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'UXzo63pIOMHnkEn9wGt', '3CffNlgt9B9StIjIUPB', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '一次性收款', '本项目财政预算下达后', '100.0000000000', '579440.0000000000', '', 'i8fwFhhz1iUPNFc8Y64--__inZ4Rf1o4l1zhdJIkAk', null);
INSERT INTO `tlk_revenue_contract_payment` VALUES ('LxHVrTWjkgj21tzSWtN--__1HnnpsDdzfkNvwYDmRk', '2023-10-24 06:13:03', '合同起草/收入类合同/revenue_contract_payment', null, null, null, 'OzdSAQBjeXp51fcn9wB', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_WZL33QFxAxQ3ThBxsd0', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_WZL33QFxAxQ3ThBxsd0', '$$$test', '2023-10-24 06:12:51', '__inZ4Rf1o4l1zhdJIkAk', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'OzdSAQBjeXp51fcn9wB', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '', '', '0.0000000000', '0.0000000000', '', 't9ZCTJdYEXFA5WpatRT--__inZ4Rf1o4l1zhdJIkAk', '');
INSERT INTO `tlk_revenue_contract_payment` VALUES ('LxHVrTWjkgj21tzSWtN--__1HnnpsDdzfkNvwYDmRk', '2023-10-24 06:13:03', '合同起草/收入类合同/revenue_contract_payment', null, null, null, 'OzdSAQBjeXp51fcn9wB', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_WZL33QFxAxQ3ThBxsd0', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_WZL33QFxAxQ3ThBxsd0', '$$$test', '2023-10-24 06:12:37', '__inZ4Rf1o4l1zhdJIkAk', '', null, '\0', '2', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'OzdSAQBjeXp51fcn9wB', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '一次性收款', '', '100.0000000000', '100000.0000000000', '', 'tXUP1R2BELVcHRUmTnV--__inZ4Rf1o4l1zhdJIkAk', '');

-- ----------------------------
-- Table structure for tlk_routine_litigation
-- ----------------------------
DROP TABLE IF EXISTS `tlk_routine_litigation`;
CREATE TABLE `tlk_routine_litigation` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_CASE_NUMBER` varchar(100) DEFAULT NULL,
  `ITEM_CASE_NAME` varchar(100) DEFAULT NULL,
  `ITEM_MEDIATE_NUMBER` varchar(100) DEFAULT NULL,
  `ITEM_ROLE` varchar(100) DEFAULT NULL,
  `ITEM_CASE_STATUS` varchar(100) DEFAULT NULL,
  `ITEM_CASE_ORIGIN` varchar(100) DEFAULT NULL,
  `ITEM_CASE_ORIGIN2` varchar(100) DEFAULT NULL,
  `ITEM_RECEIPT_DATE` datetime DEFAULT NULL,
  `ITEM_CLOSE_DATE` datetime DEFAULT NULL,
  `ITEM_CLOSE_TYPE` varchar(100) DEFAULT NULL,
  `ITEM_LITIGATION_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ITEM_LITIGATION_COST` decimal(22,10) DEFAULT NULL,
  `ITEM_CLOSE_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ITEM_AGENT` varchar(100) DEFAULT NULL,
  `ITEM_HANDLING_PERSON` varchar(100) DEFAULT NULL,
  `ITEM_VOUCHER_NO` varchar(100) DEFAULT NULL,
  `ITEM_CLOSE` varchar(100) DEFAULT NULL,
  `ITEM_FILE` longtext,
  `ITEM_NOTES` longtext,
  `ID` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_routine_litigation
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_share_contract_approval
-- ----------------------------
DROP TABLE IF EXISTS `tlk_share_contract_approval`;
CREATE TABLE `tlk_share_contract_approval` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_contract_name` varchar(100) DEFAULT NULL,
  `ITEM_contract_number` varchar(100) DEFAULT NULL,
  `ITEM_apply_dept` varchar(100) DEFAULT NULL,
  `ITEM_apply_dept_name` varchar(100) DEFAULT NULL,
  `ITEM_dept_head` varchar(100) DEFAULT NULL,
  `ITEM_apply_date` datetime DEFAULT NULL,
  `ITEM_apply_person` longtext,
  `ITEM_apply_person_name` varchar(100) DEFAULT NULL,
  `ITEM_attend_dept` longtext,
  `ITEM_re_project_name` varchar(100) DEFAULT NULL,
  `ITEM_re_project_id` varchar(100) DEFAULT NULL,
  `ITEM_re_contract_name` varchar(100) DEFAULT NULL,
  `ITEM_re_contract_id` varchar(100) DEFAULT NULL,
  `ITEM_contract_amount` decimal(22,10) DEFAULT NULL,
  `ITEM_deliver_date` datetime DEFAULT NULL,
  `ITEM_start_time` datetime DEFAULT NULL,
  `ITEM_end_time` datetime DEFAULT NULL,
  `ITEM_term` longtext,
  `ITEM_contract_description` longtext,
  `ITEM_counterpart` varchar(100) DEFAULT NULL,
  `ITEM_is_use_template` varchar(100) DEFAULT NULL,
  `ITEM_upload` longtext,
  `ITEM_annex` longtext,
  `ITEM_re_dept_person` varchar(100) DEFAULT NULL,
  `ITEM_re_dept_attitude` varchar(100) DEFAULT NULL,
  `ITEM_re_dept_date` datetime DEFAULT NULL,
  `ITEM_re_dept_head` varchar(100) DEFAULT NULL,
  `ITEM_re_dept_head_attitude` varchar(100) DEFAULT NULL,
  `ITEM_re_dept_head_date` datetime DEFAULT NULL,
  `ITEM_finance_person` varchar(100) DEFAULT NULL,
  `ITEM_finance_person_attitude` varchar(100) DEFAULT NULL,
  `ITEM_finance_person_date` datetime DEFAULT NULL,
  `ITEM_finance_head` varchar(100) DEFAULT NULL,
  `ITEM_finance_head_attitude` varchar(100) DEFAULT NULL,
  `ITEM_finance_head_date` datetime DEFAULT NULL,
  `ITEM_legal_person` varchar(100) DEFAULT NULL,
  `ITEM_legal_attitude` varchar(100) DEFAULT NULL,
  `ITEM_legal_date` datetime DEFAULT NULL,
  `ITEM_legal_head` varchar(100) DEFAULT NULL,
  `ITEM_legal_head_attitude` varchar(100) DEFAULT NULL,
  `ITEM_legal_head_date` datetime DEFAULT NULL,
  `ITEM_leader` varchar(100) DEFAULT NULL,
  `ITEM_leader_attitude` varchar(100) DEFAULT NULL,
  `ITEM_leader_date` datetime DEFAULT NULL,
  `ITEM_manager` varchar(100) DEFAULT NULL,
  `ITEM_manager_attitude` varchar(100) DEFAULT NULL,
  `ITEM_manager_date` datetime DEFAULT NULL,
  `ITEM_legal_rep` varchar(100) DEFAULT NULL,
  `ITEM_legal_rep_attitude` varchar(100) DEFAULT NULL,
  `ITEM_legal_rep_date` datetime DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  `ITEM_expiration_description` longtext,
  `ITEM_center_director` varchar(100) DEFAULT NULL,
  `ITEM_center_director_attitude` varchar(100) DEFAULT NULL,
  `ITEM_center_director_date` datetime DEFAULT NULL,
  `ITEM_zg_leader` varchar(100) DEFAULT NULL,
  `ITEM_zg_leader_attitude` varchar(100) DEFAULT NULL,
  `ITEM_zg_leader_date` datetime DEFAULT NULL,
  `ITEM_apply_person_date` datetime DEFAULT NULL,
  `ITEM_dept_head_date` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_share_contract_approval
-- ----------------------------
INSERT INTO `tlk_share_contract_approval` VALUES (null, '2023-10-09 02:43:50', '合同起草/分成类合同/share_contract_approval', '', null, null, '9MnvWeuJ3hdu3yrkyT7', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', 'AWzpejpJq4vRIvXlU8r_cnL4Ymly6nTr2n6eazZ_YQPXWBU7xxJlbQtgDmh', '$$$test', '2023-10-09 02:34:41', '__hB08FiQYT6fsHmKbhVQ', '__ZrQzS3eQ4e6yzzAExPU,__wv5qf8n1vfnhQBP9i4S', '9MnvWeuJ3hdu3yrkyT7', '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, '9MnvWeuJ3hdu3yrkyT7', 'QnkpCDiSJqeIHrbbY9X', '{\"1689675721656\":[]}', '{\"1689675721656\":[]}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '北京新媒体（集团）有限公司与捷成华视网聚（常州）文化有限公司之北京IPTV内容合作协议', 'FC2310090002', 'YQPXWBU7xxJlbQtgDmh', '运营拓展部', '齐若凡', '2023-10-09 00:00:00', '9MnvWeuJ3hdu3yrkyT7', '张树则', '', '', '', '', '', '0.0000000000', null, '2023-08-01 00:00:00', '2026-05-31 00:00:00', '向我方提供影视剧类节目内容，分成模式', '', null, '', '', '', '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, '', '', null, 'SCHPkoJE9jYI1QrTOrW--__hB08FiQYT6fsHmKbhVQ', '合同到期前，负责合同流程的同志因病住院，导致倒签', '', '', null, '', '', null, null, null);

-- ----------------------------
-- Table structure for tlk_share_contract_object
-- ----------------------------
DROP TABLE IF EXISTS `tlk_share_contract_object`;
CREATE TABLE `tlk_share_contract_object` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_OBJECT` varchar(100) DEFAULT NULL,
  `ITEM_UNIT` varchar(100) DEFAULT NULL,
  `ITEM_QUANTITY` decimal(22,10) DEFAULT NULL,
  `ITEM_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_share_contract_object
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_share_contract_payment
-- ----------------------------
DROP TABLE IF EXISTS `tlk_share_contract_payment`;
CREATE TABLE `tlk_share_contract_payment` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_PAYMENT_TYPE` varchar(100) DEFAULT NULL,
  `ITEM_PAYMENT_TERMS` varchar(100) DEFAULT NULL,
  `ITEM_PAYMENT_RATIO` decimal(22,10) DEFAULT NULL,
  `ITEM_PAYMENT_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ITEM_DESCRIPTION` longtext,
  `ID` varchar(100) NOT NULL,
  `ITEM_SETTLEMENT` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_share_contract_payment
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_signing_center
-- ----------------------------
DROP TABLE IF EXISTS `tlk_signing_center`;
CREATE TABLE `tlk_signing_center` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_STAMP_STATUS` varchar(100) DEFAULT NULL,
  `ITEM_CONTRACT_ID` varchar(100) DEFAULT NULL,
  `ITEM_ARCHIVING_STATUS` varchar(100) DEFAULT NULL,
  `ITEM_FILING_NUMBER` varchar(100) DEFAULT NULL,
  `ITEM_APPLY_DEPT` varchar(100) DEFAULT NULL,
  `ITEM_APPLY_DATE` datetime DEFAULT NULL,
  `ITEM_APPLY_PERSON` longtext,
  `ITEM_RE_CONTRACT_NAME` varchar(100) DEFAULT NULL,
  `ITEM_RE_CONTRACT_ID` varchar(100) DEFAULT NULL,
  `ITEM_RE_PROJECT_NAME` varchar(100) DEFAULT NULL,
  `ITEM_RE_PROJECT_ID` varchar(100) DEFAULT NULL,
  `ITEM_CONTRACT_NAME` varchar(100) DEFAULT NULL,
  `ITEM_CONTRACT_NUMBER` varchar(100) DEFAULT NULL,
  `ITEM_CONTRACT_TYPE` varchar(100) DEFAULT NULL,
  `ITEM_PROCURE_TYPE` varchar(100) DEFAULT NULL,
  `ITEM_START_TIME` datetime DEFAULT NULL,
  `ITEM_END_TIME` datetime DEFAULT NULL,
  `ITEM_CONTRACT_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ITEM_APPROVAL_DATE` datetime DEFAULT NULL,
  `ITEM_STAMP_DATE` datetime DEFAULT NULL,
  `ITEM_TERM` longtext,
  `ITEM_CONTRACT_DESCRIPTION` longtext,
  `ITEM_UPLOAD` longtext,
  `ITEM_STAMP_FILE` longtext,
  `ITEM_FILING_DATE` datetime DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  `ITEM_FILING_PERSON` longtext,
  `ITEM_STAMP_PERSON` longtext,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_signing_center
-- ----------------------------
INSERT INTO `tlk_signing_center` VALUES (null, '2023-11-10 04:23:48', '签署中心/signing_center', null, null, null, 'QghJDaDCt5fckOMCWAe', 'AWzpejpJq4vRIvXlU8r_29sgTuB5EkBjAiBg84W', 'AWzpejpJq4vRIvXlU8r_29sgTuB5EkBjAiBg84W', '$$$test', '2023-11-10 04:23:48', '__76cQLPT8YFrSM4mfQ9K', '__sLsbDoMV0Sc622Kgv6x,__eMGGuhtm2iy4vrBbVfP', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'QghJDaDCt5fckOMCWAe', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '未盖章', 'i1iNKJtnTvtGxo2DBkt--__7NuSLfcmZ3ILpeCe13O', '未备案', '', 'horJPFRbb5VHH3aHY3Q', '2023-11-10 00:00:00', 'Mxspdb6zQc7sibnXuG0', '', '', '', '', '223232', 'ZC2311100008', '支出合同', '竞争性谈判', '2023-11-19 00:00:00', '2023-11-27 00:00:00', '323233323.0000000000', '2023-11-10 04:23:48', null, '323232', '', '[{\"id\":\"演播室技术服务协议-io3P9tSXnfcrcv4JY8B\",\"name\":\"演播室技术服务协议.docx\",\"path\":\"\\/uploads\\/item\\/2023\\/10\\/演播室技术服务协议-io3P9tSXnfcrcv4JY8B.docx\",\"url\":\"\\/obpm\\/\\/uploads\\/item\\/2023\\/10\\/演播室技术服务协议-io3P9tSXnfcrcv4JY8B.docx\",\"fileType\":\".docx\",\"uploader\":\"吕静璇\",\"uploadTime\":\"2023-10-16 13:30:29\",\"userName\":\"吕静璇\",\"time\":\"2023-10-16 13:30:29\",\"size\":\"18079\",\"isHasCompare\":true,\"showName\":\"演播室技术服务协议.docx\",\"isDelete\":false,\"isallowcopy\":\"true\",\"isallowdownload\":\"true\",\"isallowprint\":\"true\",\"openWaterMark\":false,\"watermark\":\"\",\"newversionscript\":true,\"versioncomparescript\":true,\"downloadscript\":true,\"deletescript\":true,\"renamescript\":true,\"previewscript\":true,\"editscript\":true,\"uid\":1697434229135,\"status\":\"success\",\"isEdit\":false}]', null, null, 'ICISfnBlJyf660IkxzD--__76cQLPT8YFrSM4mfQ9K', null, null);

-- ----------------------------
-- Table structure for tlk_signing_center_all
-- ----------------------------
DROP TABLE IF EXISTS `tlk_signing_center_all`;
CREATE TABLE `tlk_signing_center_all` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_signing_center_all
-- ----------------------------

-- ----------------------------
-- Table structure for tlk_signing_center_payment
-- ----------------------------
DROP TABLE IF EXISTS `tlk_signing_center_payment`;
CREATE TABLE `tlk_signing_center_payment` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ITEM_PAYMENT_TYPE` varchar(100) DEFAULT NULL,
  `ITEM_PAYMENT_TERMS` varchar(100) DEFAULT NULL,
  `ITEM_PAYMENT_RATIO` decimal(22,10) DEFAULT NULL,
  `ITEM_PAYMENT_AMOUNT` decimal(22,10) DEFAULT NULL,
  `ITEM_DESCRIPTION` longtext,
  `ID` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_signing_center_payment
-- ----------------------------
INSERT INTO `tlk_signing_center_payment` VALUES ('ICISfnBlJyf660IkxzD--__76cQLPT8YFrSM4mfQ9K', '2023-11-10 04:23:48', '签署中心/signing_center_payment', null, null, null, 'QghJDaDCt5fckOMCWAe', 'AWzpejpJq4vRIvXlU8r_29sgTuB5EkBjAiBg84W', 'AWzpejpJq4vRIvXlU8r_29sgTuB5EkBjAiBg84W', '$$$test', '2023-11-10 04:23:48', '__sLsbDoMV0Sc622Kgv6x', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'QghJDaDCt5fckOMCWAe', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '', '', '0.0000000000', '0.0000000000', '', 'ByDJYvPVzJ2YblXkjB2--__sLsbDoMV0Sc622Kgv6x');
INSERT INTO `tlk_signing_center_payment` VALUES ('ICISfnBlJyf660IkxzD--__76cQLPT8YFrSM4mfQ9K', '2023-11-10 04:23:48', '签署中心/signing_center_payment', null, null, null, 'QghJDaDCt5fckOMCWAe', 'AWzpejpJq4vRIvXlU8r_29sgTuB5EkBjAiBg84W', 'AWzpejpJq4vRIvXlU8r_29sgTuB5EkBjAiBg84W', '$$$test', '2023-11-10 04:23:48', '__sLsbDoMV0Sc622Kgv6x', '', null, '\0', '1', '__M4oRwEVzSB1oI1D0faN', '0', '', '', null, 'QghJDaDCt5fckOMCWAe', 'QnkpCDiSJqeIHrbbY9X', '{}', '{}', '[{\"nodes\":[{\"nodeId\":\"\",\"stateLabel\":\"\",\"state\":\"\",\"auditors\":[],\"coAuditors\":[]}]}]', null, null, null, null, null, null, '一次性付款', '323', '100.0000000000', '3323.0000000000', '', 'LRn1LgCUAHXgFUBCrjr--__sLsbDoMV0Sc622Kgv6x');

-- ----------------------------
-- Table structure for tlk_urging_application
-- ----------------------------
DROP TABLE IF EXISTS `tlk_urging_application`;
CREATE TABLE `tlk_urging_application` (
  `PARENT` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `KINGGRIDSIGNATURE` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `ID` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tlk_urging_application
-- ----------------------------

-- ----------------------------
-- Table structure for t_actorhis
-- ----------------------------
DROP TABLE IF EXISTS `t_actorhis`;
CREATE TABLE `t_actorhis` (
  `ID` varchar(100) NOT NULL,
  `ACTORID` varchar(100) DEFAULT NULL,
  `NAME` varchar(100) DEFAULT NULL,
  `AGENTID` varchar(100) DEFAULT NULL,
  `AGENTNAME` varchar(100) DEFAULT NULL,
  `TYPE` int(11) DEFAULT NULL,
  `PROCESSTIME` datetime DEFAULT NULL,
  `ATTITUDE` varchar(2000) DEFAULT NULL,
  `NODEHIS_ID` varchar(100) DEFAULT NULL,
  `FLOWSTATERT_ID` varchar(100) DEFAULT NULL,
  `DOC_ID` varchar(100) DEFAULT NULL,
  `SIGNATURE` longtext,
  `RECEIVERINFO` longtext,
  `ACTIONTIME` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_actorhis
-- ----------------------------
INSERT INTO `t_actorhis` VALUES ('NuUvVQbc7FsDssRKAYx', 'Mxspdb6zQc7sibnXuG0', '吴继华', null, null, '3', '2023-11-13 07:52:16', '请审批', 'XfLKE4UY1FycDTQxTFk', '7Q0G8h9g587tohUGy0z--__7NuSLfcmZ3ILpeCe13O-1S7qeEgsRGpRsH396O8', '7Q0G8h9g587tohUGy0z--__7NuSLfcmZ3ILpeCe13O', '', '{\"users\":[{\"userid\":\"hVS4izsK5fGJSNsfAdu\",\"username\":\"姚丹\"}]}', '2023-11-13 07:52:16');

-- ----------------------------
-- Table structure for t_actorrt
-- ----------------------------
DROP TABLE IF EXISTS `t_actorrt`;
CREATE TABLE `t_actorrt` (
  `ID` varchar(100) NOT NULL,
  `ACTORID` varchar(100) DEFAULT NULL,
  `NAME` varchar(100) DEFAULT NULL,
  `ISPROCESSED` bit(1) DEFAULT NULL,
  `TYPE` int(11) DEFAULT NULL,
  `NODERT_ID` varchar(100) DEFAULT NULL,
  `FLOWSTATERT_ID` varchar(100) DEFAULT NULL,
  `DOC_ID` varchar(100) DEFAULT NULL,
  `DEADLINE` datetime DEFAULT NULL,
  `PENDING` bit(1) DEFAULT NULL,
  `ISREAD` int(11) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `APPROVAL_POSITION` int(11) DEFAULT NULL,
  `REMINDER_TIMES` int(11) DEFAULT NULL,
  `BAK` longtext,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_actorrt
-- ----------------------------
INSERT INTO `t_actorrt` VALUES ('027GaVrHNfHaEbEdsox', 'fi6zAW0bRVqGdq9vGCh', '丁晓阳', '\0', '3', 'CuU4adsmIW6r1CvarGx--__KodzsMDr3aXCy87cPrb-XSyKDUVoDRISobXVOE6-Hwm274UWSDpmUVoUXXK', 'CuU4adsmIW6r1CvarGx--__KodzsMDr3aXCy87cPrb-XSyKDUVoDRISobXVOE6', 'CuU4adsmIW6r1CvarGx--__KodzsMDr3aXCy87cPrb', null, '', '0', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('1xN6RSc9HQuRZKjOtAg', 'u2V4sCrNsyHzo4GA6RE', '潘全心', '\0', '3', 'CuU4adsmIW6r1CvarGx--__KodzsMDr3aXCy87cPrb-XSyKDUVoDRISobXVOE6-Hwm274UWSDpmUVoUXXK', 'CuU4adsmIW6r1CvarGx--__KodzsMDr3aXCy87cPrb-XSyKDUVoDRISobXVOE6', 'CuU4adsmIW6r1CvarGx--__KodzsMDr3aXCy87cPrb', null, '', '1', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('1xXMUrnmzEuLt2zpOWY', 'qZlNRmGb0t7cpTIKjLH', '邵静', '\0', '3', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5-Ecl7SkpmJSyxHtc0BJN', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb', null, '', '1', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('2CZ9zJ9sfeLsdAzElNz', 'OzdSAQBjeXp51fcn9wB', '陈兰', '\0', '3', 'RUVSgMXpStubfSUZOTn--__Y50eV6qmxDkqlFJlMaC-UeJp6PIGTT2x03ALz1D-xSjKkT064jTjQDl3h3L', 'RUVSgMXpStubfSUZOTn--__Y50eV6qmxDkqlFJlMaC-UeJp6PIGTT2x03ALz1D', 'RUVSgMXpStubfSUZOTn--__Y50eV6qmxDkqlFJlMaC', null, '', '1', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('39JwvV761wNroRLLhFQ', 'qZlNRmGb0t7cpTIKjLH', '邵静', '\0', '3', 'ItGeZBUEAlPmdg1sFiP--__7NuSLfcmZ3ILpeCe13O-I21fDotzesMy862UDkw-qBYM1kRs7uCqv3bDIn6', 'ItGeZBUEAlPmdg1sFiP--__7NuSLfcmZ3ILpeCe13O-I21fDotzesMy862UDkw', 'ItGeZBUEAlPmdg1sFiP--__7NuSLfcmZ3ILpeCe13O', null, '', '0', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('4vplN3O3HvOvnrmQdo3', 'cuUclVWNtvwD0nR9g1Z', 'spy', '\0', '3', 'ijMlM0Hwgs0WfjupIeF--__Y50eV6qmxDkqlFJlMaC-XqKPWlIH4kYmmzHmfA2-0bNCkaT0dEFK4qO9fAj', 'ijMlM0Hwgs0WfjupIeF--__Y50eV6qmxDkqlFJlMaC-XqKPWlIH4kYmmzHmfA2', 'ijMlM0Hwgs0WfjupIeF--__Y50eV6qmxDkqlFJlMaC', null, '', '0', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('8geLinUpZNj76PDYnxu', 'gWsc9qqUYEnVavXC5dM', '蔡雄威', '\0', '3', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5-Ecl7SkpmJSyxHtc0BJN', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb', null, '', '1', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('aNzPSXVXrHULWeYd7ZY', 'nSk90pVKgErBwhDxsqF', '钱辉', '\0', '3', 'q5d6SXO9k0OQR3qPGNf--__7NuSLfcmZ3ILpeCe13O-YKa163qjuajr5ArwlWb-E8TMdYQt1KynwJyxnEH', 'q5d6SXO9k0OQR3qPGNf--__7NuSLfcmZ3ILpeCe13O-YKa163qjuajr5ArwlWb', 'q5d6SXO9k0OQR3qPGNf--__7NuSLfcmZ3ILpeCe13O', null, '', '0', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('DcwcAYUmun9hXdOMBGb', 'a0yY1nD8SNLWqjpX8Qm', '测试用户', '\0', '3', 'NyZA16slCuBtL7bjLnE--__7NuSLfcmZ3ILpeCe13O-1ynoTGDaQMQjGGydRly-jATRpGHhC8CIUPPfWKN', 'NyZA16slCuBtL7bjLnE--__7NuSLfcmZ3ILpeCe13O-1ynoTGDaQMQjGGydRly', 'NyZA16slCuBtL7bjLnE--__7NuSLfcmZ3ILpeCe13O', null, '', '1', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('dmprcCqAoxowR19OWt2', 'Mxspdb6zQc7sibnXuG0', '吴继华', '\0', '3', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5-Ecl7SkpmJSyxHtc0BJN', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb', null, '', '0', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('e97jfMFusYfw2FnpOeS', 'nSk90pVKgErBwhDxsqF', '钱辉', '\0', '3', '4IFwUGhYTTkBCjDEFxK--__7NuSLfcmZ3ILpeCe13O-tCKtToXlH9o4Qh2m6Te-DubQqGi8H7dZ9skMnpe', '4IFwUGhYTTkBCjDEFxK--__7NuSLfcmZ3ILpeCe13O-tCKtToXlH9o4Qh2m6Te', '4IFwUGhYTTkBCjDEFxK--__7NuSLfcmZ3ILpeCe13O', null, '', '1', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('F5DS8L7jQJt7Ed0g1Xi', 'MfokklYeqit6DecvrOu', '吴继华1', '\0', '3', 'k7nSEkQRcT69VG9CTAl--__qKVklNXbUpNGc2yOtwK-JZCX03Un3CvQHNNB78a-nOuVJLB20AIjfOpWUG9', 'k7nSEkQRcT69VG9CTAl--__qKVklNXbUpNGc2yOtwK-JZCX03Un3CvQHNNB78a', 'k7nSEkQRcT69VG9CTAl--__qKVklNXbUpNGc2yOtwK', null, '', '1', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('fED1fKBq4z0LgaTPmlT', 'd4ExGc56rCXF28dOMdJ', '赵志成', '\0', '3', 'Fu4HtQgamPqQCULfvGt--__MPD9VyFPfx9pMhwwoeu-eKjzPKaAae1yQ8Ljn1Y-96mz6ZUI5pRqHtC4kGp', 'Fu4HtQgamPqQCULfvGt--__MPD9VyFPfx9pMhwwoeu-eKjzPKaAae1yQ8Ljn1Y', 'Fu4HtQgamPqQCULfvGt--__MPD9VyFPfx9pMhwwoeu', null, '', '0', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('fmHIlJKmZMvD60kB2eL', 'pHcrDwf94ChKcjiOoA4', '程丽君', '\0', '3', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5-Ecl7SkpmJSyxHtc0BJN', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb', null, '', '0', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('HZd1KoAkdgkp7EjIe4i', 'hnu7lc0tv0okrCNI0y0', '吕静璇', '\0', '3', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5-Ecl7SkpmJSyxHtc0BJN', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb', null, '', '0', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('IJx0DlviD8CFZGUjls9', 'MfokklYeqit6DecvrOu', '吴1', '\0', '3', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5-Ecl7SkpmJSyxHtc0BJN', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb', null, '', '1', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('JUIMw4tAq3pRuicPdga', 'QghJDaDCt5fckOMCWAe', '宗昊', '\0', '3', 'g2PHoiNm4nDFYrxKXjg--__KodzsMDr3aXCy87cPrb-VG8ZSuln3LIzjbriZLG-qvc0ryMqa1YVNtQO6Wl', 'g2PHoiNm4nDFYrxKXjg--__KodzsMDr3aXCy87cPrb-VG8ZSuln3LIzjbriZLG', 'g2PHoiNm4nDFYrxKXjg--__KodzsMDr3aXCy87cPrb', null, '', '0', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('Lw3onKLdPj4m42CORcq', 'd4ExGc56rCXF28dOMdJ', '赵志成', '\0', '3', 'w6N9qpKhzJM6EREoZji--__1HnnpsDdzfkNvwYDmRk-oTPN3BeBAnrfTfDHM5v-BzyrJ3CGjwdMGGRkkf9', 'w6N9qpKhzJM6EREoZji--__1HnnpsDdzfkNvwYDmRk-oTPN3BeBAnrfTfDHM5v', 'w6N9qpKhzJM6EREoZji--__1HnnpsDdzfkNvwYDmRk', null, '', '0', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('MhAc7acsq0glgxcgNUt', 'yEAg5BNT1NEnT5xb7dD', 'jackson', '\0', '3', 'WbRQJ5F2UgovDYi6vfM--__7NuSLfcmZ3ILpeCe13O-1p0qVm6lnca1P5EqTUP-9P20LUjWzFKhtOElUzR', 'WbRQJ5F2UgovDYi6vfM--__7NuSLfcmZ3ILpeCe13O-1p0qVm6lnca1P5EqTUP', 'WbRQJ5F2UgovDYi6vfM--__7NuSLfcmZ3ILpeCe13O', null, '', '0', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('ml3EQXfiZONWJ1QCSf1', 'QghJDaDCt5fckOMCWAe', '宗昊', '\0', '3', 'CuU4adsmIW6r1CvarGx--__KodzsMDr3aXCy87cPrb-XSyKDUVoDRISobXVOE6-Hwm274UWSDpmUVoUXXK', 'CuU4adsmIW6r1CvarGx--__KodzsMDr3aXCy87cPrb-XSyKDUVoDRISobXVOE6', 'CuU4adsmIW6r1CvarGx--__KodzsMDr3aXCy87cPrb', null, '', '0', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('MTcLVbTZQ2LYJ1NH494', '9VfzB8y5m9O1LPk5MSV', '张兴玉', '\0', '3', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5-Ecl7SkpmJSyxHtc0BJN', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb', null, '', '0', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('n0BW1GWCiyvxn18qxK5', 'hVS4izsK5fGJSNsfAdu', '姚丹', '\0', '3', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5-Ecl7SkpmJSyxHtc0BJN', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb', null, '', '0', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('NJeUuuoOPwpnjNb04mm', 'YLmZBqX3Rn2VktDTJlb', '郑宇', '\0', '3', 'SDjxRfnGdd9yHv1iRzn--__Y50eV6qmxDkqlFJlMaC-imeXmxTechHbDpiFSJk-Jf3HFeTveymaQjTq2hs', 'SDjxRfnGdd9yHv1iRzn--__Y50eV6qmxDkqlFJlMaC-imeXmxTechHbDpiFSJk', 'SDjxRfnGdd9yHv1iRzn--__Y50eV6qmxDkqlFJlMaC', null, '', '1', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('OGi7yt18AEjq8JnQm8a', 'Q3vCs63qVu2jKeu95ov', '周总', '\0', '3', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5-Ecl7SkpmJSyxHtc0BJN', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb', null, '', '0', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('OtUfU0lHbLKmspv4Dyc', 'u2V4sCrNsyHzo4GA6RE', '潘全心', '\0', '3', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5-Ecl7SkpmJSyxHtc0BJN', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb', null, '', '1', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('pbkYvpqxLjVSHnjXuTw', 'nSk90pVKgErBwhDxsqF', '钱辉', '\0', '3', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5-Ecl7SkpmJSyxHtc0BJN', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb', null, '', '0', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('pEw8I7M5HB2k7TyYRdO', 'a0yY1nD8SNLWqjpX8Qm', '测试用户', '\0', '3', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5-Ecl7SkpmJSyxHtc0BJN', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb', null, '', '0', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('Q0fW4LUSfHzkushNYiY', '2gEdMmAHfNTatT2zE2t', '齐若凡', '\0', '3', 'IX6zn9mJYQlIKk0fykd--__KodzsMDr3aXCy87cPrb-hVApS1c9K9gO1oDkR6p-eV4Om1dbhJ5jwK6hnUz', 'IX6zn9mJYQlIKk0fykd--__KodzsMDr3aXCy87cPrb-hVApS1c9K9gO1oDkR6p', 'IX6zn9mJYQlIKk0fykd--__KodzsMDr3aXCy87cPrb', null, '', '0', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('qup6sN5BtMbvtHNDJNw', 'cuUclVWNtvwD0nR9g1Z', 'spy', '\0', '3', 'YxdApAZldMHInaPz84x--__7NuSLfcmZ3ILpeCe13O-Wt7TWlNUmJ4b1POj6zG-eA7W3vi6As9s2XWuA0S', 'YxdApAZldMHInaPz84x--__7NuSLfcmZ3ILpeCe13O-Wt7TWlNUmJ4b1POj6zG', 'YxdApAZldMHInaPz84x--__7NuSLfcmZ3ILpeCe13O', null, '', '0', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('rBuT8mvCM1TyvEIWXAT', 'hVS4izsK5fGJSNsfAdu', '姚丹', '\0', '3', '7Q0G8h9g587tohUGy0z--__7NuSLfcmZ3ILpeCe13O-1S7qeEgsRGpRsH396O8-QBiN71KJr0hZp029jJT', '7Q0G8h9g587tohUGy0z--__7NuSLfcmZ3ILpeCe13O-1S7qeEgsRGpRsH396O8', '7Q0G8h9g587tohUGy0z--__7NuSLfcmZ3ILpeCe13O', null, '', '0', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('S3TAMMi7A9IekX13sab', 'taKta4DiKreB5mIj9Y5', '齐若凡', '\0', '3', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5-Ecl7SkpmJSyxHtc0BJN', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb', null, '', '0', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('tAQr1ut4MKbM13CgMud', 'HZo6ahymlIbSQhIOxyl', '程丽君', '\0', '3', 'CuU4adsmIW6r1CvarGx--__KodzsMDr3aXCy87cPrb-XSyKDUVoDRISobXVOE6-Hwm274UWSDpmUVoUXXK', 'CuU4adsmIW6r1CvarGx--__KodzsMDr3aXCy87cPrb-XSyKDUVoDRISobXVOE6', 'CuU4adsmIW6r1CvarGx--__KodzsMDr3aXCy87cPrb', null, '', '0', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('TO5LtMMlsEemcwWPRFb', 'RzJ3os3MIzsnlZIH8O9', '杨丽君', '\0', '3', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5-Ecl7SkpmJSyxHtc0BJN', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb', null, '', '0', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('tpao6E6TOBUpYiiRgvT', 'd4ExGc56rCXF28dOMdJ', '赵志成', '\0', '3', 'CuU4adsmIW6r1CvarGx--__KodzsMDr3aXCy87cPrb-XSyKDUVoDRISobXVOE6-Hwm274UWSDpmUVoUXXK', 'CuU4adsmIW6r1CvarGx--__KodzsMDr3aXCy87cPrb-XSyKDUVoDRISobXVOE6', 'CuU4adsmIW6r1CvarGx--__KodzsMDr3aXCy87cPrb', null, '', '0', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('UUWEIIpGmwe7JLxBdXh', 'NYQ0MdM0pVveVw5vXFi', '财务人员', '\0', '3', 'Q8RiogxPys5m3NJXLbq--__7NuSLfcmZ3ILpeCe13O-CHTI0PdZVdbj9TjAiyO-Yn9Idqm2SuPSA70rKz8', 'Q8RiogxPys5m3NJXLbq--__7NuSLfcmZ3ILpeCe13O-CHTI0PdZVdbj9TjAiyO', 'Q8RiogxPys5m3NJXLbq--__7NuSLfcmZ3ILpeCe13O', null, '', '0', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('vs47UJhnNkS8Tfxr4dr', '0VFlDyTyT0gylwIP52Z', 'shaw', '\0', '3', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5-Ecl7SkpmJSyxHtc0BJN', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb', null, '', '1', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('VxjfwHcH3bUI44RSOHu', 'C3So685eOeJJUnhIZLi', '万宝蔚', '\0', '3', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5-Ecl7SkpmJSyxHtc0BJN', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb', null, '', '0', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('W43duHHQjB6uFyNjsrS', 'Xh0ZcGnssCAsSdHz8RQ', '杨总', '\0', '3', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5-Ecl7SkpmJSyxHtc0BJN', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb-sZ2n81FivWooVRuGQf5', 'OhOYt9UILqolpyTgvs2--__KodzsMDr3aXCy87cPrb', null, '', '0', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('WNzwfgnieHr9IMeJyqt', 'YLmZBqX3Rn2VktDTJlb', '郑宇', '\0', '3', 'TtJdRWX4CbBE1EEsCJF--__Y50eV6qmxDkqlFJlMaC-gmzR35lmYgEamJXh2JD-47kdAxgzuCf6VbUnN4A', 'TtJdRWX4CbBE1EEsCJF--__Y50eV6qmxDkqlFJlMaC-gmzR35lmYgEamJXh2JD', 'TtJdRWX4CbBE1EEsCJF--__Y50eV6qmxDkqlFJlMaC', null, '', '1', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);
INSERT INTO `t_actorrt` VALUES ('wpD3uqXnweHDJzMbSPe', '2gEdMmAHfNTatT2zE2t', '齐若凡', '\0', '3', 'kuQiq3yW1h1p5sJDSYr--__7NuSLfcmZ3ILpeCe13O-cd8MkiLwqO3svmAu3v5-FePpKZ7CcSbWAPzbQ4C', 'kuQiq3yW1h1p5sJDSYr--__7NuSLfcmZ3ILpeCe13O-cd8MkiLwqO3svmAu3v5', 'kuQiq3yW1h1p5sJDSYr--__7NuSLfcmZ3ILpeCe13O', null, '', '0', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '0', '0', null);

-- ----------------------------
-- Table structure for t_checkin
-- ----------------------------
DROP TABLE IF EXISTS `t_checkin`;
CREATE TABLE `t_checkin` (
  `ID` varchar(100) NOT NULL,
  `WIDGET_ID` varchar(100) DEFAULT NULL,
  `APPLICATION_ID` varchar(100) DEFAULT NULL,
  `DOMAIN_ID` varchar(100) DEFAULT NULL,
  `USER_ID` varchar(100) DEFAULT NULL,
  `USER_NAME` varchar(100) DEFAULT NULL,
  `CHECKIN_TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_checkin
-- ----------------------------

-- ----------------------------
-- Table structure for t_circulator
-- ----------------------------
DROP TABLE IF EXISTS `t_circulator`;
CREATE TABLE `t_circulator` (
  `ID` varchar(100) NOT NULL,
  `NAME` varchar(100) DEFAULT NULL,
  `USERID` varchar(100) DEFAULT NULL,
  `DOC_ID` varchar(100) DEFAULT NULL,
  `NODERT_ID` varchar(100) DEFAULT NULL,
  `FLOWSTATERT_ID` varchar(100) DEFAULT NULL,
  `CCTIME` datetime DEFAULT NULL,
  `READTIME` datetime DEFAULT NULL,
  `DEADLINE` datetime DEFAULT NULL,
  `ISREAD` int(11) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `SUMMARY` longtext,
  `VERSION` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_circulator
-- ----------------------------

-- ----------------------------
-- Table structure for t_coactorrt
-- ----------------------------
DROP TABLE IF EXISTS `t_coactorrt`;
CREATE TABLE `t_coactorrt` (
  `ID` varchar(100) NOT NULL,
  `NAME` varchar(100) DEFAULT NULL,
  `USERID` varchar(100) DEFAULT NULL,
  `DOC_ID` varchar(100) DEFAULT NULL,
  `NODERT_ID` varchar(100) DEFAULT NULL,
  `FLOWSTATERT_ID` varchar(100) DEFAULT NULL,
  `CREATE_TIME` datetime DEFAULT NULL,
  `READTIME` datetime DEFAULT NULL,
  `DEADLINE` datetime DEFAULT NULL,
  `ISREAD` int(11) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `VERSION` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_coactorrt
-- ----------------------------

-- ----------------------------
-- Table structure for t_comment
-- ----------------------------
DROP TABLE IF EXISTS `t_comment`;
CREATE TABLE `t_comment` (
  `ID` varchar(100) NOT NULL,
  `FLAG` varchar(100) DEFAULT NULL,
  `APPLICATION_ID` varchar(100) DEFAULT NULL,
  `DOMAIN_ID` varchar(100) DEFAULT NULL,
  `USER_ID` varchar(100) DEFAULT NULL,
  `USER_NAME` varchar(100) DEFAULT NULL,
  `COMMENTS` longtext,
  `UNLIKE_NUM` int(11) DEFAULT NULL,
  `LIKE_NUM` int(11) DEFAULT NULL,
  `CREATE_DATE` datetime DEFAULT NULL,
  `PARENT_ID` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_comment
-- ----------------------------

-- ----------------------------
-- Table structure for t_counter
-- ----------------------------
DROP TABLE IF EXISTS `t_counter`;
CREATE TABLE `t_counter` (
  `ID` varchar(100) NOT NULL,
  `COUNTER` int(11) DEFAULT NULL,
  `NAME` varchar(100) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_counter
-- ----------------------------
INSERT INTO `t_counter` VALUES ('03JguILaW92OlmvL3vH', '1', 'FC230823', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('08hWV1EtHoHXQKshDtp', '2', 'HZ231106', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('0APC9mvSA4b3taRYYrF', '1', 'SR231015', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('0ODGOjID6kc6Xc9y4J2', '18', 'ZC230824', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('0ofUAYwifZ14i6OYtYs', '1', 'ZC231031', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('0PeQWvSDlMhrfXB1eVq', '1', 'XDF230905', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('0UHlXRCUvHSeAdmkryW', '4', 'XDF230829', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('0VgNFPBYSpUprFCvVzB', '2', '2023-XMT-IPTV-MTDS-', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('1Fxw8aolEBu2LVurbCi', '1', 'ZC230910', '__M4oRwEVzSB1oI1D0faN', 'KXsU3iF6YPBxYPWpRLa');
INSERT INTO `t_counter` VALUES ('1g2viKBmreeYMq9aV8r', '3', 'XM230822', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('1NuNxLSkTXNBZPyoASn', '10', 'ZC230830', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('1qfoXSdyl2h61d9hKoN', '1', 'FC231007', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('1RYUWkijp81f5P7Feq5', '6', 'SR231108', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('1yoWnjEw3TCNjgfhFzw', '2', 'FP230906', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('1z7TXKhxjIrqWEibJI8', '1', 'ZC230825', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('21ylthgQNrTrdxmJLml', '24', 'FK231107', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('29luwDnn6WzGshAPNB2', '4', 'ZC230829', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('3rx4O8pLASlKwDenp7b', '2', 'FC230830', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('3Tr9fEJqzlOMeVglx0e', '3', 'ZC230830', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('3vHA6zNOHAYnkmIJSiC', '4', 'XDF230825', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('3XQWcsJj87i9EgJMJdk', '1', 'XM230926', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('4jlsnk2ac5OOj8NXwLk', '1', 'QT231109', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('4pc274F61QLSNDm4KlA', '15', 'FK231108', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('52WzvloRFAHOe1kqNez', '4', 'ZC230910', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('5chRPWB5iMUjKlHV6HF', '2', 'ZC230821', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('5OxvRou0VIeSOnX0Tfq', '6', 'SR230907', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('5Tk02mr85iTyCWXJnbk', '3', 'XDF231109', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('5U9gU3WNhmmsI3t0Cn3', '1', 'FC231016', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('5Zd64GFjB0Y3o7XsUy9', '1', 'QT231107', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('6cJOAfmWiAfkR9Mdl5j', '4', 'SR230830', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('6pgeRoQ57zcnK04DtOI', '2', 'SR230815', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('6rzPdR5oeXI1t2IjaFv', '1', 'FK230906', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('6VVAqZDdAHyZHFi0dfv', '3', 'QT231106', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('6zUmXjzk4L8xfVsVQl4', '1', 'ZC230821', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('7nv8m8ge0BK7SK1zRQs', '1', 'HZ230829', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('83IeDgndfRwlOMoGIyM', '6', 'SR231016', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('8cccgS3dcyyCNfcAf7P', '2', 'SR231017', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('8g5WKBi9RweQE8FO5kA', '3', 'QT231017', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('8nP3613J1l5wBqtMKZx', '1', 'FC231031', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('8VWa51AohpDQbZncNuT', '1', 'QT231026', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('8ysTWZfoaih5WCGmYD8', '1', 'SR230830', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('9dsFKwDOXEQ1Gt6KcKL', '5', 'ZC231023', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('9GoWpBME3FtDg4KQPdp', '2', 'HZ230910', '__M4oRwEVzSB1oI1D0faN', 'KXsU3iF6YPBxYPWpRLa');
INSERT INTO `t_counter` VALUES ('9H8alWF4opftElQEvi8', '3', 'SR231009', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('9hv3aIcbgpgBg57Gt0D', '1', 'FC230815', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('9qQIQK1ZyzDtYitUomE', '4', 'FK231024', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('9rT1g5ogZrUv0Oc6w3M', '1', 'XDF231106', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('A0TcXImelU0fzO9UsLG', '4', 'HZ231108', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('a13hrAaYvspjqoNdFjI', '1', 'SR231114', '__M4oRwEVzSB1oI1D0faN', '1i9X1xRMM6BRD9GMjj9');
INSERT INTO `t_counter` VALUES ('aC6xXoWI1ziyyTmkPk2', '1', 'SR230901', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('ae9ERao7j61p4ma54iC', '7', 'SR231007', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('aj9XMKOIshmv8EPKgiG', '1', 'ZC230921', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('Al3MxTYAczkvQIzZOha', '4', 'ZC231109', '__M4oRwEVzSB1oI1D0faN', 'KXsU3iF6YPBxYPWpRLa');
INSERT INTO `t_counter` VALUES ('AMaAlHk4uyHtxRrJEAj', '1', 'FK231109', '__M4oRwEVzSB1oI1D0faN', 'KXsU3iF6YPBxYPWpRLa');
INSERT INTO `t_counter` VALUES ('AnEO9F1dQewDn8SNDW0', '2', 'ZC231015', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('aqhvzozYI37puplTAOD', '1', 'HZ230926', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('aYwkewzbWOaWp59RZqe', '3', 'SR230817', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('BiqGL6mmlcCn61Sdw3S', '1', 'HZ230901', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('BlkWCQYkGWrKS0sVa23', '4', 'ZC231026', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('BOOZaT5OFCjE0bQvcT0', '1', 'QT230910', '__M4oRwEVzSB1oI1D0faN', 'KXsU3iF6YPBxYPWpRLa');
INSERT INTO `t_counter` VALUES ('bu0L8qc4i8u70prbzHE', '24', 'ZC231108', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('c2yNVHHqyyYfpjaWcpd', '8', 'SR230828', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('c50WQLvzC6Ne3ALkxVh', '1', 'QT230828', '__M4oRwEVzSB1oI1D0faN', 'KXsU3iF6YPBxYPWpRLa');
INSERT INTO `t_counter` VALUES ('cAWGY0yITNmNTIAzNeF', '6', 'ZC230828', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('cDLcA6fk8Gbhl2UjCqI', '2', 'XM230824', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('cdvZy5MomFrjaoif4xM', '1', 'SR230926', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('CeBZZQ17Nb8pIGELqix', '1', 'ZC231115', '__M4oRwEVzSB1oI1D0faN', '1i9X1xRMM6BRD9GMjj9');
INSERT INTO `t_counter` VALUES ('ChdfqEE9cx3bHZBCgvC', '4', 'QT231108', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('CJrz9Fmn6ZhZHOHeVg8', '2', 'FK230825', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('CpgpSdY2gkDeSxr4Ndq', '1', 'QT230908', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('cQ4hTK5EDQU91mvLIPd', '4', 'ZC230823', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('cTrZwnykY5ekEmNgBYX', '3', 'XM230828', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('cvZ8IfBkoZ6uu4HZnLn', '3', 'XDF231109', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('cWpy5sUW4s8PvsGuNzs', '1', 'XM231108', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('czSUrxdxKoEvPmYaGwZ', '2', 'FK230907', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('d4q5MlyEan4RR2Z1V1i', '2', 'XDF231009', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('DCQTtbiH1FYIsNF81BZ', '3', 'XM231007', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('dLNgC7WxeKm8UG0zXBj', '5', 'HZ230824', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('Dmy3xxNoR9Pwkm5aqwn', '1', 'SR231110', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('dnK3EYPBWN2knUyJFAq', '1', 'FP230831', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('DvdTvT6sCqSQ8NI9Jyf', '7', 'FP231017', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('dYtpDZtIrqLgvKOdQdG', '1', 'FP230821', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('e2L3CfMJTqMZVU35zCD', '1', 'HZ230825', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('e35RqNHFmKBYseVCErd', '2', 'SR231031', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('e6eGNRsqPpArOSkW3x3', '4', 'ZC231017', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('EE0zxrKzXORmaLf7ZET', '1', 'ZC231102', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('EeZ9N5U7CBOVvULrNCB', '1', 'SR230824', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('EflMWwPyEmKutRozYIl', '1', 'FC230913', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('eJgCm3TZXJP0Haf3BOl', '4', 'XM230830', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('EKQ96LEa2g8lvCkA3ke', '1', 'HZ230825', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('ELnBNs6lm03G7An4RrR', '6', 'FK231018', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('eLVUOCwdxMth1jF9gwY', '1', '2023-BJXMT-CWFKB-CW-', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('eMLlv3CaPX8EVgA0mQF', '3', 'ZC230821', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('eSKZI3j4HHrOXCLB9YD', '1', 'XM230901', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('etUhm8hsRaVnlZTvkdW', '1', 'XDF231008', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('EvPDMGRBO8E7wRYcsAd', '6', 'QT230828', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('F0tnr0JC41LtDklTyDI', '3', 'QT230907', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('f7mnFyWsptpyR8HvZMc', '1', 'ZC230824', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('FaGCKverOZovM50NSGI', '1', 'FP231024', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('FaRdYgHlMJMuZ6ETu6j', '4', 'ZC231020', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('fb2I02R8YHix4yUldNQ', '2', '2023-BJXMT-IPTVSYFZZX-NRCPB-', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('FE9JGZkebhqzm4QCU3E', '1', 'FK231109', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('fgmrGA1mcBqtgHl0glN', '1', 'SR230818', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('FmfelY4gyOchFOL6Wix', '4', 'XM230825', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('fn16BCwiTCn7Y1SnbIb', '2', 'XDF231108', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('fOBGBTsTJgySToSzvCz', '6', '2023-BJSJ-JSAQZX-NRAQ-', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('fQl4i90kjAOlrBNoono', '1', 'HZ231031', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('ftIg70l3qNnpPvBgq0H', '1', 'ZC230821', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('FVQlSVpOeWIkTuLIajp', '1', 'SR230831', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('fXImFN8jH2BqOoci2ex', '1', 'ZC231016', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('g15tjWsskGBl0OFk5gr', '11', '2023-XMT-IPTV-YYTZ-', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('g4FEtrUqaGLtGZGfHgZ', '1', 'HZ230823', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('G5cP2NHGmCnIb78UuMc', '1', 'XM230830', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('g9FZJc1ps8x1UjRDsOO', '1', 'ZC230926', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('gaUPsLdvOeenguX6lOT', '1', 'ZC230821', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('Gh0fE5Y0pt0jT7HYhz7', '1', 'FC230818', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('GM8sZMiJ9ztiwR2hbGS', '1', 'ZC230919', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('GoBlexhToQxnD3jvd9n', '1', 'QT231009', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('gPVM39FeInlqyOELDXv', '10', 'FP231108', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('GTELGPVDhvmw2PShwZ7', '2', 'ZC230830', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('gTYJGwuycNKiy1Veq0s', '1', 'HZ230818', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('gy9gGzkUz9JvaiIDHA2', '4', 'ZC230831', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('H0BNr2Lp4b0Xdnm0BLx', '1', 'ZC231113', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('h17KIoiZ2i9cqIzGYoh', '11', 'ZC231016', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('h6yeJIKkGbYqrCrTQZ1', '1', 'FK231016', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('HBKEu5bXnfBcUu5pQOU', '3', 'FC230817', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('he9TPmi5rOEqG9vqWMv', '2', 'ZC230821', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('HFA5UdcNeiik0EtS3sT', '16', 'ZC231015', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('HiaQgj5k9PiSLqz59cS', '1', 'XDF230818', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('hlGdIdh5qikDsUbdq7B', '2', 'FK231108', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('hmI8nWssdi72iJJ6J9A', '4', 'FK231023', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('ho0A1S50sYN9FdYQ82F', '1', 'HZ230830', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('hSn1hmv3Hr8Jgoub9NJ', '1', 'FC230825', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('HtjLGAemtxj21fLLCsR', '4', 'SR231106', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('i0HmvHQjLtioSv0xxxn', '1', 'ZC231016', '__M4oRwEVzSB1oI1D0faN', 'KXsU3iF6YPBxYPWpRLa');
INSERT INTO `t_counter` VALUES ('IeHjoDs9LSTd2ZyJzbc', '1', 'HZ231009', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('iFoke4mXRSJqYYWJ9wp', '2', 'FP231026', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('IK2JqA4vRjqB5umwFsH', '2', 'ZC231018', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('iQI4WmbUgl5RSmFlDJG', '2', 'FK230828', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('IRIhXaO7WsJYVIivoTo', '2', 'ZC230830', '__M4oRwEVzSB1oI1D0faN', 'KXsU3iF6YPBxYPWpRLa');
INSERT INTO `t_counter` VALUES ('IrXZR99lFBhT7KitBF2', '2', 'SR230925', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('ivcCDHTJ16iIiZhSpB8', '2', 'FC230908', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('J1uf19osJQFe98MC9Wm', '1', 'XDF230831', '__M4oRwEVzSB1oI1D0faN', 'KXsU3iF6YPBxYPWpRLa');
INSERT INTO `t_counter` VALUES ('j6KH1uhEzdmN4lTl6GC', '3', 'SR231008', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('jchGkTtkozhG4rEW38n', '1', 'XM231009', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('JfmlcQne1iauASZ4r6Q', '1', 'HZ230908', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('jIQffvwt6kynmxGXsRL', '1', 'SR230831', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('JjTFiKLel2DHKtiRU4u', '1', 'QT231019', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('JQTddiqT45nlZK4Qeo4', '2', 'SR230908', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('JtbgulSN3l5DwLd4coy', '1', 'ZC230825', '__M4oRwEVzSB1oI1D0faN', 'NINmvXAxlG8RS8Bwwll');
INSERT INTO `t_counter` VALUES ('jUQp6m8gC0z6hpy1erQ', '1', 'BA230829', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('jWkpH9CPKmGLaeSd9LY', '2', 'XM230831', '__M4oRwEVzSB1oI1D0faN', 'KXsU3iF6YPBxYPWpRLa');
INSERT INTO `t_counter` VALUES ('JxKHx9USxv1g2B1w7iQ', '2', 'XDF231115', '__M4oRwEVzSB1oI1D0faN', '1i9X1xRMM6BRD9GMjj9');
INSERT INTO `t_counter` VALUES ('JXXcoB3F4OLk3wXVgpJ', '2', 'ZC230908', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('KAC1tDPkqewijUFugmN', '1', 'SR230829', '__M4oRwEVzSB1oI1D0faN', 'KXsU3iF6YPBxYPWpRLa');
INSERT INTO `t_counter` VALUES ('KcgQKnrmTtj8tZOSOQ4', '2', 'ZC230913', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('kd3YsbiHECd9pueMbN4', '1', 'QT230815', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('Kd41O7zVJMI0j9arhqv', '31', 'ZC230828', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('kGtFeBsUpxxw0TVl6Wk', '2', 'FP231031', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('KHc8smGZD6lOdYPXwi7', '1', 'SR231031', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('KLWEAAabvvioT4NNidi', '2', 'FC231008', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('KlYiFSCRwb4l2W0JS7B', '1', 'SR230824', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('knIYDVnI7IH7W0D9j21', '3', 'ZC230914', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('KRXipoSrIqHtTTdp3VS', '15', '2023-XMT-CWFK-FW-', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('ktmFpAYLwLgmEMQgxho', '2', 'ZC231010', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('KU1hqjagkLaw2VFH7LR', '1', 'SR231007', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('L0czedzGgFJ3eK5mmBb', '2', 'ZC230925', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('l4o3F6XMEr4PtGS7Tbl', '6', 'SR230824', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('lB1lrvJln6o5zNXsPeN', '1', 'ZC231026', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('LFJJa5S4SXH9IvYH1JL', '1', 'QT230901', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('LGX3gCC7EdXuraFFqoc', '1', 'FC230816', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('lK2DYNmhTFZIFCOOQOa', '1', 'FP231109', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('lludb6rblvgvPQEFlt3', '1', 'SR230821', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('LM1E6L1w2kBwgpEkl4L', '1', 'ZC231103', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('lng7fhdwYNsZS2btDNz', '1', 'FC230926', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('lO13zhMK70mNQV71RuA', '1', 'XM230907', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('lQNeU03pLn0vpen4Pwz', '1', 'HZ231109', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('lQUwCwhRiRVphbiCkER', '3', 'FC231009', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('lVAtJwQaMC8wTsftm15', '6', 'FC231019', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('lWDG0ol009U7jp40YqE', '1', 'ZC231008', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('lwkYLUzrdVROSBTPXkb', '1', 'XDF231020', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('lzxZtLEJEFOKo9PKdrs', '5', 'XDF231010', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('M9kCIMr1Mo78VXGbXo9', '25', 'ZC231106', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('mbcmPTZlOjSjZC9986h', '2', 'SR230822', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('mC4vAKezRrX3MT9dqO1', '2', 'FP230828', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('mHS0pjiDxZqE4rZpa6J', '2', 'FK231016', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('mkEZwP2wun7k1kzDqXP', '10', 'ZC230905', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('mN7dNrqU6jIqKzl03Oy', '2', 'QT230825', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('MOzhPrPiqeItFt5MaOH', '4', '2023-BJY-YW-', '__M4oRwEVzSB1oI1D0faN', 'KXsU3iF6YPBxYPWpRLa');
INSERT INTO `t_counter` VALUES ('MpOgNWOptGy9ABgplp0', '1', 'XDF230824', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('mrNydnzkbBSTqsCFimf', '1', 'FP230821', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('MwtjCsEHakF7M6OB2Wj', '1', 'XDF230919', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('Myg7BWD84Ah0LHyEQMt', '1', 'XDF230825', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('n2sOWOZbZmJHkf8qkmc', '2', 'SR231109', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('N3F1EtFEWYUnNiU6vfi', '4', 'ZC231007', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('n3rNTJAklEtlFm34jzl', '1', 'SR230920', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('N5EHsoPfYZMOV8wkav4', '1', 'ZC230818', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('NgyRHUi8m4tO2rWjk8M', '10', 'XDF231007', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('Nh5U4ev6ht0AKdhkZAR', '1', 'QT230824', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('nhExdKjvCXPsAphMbgW', '1', 'FC231026', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('njFwyeKK84C4P0ELhU9', '1', 'QT230831', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('NTFjMMTDjsBIsatQYdB', '1', 'XDF230824', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('Nu41phg5wzIDFIdPN3b', '1', 'FC231114', '__M4oRwEVzSB1oI1D0faN', '1i9X1xRMM6BRD9GMjj9');
INSERT INTO `t_counter` VALUES ('NY4xf2YS3fCY3oUFCFE', '3', 'FC231109', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('O04pVuotMDUYncO4QRf', '1', 'XDF230816', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('o48nwtK3ihp5hBlo9CH', '1', 'ZC230824', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('o9orDZOAzszrW9Aa2jN', '2', '2023-BJSJ-NRZX-DXHDGZS-', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('oFjhkQAXKKXNVALfOC4', '2', 'ZC230831', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('oKBjwGtlR7EbVGT6rzA', '4', 'ZC230816', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('OKfFGbrx0jVo2an73PO', '23', 'ZC230825', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('OlmHjOwktUAwhw5dtPN', '7', 'ZC230815', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('OOxyspbITKErsLmgdAF', '2', 'ZC231031', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('OP6AE8Vd1MYwLMLMw7w', '1', 'XDF230908', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('Oq6G7ykcyYiv4Fxvjli', '3', 'FC230907', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('OtXoxmivU8Jnx10xlox', '1', 'XM231020', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('ow0S5gC2zrutKX0G7eC', '7', 'ZC230825', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('oXYGiJ2HGmbgE5oWBsU', '7', 'QT230824', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('PaWXkhv56U6fWG75jZE', '12', 'ZC230822', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('pcM6ocN3JPG1o2mj9bc', '3', 'ZC230821', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('pe2rimRnxAk3hG5gEv7', '1', 'SR230831', '__M4oRwEVzSB1oI1D0faN', 'KXsU3iF6YPBxYPWpRLa');
INSERT INTO `t_counter` VALUES ('pfcqFZtNc4FpxE7BQ5L', '7', 'XM230828', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('PiyMg3aYdL9R2wEEdz2', '1', 'QT230823', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('PLbfbj3rnI4kwSSY5yj', '2', 'SR231008', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('POWKYQQLs8g823ItXFB', '1', 'QT230816', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('Pr5FFozPGFnw5KFKHWD', '1', 'ZC231102', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('Pr7r1E7Mz8OldnjETMq', '3', 'XDF230830', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('PtnIKgkvpc86QW2JUWo', '2', 'SR230829', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('PU6kd96A20s7oY7U5Ux', '4', 'FC231108', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('q0HN82ESpry1C3spIg6', '1', 'SR230913', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('Q16zmzobwFH9F43m5Ax', '5', 'BA230828', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('q1CQ3L8fMJQo1deHahP', '1', 'SR231109', '__M4oRwEVzSB1oI1D0faN', 'KXsU3iF6YPBxYPWpRLa');
INSERT INTO `t_counter` VALUES ('QA9I68r46ccpghnI72r', '2', 'FC230822', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('QbF8esgVFQmFMrYwtAb', '2', 'SR230825', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('QDuVGKtO3C8WhXRv3gH', '1', 'ZC230922', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('QiNKzy9BT3W95CbfMOm', '1', 'QT230830', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('QO3nU0emBylIVSem4Qs', '1', 'SR231108', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('qOimXZd2nkHEqCDd1WE', '3', '2023-XMT-CWFK-CW-', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('qpW4eGWgA4BmcNbaxvb', '1', 'XDF231031', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('r4StViE9332oy1x3lrM', '5', 'FK231106', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('RaN5POZjzLOTExZy65O', '1', 'FC231031', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('rgvcoDmjPK3SKAskCcf', '4', 'ZC231019', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('rivqPoXnPmhdl8KpslZ', '2', 'FK230829', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('RNGRzNf5QAFjI2XqrqN', '23', 'ZC231108', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('rqPWy1kIQ4XZrTMRam0', '1', 'XM230818', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('RqW9jAdwhQovXtgABRH', '6', 'ZC230825', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('RV55K6P1nKWr808DFsF', '6', 'FP231018', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('RXFjeDwRfA3YW1KZsY9', '5', 'FC230824', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('S6QgLgYkzyrQJH4z7eM', '6', 'ZC230817', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('shheOdJyLiILMhe2gVG', '7', 'XM230829', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('sO6K4xod4IC9eQLIUPG', '1', 'HZ231109', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('StrEqCbybb0TYCQC18X', '1', 'QT230901', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('SZ0EoQvFBRkRdwvzMIQ', '2', 'XDF231109', '__M4oRwEVzSB1oI1D0faN', 'KXsU3iF6YPBxYPWpRLa');
INSERT INTO `t_counter` VALUES ('sZMaHM5UWAiohoV7Ug4', '3', 'ZC230901', '__M4oRwEVzSB1oI1D0faN', 'KXsU3iF6YPBxYPWpRLa');
INSERT INTO `t_counter` VALUES ('T1oZHK136DdjgdfPgm6', '1', '2023-BJSJ-SYHZX-BQ-', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('TIHfNbf6yW8zr2GVRQp', '1', 'SR230910', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('tPpKZ6OWelPuA2QZOlj', '3', 'HZ230817', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('Tqgu0oefd26Q1rUemZu', '2', 'SR231024', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('ttFrwA0jTyzjV9bmtiz', '6', 'ZC231109', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('Tuiqg5jwuWQEgMeouax', '2', 'ZC231007', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('tzBZRpxRZbqI8qGzhCu', '2', 'QT230825', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('UEzyPirybGQ6cxgonqH', '2', 'ZC231012', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('UJCF06mysbH6Sl20kpu', '1', 'HZ230816', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('UJxaT6CjPFOqiSRC35H', '4', 'SR230825', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('uMAMZWTka8rslO6iEwz', '2', 'ZC231025', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('usQOEn0QWNU1vdMwI8B', '2', 'SR230823', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('UsrO4k7hljCfsy3ag4I', '1', 'XDF230828', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('UtyWevXJ5aKjn4nKifI', '1', 'FC231013', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('v2vJGl0cmMXwGDTtlC4', '1', 'FP231109', '__M4oRwEVzSB1oI1D0faN', 'KXsU3iF6YPBxYPWpRLa');
INSERT INTO `t_counter` VALUES ('v8c3Gt8auBeTnJUmTUV', '2', 'XM230830', '__M4oRwEVzSB1oI1D0faN', 'KXsU3iF6YPBxYPWpRLa');
INSERT INTO `t_counter` VALUES ('V8iBw2vFOkzYyou4yoe', '1', 'XDF230830', '__M4oRwEVzSB1oI1D0faN', 'KXsU3iF6YPBxYPWpRLa');
INSERT INTO `t_counter` VALUES ('v96ifRFme7flhqLePPC', '22', 'ZC230824', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('va3rLeh5h0YNZJ2HsIE', '17', 'ZC231110', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('VCOtvqIOd4rLsYoENWh', '5', 'ZC230824', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('vCoV7xvlMqnHHmo5wcB', '2', 'FP231109', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('vdztZWGifYJlnoiwMT5', '3', 'HZ230907', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('vJBvynEs5Lw5TQacXcu', '5', 'FC230828', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('VjqjYh2CA2Nd5GP3Dj2', '1', 'ZC231008', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('vKgOSsFKJePsMmN3Z0P', '4', 'QT230829', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('Vn5WdjVwpIOmQlIunbQ', '6', 'ZC230829', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('VriaAVEojIjvosfJNcf', '1', 'XDF230907', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('VtjsOVb5Xu7ONPbGG1d', '1', 'QT231031', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('vTMTF2REh5iAgxQ80Gz', '1', 'FC230901', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('VV529potHr123pUQ6ZJ', '1', 'BA230825', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('vxAHu3iWXYlQpRozFxg', '2', 'FC231106', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('VxqsnqYpVPq96LPKWGC', '3', 'ZC231024', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('VYSpSpYFFT3jYei9Ez8', '2', 'SR230816', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('W39lZhSWl3kx3YuuBV7', '3', 'SR230830', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('WbyYS7buTfMYao5LsyU', '3', 'XDF231108', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('WcveoAzzJKsmzwA6vXe', '1', 'SR230828', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('WLAWFMljrrUEbuwvDUH', '1', 'ZC230816', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('wPNe7g1nIOVrrmpNcyM', '7', 'XDF230824', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('wt6a0p088toZGEloA1e', '1', 'ZC231012', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('wUScgdKmtuCx9oDoQlC', '5', 'SR231019', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('WxUCRnXMUBY1Ev0SlAK', '1', 'HZ231019', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('wy3c62d3rjTmPpcD0o2', '4', 'XDF231008', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('x2OvIl4952hf4Xi1WjR', '4', 'ZC230824', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('xE1UZtuvQNMWIQMdSTg', '1', 'ZC230906', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('xEBKt8PZHBUTRofYAcP', '3', 'QT230817', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('xIFZo8V0Z4pmjz1aNCo', '1', 'XM231008', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('XJWRmkUrHmHx29BtdpH', '17', 'ZC231109', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('XKK1tv0YbcjMZHzpuSh', '1', 'HZ231026', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('xMtM1njggtdwcGcuDsR', '1', 'XM230824', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('XNgFlmyW3w7hjz4eLhQ', '1', 'ZC230911', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('XnTdOXA9tPXx0xKnLRD', '1', 'XDF231010', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('XRJNdsZPaDXngCuamnM', '2', 'HZ231017', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('xroaWQ8hJL2qaX4ZZf9', '4', 'SR231109', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('xZg273O7KXqqdSNSrc0', '1', '2023-BJSJ-', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('y7kJuybBmRAp8UWBzKw', '12', 'XDF231007', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('Y9x4sNPy7Fnh4POur8k', '2', 'FP231019', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('YaTu14VPmYUup1SHibG', '1', 'SR230821', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('yB6PDGlVrXjWGizqi6Q', '1', 'FC231109', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('yCwHKIkYT8vmvDYDqKr', '3', 'QT230822', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('YfNuk4dVKNF6w3C4lZr', '1', 'HZ230822', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('ygVIgbVCzyoqry3Dif8', '1', 'FC230825', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('YJN5OdJnE0RwVkcl0UH', '4', 'ZC230901', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('YtKMFXIhJHDnRAoXHWc', '2', 'SR231026', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('Yto451Sdi74il0tJtfH', '1', 'XM231016', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('yuJGzNZ1jxi3tJC9Rha', '6', 'ZC231009', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('YvG4FjSqUD0ndJJNtRd', '1', 'QT230926', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('YZ2kq3NA56Q8WyB9otI', '1', 'SR231026', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('Z43REAfG75C31cMWaoZ', '5', 'XDF230926', '__M4oRwEVzSB1oI1D0faN', '3CffNlgt9B9StIjIUPB');
INSERT INTO `t_counter` VALUES ('Z7EyxbfCZhknu853uQ4', '4', 'HZ230828', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('zfrcnJeQLoP30xjQndL', '1', 'FK231025', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('ZIrBKh8ORatxjBLWgTp', '13', 'ZC230907', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('zj3juEjRMzRAXe9Chum', '1', 'HZ230825', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('ZryxmtJlcPiyo9qvH0W', '1', 'FK231109', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');
INSERT INTO `t_counter` VALUES ('zv3epCFsOUjsZroqhK5', '10', 'ZC231107', '__M4oRwEVzSB1oI1D0faN', 'QnkpCDiSJqeIHrbbY9X');

-- ----------------------------
-- Table structure for t_document
-- ----------------------------
DROP TABLE IF EXISTS `t_document`;
CREATE TABLE `t_document` (
  `ID` varchar(100) NOT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `FORMNAME` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `AUTHORDEPTID` varchar(100) DEFAULT NULL,
  `AUTHOR_DEPT_INDEX` varchar(2000) DEFAULT NULL,
  `AUTHOR_USER_INDEX` varchar(2000) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `FORMID` varchar(100) DEFAULT NULL,
  `SUBFORMIDS` longtext,
  `ISTMP` bit(1) DEFAULT NULL,
  `VERSIONS` int(11) DEFAULT NULL,
  `SORTID` varchar(100) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `PARENT` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `STATEINT` int(11) DEFAULT NULL,
  `LASTMODIFIER` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `STATELABELINFO` longtext,
  `PREVAUDITNODE` longtext,
  `PREVAUDITUSER` longtext,
  `OPTIONITEM` longtext,
  `SIGN` longtext,
  `SECRET` varchar(100) DEFAULT NULL,
  `MAPPINGID` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_document
-- ----------------------------

-- ----------------------------
-- Table structure for t_flowhistory
-- ----------------------------
DROP TABLE IF EXISTS `t_flowhistory`;
CREATE TABLE `t_flowhistory` (
  `ID` varchar(100) NOT NULL,
  `FLOWSTATERT_ID` varchar(100) DEFAULT NULL,
  `FLOWID` varchar(100) DEFAULT NULL,
  `NODEHIS_ID` varchar(100) DEFAULT NULL,
  `STARTNODENAME` varchar(100) DEFAULT NULL,
  `ENDNODENAME` varchar(100) DEFAULT NULL,
  `NAME` varchar(100) DEFAULT NULL,
  `DOC_ID` varchar(100) DEFAULT NULL,
  `ATTITUDE` varchar(2000) DEFAULT NULL,
  `PROCESSTIME` datetime DEFAULT NULL,
  `FLOWOPERATION` varchar(100) DEFAULT NULL,
  `STARTNODEID` varchar(100) DEFAULT NULL,
  `ENDNODEID` varchar(100) DEFAULT NULL,
  `AGENTNAME` varchar(100) DEFAULT NULL,
  `AGENTID` varchar(100) DEFAULT NULL,
  `SIGNATURE` longtext,
  `ACTORID` varchar(100) DEFAULT NULL,
  `RECEIVERINFO` longtext,
  `ACTIONTIME` datetime DEFAULT NULL,
  `FLOWNAME` varchar(100) DEFAULT NULL,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `INITIATORID` varchar(100) DEFAULT NULL,
  `SUMMARY` longtext,
  `FIRSTPROCESSTIME` datetime DEFAULT NULL,
  `STATELABEL` longtext,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_flowhistory
-- ----------------------------
INSERT INTO `t_flowhistory` VALUES ('3I9b4ofLfgXEdERZvcv', 'i1iNKJtnTvtGxo2DBkt--__7NuSLfcmZ3ILpeCe13O-JMiceLty0wVuAqZiACQ', '__uIAg4VJq29YlVvCwn4g', 'ju5ukbRmWv27e5uXgy0', '申请人', '财务初审', '吴继华', 'i1iNKJtnTvtGxo2DBkt--__7NuSLfcmZ3ILpeCe13O', '请审批', '2023-11-10 03:48:39', '80', '1689675721656', '1690256013713', '', null, '', 'Mxspdb6zQc7sibnXuG0', '{\"users\":[{\"userid\":\"hVS4izsK5fGJSNsfAdu\",\"username\":\"姚丹\"}]}', '2023-11-10 03:48:39', '支出合同审批流程', '吴继华', 'Mxspdb6zQc7sibnXuG0', '{\"showTags\":false,\"summaryText\":\"合同名称：223232\"}', '2023-11-10 03:48:39', '结束', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_flowhistory` VALUES ('3xZ6JK5Bxsp7BkfnLqw', 'VtpfIAcFbwAAHUS9aHS--__Y50eV6qmxDkqlFJlMaC-Dxlor1ppChyLqtjZXlO', '__Jzz57K3kMsVSF505PRy', '1Tq316gGEqI5htilhEc', '财务负责人', '财务主管领导', '张兴玉', 'VtpfIAcFbwAAHUS9aHS--__Y50eV6qmxDkqlFJlMaC', '同意', '2023-11-08 14:21:02', '80', '1690257079040', '1693963941709', '', null, '', 'QxqqJOTgIkciwnJwap0', '{\"users\":[{\"userid\":\"HZo6ahymlIbSQhIOxyl\",\"username\":\"程丽君\"}]}', '2023-11-08 14:20:44', '开票申请流程', '吴继华', 'Mxspdb6zQc7sibnXuG0', '', '2023-11-08 14:19:35', '结束', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_flowhistory` VALUES ('5eVqumTjKD4V7dnwFmn', 'i1iNKJtnTvtGxo2DBkt--__7NuSLfcmZ3ILpeCe13O-JMiceLty0wVuAqZiACQ', '__uIAg4VJq29YlVvCwn4g', 'MCsfUO0IUqVjqEj19yb', '分管领导', '总经理', '宗昊', 'i1iNKJtnTvtGxo2DBkt--__7NuSLfcmZ3ILpeCe13O', '同意', '2023-11-10 04:22:31', '80', '1690257090180', '1690257092185', '', null, '', 'QghJDaDCt5fckOMCWAe', '{\"users\":[{\"userid\":\"lc21cFdy3cs5ti6211q\",\"username\":\"王毅\"}]}', '2023-11-10 04:22:03', '支出合同审批流程', '吴继华', 'Mxspdb6zQc7sibnXuG0', '{\"showTags\":false,\"summaryText\":\"合同名称：223232\"}', '2023-11-10 03:48:39', '结束', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_flowhistory` VALUES ('5f3SZJHI5Cmsk989JVU', 'VtpfIAcFbwAAHUS9aHS--__Y50eV6qmxDkqlFJlMaC-Dxlor1ppChyLqtjZXlO', '__Jzz57K3kMsVSF505PRy', 'b1yd4ZpO2O9829BMHu6', '分管领导', '财务初审', '程丽君', 'VtpfIAcFbwAAHUS9aHS--__Y50eV6qmxDkqlFJlMaC', '同意', '2023-11-08 14:19:55', '80', '1690257090180', '1690256013713', '', null, '', 'HZo6ahymlIbSQhIOxyl', '{\"users\":[{\"userid\":\"hVS4izsK5fGJSNsfAdu\",\"username\":\"姚丹\"}]}', '2023-11-08 14:19:35', '开票申请流程', '吴继华', 'Mxspdb6zQc7sibnXuG0', '', '2023-11-08 14:19:35', '结束', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_flowhistory` VALUES ('6d8duiiph4JRs4zBlFX', 'YQOXj5pPsc3Jgt8JWZN--__GD8U7ledIDdqSex1aw1-WKqk3fMO6z2yCET2l1x', '__ZmvZUO4NsrDxhYFBUSn', 'Iu2YU4DxvZOvn9n8ykp', '申请人', '法务部', '测试用户', 'YQOXj5pPsc3Jgt8JWZN--__GD8U7ledIDdqSex1aw1', '请审批', '2023-11-15 01:16:02', '80', '1689326046740', '1689326050220', '', null, '', 'Ro6tebUrjHw0LKx5ceU', '{\"users\":[{\"userid\":\"fVyiUPWErazp2FCoLQt\",\"username\":\"jc\"},{\"userid\":\"Ro6tebUrjHw0LKx5ceU\",\"username\":\"测试用户\"},{\"userid\":\"9aaUNnqVNJVO2vczqM8\",\"username\":\"财务\"},{\"userid\":\"mwst1zWbkuOneUxHUzc\",\"username\":\"法人\"}]}', '2023-11-15 01:16:02', '相对方审核流程', '测试用户', 'Ro6tebUrjHw0LKx5ceU', '{\"showTags\":false,\"summaryText\":\"相对方：北京新媒体集团\"}', '2023-11-15 00:00:00', '通过', '1i9X1xRMM6BRD9GMjj9', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_flowhistory` VALUES ('8goUaWOmWuvqOVCP2RM', 'i1iNKJtnTvtGxo2DBkt--__7NuSLfcmZ3ILpeCe13O-JMiceLty0wVuAqZiACQ', '__uIAg4VJq29YlVvCwn4g', '3o6g8hyfzALCLIXzXrv', '法人/授权代表', '结束', '宗昊', 'i1iNKJtnTvtGxo2DBkt--__7NuSLfcmZ3ILpeCe13O', '同意', '2023-11-10 04:23:48', '7', '1690257094009', '1689675733215', '', null, '', 'QghJDaDCt5fckOMCWAe', '', '2023-11-10 04:22:54', '支出合同审批流程', '吴继华', 'Mxspdb6zQc7sibnXuG0', '{\"showTags\":false,\"summaryText\":\"合同名称：223232\"}', '2023-11-10 03:48:39', '结束', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_flowhistory` VALUES ('B0Ay4UsK7JrryklTcTa', 'VtpfIAcFbwAAHUS9aHS--__Y50eV6qmxDkqlFJlMaC-Dxlor1ppChyLqtjZXlO', '__Jzz57K3kMsVSF505PRy', 'wCaNEyhStt1vqw0eBZ2', '总经理', '董事长', '王毅', 'VtpfIAcFbwAAHUS9aHS--__Y50eV6qmxDkqlFJlMaC', '同意', '2023-11-08 14:21:40', '80', '1690257092185', '1697596821065', '', null, '', 'lc21cFdy3cs5ti6211q', '{\"users\":[{\"userid\":\"QghJDaDCt5fckOMCWAe\",\"username\":\"宗昊\"}]}', '2023-11-08 14:21:18', '开票申请流程', '吴继华', 'Mxspdb6zQc7sibnXuG0', '', '2023-11-08 14:19:35', '结束', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_flowhistory` VALUES ('brt6eIKhVCMeatWcmlO', 'i1iNKJtnTvtGxo2DBkt--__7NuSLfcmZ3ILpeCe13O-JMiceLty0wVuAqZiACQ', '__uIAg4VJq29YlVvCwn4g', 'LmpO0dPBU6pkFlHlG7v', '法务初审', '申请人', '吕静璇', 'i1iNKJtnTvtGxo2DBkt--__7NuSLfcmZ3ILpeCe13O', '2121', '2023-11-10 04:20:21', '81', '1690257086178', '1689675721656', '', null, '', 'hnu7lc0tv0okrCNI0y0', '{\"users\":[{\"userid\":\"Mxspdb6zQc7sibnXuG0\",\"username\":\"吴继华\"}]}', '2023-11-10 03:51:49', '支出合同审批流程', '吴继华', 'Mxspdb6zQc7sibnXuG0', '{\"showTags\":false,\"summaryText\":\"合同名称：223232\"}', '2023-11-10 03:48:39', '结束', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_flowhistory` VALUES ('h6BnalDXH5jYIxaj5IE', 'i1iNKJtnTvtGxo2DBkt--__7NuSLfcmZ3ILpeCe13O-JMiceLty0wVuAqZiACQ', '__uIAg4VJq29YlVvCwn4g', 'yMjeO9TTLfincCJWIn1', '申请人', '财务初审', '吴继华', 'i1iNKJtnTvtGxo2DBkt--__7NuSLfcmZ3ILpeCe13O', '同意', '2023-11-10 04:20:43', '80', '1689675721656', '1690256013713', '', null, '', 'Mxspdb6zQc7sibnXuG0', '{\"users\":[{\"userid\":\"hVS4izsK5fGJSNsfAdu\",\"username\":\"姚丹\"}]}', '2023-11-10 04:20:21', '支出合同审批流程', '吴继华', 'Mxspdb6zQc7sibnXuG0', '{\"showTags\":false,\"summaryText\":\"合同名称：223232\"}', '2023-11-10 03:48:39', '结束', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_flowhistory` VALUES ('HR3JM4e0l7Vl0aVsMDx', 'VtpfIAcFbwAAHUS9aHS--__Y50eV6qmxDkqlFJlMaC-Dxlor1ppChyLqtjZXlO', '__Jzz57K3kMsVSF505PRy', 'ypxMpPHIA4ysiUj4KBM', '申请人', '分管领导', '吴继华', 'VtpfIAcFbwAAHUS9aHS--__Y50eV6qmxDkqlFJlMaC', '请审批', '2023-11-08 14:19:35', '80', '1689675721656', '1690257090180', '', null, '', 'Mxspdb6zQc7sibnXuG0', '{\"users\":[{\"userid\":\"HZo6ahymlIbSQhIOxyl\",\"username\":\"程丽君\"}]}', '2023-11-08 14:19:35', '开票申请流程', '吴继华', 'Mxspdb6zQc7sibnXuG0', '', '2023-11-08 14:19:35', '结束', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_flowhistory` VALUES ('Iz2hVqOBisy4jcGVSr6', 'YQOXj5pPsc3Jgt8JWZN--__GD8U7ledIDdqSex1aw1-WKqk3fMO6z2yCET2l1x', '__ZmvZUO4NsrDxhYFBUSn', 'x2qC8xaZZYbZ80btrYR', '法务部', '通过', 'jc', 'YQOXj5pPsc3Jgt8JWZN--__GD8U7ledIDdqSex1aw1', '同意', '2023-11-15 08:34:43', '7', '1689326050220', '1689326041173', '', null, '', 'fVyiUPWErazp2FCoLQt', '', '2023-11-15 01:16:02', '相对方审核流程', '测试用户', 'Ro6tebUrjHw0LKx5ceU', '{\"showTags\":false,\"summaryText\":\"相对方：北京新媒体集团\"}', '2023-11-15 00:00:00', '通过', '1i9X1xRMM6BRD9GMjj9', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_flowhistory` VALUES ('nDRsaR76f9NIHhnzWbu', 'i1iNKJtnTvtGxo2DBkt--__7NuSLfcmZ3ILpeCe13O-JMiceLty0wVuAqZiACQ', '__uIAg4VJq29YlVvCwn4g', 'K4huc3c3YKerVPMxnQn', '财务负责人', '法务初审', '张兴玉', 'i1iNKJtnTvtGxo2DBkt--__7NuSLfcmZ3ILpeCe13O', '同意', '2023-11-10 03:50:11', '80', '1690257079040', '1690257086178', '', null, '', 'QxqqJOTgIkciwnJwap0', '{\"users\":[{\"userid\":\"hnu7lc0tv0okrCNI0y0\",\"username\":\"吕静璇\"}]}', '2023-11-10 03:49:27', '支出合同审批流程', '吴继华', 'Mxspdb6zQc7sibnXuG0', '{\"showTags\":false,\"summaryText\":\"合同名称：223232\"}', '2023-11-10 03:48:39', '结束', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_flowhistory` VALUES ('oVgWLa6MKbVBHuIpIPS', 'i1iNKJtnTvtGxo2DBkt--__7NuSLfcmZ3ILpeCe13O-JMiceLty0wVuAqZiACQ', '__uIAg4VJq29YlVvCwn4g', 'gnucKgh8elp8th8sNNk', '总经理', '法人/授权代表', '王毅', 'i1iNKJtnTvtGxo2DBkt--__7NuSLfcmZ3ILpeCe13O', '同意', '2023-11-10 04:22:54', '80', '1690257092185', '1690257094009', '', null, '', 'lc21cFdy3cs5ti6211q', '{\"users\":[{\"userid\":\"QghJDaDCt5fckOMCWAe\",\"username\":\"宗昊\"}]}', '2023-11-10 04:22:31', '支出合同审批流程', '吴继华', 'Mxspdb6zQc7sibnXuG0', '{\"showTags\":false,\"summaryText\":\"合同名称：223232\"}', '2023-11-10 03:48:39', '结束', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_flowhistory` VALUES ('Ps5voBAZtwFJrQbfOti', 'VtpfIAcFbwAAHUS9aHS--__Y50eV6qmxDkqlFJlMaC-Dxlor1ppChyLqtjZXlO', '__Jzz57K3kMsVSF505PRy', 'JmSyXYZjar6tXj4XrAd', '董事长', '结束', '宗昊', 'VtpfIAcFbwAAHUS9aHS--__Y50eV6qmxDkqlFJlMaC', '同意', '2023-11-08 14:45:49', '7', '1697596821065', '1689675733215', '', null, '', 'QghJDaDCt5fckOMCWAe', '', '2023-11-08 14:21:40', '开票申请流程', '吴继华', 'Mxspdb6zQc7sibnXuG0', '', '2023-11-08 14:19:35', '结束', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_flowhistory` VALUES ('PTpBxmExI4BQwaDE0Xs', 'i1iNKJtnTvtGxo2DBkt--__7NuSLfcmZ3ILpeCe13O-JMiceLty0wVuAqZiACQ', '__uIAg4VJq29YlVvCwn4g', 'BUjpjz4OlqjxD97iLYq', '财务负责人', '法务初审', '张兴玉', 'i1iNKJtnTvtGxo2DBkt--__7NuSLfcmZ3ILpeCe13O', '同意', '2023-11-10 04:21:40', '80', '1690257079040', '1690257086178', '', null, '', 'QxqqJOTgIkciwnJwap0', '{\"users\":[{\"userid\":\"hnu7lc0tv0okrCNI0y0\",\"username\":\"吕静璇\"}]}', '2023-11-10 04:21:16', '支出合同审批流程', '吴继华', 'Mxspdb6zQc7sibnXuG0', '{\"showTags\":false,\"summaryText\":\"合同名称：223232\"}', '2023-11-10 03:48:39', '结束', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_flowhistory` VALUES ('QgaSq1fSgV4ZYiAsQoF', 'i1iNKJtnTvtGxo2DBkt--__7NuSLfcmZ3ILpeCe13O-JMiceLty0wVuAqZiACQ', '__uIAg4VJq29YlVvCwn4g', '5pmjKZrvaMmrHwbrkNQ', '法务初审', '分管领导', '吕静璇', 'i1iNKJtnTvtGxo2DBkt--__7NuSLfcmZ3ILpeCe13O', '同意', '2023-11-10 04:22:03', '80', '1690257086178', '1690257090180', '', null, '', 'hnu7lc0tv0okrCNI0y0', '{\"users\":[{\"userid\":\"QghJDaDCt5fckOMCWAe\",\"username\":\"宗昊\"}]}', '2023-11-10 04:21:40', '支出合同审批流程', '吴继华', 'Mxspdb6zQc7sibnXuG0', '{\"showTags\":false,\"summaryText\":\"合同名称：223232\"}', '2023-11-10 03:48:39', '结束', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_flowhistory` VALUES ('TABFxQ3mlb5DST7uWXj', 'i1iNKJtnTvtGxo2DBkt--__7NuSLfcmZ3ILpeCe13O-JMiceLty0wVuAqZiACQ', '__uIAg4VJq29YlVvCwn4g', 'iXWxvHdzKRF0sUGlUkt', '分管领导', '法务初审', '宗昊', 'i1iNKJtnTvtGxo2DBkt--__7NuSLfcmZ3ILpeCe13O', 'q11', '2023-11-10 03:51:49', '81', '1690257090180', '1690257086178', '', null, '', 'QghJDaDCt5fckOMCWAe', '{\"users\":[{\"userid\":\"hnu7lc0tv0okrCNI0y0\",\"username\":\"吕静璇\"}]}', '2023-11-10 03:50:41', '支出合同审批流程', '吴继华', 'Mxspdb6zQc7sibnXuG0', '{\"showTags\":false,\"summaryText\":\"合同名称：223232\"}', '2023-11-10 03:48:39', '结束', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_flowhistory` VALUES ('tEfvyD98ZMgmIJu5DWj', 'i1iNKJtnTvtGxo2DBkt--__7NuSLfcmZ3ILpeCe13O-JMiceLty0wVuAqZiACQ', '__uIAg4VJq29YlVvCwn4g', 'rk0SllXndWhvKZeiTPF', '财务初审', '财务负责人', '姚丹', 'i1iNKJtnTvtGxo2DBkt--__7NuSLfcmZ3ILpeCe13O', '同意', '2023-11-10 04:21:16', '80', '1690256013713', '1690257079040', '', null, '', 'hVS4izsK5fGJSNsfAdu', '{\"users\":[{\"userid\":\"QxqqJOTgIkciwnJwap0\",\"username\":\"张兴玉\"}]}', '2023-11-10 04:20:43', '支出合同审批流程', '吴继华', 'Mxspdb6zQc7sibnXuG0', '{\"showTags\":false,\"summaryText\":\"合同名称：223232\"}', '2023-11-10 03:48:39', '结束', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_flowhistory` VALUES ('W2vRGkKn5IpnWj6hF1s', 'VtpfIAcFbwAAHUS9aHS--__Y50eV6qmxDkqlFJlMaC-Dxlor1ppChyLqtjZXlO', '__Jzz57K3kMsVSF505PRy', 'EA0WFUQ7Yo9zuvJNq1u', '财务主管领导', '总经理', '程丽君', 'VtpfIAcFbwAAHUS9aHS--__Y50eV6qmxDkqlFJlMaC', '同意', '2023-11-08 14:21:18', '80', '1693963941709', '1690257092185', '', null, '', 'HZo6ahymlIbSQhIOxyl', '{\"users\":[{\"userid\":\"lc21cFdy3cs5ti6211q\",\"username\":\"王毅\"}]}', '2023-11-08 14:21:02', '开票申请流程', '吴继华', 'Mxspdb6zQc7sibnXuG0', '', '2023-11-08 14:19:35', '结束', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_flowhistory` VALUES ('XRbGcvOJywvfVQv2b72', 'VtpfIAcFbwAAHUS9aHS--__Y50eV6qmxDkqlFJlMaC-Dxlor1ppChyLqtjZXlO', '__Jzz57K3kMsVSF505PRy', '4lcUKZ7h6Etkb7Sdgna', '财务复审', '财务负责人', '邵静', 'VtpfIAcFbwAAHUS9aHS--__Y50eV6qmxDkqlFJlMaC', '同意', '2023-11-08 14:20:44', '80', '1693205430819', '1690257079040', '', null, '', 'qZlNRmGb0t7cpTIKjLH', '{\"users\":[{\"userid\":\"QxqqJOTgIkciwnJwap0\",\"username\":\"张兴玉\"}]}', '2023-11-08 14:20:12', '开票申请流程', '吴继华', 'Mxspdb6zQc7sibnXuG0', '', '2023-11-08 14:19:35', '结束', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_flowhistory` VALUES ('zi2pU4qQ7DKm1UEfuEn', 'i1iNKJtnTvtGxo2DBkt--__7NuSLfcmZ3ILpeCe13O-JMiceLty0wVuAqZiACQ', '__uIAg4VJq29YlVvCwn4g', 'Bj8BX47SiuA20gFrI0l', '法务初审', '分管领导', '吕静璇', 'i1iNKJtnTvtGxo2DBkt--__7NuSLfcmZ3ILpeCe13O', '同意', '2023-11-10 03:50:41', '80', '1690257086178', '1690257090180', '', null, '', 'hnu7lc0tv0okrCNI0y0', '{\"users\":[{\"userid\":\"QghJDaDCt5fckOMCWAe\",\"username\":\"宗昊\"}]}', '2023-11-10 03:50:11', '支出合同审批流程', '吴继华', 'Mxspdb6zQc7sibnXuG0', '{\"showTags\":false,\"summaryText\":\"合同名称：223232\"}', '2023-11-10 03:48:39', '结束', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_flowhistory` VALUES ('zMR4fxfyLJAe1fShwEB', 'i1iNKJtnTvtGxo2DBkt--__7NuSLfcmZ3ILpeCe13O-JMiceLty0wVuAqZiACQ', '__uIAg4VJq29YlVvCwn4g', 'Y8TGnsCWv7jm352Y2TG', '财务初审', '财务负责人', '姚丹', 'i1iNKJtnTvtGxo2DBkt--__7NuSLfcmZ3ILpeCe13O', '同意', '2023-11-10 03:49:27', '80', '1690256013713', '1690257079040', '', null, '', 'hVS4izsK5fGJSNsfAdu', '{\"users\":[{\"userid\":\"QxqqJOTgIkciwnJwap0\",\"username\":\"张兴玉\"}]}', '2023-11-10 03:48:39', '支出合同审批流程', '吴继华', 'Mxspdb6zQc7sibnXuG0', '{\"showTags\":false,\"summaryText\":\"合同名称：223232\"}', '2023-11-10 03:48:39', '结束', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_flowhistory` VALUES ('ZvrlypCh5RFebPSbxla', 'VtpfIAcFbwAAHUS9aHS--__Y50eV6qmxDkqlFJlMaC-Dxlor1ppChyLqtjZXlO', '__Jzz57K3kMsVSF505PRy', 'BGBck3waO74SWlLKvkC', '财务初审', '财务复审', '姚丹', 'VtpfIAcFbwAAHUS9aHS--__Y50eV6qmxDkqlFJlMaC', '同意', '2023-11-08 14:20:12', '80', '1690256013713', '1693205430819', '', null, '', 'hVS4izsK5fGJSNsfAdu', '{\"users\":[{\"userid\":\"qZlNRmGb0t7cpTIKjLH\",\"username\":\"邵静\"}]}', '2023-11-08 14:19:55', '开票申请流程', '吴继华', 'Mxspdb6zQc7sibnXuG0', '', '2023-11-08 14:19:35', '结束', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN');

-- ----------------------------
-- Table structure for t_flowstatert
-- ----------------------------
DROP TABLE IF EXISTS `t_flowstatert`;
CREATE TABLE `t_flowstatert` (
  `ID` varchar(100) NOT NULL,
  `DOCID` varchar(100) DEFAULT NULL,
  `FLOWID` varchar(100) DEFAULT NULL,
  `STATE` int(11) DEFAULT NULL,
  `PARENT` varchar(100) DEFAULT NULL,
  `FLOWNAME` varchar(100) DEFAULT NULL,
  `FLOWXML` longtext,
  `LASTMODIFIERID` varchar(100) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `SUBFLOWNODEID` varchar(100) DEFAULT NULL,
  `COMPLETE` int(11) DEFAULT NULL,
  `CALLBACK` int(11) DEFAULT NULL,
  `TOKEN` varchar(400) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `INITIATOR` varchar(100) DEFAULT NULL,
  `AUDITUSER` varchar(100) DEFAULT NULL,
  `AUDITORNAMES` longtext,
  `AUDITORLIST` longtext,
  `COAUDITORLIST` longtext,
  `LASTFLOWOPERATION` varchar(100) DEFAULT NULL,
  `AUDITDATE` datetime DEFAULT NULL,
  `SUB_POSITION` int(11) DEFAULT NULL,
  `ISARCHIVED` int(11) DEFAULT NULL,
  `ISTERMINATED` bit(1) DEFAULT NULL,
  `PREV_AUDIT_NODE` varchar(100) DEFAULT NULL,
  `PREV_AUDIT_USER` varchar(100) DEFAULT NULL,
  `WORKFLOW_TYPE` int(11) DEFAULT NULL,
  `ACTIONTIME` datetime DEFAULT NULL,
  `SUMMARY` longtext,
  `INITIATORID` varchar(100) DEFAULT NULL,
  `FIRSTPROCESSTIME` datetime DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_flowstatert
-- ----------------------------
INSERT INTO `t_flowstatert` VALUES ('7Q0G8h9g587tohUGy0z--__7NuSLfcmZ3ILpeCe13O-1S7qeEgsRGpRsH396O8', '7Q0G8h9g587tohUGy0z--__7NuSLfcmZ3ILpeCe13O', '__uIAg4VJq29YlVvCwn4g', '256', null, '支出合同审批流程', null, 'Mxspdb6zQc7sibnXuG0', '2023-11-13 07:52:16', '__M4oRwEVzSB1oI1D0faN', null, '0', '0', null, '财务初审', '吴继华', 'Mxspdb6zQc7sibnXuG0', '姚丹', '{\"1690256013713\":[\"hVS4izsK5fGJSNsfAdu\"]}', '{\"1690256013713\":[]}', '80', '2023-11-13 07:52:16', '0', '0', '\0', '申请人', '吴继华', '0', null, '{\"showTags\":false,\"summaryText\":\"合同名称：测试\"}', 'Mxspdb6zQc7sibnXuG0', '2023-11-13 07:52:16', 'QnkpCDiSJqeIHrbbY9X');

-- ----------------------------
-- Table structure for t_flow_proxy
-- ----------------------------
DROP TABLE IF EXISTS `t_flow_proxy`;
CREATE TABLE `t_flow_proxy` (
  `ID` varchar(100) NOT NULL,
  `FLOWNAME` varchar(100) DEFAULT NULL,
  `FLOWID` varchar(100) DEFAULT NULL,
  `DESCRIPTION` varchar(100) DEFAULT NULL,
  `STATE` varchar(100) DEFAULT NULL,
  `AGENTS` longtext,
  `AGENTSNAME` longtext,
  `OWNER` varchar(100) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `VERSION` int(11) DEFAULT NULL,
  `PROXYMODE` int(11) DEFAULT NULL,
  `STARTPROXYTIME` datetime DEFAULT NULL,
  `ENDPROXYTIME` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_flow_proxy
-- ----------------------------

-- ----------------------------
-- Table structure for t_logs
-- ----------------------------
DROP TABLE IF EXISTS `t_logs`;
CREATE TABLE `t_logs` (
  `ID` varchar(100) NOT NULL,
  `OPERATORID` varchar(100) DEFAULT NULL,
  `OPERATOR` varchar(100) DEFAULT NULL,
  `OPERATIONTIME` datetime DEFAULT NULL,
  `OPERATIONTYPE` varchar(100) DEFAULT NULL,
  `SOURCEID` varchar(100) DEFAULT NULL,
  `PARENTSOURCEID` varchar(100) DEFAULT NULL,
  `SOURCE` varchar(100) DEFAULT NULL,
  `SOURCETYPE` varchar(100) DEFAULT NULL,
  `DESCRIPTION` longtext,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_logs
-- ----------------------------
INSERT INTO `t_logs` VALUES ('00jEwEW7nuRLOdhcF2R', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 08:13:51', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('0bxhKW0EGpKaC83SCfZ', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 09:12:28', '更新表单', '__SbbmE0b6nRPmCY8KXZZ', '__2kwCPuPXiBbAPyuNCaF', 'agreement_detail_info', '表单', '更新控件:counterpart[重计算:\'false\'->\'true\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('0dIfzVkZPiBkc5g459s', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 06:33:07', '更新表单', '__inZ4Rf1o4l1zhdJIkAk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_payment', '表单', '更新控件:settlement[重计算:\'false\'->\'true\',显示:\'普通\'->\'只读\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('0eO84SX3INor3mdgL8V', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-07 11:35:15', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('0fb6CoEQMArcPaw96aX', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 08:20:36', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '新增控件[\'counterpart\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('0FLz6RwnnP3mAurQ9BD', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-20 03:48:30', '更新表单', '__yd2UvBzMe4NvtnL8fWp', '__Tua0cKltwny7b1OTzYy', 'general_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('0gj6lenKHL4j3XyJBbA', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-10 08:13:25', '更新表单', '__yd2UvBzMe4NvtnL8fWp', '__Tua0cKltwny7b1OTzYy', 'general_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('0HVcGRKvKIX20R7HZGR', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 09:38:01', '更新表单', '__CaYfthrWZGFoLiRJcLg', '__hneTQUEoxzpYNqCrYlE', 'general_contract_form', '表单', '新增控件[\'相对方信息\']\r删除控件[counterpart,counterpart_legal_person,counterpart_number,contacts,contact_info,address,bank,bank_number]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('0JZ7tkdRb2Id35kDXYA', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 08:09:22', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '更新控件:选择供应商[名称:\'选择供应商\'->\'选择相对方\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('0mw1ltqy5w8jOAoJEIt', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-03 15:32:53', '更新表单', '__2mEMWhShJw6mjoCQJot', '__QXvwZwdn5T4aclQj14P', 'collection_record', '表单', '新增控件[\'选择相对方\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('0Si5HcZmT87oo0ioFkZ', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-19 02:14:24', '更新表单', '__yd2UvBzMe4NvtnL8fWp', '__Tua0cKltwny7b1OTzYy', 'general_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('0Tqe5pqvWn0nGLc5qbh', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 05:54:31', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('0TtxS2awZMlyB8DkSml', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-23 06:21:16', '更新表单', '__yd2UvBzMe4NvtnL8fWp', '__Tua0cKltwny7b1OTzYy', 'general_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('0ZU2YyTMmC1cPsO0Do9', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-07 02:18:15', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('19mKS3NoMNsLFF3rIq9', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 08:10:23', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '新增控件[\'center_director\',\'center_director_attitude\',\'center_director_date\',\'zg_leader\',\'zg_leader_attitude\',\'zg_leader_date\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('1ATQb49CoAIjIa7Xs4Y', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-10 06:48:27', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('1choo48FJkFwA7NAhfm', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 13:51:06', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '表单:[描述:\'收入合同呈批表\'->\'收入类合同呈批表\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('1eu96UbdMw99cxwKJwI', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-09 05:43:16', '更新表单', '__csNl7TtjU9NiaT1ucmj', '__zlfY3N4sZj7uqyt0qkX', 'basic_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('1fJ1TKEuHz2oTKJQyaT', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-05 07:29:28', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('1He1k0baCeRb8ArfHyj', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 07:36:27', '新建表单', '__xCMgHhAWD4ueSOlU0Q6', '__z1fcq82tLWSqTuTsPFC', 'flow_setting', '表单', 'flow_setting,控件[\'company\',\'flow\',\'flow_node\',\'is_skip\']', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('1L6xOGTSO7AdduVHTxj', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 07:02:47', '新建表单', '__2pMIIpKj9axWSDoqNTk', '__mwU54noqAjF1UBdrb1V', 'contract_withdraw', '表单', 'contract_withdraw,控件[\'reason\',\'relateid\']', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('1RW1EunCmbvZqTxDqHq', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-07 02:16:47', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('1RYo8pFUDodvAIf3VkD', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-19 02:13:27', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('1SGauuyVZd1oUb1xJnc', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 08:20:07', '更新表单', '__PX2ukhQA9L2styARWJj', '__UyItLTLUHT2WJRP9tP1', 'routine_litigation', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('1u50MJMsvw9oz7irKZq', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 09:19:37', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('1ZLY46Im8Rsn2eqALm3', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 07:30:52', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('23Ac7k8qyw6h2eD28hP', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 03:12:00', '更新表单', '__rH9L9naFweQwnFhsqt4', '__yV15cDXVn1xnPHSBvcT', 'payment_withdraw', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('2bI0VCHPlxxpuaGdH80', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-09 01:59:26', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('2fg3Ew1wHWsifjA55nh', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-29 08:38:46', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('2HltePyVdcaq0M7KvKV', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 09:19:18', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('2HnifS3ZyaimkbpMdOk', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-10 08:45:07', '更新表单', '__csNl7TtjU9NiaT1ucmj', '__zlfY3N4sZj7uqyt0qkX', 'basic_info', '表单', '更新控件:invoice_header[宽度:\'0\'->\'100\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('2Ko1xlx0Ca50LSC33Ct', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 06:50:35', '更新表单', '__SbbmE0b6nRPmCY8KXZZ', '__2kwCPuPXiBbAPyuNCaF', 'agreement_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('2lWMxGEAXnZi0r8TrJ3', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-06 07:15:41', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('2qKeXRSAp3toAztmIWQ', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-28 01:55:52', '新建表单', '__TT9kaMtyxJJfAlmqBXV', '__UyItLTLUHT2WJRP9tP1', 'court_details', '表单', 'court_details,控件[\'start_date\',\'notes\']', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('2rvFqTGl5uIifZvOWzG', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-07 11:37:48', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('32Hhmyg5PJNwRMoX54n', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-19 01:55:18', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('35rSW3Ei5b8w9ZidBUq', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-28 07:48:15', '更新表单', '__kgtrW5PYTckYCxVWeCw', '__hneTQUEoxzpYNqCrYlE', 'revenue_contract_form_payment', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('3bqprAy8WvFEjIYpqNh', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 06:28:23', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('3EkXso08El8jUR1jwWn', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 08:11:31', '更新表单', '__vVNU6mDJ1aDK8Aav4gm', '__Tua0cKltwny7b1OTzYy', 'general_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('3exmtiSGqNxthF9MyXK', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-07 02:17:23', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('3IK24p52Vr0JGaw9huN', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 06:19:35', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('3JfqCyXEyyrZuqslsi4', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-13 09:48:38', '更新表单', '__AT5KHLX1JxOb2B8Vv6H', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_text', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('3kJQB2YP3jddQVIYFr2', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 08:12:53', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('3nMxjXknjrHnjWZh0aW', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 03:09:12', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('3pI9ZFbTvUtK1qkWLj2', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 06:20:32', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('3QOSqbx92Ejwf5ReJs5', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 13:51:26', '更新表单', '__inZ4Rf1o4l1zhdJIkAk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_payment', '表单', '表单:[描述:\'收入合同呈批表-付款约定\'->\'收入类合同呈批表-付款约定\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('3t2Nxf4zG8mYMlJ0Bzd', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 13:50:35', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '表单:[描述:\'支出合同呈批表\'->\'支出类合同呈批表\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('3w90771ME4ypl3tMfMn', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 08:11:25', '更新表单', '__vVNU6mDJ1aDK8Aav4gm', '__Tua0cKltwny7b1OTzYy', 'general_contract_detail_info', '表单', '新增控件[\'center_director\',\'center_director_attitude\',\'center_director_date\',\'zg_leader\',\'zg_leader_attitude\',\'zg_leader_date\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('40qgIMKyq6wywffsCJy', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-09 11:28:49', '更新表单', '__csNl7TtjU9NiaT1ucmj', '__zlfY3N4sZj7uqyt0qkX', 'basic_info', '表单', '删除控件[status]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('4CwZzzsfK8BF7EOETTQ', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 09:05:28', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '新增控件[\'相对方信息\']\r更新控件:counterpart[重计算:\'false\'->\'true\']\r删除控件[counterpart_number,counterpart_legal_person,counterpart_type,counterpart_address,bank,bank_number,sign_leader,contact_info,identification]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('4FgNQ5pkogDdvuQIi4q', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-17 08:23:41', '更新表单', '__CxqBJSNgHCZXOW082KZ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('4gU5KhJgfYohfS0AtnX', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-10 06:49:55', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('4I4GfPzjhP4kBQjgPPr', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-05 07:30:37', '更新表单', '__SbbmE0b6nRPmCY8KXZZ', '__2kwCPuPXiBbAPyuNCaF', 'agreement_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('4jsyVfphCkR1nbukYU0', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 10:29:07', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('4KaVAk2lpvPwfUT12Y5', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-17 05:54:55', '更新表单', '__qwo0CY05xSdyRB6uI3n', '__UyItLTLUHT2WJRP9tP1', 'other_disputes', '表单', '删除控件[case_status]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('4lCeriKr1EGUkqwGjii', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-18 09:39:47', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('4M2cjLt1bVAPXDtR7BQ', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-19 02:08:36', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('4NRQ5reaIHIkcIOlKxr', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 07:33:32', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('4Pv8xOLr6EeFzf9RHA4', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 02:51:20', '更新表单', '__CxqBJSNgHCZXOW082KZ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('4pwIGUqrDvIyEkmWJUn', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-07 07:16:42', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '新增控件[\'apply_person_date\',\'dept_head_date\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('4x5OER2EpujGIrWj4I0', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-07 11:32:38', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('5DzBGEV9J1N39eS0RIc', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 07:31:29', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('5GXoZfppFronSqep8SS', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 06:23:52', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('5iLmHkFJKTbawduw2KW', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 06:11:24', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('5L2mXpakpkzk9GO4T9v', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 03:27:35', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('5MJ8vitllG87wLlgITy', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-10 08:04:06', '更新表单', '__csNl7TtjU9NiaT1ucmj', '__zlfY3N4sZj7uqyt0qkX', 'basic_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('5qn38Zrwk4uJhQ7Mb1g', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-18 09:52:32', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('5tH3I4OTXs1PIrfIN3f', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 08:57:31', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '新增控件[\'相对方信息\']\r更新控件:选择客户[名称:\'选择客户\'->\'选择相对方\'],counterpart[重计算:\'false\'->\'true\']\r删除控件[counterpart_number,counterpart_legal_person,counterpart_type,counterpart_address,bank,bank_number,sign_leader,contact_info,identification]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('5vVz95t6js6uT1mt1yI', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 03:34:12', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('5VXLzozomcgBdvALZdN', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-05 08:17:48', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('60Pg15DdkJ2dHl3qFNE', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-10 08:12:52', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('633HgGOiHdkUPn8XsPg', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 09:34:51', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('6CVV487SwkkYme8uqAt', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 06:29:18', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('6dHgDdTwTkJqSsRNhcn', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 13:52:42', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('6FgNT2IUq4YtF7wYMZ3', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 08:04:55', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('6FkFhJ60sANKXNM14z8', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-28 07:28:02', '更新表单', '__wv5qf8n1vfnhQBP9i4S', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_payment', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('6flEBySxxSPdgw7qn0h', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-09 07:12:49', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('6gsSi40YhwvXBVKT6sd', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-16 02:32:39', '更新表单', '__wv5qf8n1vfnhQBP9i4S', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_payment', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('6ioZWmGzekutoTVpNHT', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-23 06:19:55', '更新表单', '__SbbmE0b6nRPmCY8KXZZ', '__2kwCPuPXiBbAPyuNCaF', 'agreement_detail_info', '表单', '新增控件[\'expiration_description\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('6JV4WKoE3NpkSzxgc83', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-09 06:36:09', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('6KbngijPH712QXDvds3', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 07:39:00', '更新表单', '__xCMgHhAWD4ueSOlU0Q6', '__z1fcq82tLWSqTuTsPFC', 'flow_setting', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('6NaH9DKjkXDlOeV0Irq', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 10:23:48', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '更新控件:bank[焦点切换键:\'\'->\'Tabkey\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('6Sq9FzV7k4Siu1ZOgS2', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-16 02:31:52', '更新表单', '__inZ4Rf1o4l1zhdJIkAk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_payment', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('6URoy9XfyZKmOUaSkj0', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-05 07:47:50', '更新表单', '__tDBoOxFqod4sv9TEDXH', '__hneTQUEoxzpYNqCrYlE', 'my_contract', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('6ut7FMfbkThSaqn5oA6', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 08:11:15', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '新增控件[\'center_director\',\'center_director_attitude\',\'center_director_date\',\'zg_leader\',\'zg_leader_attitude\',\'zg_leader_date\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('6VXHvC7ue0N5Sqv93bb', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 02:41:22', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('6YzwdmU1SNSPOo3B4tV', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 03:23:15', '更新表单', '__rH9L9naFweQwnFhsqt4', '__yV15cDXVn1xnPHSBvcT', 'payment_withdraw', '表单', '新增控件[\'relateid\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('70oUzknn7lxxFaqVoFm', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-21 02:32:10', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('759jXfZBNOpdvxHHVD2', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 10:37:46', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('78evGEhMKcskZ9WhGFq', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-07 09:10:52', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '新增控件[\'apply_person_date\',\'dept_head_date\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('7ABG99ejKUCz4tT1xgb', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 10:36:49', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '新增控件[\'identification\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('7ATOp3sRI1aOyjPegrd', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:17:12', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '更新控件:start_time[描述:\'合同开始时间\'->\'合同起始时间\'],end_time[描述:\'合同结束时间\'->\'合同终止时间\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('7nA6CSwBuX7EnhMO5Jq', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-18 09:35:43', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('7qQ2pXww5Nm5ZEsLr1t', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 03:11:32', '更新表单', '__rH9L9naFweQwnFhsqt4', '__yV15cDXVn1xnPHSBvcT', 'payment_withdraw', '表单', '更新控件:opinion[描述:\'意见\'->\'原因说明\',名称:\'opinion\'->\'reason\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('7VLSVvx8pDZgXWdcqyu', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-03 15:28:39', '更新表单', '__NgXWRPuKStY5nygCkmL', '__WOgFyT9N1Dmtrzag3hW', 'invoice_form', '表单', '更新控件:选择相对方[重计算:\'false\'->\'true\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('82yoTYiXD2WqZG6bpcX', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-28 02:19:11', '更新表单', '__qwo0CY05xSdyRB6uI3n', '__UyItLTLUHT2WJRP9tP1', 'other_disputes', '表单', '新增控件[\'obligee\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('87CEsKYR0OWFZeTN8Ei', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-10 08:04:26', '更新表单', '__csNl7TtjU9NiaT1ucmj', '__zlfY3N4sZj7uqyt0qkX', 'basic_info', '表单', '更新控件:currency[宽度:\'100\'->\'0\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('8Ccc65GfAtUwJj5HAN5', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-07 10:08:25', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('8D2G8zPARIPcVntgCFm', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:19:57', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '更新控件:start_time[描述:\'合同起始时间\'->\'合同起始日期\'],end_time[描述:\'合同终止时间\'->\'合同终止日期\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('8ELURaZcHD0bTWYV54O', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-09 02:22:35', '更新表单', '__wv5qf8n1vfnhQBP9i4S', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_payment', '表单', '表单:[描述:\'分成类合同呈批表-付款约定\'->\'分成类合同呈批表-收款约定\']\r更新控件:payment_type[描述:\'付款类型\'->\'收款类型\'],payment_terms[描述:\'付款条件\'->\'收款条件\'],payment_ratio[描述:\'付款比例\'->\'收款比例\'],payment_amount[描述:\'付款金额\'->\'收款金额\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('8Gk7eZ7Lpe7nE8nuJqq', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-09 07:13:20', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('8gqE5jsRkXxYWac7Lui', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 08:19:03', '更新表单', '__CxqBJSNgHCZXOW082KZ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('8HV26Vn7uHORQGIkVBU', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-29 08:47:40', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('8i8nf8mjGOrudi5z22w', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-07 11:29:36', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '新增控件[\'重置2\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('8IjxYYPFGBuGFAJZ4us', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 08:07:31', '更新表单', '__4WlF2OuywICW02IsUW8', '__hneTQUEoxzpYNqCrYlE', 'revenue_contract_form', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('8jLEuuKpAj6z61JLzVL', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-29 08:47:51', '更新表单', '__yd2UvBzMe4NvtnL8fWp', '__Tua0cKltwny7b1OTzYy', 'general_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('8lDwNkXc5JRTWT7nihG', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 03:33:04', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('8LlFhbfLYJ4CrQjhsOf', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-07 11:40:21', '更新表单', '__SbbmE0b6nRPmCY8KXZZ', '__2kwCPuPXiBbAPyuNCaF', 'agreement_detail_info', '表单', '新增控件[\'重置\',\'重置2\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('8lvlipG9OD0zi0ggkIV', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-06 06:00:55', '更新表单', '__SbbmE0b6nRPmCY8KXZZ', '__2kwCPuPXiBbAPyuNCaF', 'agreement_detail_info', '表单', '更新控件:counterpart[显示:\'普通\'->\'只读\'],counterpart_legal_person[显示:\'普通\'->\'只读\'],bank[显示:\'普通\'->\'只读\'],bank_number[显示:\'普通\'->\'只读\'],sign_leader[显示:\'普通\'->\'只读\'],contact_info[显示:\'普通\'->\'只读\'],identification[显示:\'普通\'->\'只读\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('8M2EOGNapbwsPrF53Fm', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-07 02:14:47', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('8M3IkTfTNq8u7w0t4HY', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:22:44', '更新表单', '__CxqBJSNgHCZXOW082KZ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('8NgXBM3eOpojBJcmYYq', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-13 08:02:04', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('8Qa7pGcRTY0BihJhV4h', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-13 08:02:22', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('8sNQ52F9zjfM3xW7FUy', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-07 11:44:17', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '更新控件:重置[标签:\'\'->\'取消\'],重置2[标签:\'重置\'->\'取消\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('8tL5KmPsQGKhFcNUPmC', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-13 07:59:38', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('8V2RWbmgWe7v72p0Bh5', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 10:46:45', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '更新控件:expiration_description[描述:\'合同到期说明\'->\'合同倒签订说明\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('8YpWU4vU9ie6jAEyN7T', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-21 06:02:45', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('9bAFeIbT0KgnPBvI27b', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 05:54:55', '更新表单', '__yd2UvBzMe4NvtnL8fWp', '__Tua0cKltwny7b1OTzYy', 'general_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('9BgIEs7kLF2mbUbnTE9', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-06 09:00:04', '更新表单', '__TT9kaMtyxJJfAlmqBXV', '__UyItLTLUHT2WJRP9tP1', 'court_details', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('9FCKa77yHM4liZbWQJ7', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-13 10:28:29', '更新表单', '__GD8U7ledIDdqSex1aw1', '__zlfY3N4sZj7uqyt0qkX', 'counterpart_management', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('9hsjzZTQTcuY5ilj6gQ', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 10:40:59', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('9k2m4z3VErFoFwG0kzV', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-07 07:28:49', '更新表单', '__GD8U7ledIDdqSex1aw1', '__zlfY3N4sZj7uqyt0qkX', 'counterpart_management', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('9k4Z6gfjlJ8azB3qGUu', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 05:47:19', '更新表单', '__yd2UvBzMe4NvtnL8fWp', '__Tua0cKltwny7b1OTzYy', 'general_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('9kZ6qkAjYrKFKGK2ZJb', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-06 06:04:44', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '新增控件[\'filing_person\',\'stamp_person\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('9TAd03eo05TvDW8YfaM', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 09:20:23', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('9W56orRHcC82BqJMjhH', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 06:26:37', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('9woCOzrCZoqnbXK61Re', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-15 10:36:00', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('a3KIN5zMYxIznQlHlYr', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 02:25:46', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('a3zgG3HFR6l2DfQ42tU', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-16 02:25:37', '更新表单', '__bjuatsuk2Hyiet4Z5k6', '__mwU54noqAjF1UBdrb1V', 'expense_contract_payment', '表单', '更新控件:payment_ratio[重计算:\'false\'->\'true\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('A7E3kBxn6bM4DpXmSlU', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-17 08:24:22', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('a8QsaX4PqqgB1xLnZRY', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 09:11:51', '更新表单', '__SbbmE0b6nRPmCY8KXZZ', '__2kwCPuPXiBbAPyuNCaF', 'agreement_detail_info', '表单', '新增控件[\'相对方信息\']\r更新控件:选择合作方[名称:\'选择合作方\'->\'选择相对方\']\r删除控件[counterpart_number,counterpart_legal_person,counterpart_type,counterpart_address,bank,bank_number,sign_leader,contact_info,identification]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('aAN8ZTSzg8gt7ABz01Z', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 09:22:49', '更新表单', '__yd2UvBzMe4NvtnL8fWp', '__Tua0cKltwny7b1OTzYy', 'general_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('aAYAeIGkmwEz6Ly7aTR', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-17 08:26:31', '更新表单', '__SbbmE0b6nRPmCY8KXZZ', '__2kwCPuPXiBbAPyuNCaF', 'agreement_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('aBaSvojKn7MPABDuz9K', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 10:16:33', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('abtikkp1J2uGKwalrJL', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-09 02:19:27', '更新表单', '__inZ4Rf1o4l1zhdJIkAk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_payment', '表单', '表单:[描述:\'收入类合同呈批表-付款约定\'->\'收入类合同呈批表-收款约定\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('aCiQiN8sOKioRPxx9YD', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-10 08:12:14', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('AcNl8ALraFgywYgoyF6', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-05 07:31:48', '更新表单', '__CxqBJSNgHCZXOW082KZ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('aDWEBUQTBuVkMQsjNSQ', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 08:08:03', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Ae8araXOTixjgYwsonX', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 08:06:46', '更新表单', '__jvpXgGxXUFqKjojY4Qy', '__hneTQUEoxzpYNqCrYlE', 'expense_contract_form', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('aGNaAiMDnMuEGSmETet', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 02:27:48', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '更新控件:dept_head[显示:\'普通\'->\'只读\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('agYF29q3wx2p5slmtey', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 02:49:54', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('ai5zBJuXVV3OnqxTaqY', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-21 02:32:14', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('AIHcZHIWlQRfc8HcUqf', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-11 01:32:49', '更新表单', '__csNl7TtjU9NiaT1ucmj', '__zlfY3N4sZj7uqyt0qkX', 'basic_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('AJi4Bmo9bn5fzmzGBoT', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 13:28:35', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('amFPfQ3na2BfRb4LebE', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 13:51:21', '更新表单', '__b6zlIys9xCcLYu4pnuW', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_object', '表单', '表单:[描述:\'收入合同呈批表-标的物\'->\'收入类合同呈批表-标的物\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('aMnVfwpHs8BAZxHVe9H', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-28 07:47:36', '更新表单', '__sLsbDoMV0Sc622Kgv6x', '__8tbV9gKZXznok4jo995', 'signing_center_payment', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('aOGzEt6D4DXDDP1lSD6', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:19:36', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '更新控件:start_time[描述:\'合同起始时间\'->\'合同起始日期\'],end_time[描述:\'合同终止时间\'->\'合同终止日期\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('AQuP5jqt2PD70GOZ6Ek', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-17 08:26:59', '更新表单', '__vVNU6mDJ1aDK8Aav4gm', '__Tua0cKltwny7b1OTzYy', 'general_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Argd7cBe5yPzgBDXEuN', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 06:58:47', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('aT72qoOtDSWy3XNxTy6', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 02:11:04', '更新表单', '__TPksi5U5JPY35O51oMu', '__UyItLTLUHT2WJRP9tP1', 'protect_litigation', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('AtQbwEia0DAdWLretQv', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-21 02:33:37', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('AUUAY3bU68xNkKtvxyz', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 07:08:50', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Azgel7k2EXCgpLY944F', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-07 09:12:06', '更新表单', '__SbbmE0b6nRPmCY8KXZZ', '__2kwCPuPXiBbAPyuNCaF', 'agreement_detail_info', '表单', '删除控件[center_director,center_director_attitude,center_director_date,zg_leader,zg_leader_attitude,zg_leader_date]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('b5P3R2VQCnjqjcYb1Vk', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 05:56:59', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('b78a8lbczUF4HCrSxbg', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 06:53:07', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('bArlqLT1IFiniUiI2wq', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-21 02:31:01', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('BBbAKSVBtPo3rDtrqQt', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 07:54:14', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '新增控件[\'相对方明细\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('bBbMa3bm6rokgkInRwJ', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-29 06:52:28', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('bcoPNtB19hdcEvDWylf', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-05 07:31:39', '更新表单', '__yd2UvBzMe4NvtnL8fWp', '__Tua0cKltwny7b1OTzYy', 'general_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('BdE0bF29YNxPMhv2MhM', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-13 10:29:06', '更新表单', '__GD8U7ledIDdqSex1aw1', '__zlfY3N4sZj7uqyt0qkX', 'counterpart_management', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('BDieRcuZ4XUnWlffiQf', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 02:10:01', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '新增控件[\'deputy_dept_head\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('BEanIqtl6O0UQyYA19d', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-09 01:58:37', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('bFUOVDZrb8L2SXcrlGV', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-10 16:38:55', '更新表单', '__yd2UvBzMe4NvtnL8fWp', '__Tua0cKltwny7b1OTzYy', 'general_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('bGozH3dt4f4J8JtL6OB', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 08:44:00', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('bgQJqGmaKoemHET2ly0', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 07:16:13', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('bgXhzCKrAN7iP8m7lGl', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-10 15:45:00', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('BgyAWULiZ4pyN2idcTr', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 13:49:48', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '表单:[描述:\'战略合作协议呈批表\'->\'合作类合同呈批表\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('bJ4chpmM2TewC2vOZLx', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 13:50:51', '更新表单', '__vbHLPkSRLphAB2bSquz', '__mwU54noqAjF1UBdrb1V', 'expense_contract_object', '表单', '表单:[描述:\'支出合同呈批表-标的物\'->\'支出类合同呈批表-标的物\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('BP3XKJJp1yTD6N1uNO9', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-23 06:15:59', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('bPX14WzJL6RZKz7dVRs', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-22 05:59:12', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('bROw2lroqQBte8zcVY2', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 10:23:19', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '更新控件:bank[焦点切换键:\'\'->\'Tabkey\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('BSkVbqy67y3XvWfkgtR', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 13:51:55', '更新表单', '__wv5qf8n1vfnhQBP9i4S', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_payment', '表单', '表单:[描述:\'分成合同呈批表-付款约定\'->\'分成类合同呈批表-付款约定\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('btiZpR1jDGCQCzjZnuv', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-10 15:21:19', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('bVAjvDcDptdofRjsKcx', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-19 02:12:54', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('bvKT4gGaB3zpNcz0DcY', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-14 05:40:42', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('bw3d66j2L7UTbMmEOS5', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 10:36:13', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '新增控件[\'identification\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('c3dmGtr61n6lVoa6DXm', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 06:09:38', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('c6obUdy5j9FuXOz1SYF', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 08:41:43', '更新表单', '__2mEMWhShJw6mjoCQJot', '__QXvwZwdn5T4aclQj14P', 'collection_record', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('c70xmKtzmT2uty3p4oB', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-15 10:33:49', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('c8ZH9dDDvWmJc5NuX97', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-09 07:13:07', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('C9KGkTcqiPUf8HTkfE9', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-10 07:56:15', '更新表单', '__csNl7TtjU9NiaT1ucmj', '__zlfY3N4sZj7uqyt0qkX', 'basic_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('CajiVLf2zu0s4EHqfSm', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 09:37:07', '更新表单', '__lSbiziySPDXouxVboVF', '__hneTQUEoxzpYNqCrYlE', 'general_contract_form_basic', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('CbmeYH4fFTZf7mduksS', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 02:40:36', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('ceaOjHFEJtIjdLuLbZf', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 03:00:07', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('CGICjOGYKJCk0VhAjXw', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 08:08:48', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('chf1thx0sjf8dQ64NBC', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-13 10:30:20', '更新表单', '__GD8U7ledIDdqSex1aw1', '__zlfY3N4sZj7uqyt0qkX', 'counterpart_management', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Cj9LqakA5XKkrU5VIIQ', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 07:00:23', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('cKDWpeN1wT1lqucPzp3', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 06:49:22', '更新表单', '__ekGXAf28bxqZFnKUTyq', '__mwU54noqAjF1UBdrb1V', 'expense_contract_text', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('CLwDeU9x1SX7a4cqWWp', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:24:41', '更新表单', '__yd2UvBzMe4NvtnL8fWp', '__Tua0cKltwny7b1OTzYy', 'general_contract_approval', '表单', '更新控件:start_time[描述:\'合同开始日期\'->\'合同起始日期\'],end_time[描述:\'合同结束日期\'->\'合同终止日期\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('CMyfXEYONK0qEvKnVxa', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 08:09:12', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('cnxsUBGyJArAvSTHNyS', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 05:52:17', '新建表单', '__1F5mMirX3quzQ340SbQ', '__pHVX22ylJECnk1NeQ1A', 'invoicing_withdraw', '表单', 'invoicing_withdraw,控件[\'reason\',\'relateid\']', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('cnyIouNerA8XGq2RmAL', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 06:34:39', '更新表单', '__bjuatsuk2Hyiet4Z5k6', '__mwU54noqAjF1UBdrb1V', 'expense_contract_payment', '表单', '更新控件:settlement[描述:\'据实结算\'->\'结算类型\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('cQ14jzfF5FUTQsZ7O9W', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-13 10:30:01', '更新表单', '__GD8U7ledIDdqSex1aw1', '__zlfY3N4sZj7uqyt0qkX', 'counterpart_management', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('cQsToW1uTvJyRrYhYf8', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-21 02:33:25', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('ctCiLCU224eoYZMyFBd', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 13:49:58', '更新表单', '__SbbmE0b6nRPmCY8KXZZ', '__2kwCPuPXiBbAPyuNCaF', 'agreement_detail_info', '表单', '表单:[描述:\'合作协议呈批表-详细信息\'->\'合作类合同呈批表-详细信息\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('cVt3hv0nVzuDyDZHYsq', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-16 02:26:58', '更新表单', '__bjuatsuk2Hyiet4Z5k6', '__mwU54noqAjF1UBdrb1V', 'expense_contract_payment', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('CwAm5pP9MnrIFYksJhE', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 08:24:55', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('cXqhjLSu3ZF0nTogmEs', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 06:46:46', '更新表单', '__wv5qf8n1vfnhQBP9i4S', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_payment', '表单', '更新控件:settlement[显示:\'只读\'->\'普通\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('cyRRDqCkMQHzfuhdXc8', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-10 08:41:56', '更新表单', '__csNl7TtjU9NiaT1ucmj', '__zlfY3N4sZj7uqyt0qkX', 'basic_info', '表单', '新增控件[\'带入\']\r更新控件:invoice_header[重计算:\'false\'->\'true\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('cZbEum81Yz5WafPhDHu', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 06:54:27', '更新表单', '__ekGXAf28bxqZFnKUTyq', '__mwU54noqAjF1UBdrb1V', 'expense_contract_text', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('D09aisK6Ub5WqlooOAa', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 03:03:41', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('D0bK0mkzIj1xvHvzISP', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 08:17:15', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('d0kiC98A5RcqXVbrYOh', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 03:08:47', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('d0x3dqI1wuaFM9NtANb', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-10 07:56:52', '更新表单', '__csNl7TtjU9NiaT1ucmj', '__zlfY3N4sZj7uqyt0qkX', 'basic_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('d3yZ0J9yUKEqyH8IUkV', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-09 06:31:38', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('D6l5B2grrBqORQbwoLe', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 07:52:43', '更新表单', '__eMGGuhtm2iy4vrBbVfP', '__mwU54noqAjF1UBdrb1V', 'counterpart_detail', '表单', '更新控件:counterpart_number[显示:\'普通\'->\'只读\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('D8Ega7jXjq4xcEZarrS', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 06:11:01', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('dbT5EikCEylwNq4ShSN', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-13 08:02:30', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('dDvAjsoR6e3dXHwrarx', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-09 07:13:04', '更新表单', '__CxqBJSNgHCZXOW082KZ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('DKMLBSoOOT2QzPKHphA', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 05:47:00', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('dMCPzljIdGf5q1oe45C', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-07 02:12:10', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('dnEKIDkMbTda4JidRte', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 10:36:19', '更新表单', '__vVNU6mDJ1aDK8Aav4gm', '__Tua0cKltwny7b1OTzYy', 'general_contract_detail_info', '表单', '更新控件:expiration_description[描述:\'合同到期说明\'->\'合同倒签订说明\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('dNYzpaVMKFyNbKE5jt9', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 02:25:43', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '删除控件[deputy_dept_head]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('DpsILyCtZAhSSZeld94', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-13 11:53:39', '更新表单', '__MPD9VyFPfx9pMhwwoeu', '__9Dddik4WtgZ2z0uGrwr', 'contract_change', '表单', '删除控件[变更前明细条款,变更后明细条款]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('dq17viM1SLGsvwizbLO', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 07:32:44', '更新表单', '__vVNU6mDJ1aDK8Aav4gm', '__Tua0cKltwny7b1OTzYy', 'general_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('dq8LISBe7gKtqP7MHG7', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 06:21:05', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '更新控件:upload[重计算:\'false\'->\'true\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('DrRD6fMBKSz4xQq3A0I', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-02 09:57:48', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '更新控件:counterpart[显示:\'只读\'->\'普通\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('dtGsLztfRPLp4MUST2D', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 05:52:37', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '新增控件[\'contract_link\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('DURZMB1SrDm7XppvRxN', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-06 06:00:23', '更新表单', '__CxqBJSNgHCZXOW082KZ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_detail_info', '表单', '更新控件:counterpart[显示:\'普通\'->\'只读\'],counterpart_legal_person[显示:\'普通\'->\'只读\'],counterpart_address[显示:\'普通\'->\'只读\'],bank[显示:\'普通\'->\'只读\'],bank_number[显示:\'普通\'->\'只读\'],sign_leader[显示:\'普通\'->\'只读\'],contact_info[显示:\'普通\'->\'只读\'],identification[显示:\'普通\'->\'只读\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('dWvXf0Vk26iCpbYqE8k', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:22:58', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('DwxNKuTSCwDxYXO8XYg', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 07:25:03', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '删除控件[cs]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('dxcO4p3fTA8bNFEc92j', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 06:11:49', '更新表单', '__SbbmE0b6nRPmCY8KXZZ', '__2kwCPuPXiBbAPyuNCaF', 'agreement_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('dXfE4XmLSMC7DrlEuGL', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-28 02:17:31', '更新表单', '__rFqfTy8Gx6HF0EmAiun', '__UyItLTLUHT2WJRP9tP1', 'litigation_ledger', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('dxu8iCSaNJV9agMbXx9', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-09 05:41:29', '更新表单', '__csNl7TtjU9NiaT1ucmj', '__zlfY3N4sZj7uqyt0qkX', 'basic_info', '表单', '新增控件[\'must7\',\'must8\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('dYOn4nSk9GmzLwHNnCW', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-28 01:58:43', '更新表单', '__PX2ukhQA9L2styARWJj', '__UyItLTLUHT2WJRP9tP1', 'routine_litigation', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('DzIUceT1plNFvk54cQH', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-02 11:33:10', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('e33AYjMcKamnvuyfI7g', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-07 11:45:09', '更新表单', '__vVNU6mDJ1aDK8Aav4gm', '__Tua0cKltwny7b1OTzYy', 'general_contract_detail_info', '表单', '更新控件:重置[标签:\'\'->\'取消\'],重置2[标签:\'重置\'->\'取消\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('E54GiG1AgL3fXDI28tw', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 13:49:36', '更新表单', '__yd2UvBzMe4NvtnL8fWp', '__Tua0cKltwny7b1OTzYy', 'general_contract_approval', '表单', '表单:[描述:\'通用合同呈批表\'->\'其他类合同呈批表\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('E6Qwp4vjNM4A84CFHgt', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-06 07:18:00', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('ecq9hz6tZKBzqQPvyoS', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-15 10:41:25', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('EfURPwTk1jgzHUfHbhE', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-07 11:30:14', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '更新控件:重置2[手机可用:\'true\'->\'false\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('eGJ31e1rhwrg2q1rQiu', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-13 09:52:49', '更新表单', '__AT5KHLX1JxOb2B8Vv6H', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_text', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('eIpjASfF3vzliBIeOtU', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 03:27:41', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('EjpuA0zMubU6mt8MoYr', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-29 07:29:27', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('ekbci6gCI3MiWpPVpeE', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-13 09:46:53', '更新表单', '__ekGXAf28bxqZFnKUTyq', '__mwU54noqAjF1UBdrb1V', 'expense_contract_text', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('EKoyz6je2zMF8T7m5hB', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 06:54:37', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('eLot0ifjDnlxcjY0DLy', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 10:34:52', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('eMMBtiBOA21cxRElBrm', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 10:25:35', '更新表单', '__hXoOzD8ajWJgWrzmaUo', '__hneTQUEoxzpYNqCrYlE', 'expense_contract_form_basic', '表单', '更新控件:bank[焦点切换键:\'\'->\'Tabkey\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('eNkuf96VqnREDPaX68H', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-09 01:59:55', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('EP2H9aYVO3vYWr5Phl7', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-07 09:14:03', '更新表单', '__yd2UvBzMe4NvtnL8fWp', '__Tua0cKltwny7b1OTzYy', 'general_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('eq3ePs8Kb60JntpiFqK', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-06 07:18:11', '更新表单', '__yd2UvBzMe4NvtnL8fWp', '__Tua0cKltwny7b1OTzYy', 'general_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('EQ4cjRfjnbRP0scf9Tx', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 10:46:31', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '更新控件:expiration_description[描述:\'合同到期说明\'->\'合同倒签订说明\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('EQjkImsjlcb9N7EU6JZ', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 06:00:07', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('EtembgOkhTcvIpMANHg', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 06:50:19', '更新表单', '__CxqBJSNgHCZXOW082KZ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Ew2NfRrY963yROd5BSg', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 09:45:49', '更新表单', '__hXoOzD8ajWJgWrzmaUo', '__hneTQUEoxzpYNqCrYlE', 'expense_contract_form_basic', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('EwToxJP0r3UfmQOoTxu', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 06:37:23', '更新表单', '__wv5qf8n1vfnhQBP9i4S', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_payment', '表单', '更新控件:settlement[重计算:\'false\'->\'true\',描述:\'据实结算\'->\'结算类型\',显示:\'普通\'->\'只读\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('F5EOck9mwOgmmuiNoNw', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 08:11:11', '更新表单', '__SbbmE0b6nRPmCY8KXZZ', '__2kwCPuPXiBbAPyuNCaF', 'agreement_detail_info', '表单', '新增控件[\'center_director\',\'center_director_attitude\',\'center_director_date\',\'zg_leader\',\'zg_leader_attitude\',\'zg_leader_date\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('f6IIdBS3ajD9VUZ7tYo', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-10 08:45:26', '更新表单', '__csNl7TtjU9NiaT1ucmj', '__zlfY3N4sZj7uqyt0qkX', 'basic_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('f6xx8WrlHGBKFdVo0Bc', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-07 09:12:49', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('f7oLR1509tekcXs5A1Z', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:21:43', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('fAdH7WNlIYolicRhRm8', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-07 11:39:23', '更新表单', '__CxqBJSNgHCZXOW082KZ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_detail_info', '表单', '新增控件[\'重置\',\'重置2\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('fAj1Rwi9NGGwU7tJ7dQ', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-03 14:56:39', '更新表单', '__eMGGuhtm2iy4vrBbVfP', '__mwU54noqAjF1UBdrb1V', 'counterpart_detail', '表单', '新增控件[\'invoice_header\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('faYoEJhUPfTzod0nuFq', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 08:21:04', '更新表单', '__PX2ukhQA9L2styARWJj', '__UyItLTLUHT2WJRP9tP1', 'routine_litigation', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('FBLDS0Xq2ETwNnjfyDK', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-07 11:44:43', '更新表单', '__CxqBJSNgHCZXOW082KZ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_detail_info', '表单', '更新控件:重置[标签:\'\'->\'取消\'],重置2[标签:\'重置\'->\'取消\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('FF8ZunN5968jyezWbHy', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-05 09:23:35', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('FFgC08RYfRKdxkUTTWo', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 08:00:46', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('ffVckNrtHUDpRRYyXE5', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 02:33:10', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('FglQpX7lWa2mHSW9X2K', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-09 07:11:40', '更新表单', '__wv5qf8n1vfnhQBP9i4S', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_payment', '表单', '新增控件[\'settlement\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('FHX9oqt1UlLY456WMaZ', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-03 14:58:40', '更新表单', '__eMGGuhtm2iy4vrBbVfP', '__mwU54noqAjF1UBdrb1V', 'counterpart_detail', '表单', '新增控件[\'invoice_type\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('FknouHVhhu1PDvvWbiL', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 10:23:22', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('FnhndhbQ3hc7kVz04PI', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 10:25:02', '更新表单', '__yd2UvBzMe4NvtnL8fWp', '__Tua0cKltwny7b1OTzYy', 'general_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('fo4A6yREYJaf06pvS63', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-10 08:13:09', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('forEBTAiTP8lO97EcaG', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-10 15:46:19', '更新表单', '__yd2UvBzMe4NvtnL8fWp', '__Tua0cKltwny7b1OTzYy', 'general_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('fQLC51sN6MfxJE8hdhl', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 08:45:44', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('FrhRTqRjqMjKYXXnp6r', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-10 16:38:20', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('fSlOouPNuIyYQjcHJYc', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-10 16:23:20', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('fsyVah9MAQZkqIrWh5p', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:21:47', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '更新控件:start_time[描述:\'合同开始时间\'->\'合同起始日期\'],end_time[描述:\'合同结束时间\'->\'合同终止日期\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('fUAxrq71QL6wHSdBZTi', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-06 05:46:31', '更新表单', '__tDBoOxFqod4sv9TEDXH', '__hneTQUEoxzpYNqCrYlE', 'my_contract', '表单', '删除控件[合同总数,合同总金额,履行中合同总金额,解除合同总金额,支出类合同]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('fUOthK87Dt3hAahBLkp', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-02 11:33:39', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('FvBtrqHLYqJX0JEo6fP', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 10:40:10', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('fVJ37V2lIjXyBsTYL10', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-13 08:00:59', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('fW4NhLepKJ6K6GPGMsx', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-07 02:07:17', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('fxpoFv1gIjJsWFwv8Af', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 08:13:20', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Fyi1XHQuVaGrN98FTsz', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-07 11:32:18', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('G0zlPVlGsIxAb6NX04s', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 10:41:48', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('g3Dtia4kVAiOhSrTvky', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:01:21', '更新表单', '__vVNU6mDJ1aDK8Aav4gm', '__Tua0cKltwny7b1OTzYy', 'general_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('g3JeK80jCgciIbC8ghU', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-11 01:32:53', '更新表单', '__csNl7TtjU9NiaT1ucmj', '__zlfY3N4sZj7uqyt0qkX', 'basic_info', '表单', '删除控件[带入]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('g73jFoU1K6Bp3wUM5oh', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-07 09:11:12', '更新表单', '__CxqBJSNgHCZXOW082KZ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_detail_info', '表单', '删除控件[center_director,center_director_attitude,center_director_date,zg_leader,zg_leader_attitude,zg_leader_date]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('GAnn89OUpxqneXCJnCw', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-23 06:20:28', '更新表单', '__SbbmE0b6nRPmCY8KXZZ', '__2kwCPuPXiBbAPyuNCaF', 'agreement_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('gaoZLvKFi9gkeCOVgMB', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 06:11:40', '更新表单', '__CxqBJSNgHCZXOW082KZ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('gbKlAwTFmGWEKEPL0mG', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-23 06:09:52', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '新增控件[\'expiration_description\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('gCYiQw1fEY1djbCXR0k', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 06:15:11', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('gF32Y8qkHcocUxn7j74', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 06:19:20', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('gfrB4H3YqqHqELTg0TI', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 06:46:18', '更新表单', '__inZ4Rf1o4l1zhdJIkAk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_payment', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('GGTVizDXhQ5trpqvVdW', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-29 08:47:45', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('GH8WqYKmAwAxO2u15vj', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-10 15:39:27', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('gHquFL2m5KG88mDJUEv', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-05 10:06:32', '更新表单', '__2mEMWhShJw6mjoCQJot', '__QXvwZwdn5T4aclQj14P', 'collection_record', '表单', '新增控件[\'registrant\',\'registration_date\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('GHxZdNgNsGNkBSg5hui', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 02:36:08', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('GIABfoqJKCrNOmyRzgx', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 05:51:28', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('gICdML7yxloGxtftOMm', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 06:59:12', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('gJwBSfpFytDi8grjFTN', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 05:46:09', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('GkEaIluDTt4BSY0UhFp', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 06:33:34', '更新表单', '__inZ4Rf1o4l1zhdJIkAk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_payment', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('gMp5hcaZ9WaHlVLPhxl', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-05 07:30:55', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('gmy27zvzuSkwe5TI5D0', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 06:02:37', '新建表单', '__5eBf40oS60oqJaHeGNK', '__8tbV9gKZXznok4jo995', 'counter', '表单', 'counter,控件[\'name\',\'counter\']', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('gnOyZNtYDrnAZ7191tB', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-05 09:19:58', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('gORFrj4KbZQqaK3E4Fz', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-21 02:33:04', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('gPKgfa1YG9Kzs5aV5Jf', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-21 02:31:18', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('gQ2LUNU6NOdoIofG9Ei', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-21 02:31:10', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('GrrLwnHrJpnDcaGbHWD', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-07 09:13:05', '更新表单', '__vVNU6mDJ1aDK8Aav4gm', '__Tua0cKltwny7b1OTzYy', 'general_contract_detail_info', '表单', '删除控件[center_director,center_director_attitude,center_director_date,zg_leader,zg_leader_attitude,zg_leader_date]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('GTPytzothNGqbi54iYP', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 09:16:28', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('gwk3IJyoHPVldw4vxvc', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 08:25:45', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('GyTsutQjAJe6EiySfDM', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 07:40:03', '更新表单', '__xCMgHhAWD4ueSOlU0Q6', '__z1fcq82tLWSqTuTsPFC', 'flow_setting', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('H4qCjRc2HhoIrhzebYc', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-06 07:03:24', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '新增控件[\'payer\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('h5EETOYClE1OBpa8Uj1', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-08 10:38:39', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('H6TtEK12NANQkQimXg0', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 03:02:30', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('h7T0XmIJbHy7MZsfcIV', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-05 07:31:35', '更新表单', '__vVNU6mDJ1aDK8Aav4gm', '__Tua0cKltwny7b1OTzYy', 'general_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('h8lgFprKJC3aPgbXJGk', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-09 02:04:08', '更新表单', '__2mEMWhShJw6mjoCQJot', '__QXvwZwdn5T4aclQj14P', 'collection_record', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('hA2wHpaZCiIufJKJHgt', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 13:51:44', '更新表单', '__Q0G9HYUmB73T6ANahXJ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_text', '表单', '表单:[描述:\'分成合同呈批表-正文\'->\'分成类合同呈批表-正文\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('HCbXldVyF1CR1Le2jpT', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-15 10:35:53', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('hCMyONArWvP2wSOxPbm', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-06 06:01:34', '更新表单', '__vVNU6mDJ1aDK8Aav4gm', '__Tua0cKltwny7b1OTzYy', 'general_contract_detail_info', '表单', '更新控件:counterpart[显示:\'普通\'->\'只读\'],counterpart_legal_person[显示:\'普通\'->\'只读\'],counterpart_address[显示:\'普通\'->\'只读\'],bank[显示:\'普通\'->\'只读\'],bank_number[显示:\'普通\'->\'只读\'],sign_leader[显示:\'普通\'->\'只读\'],contact_info[显示:\'普通\'->\'只读\'],identification[显示:\'普通\'->\'只读\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('HgWcGv9u1Ea7rje8ulS', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-13 09:52:05', '更新表单', '__AT5KHLX1JxOb2B8Vv6H', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_text', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('hhtOJ9Z69EFjYeRoqb7', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 03:23:05', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('hJdhPjhisiUywQMSAof', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-05 07:32:11', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('hJlrd3hMCLUAS3ApY2t', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-07 09:14:00', '更新表单', '__vVNU6mDJ1aDK8Aav4gm', '__Tua0cKltwny7b1OTzYy', 'general_contract_detail_info', '表单', '新增控件[\'apply_person_date\',\'dept_head_date\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('HjVu3CcJxjZsovQ0FNu', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 08:18:07', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('HkBuCw9iCaAykCD4WN4', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 06:49:21', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('HKL6X6MAc77WO84DAGp', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 09:17:27', '更新表单', '__vVNU6mDJ1aDK8Aav4gm', '__Tua0cKltwny7b1OTzYy', 'general_contract_detail_info', '表单', '新增控件[\'相对方信息\']\r更新控件:counterpart[重计算:\'false\'->\'true\']\r删除控件[counterpart_number,counterpart_legal_person,counterpart_type,counterpart_address,bank,bank_number,sign_leader,contact_info,identification]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('HLRdvR9k9pfnrCBGzYn', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 07:51:54', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '新增控件[\'center_director\',\'center_director_attitude\',\'center_director_date\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('hoH5XdAdxrJy3Ald7jV', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 10:28:40', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('hpKnjLr7e57G0s4KyNV', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 06:34:25', '更新表单', '__inZ4Rf1o4l1zhdJIkAk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_payment', '表单', '更新控件:settlement[描述:\'据实结算\'->\'结算类型\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('HqwsADPamG6pPlMZktR', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 08:24:40', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('hWfvQRpvhEoNvoZWYsz', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 13:43:30', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('HWrQjsFByggybjflChv', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-20 03:46:17', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('hxHudrZb5mStqjkd7F7', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 06:44:46', '更新表单', '__bjuatsuk2Hyiet4Z5k6', '__mwU54noqAjF1UBdrb1V', 'expense_contract_payment', '表单', '更新控件:settlement[显示:\'只读\'->\'普通\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('HXPNCwVBL4Gjiamg7Hk', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 02:51:45', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('hzJUZEu8VpW2kvpjZDp', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-10 16:38:29', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('hZPgAXEmSttYZWtlWkE', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:23:57', '更新表单', '__SbbmE0b6nRPmCY8KXZZ', '__2kwCPuPXiBbAPyuNCaF', 'agreement_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('I3jWSWjUVtMBOmfIW2P', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 03:28:44', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('i84Akvw9WAxqGHOugzg', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 03:04:53', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('IaCLxMrjD2pJf3C6Uzj', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 10:13:20', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('ibB2KPPhvhowPHGauHa', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-19 02:18:40', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('icl00BDT3AKPO2wfBib', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-13 08:00:13', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Id2eU99ih8xySsp0ewj', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:25:08', '更新表单', '__csNl7TtjU9NiaT1ucmj', '__zlfY3N4sZj7uqyt0qkX', 'basic_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('id9LZUKHVeAr17lYehY', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 13:30:10', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '更新控件:dept_head[显示:\'只读\'->\'普通\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('iDnHk8xY0D6OTzEEwN3', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-20 03:45:42', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('IdzRfJsKvHlAxcri8I6', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 09:43:29', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('IedIdX6vN3TrJJKaY7N', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 08:47:19', '更新表单', '__yd2UvBzMe4NvtnL8fWp', '__Tua0cKltwny7b1OTzYy', 'general_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('iFxjdqws87oPcmrN1OT', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-28 02:16:28', '更新表单', '__qwo0CY05xSdyRB6uI3n', '__UyItLTLUHT2WJRP9tP1', 'other_disputes', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('ihi0zmjpAsPMMXHsfcn', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 07:24:11', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '更新控件:dept_head[重计算:\'false\'->\'true\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('IK5ce2sf5D0iSahCQ1S', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-09 07:12:34', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('IkaJpdvnJp2GqQbdgYB', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-10 07:59:52', '更新表单', '__csNl7TtjU9NiaT1ucmj', '__zlfY3N4sZj7uqyt0qkX', 'basic_info', '表单', '新增控件[\'must5\',\'must6\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('iKcgobQFCRTUuc2nz5w', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 05:55:21', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('IL6hd60kw2tvRrJWPBO', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:24:00', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '更新控件:start_time[描述:\'合同签约日期\'->\'合同起始日期\'],end_time[描述:\'协议失效时间\'->\'合同终止日期\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('IMRGAlpGk9VZX4tefIP', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 07:14:41', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('imWwfLLzCaxGuVTt5AR', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:00:48', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('iOXDDCkfuR1yHT41kxa', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 06:43:35', '更新表单', '__inZ4Rf1o4l1zhdJIkAk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_payment', '表单', '更新控件:settlement[显示:\'只读\'->\'普通\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('IPqCjaZo5V7I0YkHhf2', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-05 07:31:58', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('IuZPq46n5wMEhQfHxiv', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-03 15:28:54', '更新表单', '__NgXWRPuKStY5nygCkmL', '__WOgFyT9N1Dmtrzag3hW', 'invoice_form', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('ivjRSL3ptZ6lGtNoXMW', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-29 07:04:02', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('IwCANVrT0o85flTzps5', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-07 07:29:27', '更新表单', '__GD8U7ledIDdqSex1aw1', '__zlfY3N4sZj7uqyt0qkX', 'counterpart_management', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('iWGqsMRrjEdyT65oJFL', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-05 07:50:04', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('iWp7kU8R3DNqHP4lfNT', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 08:39:34', '更新表单', '__2mEMWhShJw6mjoCQJot', '__QXvwZwdn5T4aclQj14P', 'collection_record', '表单', '删除控件[bank]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('iwz1QWGNjUaDjI2ymKK', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-25 02:50:23', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '更新控件:contract_number[描述:\'合同编号\'->\'合同备案号\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('IZh4LxXyVxnVb1Z7HLo', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 03:29:21', '更新表单', '__rH9L9naFweQwnFhsqt4', '__yV15cDXVn1xnPHSBvcT', 'payment_withdraw', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('iZJoMnaqlJcyZ6cd8Gy', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 07:04:12', '更新表单', '__ekGXAf28bxqZFnKUTyq', '__mwU54noqAjF1UBdrb1V', 'expense_contract_text', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('IzWvsoirjKkpXOfhUKL', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:45:03', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('J0nAVAjFD6BbAVGTeoL', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 05:44:55', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('J1FPC6BDUJaoCQ0w9lg', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 07:40:54', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('j1Pjb82NgWWLTOSvywb', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 10:28:52', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '更新控件:bank[焦点切换键:\'\'->\'Tabkey\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('J2Nq3n0TzLZ7l1iuxRl', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-21 02:31:49', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('j4ClJbfq7OVB9UGb9B4', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-23 06:20:59', '更新表单', '__vVNU6mDJ1aDK8Aav4gm', '__Tua0cKltwny7b1OTzYy', 'general_contract_detail_info', '表单', '新增控件[\'expiration_description\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('J98i6G66mtZxYDLRDhc', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-07 07:27:27', '更新表单', '__GD8U7ledIDdqSex1aw1', '__zlfY3N4sZj7uqyt0qkX', 'counterpart_management', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Ja4DI98A1bshNvJ2V0V', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 02:49:28', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('jFqMymSuELG60DAKRHh', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 09:35:39', '更新表单', '__wqeHK6Z51KmsrUBZhpw', '__hneTQUEoxzpYNqCrYlE', 'revenue_contract_form_basic', '表单', '新增控件[\'相对方信息\']\r删除控件[counterpart,counterpart_legal_person,counterpart_number,contacts,contact_info,address,bank,bank_number]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('jH1uGPGLnbTLvdOPT5d', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 08:57:51', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('JIvm8PtqzYhS4WIpMFK', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 05:46:41', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('jIXmYBYRGW9Xcq6EqIr', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 03:25:27', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('JKpRyxaduyNorTHTTta', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 10:24:16', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('jNvY5Lc0zwWThQXwVwO', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 02:52:42', '更新表单', '__SbbmE0b6nRPmCY8KXZZ', '__2kwCPuPXiBbAPyuNCaF', 'agreement_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('JTCDnsnRBZCq5VjkTgM', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-05 07:50:36', '更新表单', '__lSbiziySPDXouxVboVF', '__hneTQUEoxzpYNqCrYlE', 'general_contract_form_basic', '表单', '表单:[描述:\'通用合同卡片-基本信息\'->\'其他类合同卡片-基本信息\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('juP9QpGCAuNxkpRMarH', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 08:29:27', '更新表单', '__csNl7TtjU9NiaT1ucmj', '__zlfY3N4sZj7uqyt0qkX', 'basic_info', '表单', '更新控件:legal_representative[描述:\'法定代表\'->\'法定代表人\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Jx8rVmH8hPaHAfodqp4', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-07 11:43:05', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '更新控件:重置[名称:\'重置\'->\'取消\'],重置2[标签:\'重置\'->\'取消\',名称:\'重置2\'->\'取消2\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('jXcXgeRbQ5uth1YeA1F', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-07 09:12:46', '更新表单', '__SbbmE0b6nRPmCY8KXZZ', '__2kwCPuPXiBbAPyuNCaF', 'agreement_detail_info', '表单', '新增控件[\'apply_person_date\',\'dept_head_date\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Jxn1rajxIvijV47c7lE', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-13 07:57:12', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('jy6dtLeATG0kPnfsmdt', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 09:09:12', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('JyLTvzFmPW7elbYJ9pg', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-21 02:23:24', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('jzidvtVaGyNMzN6swZb', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 02:53:26', '更新表单', '__vVNU6mDJ1aDK8Aav4gm', '__Tua0cKltwny7b1OTzYy', 'general_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('K0xV5iipFzIfCRJkiT4', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-23 06:21:13', '更新表单', '__vVNU6mDJ1aDK8Aav4gm', '__Tua0cKltwny7b1OTzYy', 'general_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('K4Iqs6iRUIqKfcslCox', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 05:54:19', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('k6YRAG6dSuOqU6R6FmD', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-09 07:12:52', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('k7HG0cixRGe1pNInG5F', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 08:06:29', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('K8HqcyHkXnJaWm9m44r', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 07:17:34', '更新表单', '__ekGXAf28bxqZFnKUTyq', '__mwU54noqAjF1UBdrb1V', 'expense_contract_text', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('kamUZVFlcR5cCPg8nUN', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 07:26:40', '更新表单', '__ekGXAf28bxqZFnKUTyq', '__mwU54noqAjF1UBdrb1V', 'expense_contract_text', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('kB0vUkyfw16crp6P4zh', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-03 15:12:59', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('kBRnhLPPSuPSQh6xIRk', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-20 03:43:17', '更新表单', '__2pMIIpKj9axWSDoqNTk', '__mwU54noqAjF1UBdrb1V', 'contract_withdraw', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('kBwR79LJkMN5ZmoDV4K', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:49:44', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('kc7fR5Rrv7hGHwtJZgt', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-10 08:47:46', '更新表单', '__csNl7TtjU9NiaT1ucmj', '__zlfY3N4sZj7uqyt0qkX', 'basic_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('KCcVu49OaQ1y2aY74pI', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-28 02:12:33', '更新表单', '__qwo0CY05xSdyRB6uI3n', '__UyItLTLUHT2WJRP9tP1', 'other_disputes', '表单', '新增控件[\'case_status\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('kdJa8ge0SXUzovBmMnl', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 07:46:13', '删除表单', '__xCMgHhAWD4ueSOlU0Q6', '__z1fcq82tLWSqTuTsPFC', 'flow_setting', '表单', 'flow_setting', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('kDLVf91TWPsZMK5zHPB', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-10 08:44:23', '更新表单', '__csNl7TtjU9NiaT1ucmj', '__zlfY3N4sZj7uqyt0qkX', 'basic_info', '表单', '更新控件:invoice_header[宽度:\'100\'->\'0\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Kgq1TVefpvEfLUj83te', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 09:36:42', '更新表单', '__lSbiziySPDXouxVboVF', '__hneTQUEoxzpYNqCrYlE', 'general_contract_form_basic', '表单', '新增控件[\'相对方信息\']\r删除控件[counterpart,counterpart_legal_person,counterpart_number,contacts,contact_info,address,bank,bank_number]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('KhMcOUYhBpcvUSGK4mt', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 06:55:24', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('kIjbSolZoCI2v85Vsnu', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 09:07:06', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('kIPSddNS6Mjkh8OiLHf', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 07:00:15', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Kj3AGFhSmQB7OimUmcv', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-07 11:41:50', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('koJqDI6Skhw51B4LkPH', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:24:38', '更新表单', '__vVNU6mDJ1aDK8Aav4gm', '__Tua0cKltwny7b1OTzYy', 'general_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('ktKfLcpaNedxEwcJVSx', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-03 15:28:34', '更新表单', '__NgXWRPuKStY5nygCkmL', '__WOgFyT9N1Dmtrzag3hW', 'invoice_form', '表单', '新增控件[\'选择相对方\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Ku94nVFgtphp3ktjmtw', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 10:34:36', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '更新控件:expiration_description[描述:\'合同到期说明\'->\'合同倒签订说明\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('KuOHp0EvONJThI3j1YY', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 10:46:54', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '更新控件:expiration_description[描述:\'合同到期说明\'->\'合同倒签订说明\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('kUsHMvHEuqozQlCmYTj', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 08:11:34', '更新表单', '__yd2UvBzMe4NvtnL8fWp', '__Tua0cKltwny7b1OTzYy', 'general_contract_approval', '表单', '新增控件[\'center_director\',\'center_director_attitude\',\'center_director_date\',\'zg_leader\',\'zg_leader_attitude\',\'zg_leader_date\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('KvrwqmdREUO2Aw9DdJb', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 02:28:45', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Kvrzwp87uptyX4Q3YE3', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-05 10:07:07', '更新表单', '__2mEMWhShJw6mjoCQJot', '__QXvwZwdn5T4aclQj14P', 'collection_record', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('KVYl8sAIpK8FwZEZuMc', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-07 02:17:53', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('kyxcb7NXOAikoztcd6V', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-13 08:00:01', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('L20GWBxhhQWJPGHFeLt', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-15 10:36:11', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('l59F0xlpG4CU4wJp1JG', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 08:08:40', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('L6a9xnwStk5R9Cl6fJT', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 09:35:36', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('LApCUeKmlLTuPFprsKn', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 06:54:32', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('LazzX3hKTtBTb7c6Mqi', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-07 09:11:49', '更新表单', '__CxqBJSNgHCZXOW082KZ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_detail_info', '表单', '新增控件[\'apply_person_date\',\'dept_head_date\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('lDA619rC4lVmBit0WH5', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 10:27:19', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '更新控件:bank[焦点切换键:\'\'->\'Tabkey\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('leFphfCyI5KiCasoa0Q', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-15 10:34:53', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('lex1B4j6I4ziK7ky6EJ', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 03:28:08', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('lGHPufSwkLyzoq8qXZ8', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-07 11:37:09', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '新增控件[\'重置\',\'重置2\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('LI3GhZYenrkuQkYW4rp', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-15 10:41:50', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('ligUge623KEAeIuGSVv', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 09:59:56', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '新增控件[\'relateid\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Liss12R4XHOYOPfPcmm', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 13:30:35', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '更新控件:dept_head[显示:\'普通\'->\'只读\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('lj4mFJ3ZOuudkKilsgT', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-10 08:43:59', '更新表单', '__csNl7TtjU9NiaT1ucmj', '__zlfY3N4sZj7uqyt0qkX', 'basic_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('lJ9Yu7IPOduighosHvK', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:23:01', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('LjE2SwSFcGQB7S3RVRQ', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 05:59:06', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('lKUsAt5tyOjtaHWx5tr', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-15 10:42:27', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('LkVmu9KfbzCv2F1u0DU', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 13:39:25', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('LLvV1dRna9wp20wGdT0', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-19 02:13:58', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('LM7D3hWMYDkVY6I27L3', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 09:09:54', '更新表单', '__MPD9VyFPfx9pMhwwoeu', '__9Dddik4WtgZ2z0uGrwr', 'contract_change', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('lmQr5i7pNwsmHeQxJ6Y', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 03:04:16', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('loFWHbCe4SoAcHfIcn9', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 07:32:41', '更新表单', '__36EXzd2IkVoJJgnaOnE', '__2kwCPuPXiBbAPyuNCaF', 'agreement_text', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('lrRWdRsmyWTp9xAxibo', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 07:47:24', '新建表单', '__eMGGuhtm2iy4vrBbVfP', '__mwU54noqAjF1UBdrb1V', 'counterpart_detail', '表单', 'counterpart_detail,控件[\'payment_type\',\'payment_terms\',\'payment_ratio\',\'payment_amount\',\'settlement\',\'description\']', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('LTGQsridWJsvQVJt50X', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 08:10:51', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('lvbaUPUtIYC4cjAfzcJ', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 10:37:43', '更新表单', '__yd2UvBzMe4NvtnL8fWp', '__Tua0cKltwny7b1OTzYy', 'general_contract_approval', '表单', '新增控件[\'identification\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('lZChLJxEovMYy3VKek1', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-21 02:26:43', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('M2Cmm4yvDsjEnBJ8JEk', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-29 08:38:52', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('M40n2lwuOHpqbiTnDp2', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 03:04:48', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('M5G8ojOFwuYV44AJmBx', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 08:08:35', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('M5vCnR5NsaTi5jDf6mZ', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 06:31:22', '更新表单', '__bjuatsuk2Hyiet4Z5k6', '__mwU54noqAjF1UBdrb1V', 'expense_contract_payment', '表单', '更新控件:settlement[重计算:\'false\'->\'true\',显示:\'普通\'->\'只读\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('m6YU2OBohZ3l6vShgc4', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-13 08:10:34', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('M8bMq968F2zpkBbWmbk', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-09 07:13:30', '更新表单', '__vVNU6mDJ1aDK8Aav4gm', '__Tua0cKltwny7b1OTzYy', 'general_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('mD81mY64dZ2zAUauXxI', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 13:51:16', '更新表单', '__AT5KHLX1JxOb2B8Vv6H', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_text', '表单', '表单:[描述:\'收入合同呈批表-正文\'->\'收入类合同呈批表-正文\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('MFGSQ0DyiwsoGu3IcJO', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 07:53:52', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '新增控件[\'center_director\',\'center_director_attitude\',\'center_director_date\',\'zg_leader\',\'zg_leader_attitude\',\'zg_leader_date\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('MFkbdIUgcDU81d6qY9t', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 06:59:46', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('mfmEFQS6Fn5mxKQVoCj', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-13 08:00:22', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('mhpxB0FjTG8YubWR8dH', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-09 07:10:44', '更新表单', '__bjuatsuk2Hyiet4Z5k6', '__mwU54noqAjF1UBdrb1V', 'expense_contract_payment', '表单', '新增控件[\'settlement\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('mioLngfHOwqdlgnIOp9', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-18 06:05:26', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('mjUUEp7YjZYFqYbD7K1', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 13:51:11', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '表单:[描述:\'收入合同呈批表-详细信息\'->\'收入类合同呈批表-详细信息\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('mKmnf6LHC7MDouGPYOk', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-03 15:00:08', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Ml75Khe5RBMq5cxch83', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 08:33:41', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '新增控件[\'相对方信息\']\r删除控件[counterpart,counterpart_number,bank,bank_number,sign_leader]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('MLkvJKTebLRLhg1Ar1L', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-29 08:47:32', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('MLmalGA1UstNXSrJFhu', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 09:34:50', '更新表单', '__hXoOzD8ajWJgWrzmaUo', '__hneTQUEoxzpYNqCrYlE', 'expense_contract_form_basic', '表单', '新增控件[\'相对方信息\']\r删除控件[counterpart,counterpart_legal_person,contacts,contact_info,counterpart_number,counterpart_type,address,bank,bank_number]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('MMYdElc8vGzeXZblHJx', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 08:06:32', '更新表单', '__hXoOzD8ajWJgWrzmaUo', '__hneTQUEoxzpYNqCrYlE', 'expense_contract_form_basic', '表单', '新增控件[\'contract_ref\']\r更新控件:contract_number[描述:\'合同编号\'->\'合同流水号\',显示:\'只读\'->\'普通\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('mn81L0upQRzuMqSKFvE', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 10:33:44', '更新表单', '__CxqBJSNgHCZXOW082KZ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Mo1ilqbAerVLCnY2wHn', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 05:53:44', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '新增控件[\'contract_link\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Mpav84Oyam85iL1cNKG', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-05 07:29:23', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('mPkUGuOrbFjTzOPM7lv', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-23 06:16:44', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('mPRHha7j5VdmobsLf8e', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:18:25', '更新表单', '__CxqBJSNgHCZXOW082KZ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_detail_info', '表单', '更新控件:start_time[描述:\'合同开始时间\'->\'合同起始日期\'],end_time[描述:\'合同结束时间\'->\'合同终止日期\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('mqsLm9MGE1Hqgmnmnqw', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-16 02:31:05', '更新表单', '__wv5qf8n1vfnhQBP9i4S', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_payment', '表单', '更新控件:payment_ratio[重计算:\'false\'->\'true\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('mS3uNm3zWOJsb8CJoJ0', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 10:12:35', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('muaR3Z1Ons11C7lIqie', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 07:53:08', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '更新控件:archiving_status[描述:\'归档状态\'->\'备案状态\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('muoyPDNbtGX0O3Lqcwt', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-05 09:15:07', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('mUT1F9uqdsukfsD2Hoo', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 10:40:13', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('MUwJPUUpv9XCQeG7a0d', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 10:35:26', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '新增控件[\'identification\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('mvjjLKw0U4HkZc2GmfC', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-18 09:35:23', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('MwLmxQq9vENiXw7nVxJ', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-10 16:17:10', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('mYerUBpiHcsl4QD6GAa', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-09 02:34:14', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('myv9IMtyuVoW83Cet3O', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-14 05:43:11', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('mYy6JpXLvqz1dl8D9Fo', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 03:30:08', '更新表单', '__GD8U7ledIDdqSex1aw1', '__zlfY3N4sZj7uqyt0qkX', 'counterpart_management', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('n00I5BNv5uVN8GXsmSx', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-23 06:09:48', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '新增控件[\'expiration_description\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('N03WxUweCiuy9eOE3EP', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 08:09:07', '更新表单', '__yd2UvBzMe4NvtnL8fWp', '__Tua0cKltwny7b1OTzYy', 'general_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('N0HUfEXXDac7P7nPK8U', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 08:07:27', '更新表单', '__wqeHK6Z51KmsrUBZhpw', '__hneTQUEoxzpYNqCrYlE', 'revenue_contract_form_basic', '表单', '新增控件[\'contract_ref\']\r更新控件:contract_number[描述:\'合同编号\'->\'合同流水号\',显示:\'只读\'->\'普通\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('n1xdtNkSGJdqA85Zczl', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-21 02:33:48', '更新表单', '__yd2UvBzMe4NvtnL8fWp', '__Tua0cKltwny7b1OTzYy', 'general_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('N3frqnHW81A7aS4MB8F', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-23 06:19:12', '更新表单', '__CxqBJSNgHCZXOW082KZ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_detail_info', '表单', '新增控件[\'expiration_description\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('N8dZUDpBcodWpzIK7ps', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-28 01:59:44', '更新表单', '__PX2ukhQA9L2styARWJj', '__UyItLTLUHT2WJRP9tP1', 'routine_litigation', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('n9uA5jqBNIByIyBoXnU', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 08:11:11', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '新增控件[\'相对方明细\']\r更新控件:选择供应商[名称:\'选择供应商\'->\'选择相对方\']\r删除控件[counterpart,counterpart_number,counterpart_legal_person,counterpart_type,counterpart_address,bank,bank_number,sign_leader,contact_info,identification]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('nA6css4AfI1EwFQ8tew', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-06 05:59:46', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '更新控件:counterpart[显示:\'普通\'->\'只读\'],counterpart_legal_person[显示:\'普通\'->\'只读\'],bank[显示:\'普通\'->\'只读\'],bank_number[显示:\'普通\'->\'只读\'],sign_leader[显示:\'普通\'->\'只读\'],contact_info[显示:\'普通\'->\'只读\'],identification[显示:\'普通\'->\'只读\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('NACcYKMTd6Uo8bApFOk', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 10:24:59', '更新表单', '__vVNU6mDJ1aDK8Aav4gm', '__Tua0cKltwny7b1OTzYy', 'general_contract_detail_info', '表单', '更新控件:bank[焦点切换键:\'\'->\'Tabkey\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Nava0KJezdBAWrvqsyt', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 07:30:11', '更新表单', '__ekGXAf28bxqZFnKUTyq', '__mwU54noqAjF1UBdrb1V', 'expense_contract_text', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('nBE9b7D4K0DmRX1Puvk', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-05 09:22:02', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('NBv8wG2YjJHjtI2EUDH', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 01:57:14', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('NcvNO6xFoLu76lMU0b1', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-23 06:13:51', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('nEXYN58VJrfXOd3pLfC', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-21 02:33:18', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Nf6PuKQweCJwjM4FFEL', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-28 01:57:49', '更新表单', '__PX2ukhQA9L2styARWJj', '__UyItLTLUHT2WJRP9tP1', 'routine_litigation', '表单', '新增控件[\'开庭日期\']\r删除控件[start_date]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('nGftE9huc8XosF0OZYu', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-23 06:10:15', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '更新控件:expiration_description[重计算:\'false\'->\'true\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('nHiwAHSjVBHJSOve6XO', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 08:08:13', '更新表单', '__5ULM7zqjguD6H42SjZe', '__hneTQUEoxzpYNqCrYlE', 'agreement_contract_form', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('njFGKj6GefomXCa189d', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-28 07:20:39', '更新表单', '__bjuatsuk2Hyiet4Z5k6', '__mwU54noqAjF1UBdrb1V', 'expense_contract_payment', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('NOnVFQ1bSzWCQnxBOR6', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-07 11:38:21', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('nu0w8JQAzyG6tevbyRB', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:00:34', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('NVOMkfY1HRlkbubmRlC', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 10:36:07', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '新增控件[\'identification\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('nVvCsQlhC2doyYWwCgU', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-28 07:21:03', '更新表单', '__inZ4Rf1o4l1zhdJIkAk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_payment', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('NZa1vbHljuxxfiwVBH4', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-15 10:35:47', '更新表单', '__yd2UvBzMe4NvtnL8fWp', '__Tua0cKltwny7b1OTzYy', 'general_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('O0IFj9G6ajxnbjJNOoX', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 02:51:35', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('O2qOg0gyCrUrEpBsxeM', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-10 07:59:57', '更新表单', '__csNl7TtjU9NiaT1ucmj', '__zlfY3N4sZj7uqyt0qkX', 'basic_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('o7q6fS4oUkGiZ9XKtRg', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 07:47:44', '更新表单', '__eMGGuhtm2iy4vrBbVfP', '__mwU54noqAjF1UBdrb1V', 'counterpart_detail', '表单', '删除控件[payment_type,payment_terms,payment_ratio,payment_amount,settlement,description]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('OaBF9JItZB01gcmhEUF', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-09 07:12:55', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('ObrBytyKu5Pjuy9dSr8', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-09 06:34:23', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '新增控件[\'must1\',\'must2\',\'must3\',\'must4\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('OBx2mZjifWixfxt87rk', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-28 02:15:56', '更新表单', '__TPksi5U5JPY35O51oMu', '__UyItLTLUHT2WJRP9tP1', 'protect_litigation', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('obZQ3Zl8ROBxqJDpPFf', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-19 02:17:57', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Od3QW689KCVwk0ik3g0', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 07:50:34', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('OIBDXIOcTSxyD6BPvln', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-07 02:16:00', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('oIud3BZWDySVnafLVp7', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 02:53:33', '更新表单', '__vVNU6mDJ1aDK8Aav4gm', '__Tua0cKltwny7b1OTzYy', 'general_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('OiZk7YyiaKGySyj0J91', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 10:46:09', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '更新控件:expiration_description[描述:\'合同到期说明\'->\'合同倒签订说明\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Oj5k6bnCEKuYyHgXkeZ', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 06:50:50', '更新表单', '__vVNU6mDJ1aDK8Aav4gm', '__Tua0cKltwny7b1OTzYy', 'general_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('oK7OIvjESpWLn1uNifj', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-10 16:38:48', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Ol1tCQVpaB9sHYtvFIq', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-06 10:06:18', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('oLAlWeot1bNsRUd78AT', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-18 09:36:52', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('OLgZmR8mCcx2VgV3MJj', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-15 10:42:04', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('OlkstHvvuZPOYVWOCBh', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-10 16:38:36', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('OMaNE1rTIqLxD9Cjl34', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 10:22:36', '更新表单', '__csNl7TtjU9NiaT1ucmj', '__zlfY3N4sZj7uqyt0qkX', 'basic_info', '表单', '更新控件:bank[焦点切换键:\'\'->\'Tabkey\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('onne8s2Fm6R7Et8KAcJ', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-07 09:45:34', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('OoFMFpUZilGp477fcfB', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 06:47:08', '更新表单', '__wv5qf8n1vfnhQBP9i4S', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_payment', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('OOldyYozeupnUpe6fAb', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-18 09:24:05', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('OpJ49LP5kFdGW5qvUCs', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:19:18', '更新表单', '__vVNU6mDJ1aDK8Aav4gm', '__Tua0cKltwny7b1OTzYy', 'general_contract_detail_info', '表单', '更新控件:start_time[描述:\'合同开始日期\'->\'合同起始日期\'],end_time[描述:\'合同结束日期\'->\'合同终止日期\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('oQlazs4QEDRJ88VYecr', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 09:45:30', '更新表单', '__jvpXgGxXUFqKjojY4Qy', '__hneTQUEoxzpYNqCrYlE', 'expense_contract_form', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('OqumEZmuR5puPHH1EEU', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 10:35:29', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '新增控件[\'identification\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('oRdV7KkukmqxiK8RSlY', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-29 08:38:59', '更新表单', '__yd2UvBzMe4NvtnL8fWp', '__Tua0cKltwny7b1OTzYy', 'general_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('ORFJlzav3CuglxQAiQg', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 10:26:04', '更新表单', '__wqeHK6Z51KmsrUBZhpw', '__hneTQUEoxzpYNqCrYlE', 'revenue_contract_form_basic', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('oTKLhaXTOzBH19uwqgR', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 08:17:12', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('OUMgvuerwiMaRFWMcvO', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-03 15:15:02', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '新增控件[\'选择相对方\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('ovqvdS98pAZl0pHff5G', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 07:47:43', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Ovw7m0ZKbcOlbhfgtro', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-13 09:48:56', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('oX2hgLvk7JCV9c9olKv', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 08:40:09', '更新表单', '__2mEMWhShJw6mjoCQJot', '__QXvwZwdn5T4aclQj14P', 'collection_record', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Oxrb86a1vxvnTq150BL', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 05:58:54', '更新表单', '__1F5mMirX3quzQ340SbQ', '__pHVX22ylJECnk1NeQ1A', 'invoicing_withdraw', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('p2fGyxCkge8XC4gMRFS', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 02:28:02', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('P51ySiEb11dQSppIAAB', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 07:46:26', '更新表单', '__2pMIIpKj9axWSDoqNTk', '__mwU54noqAjF1UBdrb1V', 'contract_withdraw', '表单', '表单:[描述:\'\'->\'撤回意见表\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('P5tBv8o2XM19oKOV0Pa', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 09:11:11', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('P5TzVzjKvAsvwWeQlAT', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:00:58', '更新表单', '__CxqBJSNgHCZXOW082KZ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('PBmjx3VsNMhvZl9HOZK', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 10:37:18', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '新增控件[\'identification\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('PbXzvLONIzwYurXdHza', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 05:45:34', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('PeI4SxGqbAKkgBJFqTN', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-09 02:12:08', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('PIa32z3STSkAfzpMdzI', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 07:25:55', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('PiHG0DRvB5Di33oDrtw', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 08:10:45', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('PJ1hqXvSvXTnMlUnbRv', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-14 05:41:53', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Pjz0ywXJsUdRu0NRxqV', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-21 02:33:43', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('PKKApOKpU3F0zW1WD7o', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-17 08:24:38', '更新表单', '__SbbmE0b6nRPmCY8KXZZ', '__2kwCPuPXiBbAPyuNCaF', 'agreement_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('pkkZ2zNLPMSx5NqTxA9', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-17 06:31:36', '更新表单', '__mSryjVglOxq2vz1msZZ', '__TGwWuePbmB2dIKpdaDi', 'contract_template', '表单', '删除控件[type]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('pkMBCgGhCkFMk9dS0Cc', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 13:51:39', '更新表单', '__CxqBJSNgHCZXOW082KZ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_detail_info', '表单', '表单:[描述:\'分成合同呈批表-详细信息\'->\'分成类合同呈批表-详细信息\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('PkR2S5dErQ0MoWqN7Vs', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 10:24:14', '更新表单', '__CxqBJSNgHCZXOW082KZ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_detail_info', '表单', '更新控件:bank[焦点切换键:\'\'->\'Tabkey\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('pM3JIWVjNFQ3xQPFIWy', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-07 09:10:55', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('PmWnjJQaa6SSXLxwYpf', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-15 10:36:06', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('pONULMKPFnalP9V2t0R', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-10 16:17:39', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '更新控件:contract_id[显示:\'普通\'->\'只读\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('pPFbPKdMJKBSGSpCWua', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 06:05:05', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('pR9i0rzF8YfLB95Eu8q', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 13:50:22', '更新表单', '__tMz0TQ5mTDTjowyFUAe', '__Tua0cKltwny7b1OTzYy', 'general_contract_text', '表单', '表单:[描述:\'通用合同呈批表-正文\'->\'其他类合同呈批表-正文\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('PtAFs0RHcFnxFIKeKpw', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 10:01:06', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('ptj3F97L8QI0CCGRgO7', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 03:01:27', '更新表单', '__rH9L9naFweQwnFhsqt4', '__yV15cDXVn1xnPHSBvcT', 'payment_withdraw', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('PTJyBPaB7DdvAlDIauI', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 07:17:00', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Pwd58tz2XM0idPdZySt', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:49:53', '更新表单', '__CxqBJSNgHCZXOW082KZ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('px8v3nGx5Bmm41wYu8c', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-23 06:19:25', '更新表单', '__CxqBJSNgHCZXOW082KZ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('PYK66oUmBB2MAaA8EtT', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-18 06:05:02', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '新增控件[\'dept_head_date\',\'finance_person_date\',\'finance_person2\',\'finance_person_date2\',\'leader_date\',\'finance_head_date\',\'finance_manager\',\'finance_manager_date\',\'manager_date\',\'chairman_date\']\r更新控件:finance_person[描述:\'财务人员\'->\'财务初审\'],legal_rep[描述:\'法人代表\'->\'董事长\',名称:\'legal_rep\'->\'chairman\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('pzfu1RokKdrZ66lPulP', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 08:20:22', '更新表单', '__PX2ukhQA9L2styARWJj', '__UyItLTLUHT2WJRP9tP1', 'routine_litigation', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('pZtz9CRq95zjywireIp', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-29 08:38:19', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('PZub8ZHOI5RFeiDxubR', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 13:50:57', '更新表单', '__bjuatsuk2Hyiet4Z5k6', '__mwU54noqAjF1UBdrb1V', 'expense_contract_payment', '表单', '表单:[描述:\'支出合同呈批表-付款约定\'->\'支出类合同呈批表-付款约定\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Q2r1ejxkCQNrMx5XxZq', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-03 15:02:16', '更新表单', '__vVNU6mDJ1aDK8Aav4gm', '__Tua0cKltwny7b1OTzYy', 'general_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('QA1BQzhtgnqoISB0oQQ', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 10:01:25', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '更新控件:filing_number[宽度:\'0\'->\'100\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('QbfNY0Vk1q8GAQsMzHz', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 09:05:08', '更新表单', '__CxqBJSNgHCZXOW082KZ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_detail_info', '表单', '新增控件[\'相对方信息\']\r更新控件:counterpart[重计算:\'false\'->\'true\']\r删除控件[counterpart_number,counterpart_legal_person,counterpart_type,counterpart_address,bank,bank_number,sign_leader,contact_info,identification]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('QFmdYxjxUlur4Br3RUw', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 08:21:24', '更新表单', '__TPksi5U5JPY35O51oMu', '__UyItLTLUHT2WJRP9tP1', 'protect_litigation', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('QfQETDn1KA2Jw6giFK7', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-25 02:47:57', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('QGsmsZMYOFsOXJaW3rR', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:18:55', '更新表单', '__SbbmE0b6nRPmCY8KXZZ', '__2kwCPuPXiBbAPyuNCaF', 'agreement_detail_info', '表单', '更新控件:start_time[描述:\'合同签约日期\'->\'合同起始日期\'],end_time[描述:\'协议失效时间\'->\'合同终止日期\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('QHANABffSNBPOJV0NGA', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 10:42:29', '更新表单', '__CxqBJSNgHCZXOW082KZ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('QjEu5x8kkxnEZCVzquF', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-03 15:16:07', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '新增控件[\'选择相对方\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('qjKWYEBXWAI9sPDK1af', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 02:34:35', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('qKAHX1wiGIApgRBzLWS', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 10:40:44', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('QKsPIx0LQu0jMlLdKuc', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 07:47:56', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('qoXyfiPT5VD9TwKKEzE', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-21 02:32:23', '更新表单', '__yd2UvBzMe4NvtnL8fWp', '__Tua0cKltwny7b1OTzYy', 'general_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('QpxfHsTaoq1bfisAc4s', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 10:42:15', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('QqfDrlfOXij8Rnksfxr', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 08:29:03', '更新表单', '__NgXWRPuKStY5nygCkmL', '__WOgFyT9N1Dmtrzag3hW', 'invoice_form', '表单', '更新控件:invoice_header[宽度:\'0\'->\'100\'],identification[宽度:\'0\'->\'100\'],invoice_number[宽度:\'0\'->\'100\'],invoice_amount[宽度:\'0\'->\'100\'],invoice_date[宽度:\'0\'->\'100\'],invoice_point[宽度:\'0\'->\'100\'],invoice_content[宽度:\'0\'->\'100\'],receipt_date[宽度:\'0\'->\'100\'],invoice_status[宽度:\'0\'->\'100\'],invoice_type[宽度:\'0\'->\'100\'],receipt_unit[宽度:\'0\'->\'100\'],invoicing_unit[宽度:\'0\'->\'100\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('qqZup8WAM56l6cCthGL', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 13:50:39', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '表单:[描述:\'支出合同呈批表-详细信息\'->\'支出类合同呈批表-详细信息\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('qsf1QuNbTr6iIMwO9JG', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 10:17:25', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('qtcTJfkzMCunSgkvz7O', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 07:40:41', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('qUoEZHflVmM00Gxt6Zj', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-09 02:03:09', '更新表单', '__2mEMWhShJw6mjoCQJot', '__QXvwZwdn5T4aclQj14P', 'collection_record', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('QV4wMLucOeBNxdPZ8OY', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 08:21:34', '更新表单', '__TPksi5U5JPY35O51oMu', '__UyItLTLUHT2WJRP9tP1', 'protect_litigation', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('QVCeq8yNFfNJgceSleY', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 10:26:01', '更新表单', '__wqeHK6Z51KmsrUBZhpw', '__hneTQUEoxzpYNqCrYlE', 'revenue_contract_form_basic', '表单', '更新控件:bank[焦点切换键:\'\'->\'Tabkey\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('qVUnWOVw0F0GgeL8rPq', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 08:09:33', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '删除控件[counterpart,counterpart_number,counterpart_legal_person,counterpart_type,counterpart_address,bank,bank_number,sign_leader,contact_info,identification]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('qvVq16ba7Lzl5fOFNKp', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 10:40:32', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('qZsMtrIAbBqydQ9MUQj', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-23 06:20:35', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('R5sqfselkhn49yTatVC', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-10 16:21:27', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('r7TJaHcBYoToCKvmfOf', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-07 07:27:59', '更新表单', '__GD8U7ledIDdqSex1aw1', '__zlfY3N4sZj7uqyt0qkX', 'counterpart_management', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('raKBa7UIN6yyCSkSDOt', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-06 05:58:34', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '更新控件:counterpart[显示:\'普通\'->\'只读\'],counterpart_legal_person[显示:\'普通\'->\'只读\'],counterpart_address[显示:\'普通\'->\'只读\'],bank[显示:\'普通\'->\'只读\'],bank_number[显示:\'普通\'->\'只读\'],sign_leader[显示:\'普通\'->\'只读\'],contact_info[显示:\'普通\'->\'只读\'],identification[显示:\'普通\'->\'只读\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('ral3JFQ4vJGscl9IzWJ', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-29 06:52:51', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '更新控件:apply_date_show[显示:\'只读\'->\'普通\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('RbNFr6uk0BEccbr0oUX', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-29 07:14:47', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '新增控件[\'apply_date_show\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('rBvTJPWsFM9fDUSU9WT', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:23:19', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('RCNBJAI6i1COlHlxRMd', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-16 02:30:37', '更新表单', '__inZ4Rf1o4l1zhdJIkAk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_payment', '表单', '更新控件:payment_ratio[重计算:\'false\'->\'true\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('RcucAznwgQjdFAczEYc', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 03:24:51', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('rCvsSQRoLHgZ5tvNJ5v', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 09:37:57', '更新表单', '__5ULM7zqjguD6H42SjZe', '__hneTQUEoxzpYNqCrYlE', 'agreement_contract_form', '表单', '新增控件[\'相对方信息\']\r删除控件[counterpart,counterpart_legal_person,counterpart_number,contacts,contact_info,address,bank,bank_number]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('RF2llQtgpbZvzAOt9mT', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:43:32', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('rFaxqry4MvZOQZc9JLh', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 09:45:54', '更新表单', '__jvpXgGxXUFqKjojY4Qy', '__hneTQUEoxzpYNqCrYlE', 'expense_contract_form', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('RFhXB4gtv6cBeXrmaH5', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 07:40:30', '更新表单', '__xCMgHhAWD4ueSOlU0Q6', '__z1fcq82tLWSqTuTsPFC', 'flow_setting', '表单', '更新控件:company[宽度:\'0\'->\'100\'],flow[宽度:\'0\'->\'100\'],flow_node[宽度:\'0\'->\'100\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('RhX2wszffM6jb9b2F4l', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 08:25:30', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '更新控件:bank[焦点切换键:\'\'->\'Tabkey\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('RiNkktaquOtuIMpvcQW', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-03 15:27:36', '更新表单', '__NgXWRPuKStY5nygCkmL', '__WOgFyT9N1Dmtrzag3hW', 'invoice_form', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Rk5Y8dbN6jpBAszQNX4', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 06:08:39', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('rkl8sU3UhVdz31Nwf8S', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 02:56:06', '更新表单', '__rH9L9naFweQwnFhsqt4', '__yV15cDXVn1xnPHSBvcT', 'payment_withdraw', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('rLzMFRjPZOp32by8KNO', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 03:03:00', '更新表单', '__rH9L9naFweQwnFhsqt4', '__yV15cDXVn1xnPHSBvcT', 'payment_withdraw', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('rmYN3TFhtmonZxkPJYP', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 08:08:52', '更新表单', '__CaYfthrWZGFoLiRJcLg', '__hneTQUEoxzpYNqCrYlE', 'general_contract_form', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('rnnQOaPWlHAMPw4VU9c', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-05 07:30:48', '更新表单', '__SbbmE0b6nRPmCY8KXZZ', '__2kwCPuPXiBbAPyuNCaF', 'agreement_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('roerdxE9UplyrzqlaAL', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-05 09:14:32', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('rpw5zSgnbZnstHgUbCv', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 08:10:48', '更新表单', '__CxqBJSNgHCZXOW082KZ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_detail_info', '表单', '新增控件[\'center_director\',\'center_director_attitude\',\'center_director_date\',\'zg_leader\',\'zg_leader_attitude\',\'zg_leader_date\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('RR2yENpdjUp4YScffQg', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 03:09:38', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('RRcEcoOUwmDw12MWABD', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-13 09:51:35', '更新表单', '__ekGXAf28bxqZFnKUTyq', '__mwU54noqAjF1UBdrb1V', 'expense_contract_text', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('rsWaWg730bAeNRp5zol', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-28 02:16:13', '更新表单', '__qwo0CY05xSdyRB6uI3n', '__UyItLTLUHT2WJRP9tP1', 'other_disputes', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('rTrmCMkAabyfyCTqTaJ', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-07 01:58:21', '更新表单', '__MPD9VyFPfx9pMhwwoeu', '__9Dddik4WtgZ2z0uGrwr', 'contract_change', '表单', '新增控件[\'contract_link\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Rvix9mhjFxlmCRFrPqW', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 02:34:32', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('RXqjCx5PVqmuK9X4nLs', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-05 07:30:21', '更新表单', '__CxqBJSNgHCZXOW082KZ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('RXqvYfiIv87FfrD9ctD', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 03:09:57', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Rxy5T7gQyFQu9aBz9Dl', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 08:07:49', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('RxzYykNLrfP4MOS7hNF', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-06 08:51:18', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('s7x7KAI5EGPyTkLnXuc', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 09:20:06', '更新表单', '__yd2UvBzMe4NvtnL8fWp', '__Tua0cKltwny7b1OTzYy', 'general_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('s9gETmr7tFj7gbnKItz', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 08:46:32', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('S9QUMs0DKNLjDOPr7Ft', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 13:50:15', '更新表单', '__vVNU6mDJ1aDK8Aav4gm', '__Tua0cKltwny7b1OTzYy', 'general_contract_detail_info', '表单', '表单:[描述:\'通用合同呈批表-详细信息\'->\'其他类合同呈批表-详细信息\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('sAfmnDxgkLkpkKLtMjM', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 10:40:17', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('SBDXTLmWsJrUZoPcfQX', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 02:00:04', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('sDruOwPwwXRuhBg896q', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-21 02:31:35', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('sHbAUjbL0t0MKFmAb6M', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 06:30:40', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('SiaE4vFOCxy4LotjgOM', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 08:40:06', '更新表单', '__2mEMWhShJw6mjoCQJot', '__QXvwZwdn5T4aclQj14P', 'collection_record', '表单', '新增控件[\'bank\']\r更新控件:unit[宽度:\'0\'->\'100\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('SIGQrN9axUAwd5sjpWq', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 10:36:47', '更新表单', '__CxqBJSNgHCZXOW082KZ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_detail_info', '表单', '新增控件[\'identification\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('SiIqFQiW5YAg0l5cRkG', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:22:48', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '更新控件:start_time[描述:\'合同开始时间\'->\'合同起始日期\'],end_time[描述:\'合同结束时间\'->\'合同终止日期\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('sjLgfVZBdH323yBzYzp', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 03:11:45', '更新表单', '__rH9L9naFweQwnFhsqt4', '__yV15cDXVn1xnPHSBvcT', 'payment_withdraw', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('SOuE6yEkKyMtSUR31RV', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 07:10:18', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('SQ8w16DfFWyhFauBFWE', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 06:10:34', '更新表单', '__yd2UvBzMe4NvtnL8fWp', '__Tua0cKltwny7b1OTzYy', 'general_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('SR8eCwc3JU9Fp2weQW2', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 10:34:58', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '更新控件:expiration_description[描述:\'合同到期说明\'->\'合同倒签订说明\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('syguj3WOue8jZwnyCQn', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 02:33:44', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('t0xg6wpS0bUnJtUeoeQ', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 08:55:38', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('t2FEseLObNktYphsBad', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-07 11:28:22', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '新增控件[\'重置\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('t6RKreRlqqN5KfJVt0n', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-28 02:12:00', '新建表单', '__qwo0CY05xSdyRB6uI3n', '__UyItLTLUHT2WJRP9tP1', 'other_disputes', '表单', 'other_disputes,控件[\'brief_content\',\'notes\',\'annex\']', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('t705kWrlHZ4VDRb1h5t', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-09 02:02:34', '更新表单', '__2mEMWhShJw6mjoCQJot', '__QXvwZwdn5T4aclQj14P', 'collection_record', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('TAeNyzXEbyTfHhEaLla', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-10 15:50:07', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('tDOhrLZOqpAw09P20BG', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-10 08:12:37', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('TEtqo1w3DD4Yb1pQ2Q5', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-23 06:17:38', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '新增控件[\'expiration_description\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Tfabf3KOjN7o8k7D56H', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-03 15:01:23', '更新表单', '__CxqBJSNgHCZXOW082KZ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Tk8pMpdO67V56l85BvO', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 09:36:05', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('TKnhSN5Cfvwp6f2RPIM', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-29 06:52:05', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '更新控件:apply_date_show[显示:\'普通\'->\'只读\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('TMH2DVWcHvnNJBXw3Qm', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-03 15:01:54', '更新表单', '__SbbmE0b6nRPmCY8KXZZ', '__2kwCPuPXiBbAPyuNCaF', 'agreement_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('TmKvvV5dWxm6rIy9U5p', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-10 06:51:01', '更新表单', '__CxqBJSNgHCZXOW082KZ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('toGCugy7eZhT6aNpB3c', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-09 02:19:19', '更新表单', '__inZ4Rf1o4l1zhdJIkAk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_payment', '表单', '更新控件:payment_type[描述:\'付款类型\'->\'收款类型\'],payment_terms[描述:\'付款条件\'->\'收款条件\'],payment_ratio[描述:\'付款比例\'->\'收款比例\'],payment_amount[描述:\'付款金额\'->\'收款金额\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Tpn9S3d3mOiye14j6h9', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 09:37:48', '更新表单', '__4WlF2OuywICW02IsUW8', '__hneTQUEoxzpYNqCrYlE', 'revenue_contract_form', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('tsmg0nh4xEosPAJyfV6', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 07:48:22', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('ttofwrNTrVOvdFNWGSJ', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 07:01:36', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '更新控件:dept_head[重计算:\'false\'->\'true\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('tTsjRu7zg9pSC57jVZa', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 05:52:04', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('tUosxZAMhug4Tk1gmF8', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 10:35:43', '更新表单', '__SbbmE0b6nRPmCY8KXZZ', '__2kwCPuPXiBbAPyuNCaF', 'agreement_detail_info', '表单', '更新控件:expiration_description[描述:\'合同到期说明\'->\'合同倒签订说明\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('TwIDPZfRFPYvUcWhT8q', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 07:32:14', '更新表单', '__Q0G9HYUmB73T6ANahXJ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_text', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('TxDI7iOEAtzi1LGR1xD', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 13:50:43', '更新表单', '__ekGXAf28bxqZFnKUTyq', '__mwU54noqAjF1UBdrb1V', 'expense_contract_text', '表单', '表单:[描述:\'支出合同呈批表-正文\'->\'支出类合同呈批表-正文\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('TxhMLhe918kNqodtZAl', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 13:34:37', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('TxHYndr8f0yrifYsFt1', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-07 09:09:39', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '删除控件[center_director,center_director_attitude,center_director_date,zg_leader,zg_leader_attitude,zg_leader_date]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('U0UfkhZTQk4jErEbGUu', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-18 09:31:10', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('u2exZ5CYPkXgTj2PdAm', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 06:59:46', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '新增控件[\'cs\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('U6Kkt90dhFvudoHE7Dp', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 08:18:18', '更新表单', '__CxqBJSNgHCZXOW082KZ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('u8LDwi0hZQm6VBpEE6B', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 02:55:33', '新建表单', '__rH9L9naFweQwnFhsqt4', '__yV15cDXVn1xnPHSBvcT', 'payment_withdraw', '表单', 'payment_withdraw,控件[\'opinion\']', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('UayEFeivTldEk5AgdRU', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-07 11:44:55', '更新表单', '__SbbmE0b6nRPmCY8KXZZ', '__2kwCPuPXiBbAPyuNCaF', 'agreement_detail_info', '表单', '更新控件:重置[标签:\'\'->\'取消\'],重置2[标签:\'重置\'->\'取消\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('UBj7JYgo2ucFc3RUycF', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 13:34:01', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('uDFq5BIfQhzy3jVoRRw', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 06:00:15', '更新表单', '__rH9L9naFweQwnFhsqt4', '__yV15cDXVn1xnPHSBvcT', 'payment_withdraw', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('uFatujpj879z6KWoUb9', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 06:00:58', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('uJCRKZYpPkmPlodRFBX', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 13:51:34', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '表单:[描述:\'分成合同呈批表\'->\'分成类合同呈批表\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('uLyIH4d92PpEbWzMhbQ', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 10:41:12', '更新表单', '__yd2UvBzMe4NvtnL8fWp', '__Tua0cKltwny7b1OTzYy', 'general_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('umgn2zlzyl26rmHRw7Y', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 03:30:55', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('unDFeXEtwpvncQbtkso', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 09:36:09', '更新表单', '__PxsCNOgehKAfBOV1jzH', '__hneTQUEoxzpYNqCrYlE', 'agreement_contract_form_basic', '表单', '新增控件[\'相对方信息\']\r删除控件[counterpart,counterpart_legal_person,counterpart_number,contacts,contact_info,address,bank,bank_number]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('unGkfw2PDr5WmbQ4I5K', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-05 08:17:36', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('uPS529fabjCtIhVaTWy', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 02:54:26', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('UsdhOGLjWWKj85El1Ak', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-10 07:55:18', '更新表单', '__csNl7TtjU9NiaT1ucmj', '__zlfY3N4sZj7uqyt0qkX', 'basic_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Ut1BVbCnHqWlBOGMh9r', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 10:24:42', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('uTl9XH561c00QIGN92e', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-10 16:18:11', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('ux7lN5sTURxN4H2PAZD', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:22:06', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('UxQo1wtDUfA18bk8uIL', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 10:35:10', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('uyBisqZrKaLq1dAL0yA', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 09:19:00', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('v0G17FRvD57jdiADIqf', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 08:11:08', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('V4w7D90hSwlrF8FzHSJ', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 02:49:08', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('vASA27EmMeIwi5OiRFn', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-15 10:34:18', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('vc5rNaawMKrfaJOFT8j', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-14 03:00:12', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('vc60iGu8NivFmJzlO7X', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-06 06:05:18', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('vCNnu2okRCYDn8O5chd', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-05 09:30:51', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('VCtO3UlQATAqjlmrhXL', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-21 02:31:29', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('vIcRpwnoVZmaxAPNWjR', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-13 07:56:00', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('viGFEDLBqyQwPhuXIcu', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-22 06:05:38', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('vkC865Pt2fyliq1lq94', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-06 07:17:46', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('VKQhn8HcquKbSqO0FGB', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 05:53:39', '更新表单', '__1F5mMirX3quzQ340SbQ', '__pHVX22ylJECnk1NeQ1A', 'invoicing_withdraw', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('VliqofPWc935nRhvEUt', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:17:36', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '更新控件:start_time[描述:\'合同开始时间\'->\'合同起始时间\'],end_time[描述:\'合同结束时间\'->\'合同终止时间\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('VLJ6AVTBVoaZBzSpBgB', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-07 11:41:18', '更新表单', '__vVNU6mDJ1aDK8Aav4gm', '__Tua0cKltwny7b1OTzYy', 'general_contract_detail_info', '表单', '新增控件[\'重置\',\'重置2\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('VMH3MLLBPg5xpCKRZwC', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 09:11:07', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('VMz8Fy4nmAKZuGdsX8w', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-05 09:13:47', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('vPFUBubm1rVxEa4DUgy', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 10:35:28', '更新表单', '__CxqBJSNgHCZXOW082KZ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_detail_info', '表单', '更新控件:expiration_description[描述:\'合同到期说明\'->\'合同倒签订说明\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('VPLCcaAxg8tefxGFiaL', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-09 07:11:07', '更新表单', '__inZ4Rf1o4l1zhdJIkAk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_payment', '表单', '新增控件[\'settlement\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('vqo5RvyreyOlgUtd4JF', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 07:28:38', '更新表单', '__ekGXAf28bxqZFnKUTyq', '__mwU54noqAjF1UBdrb1V', 'expense_contract_text', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('VrLuYuKNsrPuwRVSQwC', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 06:10:19', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('vrOEZM4k1s7zpJpDEGg', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-23 06:13:54', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('vrW24XmvlFvRagD1PKQ', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-06 10:37:27', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('vSDrk6HaF3EW7DKp6QB', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 02:29:42', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('VSyiGjHQq6xoq2hRhBV', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 08:37:10', '更新表单', '__2mEMWhShJw6mjoCQJot', '__QXvwZwdn5T4aclQj14P', 'collection_record', '表单', '更新控件:contract[宽度:\'0\'->\'100\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('vvyIVFKGFOzlprLVCG3', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-23 06:18:02', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('W16QJFwmUDMipD4pdDD', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 07:51:38', '更新表单', '__eMGGuhtm2iy4vrBbVfP', '__mwU54noqAjF1UBdrb1V', 'counterpart_detail', '表单', '新增控件[\'counterpart\',\'counterpart_number\',\'counterpart_legal_person\',\'counterpart_type\',\'counterpart_address\',\'bank\',\'bank_number\',\'sign_leader\',\'contact_info\',\'identification\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('W7HqvK3LqKvsJYLY1FS', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 06:30:36', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('WAuTm1hcIfgJMSiFr19', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-17 08:25:46', '更新表单', '__CxqBJSNgHCZXOW082KZ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('wBfPVDm88TrhcBYjdsp', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 10:12:29', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('wCsRp8pwCMoxQWvq9XQ', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:23:24', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('wEBNRwD3EJuvcWFweTV', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-21 02:33:31', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('wf3HOfUtt5vaOMozHTZ', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-23 06:18:06', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('WfFe91rkuwYbtO9WmwN', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 03:04:46', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '更新控件:upload[重计算:\'true\'->\'false\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Wg58yGo2fq4qeDpjJsS', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 09:37:23', '更新表单', '__jvpXgGxXUFqKjojY4Qy', '__hneTQUEoxzpYNqCrYlE', 'expense_contract_form', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('WgKyj03VeR22xZlv038', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-05 07:29:54', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('wIcvRNqTJI1ynBnHqd0', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-07 07:14:58', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '删除控件[center_director,center_director_attitude,center_director_date,zg_leader,zg_leader_attitude,zg_leader_date]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('wkCmH3N83nruSdFlriq', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 06:10:10', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('WoTikkNxI96A2qLdocD', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-07 09:11:52', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('wPFfR9GO8PBIitXyEaY', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 06:53:39', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('wpPUd7LiRHYhpUftp4J', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-09 02:17:40', '更新表单', '__2mEMWhShJw6mjoCQJot', '__QXvwZwdn5T4aclQj14P', 'collection_record', '表单', '更新控件:contract_number[描述:\'合同编号\'->\'合同备案号\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('WRDYJ6omMFnRTdpfQmV', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-05 07:30:25', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Ws2bu1njvdYbX8aRLPG', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-21 02:24:18', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('wUuxYFhmS4hhty05KaQ', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 10:33:27', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('WWYHgKbpQa0MX0lObd4', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-28 07:28:29', '更新表单', '__bjuatsuk2Hyiet4Z5k6', '__mwU54noqAjF1UBdrb1V', 'expense_contract_payment', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('WXClZyfB5a1QPVnEv9n', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 07:39:49', '更新表单', '__xCMgHhAWD4ueSOlU0Q6', '__z1fcq82tLWSqTuTsPFC', 'flow_setting', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('WxlD6FeQR01B8Dwh3Bt', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 01:54:19', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('WYLZQEYKmZ8Lu7bP6L1', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 06:11:57', '更新表单', '__vVNU6mDJ1aDK8Aav4gm', '__Tua0cKltwny7b1OTzYy', 'general_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('X11u6e6VyVh6nsMI5Pi', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 10:26:22', '更新表单', '__PxsCNOgehKAfBOV1jzH', '__hneTQUEoxzpYNqCrYlE', 'agreement_contract_form_basic', '表单', '更新控件:bank[焦点切换键:\'\'->\'Tabkey\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('x1AxttsKmEnLyUM3CvY', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-07 11:42:10', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('X6DmDufNoqLypFLbQog', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 10:37:40', '更新表单', '__vVNU6mDJ1aDK8Aav4gm', '__Tua0cKltwny7b1OTzYy', 'general_contract_detail_info', '表单', '新增控件[\'identification\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('X6PdnsScku2FLOky1Rz', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-29 08:38:39', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('X6sDRDfBabOtsSCFdyU', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-07 11:43:52', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('X76siKW1reUCe5z7FoN', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-09 07:13:32', '更新表单', '__yd2UvBzMe4NvtnL8fWp', '__Tua0cKltwny7b1OTzYy', 'general_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('X8JBHl5dQ1JTwqtznNi', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 08:45:03', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('x9ZgaDGjNFdnb84PkoS', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-28 02:15:32', '更新表单', '__qwo0CY05xSdyRB6uI3n', '__UyItLTLUHT2WJRP9tP1', 'other_disputes', '表单', '新增控件[\'handling_person\',\'from\',\'dept\',\'receiving_time\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('XaA5pfBtQN3QL3VIxff', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-03 15:33:05', '更新表单', '__2mEMWhShJw6mjoCQJot', '__QXvwZwdn5T4aclQj14P', 'collection_record', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('XAnHY1itVQTPvL0lLr1', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 05:59:40', '更新表单', '__rH9L9naFweQwnFhsqt4', '__yV15cDXVn1xnPHSBvcT', 'payment_withdraw', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('xBJ4sLLqPsrkoKm7CKh', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 06:10:16', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('xDNSrK6oJGtSUhrxIxr', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-03 10:48:26', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '新增控件[\'relateid\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('XGMRkdkbNe1mSUCOLUO', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-21 02:31:53', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Xgsmsb1fC2zxigSR6W2', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 08:08:47', '更新表单', '__lSbiziySPDXouxVboVF', '__hneTQUEoxzpYNqCrYlE', 'general_contract_form_basic', '表单', '新增控件[\'contract_ref\']\r更新控件:contract_number[描述:\'合同编号\'->\'合同流水号\',显示:\'只读\'->\'普通\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('xhOh1GotykpzruvUpVl', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-05 10:10:45', '更新表单', '__NgXWRPuKStY5nygCkmL', '__WOgFyT9N1Dmtrzag3hW', 'invoice_form', '表单', '新增控件[\'registrant\',\'registration_date\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Xj77js81oDHKzRmH1Mt', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 10:39:26', '更新表单', '__9xLa19mFr63PMS7EYud', '__fKkLNdpotnYbCJSZO9W', 'project_management', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('xpJDEfajTp233R1Zewh', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 10:34:20', '更新表单', '__vVNU6mDJ1aDK8Aav4gm', '__Tua0cKltwny7b1OTzYy', 'general_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('xqj8RGg83h7z8bBHarj', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 07:33:03', '更新表单', '__tMz0TQ5mTDTjowyFUAe', '__Tua0cKltwny7b1OTzYy', 'general_contract_text', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('xS4y0ddDlub9A5bhoAJ', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 13:50:03', '更新表单', '__36EXzd2IkVoJJgnaOnE', '__2kwCPuPXiBbAPyuNCaF', 'agreement_text', '表单', '表单:[描述:\'合诚协议呈批表-正文\'->\'合作类合同呈批表-正文\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('XTLoRM4wNiZlw284XKr', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 02:26:32', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '新增控件[\'payment_purpose\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('XUMqfw7jCI02cOSclJQ', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 08:41:32', '更新表单', '__2mEMWhShJw6mjoCQJot', '__QXvwZwdn5T4aclQj14P', 'collection_record', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('XVCr1A6vIWTkFnou0rL', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:50:11', '更新表单', '__vVNU6mDJ1aDK8Aav4gm', '__Tua0cKltwny7b1OTzYy', 'general_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('XXXuRzeRe3uoF7pSXnO', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 10:47:04', '更新表单', '__yd2UvBzMe4NvtnL8fWp', '__Tua0cKltwny7b1OTzYy', 'general_contract_approval', '表单', '更新控件:expiration_description[描述:\'合同到期说明\'->\'合同倒签订说明\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('XYbBxCDFt5Hj44ilO9j', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:49:35', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('XyhIa9RrA0Dz9kDaOCw', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 06:52:38', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('XzdeKjVNENsB0EJZEjG', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-05 07:48:32', '更新表单', '__yd2UvBzMe4NvtnL8fWp', '__Tua0cKltwny7b1OTzYy', 'general_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('XzTFrp1qsAXBrgVrfOV', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-20 03:42:12', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('y00C24XLwg5EQx1w4lE', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 09:18:12', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('y0OZOetix9wjwfD2pAJ', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:22:11', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '更新控件:start_time[描述:\'合同开始时间\'->\'合同起始日期\'],end_time[描述:\'合同结束时间\'->\'合同终止日期\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Y0wz3iv66LRY85s1Tob', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 06:00:47', '更新表单', '__rH9L9naFweQwnFhsqt4', '__yV15cDXVn1xnPHSBvcT', 'payment_withdraw', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Y1BV10CPOwrBdQGiZUS', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 08:59:22', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('y5CjPSRj3cL4WpBQ9Cz', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 13:10:37', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('y6Hp8SGPzpwuEFsoebG', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-28 07:21:50', '更新表单', '__inZ4Rf1o4l1zhdJIkAk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_payment', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Y7oVVrPNYlLxHX9OQL7', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-21 02:27:49', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Y96U9wVup1VzM6JdpEJ', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-29 07:27:52', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Yb0dPg14I8SmkiL0XnO', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 08:08:06', '更新表单', '__PxsCNOgehKAfBOV1jzH', '__hneTQUEoxzpYNqCrYlE', 'agreement_contract_form_basic', '表单', '新增控件[\'contract_ref\']\r更新控件:contract_number[描述:\'合同编号\'->\'合同流水号\',显示:\'只读\'->\'普通\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('YbWHVsaTd3FIVNFEkGS', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-03 15:00:55', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('YCqRGBRo1SaodF3YWkh', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 09:16:52', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('ydlDBW3EW1qZf9KtT86', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 07:20:16', '更新表单', '__ekGXAf28bxqZFnKUTyq', '__mwU54noqAjF1UBdrb1V', 'expense_contract_text', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('yeeSiKXHlc0Tj3ngpRc', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 10:24:40', '更新表单', '__SbbmE0b6nRPmCY8KXZZ', '__2kwCPuPXiBbAPyuNCaF', 'agreement_detail_info', '表单', '更新控件:bank[焦点切换键:\'\'->\'Tabkey\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('YhqdPPYsZOdrLY3PQL4', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-18 09:41:08', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('YHtForIZfsFrWOvkSKC', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 08:10:19', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '新增控件[\'center_director\',\'center_director_attitude\',\'center_director_date\',\'zg_leader\',\'zg_leader_attitude\',\'zg_leader_date\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('yhuvuk8Ozdos6DbCvKe', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-29 06:51:23', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '新增控件[\'apply_date_show\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('YIQIk8R2zlryHfvkFcY', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-31 10:37:15', '更新表单', '__SbbmE0b6nRPmCY8KXZZ', '__2kwCPuPXiBbAPyuNCaF', 'agreement_detail_info', '表单', '新增控件[\'identification\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('YIxOqZCKvM3PMNMPBG2', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 02:29:54', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('yj0kwQ8WGMRp0O9QNJ8', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-20 03:47:57', '更新表单', '__qKVklNXbUpNGc2yOtwK', '__2kwCPuPXiBbAPyuNCaF', 'agreement_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('yjRZs3HIpzQbm9VCdnj', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-17 08:25:26', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('ykaYKi5sPWyYwplYtPu', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-06 06:14:11', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('yKGSfitxdpxwbIj77jX', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 03:04:02', '更新表单', '__KodzsMDr3aXCy87cPrb', '__yV15cDXVn1xnPHSBvcT', 'payment_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('YmPDQ2VZwnL7wosjp6Y', 'Ai6hllhNjS2ull9TKGb', 'Admin', '2023-08-23 06:19:28', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('yoKT3pujyTHnCVl9C9H', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-29 08:47:26', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('yq0qaFaSCo5T93b9mEL', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 07:31:21', '更新表单', '__AT5KHLX1JxOb2B8Vv6H', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_text', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('YRJbQh7nhNk3kac6nLj', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:01:09', '更新表单', '__SbbmE0b6nRPmCY8KXZZ', '__2kwCPuPXiBbAPyuNCaF', 'agreement_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Yrm0NhmwgLxAOFUlsr8', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-07 11:34:08', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('ySHebKHk09wuChKEniw', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-10 07:55:57', '更新表单', '__csNl7TtjU9NiaT1ucmj', '__zlfY3N4sZj7uqyt0qkX', 'basic_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('YsmTsQluh4KsiVXxUwa', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 05:56:15', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('YtJAj0JBD47qhHpPvBP', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-08 13:51:50', '更新表单', '__ZrQzS3eQ4e6yzzAExPU', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_object', '表单', '表单:[描述:\'分成合同呈批表-标的物\'->\'分成类合同呈批表-标的物\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('YVGo2ICKHUz2pBuKOTP', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-18 09:55:25', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '新增控件[\'dept_head_date\',\'finance_person_date\',\'finance_person2\',\'finance_person_date2\',\'leader_date\',\'finance_head_date\',\'finance_manager\',\'finance_manager_date\',\'manager\',\'manager_date\',\'chairman\',\'chairman_date\']\r更新控件:finance_person[描述:\'财务人员\'->\'财务初审\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('YxSG53h4fTMsVKdC3Pk', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 09:45:24', '更新表单', '__76cQLPT8YFrSM4mfQ9K', '__8tbV9gKZXznok4jo995', 'signing_center', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('z0n8v6fRMYf4CCD58es', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-07 07:16:58', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '新增控件[\'apply_person_date\',\'dept_head_date\']\r删除控件[center_director,center_director_attitude,center_director_date,zg_leader,zg_leader_attitude,zg_leader_date]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Z4Tp9pItbWJTbWpRZ9w', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-07 11:34:40', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '更新控件:重置[重计算:\'false\'->\'true\'],重置2[重计算:\'false\'->\'true\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Zh3Z96MXimC7Qbu2M8A', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-11-10 02:37:48', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('zhJtgDUmYyE7RcW9Zgv', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-10 15:38:25', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Zhmwz21EDH5iiYdPDbS', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-05 07:29:50', '更新表单', '__klMekTlZmoto2hMVHJy', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('ZJaqEHrrfvvIPy2gSww', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-19 06:42:56', '更新表单', '__inZ4Rf1o4l1zhdJIkAk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_payment', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('zmptSLxgbE76PjGqDKk', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-05 09:22:05', '更新表单', '__7NuSLfcmZ3ILpeCe13O', '__mwU54noqAjF1UBdrb1V', 'expense_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('zMRSDMCqkNPqhEhCEGJ', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-25 02:50:43', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '更新控件:contract_number[描述:\'合同编号\'->\'合同备案号\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('zMYRz0qxRtYUjznOfBF', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 10:26:35', '更新表单', '__lSbiziySPDXouxVboVF', '__hneTQUEoxzpYNqCrYlE', 'general_contract_form_basic', '表单', '更新控件:bank[焦点切换键:\'\'->\'Tabkey\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('ZNE3x2K5sDgsJkHwFRL', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-24 10:23:53', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('ZNqHtBVUOhokqzFGtCc', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-10-09 07:13:16', '更新表单', '__SbbmE0b6nRPmCY8KXZZ', '__2kwCPuPXiBbAPyuNCaF', 'agreement_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('zolRiFanfAdrsrxLJih', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-25 07:50:02', '更新表单', '__SbbmE0b6nRPmCY8KXZZ', '__2kwCPuPXiBbAPyuNCaF', 'agreement_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Zr0UCUfmm9gAgpEwi3Y', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 07:53:48', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '新增控件[\'zg_leader\',\'zg_leader_attitude\',\'zg_leader_date\']\r', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('zRAEH6mXmJL3vD3Hu2v', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-28 01:59:37', '更新表单', '__TPksi5U5JPY35O51oMu', '__UyItLTLUHT2WJRP9tP1', 'protect_litigation', '表单', '新增控件[\'开庭日期\']\r删除控件[start_date]', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('Zsh7XuKLTQvca3dQy1W', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-18 09:56:16', '更新表单', '__Y50eV6qmxDkqlFJlMaC', '__pHVX22ylJECnk1NeQ1A', 'invoicing_application', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('zUYLX7t1uIuUpzUTUCB', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-13 11:53:17', '更新表单', '__MPD9VyFPfx9pMhwwoeu', '__9Dddik4WtgZ2z0uGrwr', 'contract_change', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('ZWasMfHoGGRBMNkotRK', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-09-18 05:54:37', '更新表单', '__hB08FiQYT6fsHmKbhVQ', '__o9p4LltM1Mrlr7zwd8g', 'share_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('ZX8URrTGZbSXWWjoq3r', '__e8sbSf9xp3fzscyU9ca', '超级管理员', '2023-08-30 09:19:49', '更新表单', '__1HnnpsDdzfkNvwYDmRk', '__7ATb48DneXgZoCF3uMn', 'revenue_contract_approval', '表单', '', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_logs` VALUES ('zZczkgFRN2KyczmuK4p', '__lQNYDRCmEeFKfrV1qck', '超级管理员', '2023-10-26 07:55:01', '更新表单', '__MWA4Ldefm4k3bLfmhLw', '__mwU54noqAjF1UBdrb1V', 'expense_contract_detail_info', '表单', '', '__M4oRwEVzSB1oI1D0faN');

-- ----------------------------
-- Table structure for t_nodert
-- ----------------------------
DROP TABLE IF EXISTS `t_nodert`;
CREATE TABLE `t_nodert` (
  `ID` varchar(100) NOT NULL,
  `NAME` varchar(100) DEFAULT NULL,
  `NODEID` varchar(100) DEFAULT NULL,
  `FLOWID` varchar(100) DEFAULT NULL,
  `DOCID` varchar(100) DEFAULT NULL,
  `FLOWSTATERT_ID` varchar(100) DEFAULT NULL,
  `NOTIFIABLE` bit(1) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `STATELABEL` varchar(100) DEFAULT NULL,
  `FLOWOPTION` varchar(100) DEFAULT NULL,
  `SPLITTOKEN` varchar(100) DEFAULT NULL,
  `PASSCONDITION` int(11) DEFAULT NULL,
  `PARENTNODERTID` varchar(100) DEFAULT NULL,
  `DEADLINE` datetime DEFAULT NULL,
  `ORDERLY` bit(1) DEFAULT NULL,
  `APPROVAL_POSITION` int(11) DEFAULT NULL,
  `STATE` int(11) DEFAULT NULL,
  `LASTPROCESSTIME` datetime DEFAULT NULL,
  `REMINDER_TIMES` int(11) DEFAULT NULL,
  `ACTIONTIME` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_nodert
-- ----------------------------
INSERT INTO `t_nodert` VALUES ('7Q0G8h9g587tohUGy0z--__7NuSLfcmZ3ILpeCe13O-1S7qeEgsRGpRsH396O8-QBiN71KJr0hZp029jJT', '财务初审', '1690256013713', '__uIAg4VJq29YlVvCwn4g', '7Q0G8h9g587tohUGy0z--__7NuSLfcmZ3ILpeCe13O', '7Q0G8h9g587tohUGy0z--__7NuSLfcmZ3ILpeCe13O-1S7qeEgsRGpRsH396O8', '\0', 'QnkpCDiSJqeIHrbbY9X', '__M4oRwEVzSB1oI1D0faN', '财务初审', '80', '', '0', '1689675721656', null, '\0', '0', '0', null, '0', '2023-11-13 07:52:16');
INSERT INTO `t_nodert` VALUES ('z2qwad9H4FNaPSnu0tx--__KodzsMDr3aXCy87cPrb-vpUjmab7LlcA2hLM6mV-xPWfQ9tfXc1n8prr51z', '总经理', '1690257092185', '__3bBUiL7zAtViqXucPVg', 'z2qwad9H4FNaPSnu0tx--__KodzsMDr3aXCy87cPrb', 'z2qwad9H4FNaPSnu0tx--__KodzsMDr3aXCy87cPrb-vpUjmab7LlcA2hLM6mV', '\0', '3CffNlgt9B9StIjIUPB', '__M4oRwEVzSB1oI1D0faN', '总经理', '80', '', '0', '1693963941709', null, '\0', '0', '0', null, '0', '2023-11-08 03:51:24');

-- ----------------------------
-- Table structure for t_relationhis
-- ----------------------------
DROP TABLE IF EXISTS `t_relationhis`;
CREATE TABLE `t_relationhis` (
  `ID` varchar(100) NOT NULL,
  `ACTIONTIME` datetime DEFAULT NULL,
  `PROCESSTIME` datetime DEFAULT NULL,
  `STARTNODENAME` varchar(100) DEFAULT NULL,
  `FLOWID` varchar(100) DEFAULT NULL,
  `FLOWNAME` varchar(100) DEFAULT NULL,
  `DOCID` varchar(100) DEFAULT NULL,
  `ENDNODEID` varchar(100) DEFAULT NULL,
  `ENDNODENAME` varchar(100) DEFAULT NULL,
  `STARTNODEID` varchar(100) DEFAULT NULL,
  `ISPASSED` bit(1) DEFAULT NULL,
  `ATTITUDE` varchar(2000) DEFAULT NULL,
  `AUDITOR` varchar(100) DEFAULT NULL,
  `FLOWOPERATION` varchar(100) DEFAULT NULL,
  `REMINDERCOUNT` int(11) DEFAULT NULL,
  `FLOWSTATERT_ID` varchar(100) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_relationhis
-- ----------------------------
INSERT INTO `t_relationhis` VALUES ('5tQas02dLqt0TNVLVrH', '2023-11-08 03:15:17', '2023-11-08 03:16:18', '部门负责人', '__3bBUiL7zAtViqXucPVg', '付款申请流程', 'z2qwad9H4FNaPSnu0tx--__KodzsMDr3aXCy87cPrb', '1690257090180', '分管领导', '1689675729629', '\0', '同意', '7FxMvcJsdDigaTisPwY', '80', '0', 'z2qwad9H4FNaPSnu0tx--__KodzsMDr3aXCy87cPrb-vpUjmab7LlcA2hLM6mV', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_relationhis` VALUES ('7X7R2qQpRaOsY1GDRWA', '2023-11-08 03:16:44', '2023-11-08 03:17:17', '财务初审', '__3bBUiL7zAtViqXucPVg', '付款申请流程', 'z2qwad9H4FNaPSnu0tx--__KodzsMDr3aXCy87cPrb', '1693205430819', '财务复审', '1690256013713', '\0', '同意', 'IsxmiyIbM4Cju3VFwZY', '80', '0', 'z2qwad9H4FNaPSnu0tx--__KodzsMDr3aXCy87cPrb-vpUjmab7LlcA2hLM6mV', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_relationhis` VALUES ('7yq0lexpSJXIuhzKe3e', '2023-11-08 03:15:00', '2023-11-08 03:15:17', '副部门负责人', '__3bBUiL7zAtViqXucPVg', '付款申请流程', 'z2qwad9H4FNaPSnu0tx--__KodzsMDr3aXCy87cPrb', '1689675729629', '部门负责人', '1699003160582', '\0', '同意', '5eYCjxhwUlUnoCwIY42', '80', '0', 'z2qwad9H4FNaPSnu0tx--__KodzsMDr3aXCy87cPrb-vpUjmab7LlcA2hLM6mV', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_relationhis` VALUES ('8RedX5Olc1v1HVarXuy', '2023-11-08 03:18:26', '2023-11-08 03:18:44', '财务主管领导', '__3bBUiL7zAtViqXucPVg', '付款申请流程', 'z2qwad9H4FNaPSnu0tx--__KodzsMDr3aXCy87cPrb', '1690257092185', '总经理', '1693963941709', '\0', '同意', 'MXQXjNOvAC9rTf1tYNE', '80', '0', 'z2qwad9H4FNaPSnu0tx--__KodzsMDr3aXCy87cPrb-vpUjmab7LlcA2hLM6mV', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_relationhis` VALUES ('kdm2re5rdNAjrVkE1mn', '2023-11-08 03:18:44', '2023-11-08 03:50:58', '总经理', '__3bBUiL7zAtViqXucPVg', '付款申请流程', 'z2qwad9H4FNaPSnu0tx--__KodzsMDr3aXCy87cPrb', '1693963941709', '财务主管领导', '1690257092185', '\0', '11', 'i9WtHnQb9VuJA0Xo8ka', '81', '0', 'z2qwad9H4FNaPSnu0tx--__KodzsMDr3aXCy87cPrb-vpUjmab7LlcA2hLM6mV', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_relationhis` VALUES ('kqgfXLUSmcFlCY6YBzy', '2023-11-08 03:50:58', '2023-11-08 03:51:24', '财务主管领导', '__3bBUiL7zAtViqXucPVg', '付款申请流程', 'z2qwad9H4FNaPSnu0tx--__KodzsMDr3aXCy87cPrb', '1690257092185', '总经理', '1693963941709', '\0', '同意', 'i9WtHnQb9VuJA0Xo8ka', '80', '0', 'z2qwad9H4FNaPSnu0tx--__KodzsMDr3aXCy87cPrb-vpUjmab7LlcA2hLM6mV', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_relationhis` VALUES ('L9mjv0cAbe2NKHPlTve', '2023-11-08 03:17:17', '2023-11-08 03:17:39', '财务复审', '__3bBUiL7zAtViqXucPVg', '付款申请流程', 'z2qwad9H4FNaPSnu0tx--__KodzsMDr3aXCy87cPrb', '1690257079040', '财务负责人', '1693205430819', '\0', '同意', '6GVKMsHFmLKutPbUzXv', '80', '0', 'z2qwad9H4FNaPSnu0tx--__KodzsMDr3aXCy87cPrb-vpUjmab7LlcA2hLM6mV', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_relationhis` VALUES ('LV80FmVlLsOrHYeMr2c', '2023-11-08 03:17:39', '2023-11-08 03:18:26', '财务负责人', '__3bBUiL7zAtViqXucPVg', '付款申请流程', 'z2qwad9H4FNaPSnu0tx--__KodzsMDr3aXCy87cPrb', '1693963941709', '财务主管领导', '1690257079040', '\0', '同意', 'i9WtHnQb9VuJA0Xo8ka', '80', '0', 'z2qwad9H4FNaPSnu0tx--__KodzsMDr3aXCy87cPrb-vpUjmab7LlcA2hLM6mV', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_relationhis` VALUES ('MY2WM60jHuyHN863q7M', '2023-11-08 03:15:00', '2023-11-08 03:15:00', '申请人', '__3bBUiL7zAtViqXucPVg', '付款申请流程', 'z2qwad9H4FNaPSnu0tx--__KodzsMDr3aXCy87cPrb', '1699003160582', '副部门负责人', '1689675721656', '\0', '请审批', '07t19Wc4wu1Er8870fJ', '80', '0', 'z2qwad9H4FNaPSnu0tx--__KodzsMDr3aXCy87cPrb-vpUjmab7LlcA2hLM6mV', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_relationhis` VALUES ('XfLKE4UY1FycDTQxTFk', '2023-11-13 07:52:16', '2023-11-13 07:52:16', '申请人', '__uIAg4VJq29YlVvCwn4g', '支出合同审批流程', '7Q0G8h9g587tohUGy0z--__7NuSLfcmZ3ILpeCe13O', '1690256013713', '财务初审', '1689675721656', '\0', '请审批', 'Mxspdb6zQc7sibnXuG0', '80', '0', '7Q0G8h9g587tohUGy0z--__7NuSLfcmZ3ILpeCe13O-1S7qeEgsRGpRsH396O8', '__M4oRwEVzSB1oI1D0faN');
INSERT INTO `t_relationhis` VALUES ('YFDc5xT6hn6nZ36oYjv', '2023-11-08 03:16:18', '2023-11-08 03:16:44', '分管领导', '__3bBUiL7zAtViqXucPVg', '付款申请流程', 'z2qwad9H4FNaPSnu0tx--__KodzsMDr3aXCy87cPrb', '1690256013713', '财务初审', '1690257090180', '\0', '同意', '0FRa9PPu6GzctvcFxIX', '80', '0', 'z2qwad9H4FNaPSnu0tx--__KodzsMDr3aXCy87cPrb-vpUjmab7LlcA2hLM6mV', '__M4oRwEVzSB1oI1D0faN');

-- ----------------------------
-- Table structure for t_shortmessage_received
-- ----------------------------
DROP TABLE IF EXISTS `t_shortmessage_received`;
CREATE TABLE `t_shortmessage_received` (
  `ID` varchar(100) NOT NULL,
  `CONTENT` longtext,
  `SENDER` varchar(255) DEFAULT NULL,
  `RECEIVER` varchar(255) DEFAULT NULL,
  `RECEIVEDATE` varchar(255) DEFAULT NULL,
  `STATUS` int(11) DEFAULT NULL,
  `PARENT` varchar(255) DEFAULT NULL,
  `DOCID` varchar(255) DEFAULT NULL,
  `APPLICATIONID` varchar(255) DEFAULT NULL,
  `DOMAINID` varchar(255) DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_shortmessage_received
-- ----------------------------

-- ----------------------------
-- Table structure for t_shortmessage_submit
-- ----------------------------
DROP TABLE IF EXISTS `t_shortmessage_submit`;
CREATE TABLE `t_shortmessage_submit` (
  `ID` varchar(100) NOT NULL,
  `CONTENTTYPE` int(11) DEFAULT NULL,
  `TITLE` varchar(255) DEFAULT NULL,
  `CONTENT` varchar(255) DEFAULT NULL,
  `SENDDATE` datetime DEFAULT NULL,
  `REPLYCODE` varchar(255) DEFAULT NULL,
  `SENDER` varchar(255) DEFAULT NULL,
  `RECEIVER` longtext,
  `SUBMISSION` bit(1) DEFAULT NULL,
  `ISFAILURE` bit(1) DEFAULT NULL,
  `ISREPLY` bit(1) DEFAULT NULL,
  `ISTRASH` bit(1) DEFAULT NULL,
  `ISDRAFT` bit(1) DEFAULT NULL,
  `NEEDREPLY` bit(1) DEFAULT NULL,
  `MASS` bit(1) DEFAULT NULL,
  `DOCID` varchar(255) DEFAULT NULL,
  `APPLICATIONID` varchar(255) DEFAULT NULL,
  `DOMAINID` varchar(255) DEFAULT NULL,
  `RECEIVERUSERID` varchar(255) DEFAULT NULL,
  `RECEIVERNAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_shortmessage_submit
-- ----------------------------

-- ----------------------------
-- Table structure for t_trigger
-- ----------------------------
DROP TABLE IF EXISTS `t_trigger`;
CREATE TABLE `t_trigger` (
  `ID` varchar(100) NOT NULL,
  `TOKEN` varchar(400) DEFAULT NULL,
  `JOB_TYPE` int(11) DEFAULT NULL,
  `JOB_DATA` longtext,
  `STATE` varchar(255) DEFAULT NULL,
  `DEADLINE` bigint(20) DEFAULT NULL,
  `IS_LOOP` bit(1) DEFAULT NULL,
  `APPLICATIONID` varchar(255) DEFAULT NULL,
  `DOCUMENTS` longtext,
  `RUNTIMES` int(11) DEFAULT NULL,
  `LASTMODIFIDATE` datetime DEFAULT NULL,
  `TASKID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_trigger
-- ----------------------------

-- ----------------------------
-- Table structure for t_upload
-- ----------------------------
DROP TABLE IF EXISTS `t_upload`;
CREATE TABLE `t_upload` (
  `ID` varchar(100) DEFAULT NULL,
  `NAME` varchar(100) DEFAULT NULL,
  `IMGBINARY` mediumblob,
  `FIELDID` varchar(100) DEFAULT NULL,
  `TYPE` varchar(100) DEFAULT NULL,
  `FILESIZE` int(11) DEFAULT NULL,
  `USERID` varchar(100) DEFAULT NULL,
  `MODIFYDATE` datetime DEFAULT NULL,
  `PATH` longtext,
  `FOLDERPATH` longtext,
  `SOURCEFILEID` varchar(100) DEFAULT NULL,
  `VERSIONNO` varchar(100) DEFAULT NULL,
  `DOCID` varchar(100) DEFAULT NULL,
  `RELATEMSG` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_upload
-- ----------------------------
INSERT INTO `t_upload` VALUES ('新建 DOCX 文档-bNmRQG8n5DGl15Mjhg7', '新建 DOCX 文档.docx', null, 'AJTNhnlfGwUjIDZrJRK--__7NuSLfcmZ3ILpeCe13O_upload', '.docx', '0', 'yEAg5BNT1NEnT5xb7dD', '2023-08-15 10:30:05', '/uploads/item/2023/8/8新建 DOCX 文档-bNmRQG8n5DGl15Mjhg7.docx', '/uploads/item/', '', 'V1.0', 'AJTNhnlfGwUjIDZrJRK--__7NuSLfcmZ3ILpeCe13O', '合同名称：');
INSERT INTO `t_upload` VALUES ('新建 DOCX 文档-C0eP4zLKy7jWSTuChhi', '新建 DOCX 文档.docx', null, 'HtxuHcuOdEbnwzLcdvB--__7NuSLfcmZ3ILpeCe13O_upload', '.docx', '0', 'yEAg5BNT1NEnT5xb7dD', '2023-08-15 10:37:42', '/uploads/item/2023/8/8新建 DOCX 文档-C0eP4zLKy7jWSTuChhi.docx', '/uploads/item/', '', 'V1.0', 'HtxuHcuOdEbnwzLcdvB--__7NuSLfcmZ3ILpeCe13O', '合同名称：');
INSERT INTO `t_upload` VALUES ('合同模板-SavThntmBxb7ei2CMVw', '合同模板.docx', null, 'Q8RiogxPys5m3NJXLbq--__7NuSLfcmZ3ILpeCe13O_upload', '.docx', '10239', 'yEAg5BNT1NEnT5xb7dD', '2023-08-22 09:15:00', '/uploads/item/2023/8/合同模板-SavThntmBxb7ei2CMVw.docx', '/uploads/item/', '', 'V1.0', 'Q8RiogxPys5m3NJXLbq--__7NuSLfcmZ3ILpeCe13O', '合同名称：测试');
INSERT INTO `t_upload` VALUES ('XntKdqbZIsqY1BbGPla', '项目导入.xlsx', null, '', '.xlsx', '9377', 'yEAg5BNT1NEnT5xb7dD', '2023-08-22 10:27:12', '/uploads/excel/2023/XntKdqbZIsqY1BbGPla.xlsx', '/uploads/excel/', null, 'V1.0', null, null);
INSERT INTO `t_upload` VALUES ('vq8PIcICPK9rSr5ti9c', '项目导入.xlsx', null, '', '.xlsx', '9385', 'yEAg5BNT1NEnT5xb7dD', '2023-08-22 10:27:44', '/uploads/excel/2023/vq8PIcICPK9rSr5ti9c.xlsx', '/uploads/excel/', null, 'V1.0', null, null);
INSERT INTO `t_upload` VALUES ('借款合同模板-对内-doOphdWhgo4e5GPNMdU', '借款合同模板-对内.docx', null, 'k7nSEkQRcT69VG9CTAl--__qKVklNXbUpNGc2yOtwK_upload', '.docx', '21715', 'MfokklYeqit6DecvrOu', '2023-08-24 04:49:42', '/uploads/item/2023/8/借款合同模板-对内-doOphdWhgo4e5GPNMdU.docx', '/uploads/item/', '89597e14-33df-4283-8aa0-0eda7985d4e5', 'V1.1', 'k7nSEkQRcT69VG9CTAl--__qKVklNXbUpNGc2yOtwK', '合同名称：借款合同');
INSERT INTO `t_upload` VALUES ('89597e14-33df-4283-8aa0-0eda7985d4e5', '借款合同模板-对内.docx', null, 'k7nSEkQRcT69VG9CTAl--__qKVklNXbUpNGc2yOtwK_upload', '.docx', '21715', 'MfokklYeqit6DecvrOu', '2023-08-24 03:42:50', '/uploads/item/2023/8/89597e14-33df-4283-8aa0-0eda7985d4e5.docx', '/uploads/item/', '', 'V1.0', 'k7nSEkQRcT69VG9CTAl--__qKVklNXbUpNGc2yOtwK', '合同名称：借款合同');
INSERT INTO `t_upload` VALUES ('合同模板-gBumPVHK619k4KHpu9O', '合同模板.docx', null, 'WbRQJ5F2UgovDYi6vfM--__7NuSLfcmZ3ILpeCe13O_upload', '.docx', '10239', 'yEAg5BNT1NEnT5xb7dD', '2023-08-24 05:46:30', '/uploads/item/2023/8/合同模板-gBumPVHK619k4KHpu9O.docx', '/uploads/item/', 'ee977465-ae0d-441b-9acd-a9c21c47b996', 'V1.1', 'WbRQJ5F2UgovDYi6vfM--__7NuSLfcmZ3ILpeCe13O', '合同名称：');
INSERT INTO `t_upload` VALUES ('ee977465-ae0d-441b-9acd-a9c21c47b996', '合同模板.docx', null, 'WbRQJ5F2UgovDYi6vfM--__7NuSLfcmZ3ILpeCe13O_upload', '.docx', '10239', 'yEAg5BNT1NEnT5xb7dD', '2023-08-24 05:40:08', '/uploads/item/2023/8/ee977465-ae0d-441b-9acd-a9c21c47b996.docx', '/uploads/item/', '', 'V1.0', 'WbRQJ5F2UgovDYi6vfM--__7NuSLfcmZ3ILpeCe13O', '合同名称：');
INSERT INTO `t_upload` VALUES ('合同模板-iPZ0Ym93D2GIh8z5urU', '合同模板.docx', null, 'pIe7FzHKdmoyJA4BYIo--__7NuSLfcmZ3ILpeCe13O_upload', '.docx', '10239', '6zKKV9YUfRbQCaDaDwF', '2023-08-25 02:25:17', '/uploads/item/2023/8/合同模板-iPZ0Ym93D2GIh8z5urU.docx', '/uploads/item/', '33067320-e4a6-4a98-9ee3-0c40c43c0ad5', 'V2.4', 'pIe7FzHKdmoyJA4BYIo--__7NuSLfcmZ3ILpeCe13O', '合同名称：cs');
INSERT INTO `t_upload` VALUES ('435a75ae-ad86-4d3f-965b-404d8d689df5', '合同模板.docx', null, 'pIe7FzHKdmoyJA4BYIo--__7NuSLfcmZ3ILpeCe13O_upload', '.docx', '10239', '6zKKV9YUfRbQCaDaDwF', '2023-08-24 06:27:45', '/uploads/item/2023/8/435a75ae-ad86-4d3f-965b-404d8d689df5.docx', '/uploads/item/', '', 'V1.0', 'pIe7FzHKdmoyJA4BYIo--__7NuSLfcmZ3ILpeCe13O', '合同名称：cs');
INSERT INTO `t_upload` VALUES ('785b6346-ec98-42d0-94bf-8e060e83309d', '合同模板.docx', null, 'pIe7FzHKdmoyJA4BYIo--__7NuSLfcmZ3ILpeCe13O_upload', '.docx', '10239', '6zKKV9YUfRbQCaDaDwF', '2023-08-24 06:31:58', '/uploads/item/2023/8/785b6346-ec98-42d0-94bf-8e060e83309d.docx', '/uploads/item/', '435a75ae-ad86-4d3f-965b-404d8d689df5', 'V1.1', 'pIe7FzHKdmoyJA4BYIo--__7NuSLfcmZ3ILpeCe13O', '合同名称：cs');
INSERT INTO `t_upload` VALUES ('9c95706a-9111-4593-89f4-5ef38510e265', '合同模板.docx', null, 'pIe7FzHKdmoyJA4BYIo--__7NuSLfcmZ3ILpeCe13O_upload', '.docx', '10239', '6zKKV9YUfRbQCaDaDwF', '2023-08-24 06:34:31', '/uploads/item/2023/8/9c95706a-9111-4593-89f4-5ef38510e265.docx', '/uploads/item/', '785b6346-ec98-42d0-94bf-8e060e83309d', 'V1.2', 'pIe7FzHKdmoyJA4BYIo--__7NuSLfcmZ3ILpeCe13O', '合同名称：cs');
INSERT INTO `t_upload` VALUES ('073981aa-9dbe-4f0f-8cfe-11ddb07f6676', '合同模板.docx', null, 'pIe7FzHKdmoyJA4BYIo--__7NuSLfcmZ3ILpeCe13O_upload', '.docx', '10239', '6zKKV9YUfRbQCaDaDwF', '2023-08-24 06:35:52', '/uploads/item/2023/8/073981aa-9dbe-4f0f-8cfe-11ddb07f6676.docx', '/uploads/item/', '9c95706a-9111-4593-89f4-5ef38510e265', 'V1.3', 'pIe7FzHKdmoyJA4BYIo--__7NuSLfcmZ3ILpeCe13O', '合同名称：cs');
INSERT INTO `t_upload` VALUES ('8064c353-dcc2-46fa-b42d-450d25ff1731', '合同模板.docx', null, 'pIe7FzHKdmoyJA4BYIo--__7NuSLfcmZ3ILpeCe13O_upload', '.docx', '10239', '6zKKV9YUfRbQCaDaDwF', '2023-08-24 06:41:30', '/uploads/item/2023/8/8064c353-dcc2-46fa-b42d-450d25ff1731.docx', '/uploads/item/', '073981aa-9dbe-4f0f-8cfe-11ddb07f6676', 'V1.4', 'pIe7FzHKdmoyJA4BYIo--__7NuSLfcmZ3ILpeCe13O', '合同名称：cs');
INSERT INTO `t_upload` VALUES ('93429e7c-c3c0-4840-8757-5448a755cd26', '合同模板.docx', null, 'pIe7FzHKdmoyJA4BYIo--__7NuSLfcmZ3ILpeCe13O_upload', '.docx', '10239', '6zKKV9YUfRbQCaDaDwF', '2023-08-24 06:48:03', '/uploads/item/2023/8/93429e7c-c3c0-4840-8757-5448a755cd26.docx', '/uploads/item/', '8064c353-dcc2-46fa-b42d-450d25ff1731', 'V1.5', 'pIe7FzHKdmoyJA4BYIo--__7NuSLfcmZ3ILpeCe13O', '合同名称：cs');
INSERT INTO `t_upload` VALUES ('e195aae3-4cd4-4848-80bf-f706b346a338', '合同模板.docx', null, 'pIe7FzHKdmoyJA4BYIo--__7NuSLfcmZ3ILpeCe13O_upload', '.docx', '10239', '6zKKV9YUfRbQCaDaDwF', '2023-08-24 06:51:28', '/uploads/item/2023/8/e195aae3-4cd4-4848-80bf-f706b346a338.docx', '/uploads/item/', '93429e7c-c3c0-4840-8757-5448a755cd26', 'V1.6', 'pIe7FzHKdmoyJA4BYIo--__7NuSLfcmZ3ILpeCe13O', '合同名称：cs');
INSERT INTO `t_upload` VALUES ('a7318898-aac2-4722-bff3-29dcad444fef', '合同模板.docx', null, 'pIe7FzHKdmoyJA4BYIo--__7NuSLfcmZ3ILpeCe13O_upload', '.docx', '10239', '6zKKV9YUfRbQCaDaDwF', '2023-08-24 06:54:40', '/uploads/item/2023/8/a7318898-aac2-4722-bff3-29dcad444fef.docx', '/uploads/item/', 'e195aae3-4cd4-4848-80bf-f706b346a338', 'V1.7', 'pIe7FzHKdmoyJA4BYIo--__7NuSLfcmZ3ILpeCe13O', '合同名称：cs');
INSERT INTO `t_upload` VALUES ('9ffa470b-56b2-4e4e-8df7-6edb401d0631', '合同模板.docx', null, 'pIe7FzHKdmoyJA4BYIo--__7NuSLfcmZ3ILpeCe13O_upload', '.docx', '10239', '6zKKV9YUfRbQCaDaDwF', '2023-08-24 06:55:35', '/uploads/item/2023/8/9ffa470b-56b2-4e4e-8df7-6edb401d0631.docx', '/uploads/item/', 'a7318898-aac2-4722-bff3-29dcad444fef', 'V1.8', 'pIe7FzHKdmoyJA4BYIo--__7NuSLfcmZ3ILpeCe13O', '合同名称：cs');
INSERT INTO `t_upload` VALUES ('5f31d0ab-1ff6-4382-99ec-6cc287953aa7', '合同模板.docx', null, 'pIe7FzHKdmoyJA4BYIo--__7NuSLfcmZ3ILpeCe13O_upload', '.docx', '10239', '6zKKV9YUfRbQCaDaDwF', '2023-08-24 06:57:30', '/uploads/item/2023/8/5f31d0ab-1ff6-4382-99ec-6cc287953aa7.docx', '/uploads/item/', '9ffa470b-56b2-4e4e-8df7-6edb401d0631', 'V1.9', 'pIe7FzHKdmoyJA4BYIo--__7NuSLfcmZ3ILpeCe13O', '合同名称：cs');
INSERT INTO `t_upload` VALUES ('a526d638-a3b7-4f61-97d0-c89abbdea741', '合同模板.docx', null, 'pIe7FzHKdmoyJA4BYIo--__7NuSLfcmZ3ILpeCe13O_upload', '.docx', '10239', '6zKKV9YUfRbQCaDaDwF', '2023-08-24 07:00:18', '/uploads/item/2023/8/a526d638-a3b7-4f61-97d0-c89abbdea741.docx', '/uploads/item/', '5f31d0ab-1ff6-4382-99ec-6cc287953aa7', 'V2.0', 'pIe7FzHKdmoyJA4BYIo--__7NuSLfcmZ3ILpeCe13O', '合同名称：cs');
INSERT INTO `t_upload` VALUES ('84a92616-92f1-401a-8cc8-95eb7eadd8b2', '合同模板.docx', null, 'pIe7FzHKdmoyJA4BYIo--__7NuSLfcmZ3ILpeCe13O_upload', '.docx', '10239', '6zKKV9YUfRbQCaDaDwF', '2023-08-24 07:14:30', '/uploads/item/2023/8/84a92616-92f1-401a-8cc8-95eb7eadd8b2.docx', '/uploads/item/', 'a526d638-a3b7-4f61-97d0-c89abbdea741', 'V2.1', 'pIe7FzHKdmoyJA4BYIo--__7NuSLfcmZ3ILpeCe13O', '合同名称：cs');
INSERT INTO `t_upload` VALUES ('7e9e1055-99d9-4733-9411-62b431c27745', '合同模板.docx', null, 'pIe7FzHKdmoyJA4BYIo--__7NuSLfcmZ3ILpeCe13O_upload', '.docx', '10239', '6zKKV9YUfRbQCaDaDwF', '2023-08-24 07:18:14', '/uploads/item/2023/8/7e9e1055-99d9-4733-9411-62b431c27745.docx', '/uploads/item/', '84a92616-92f1-401a-8cc8-95eb7eadd8b2', 'V2.2', 'pIe7FzHKdmoyJA4BYIo--__7NuSLfcmZ3ILpeCe13O', '合同名称：cs');
INSERT INTO `t_upload` VALUES ('新建 DOCX 文档-TIEk90R6pRwtYS9ZMsa', '新建 DOCX 文档.docx', null, 'k7nSEkQRcT69VG9CTAl--__qKVklNXbUpNGc2yOtwK_upload', '.docx', '0', 'hnu7lc0tv0okrCNI0y0', '2023-08-24 08:27:39', '/uploads/item/2023/8/新建 DOCX 文档-TIEk90R6pRwtYS9ZMsa.docx', '/uploads/item/', '', 'V1.0', 'k7nSEkQRcT69VG9CTAl--__qKVklNXbUpNGc2yOtwK', '合同名称：借款合同');
INSERT INTO `t_upload` VALUES ('33067320-e4a6-4a98-9ee3-0c40c43c0ad5', '合同模板.docx', null, 'pIe7FzHKdmoyJA4BYIo--__7NuSLfcmZ3ILpeCe13O_upload', '.docx', '10239', 'umqW0GWXmQHBI7B9m22', '2023-08-24 07:18:46', '/uploads/item/2023/8/33067320-e4a6-4a98-9ee3-0c40c43c0ad5.docx', '/uploads/item/', '7e9e1055-99d9-4733-9411-62b431c27745', 'V2.3', 'pIe7FzHKdmoyJA4BYIo--__7NuSLfcmZ3ILpeCe13O', '合同名称：cs');
INSERT INTO `t_upload` VALUES ('新建 DOCX 文档-LDXRNRp4FCSgc3tRch0', '新建 DOCX 文档.docx', null, 'NyZA16slCuBtL7bjLnE--__7NuSLfcmZ3ILpeCe13O_upload', '.docx', '0', 'a0yY1nD8SNLWqjpX8Qm', '2023-08-25 03:24:51', '/uploads/item/2023/8/新建 DOCX 文档-LDXRNRp4FCSgc3tRch0.docx', '/uploads/item/', '', 'V1.0', 'NyZA16slCuBtL7bjLnE--__7NuSLfcmZ3ILpeCe13O', '合同名称：');
INSERT INTO `t_upload` VALUES ('会议记录-PELBm0wuvDWrLCSJmn6', '会议记录.docx', null, '9v76F3HY5rwVtHodJGc--__7NuSLfcmZ3ILpeCe13O_upload', '.docx', '10505', 'MfokklYeqit6DecvrOu', '2023-08-25 03:36:20', '/uploads/item/2023/8/会议记录-PELBm0wuvDWrLCSJmn6.docx', '/uploads/item/', '', 'V1.0', '9v76F3HY5rwVtHodJGc--__7NuSLfcmZ3ILpeCe13O', '合同名称：');
INSERT INTO `t_upload` VALUES ('会议记录-LYawwHKDmE773T3TmjG', '会议记录.docx', null, 'ZEvetcAiUXdwg9OFb9v--__7NuSLfcmZ3ILpeCe13O_upload', '.docx', '10505', 'MfokklYeqit6DecvrOu', '2023-08-25 03:39:55', '/uploads/item/2023/8/会议记录-LYawwHKDmE773T3TmjG.docx', '/uploads/item/', '', 'V1.0', 'ZEvetcAiUXdwg9OFb9v--__7NuSLfcmZ3ILpeCe13O', '合同名称：');
INSERT INTO `t_upload` VALUES ('2021-新媒体集团员工请（销）假申请表-YYuaKbukhmXBOK22KzT', '2021-新媒体集团员工请（销）假申请表.doc', null, '0sjZqSNu40mRNB0C8gG--__7NuSLfcmZ3ILpeCe13O_upload', '.doc', '57856', 'MfokklYeqit6DecvrOu', '2023-08-25 03:51:49', '/uploads/item/2023/8/2021-新媒体集团员工请（销）假申请表-YYuaKbukhmXBOK22KzT.doc', '/uploads/item/', '', 'V1.0', '0sjZqSNu40mRNB0C8gG--__7NuSLfcmZ3ILpeCe13O', '合同名称：111');
INSERT INTO `t_upload` VALUES ('关于“采购合同管理系统软件”的请示-20230323-jdCVOtKkve8s8a2nQtA', '关于“采购合同管理系统软件”的请示-20230323.pdf', null, 'm2dWB131J19naz5E4vf--__7NuSLfcmZ3ILpeCe13O_annex', '.pdf', '298378', 'MfokklYeqit6DecvrOu', '2023-08-25 04:30:46', '/uploads/item/2023/8/关于“采购合同管理系统软件”的请示-20230323-jdCVOtKkve8s8a2nQtA.pdf', '/uploads/item/', '', 'V1.0', 'm2dWB131J19naz5E4vf--__7NuSLfcmZ3ILpeCe13O', '合同名称：技术开发（委托）合同');
INSERT INTO `t_upload` VALUES ('合同审核软件部署方案会议纪要-20230331-FI8b8HbmI4wl45v8GTz', '合同审核软件部署方案会议纪要-20230331.pdf', null, 'm2dWB131J19naz5E4vf--__7NuSLfcmZ3ILpeCe13O_annex', '.pdf', '714486', 'MfokklYeqit6DecvrOu', '2023-08-25 04:31:07', '/uploads/item/2023/8/合同审核软件部署方案会议纪要-20230331-FI8b8HbmI4wl45v8GTz.pdf', '/uploads/item/', '', 'V1.0', 'm2dWB131J19naz5E4vf--__7NuSLfcmZ3ILpeCe13O', '合同名称：技术开发（委托）合同');
INSERT INTO `t_upload` VALUES ('免税备案批复(1)-haYmAm0hGWORfOa4jx3', '免税备案批复(1).pdf', null, 'm2dWB131J19naz5E4vf--__7NuSLfcmZ3ILpeCe13O_annex', '.pdf', '50079', 'MfokklYeqit6DecvrOu', '2023-08-25 04:31:29', '/uploads/item/2023/8/免税备案批复(1)-haYmAm0hGWORfOa4jx3.pdf', '/uploads/item/', '', 'V1.0', 'm2dWB131J19naz5E4vf--__7NuSLfcmZ3ILpeCe13O', '合同名称：技术开发（委托）合同');
INSERT INTO `t_upload` VALUES ('合同审核软件（委托）合同-20230323-8xd6z2ZRf0Epdf3ELZo', '合同审核软件（委托）合同-20230323.doc', null, 'm2dWB131J19naz5E4vf--__7NuSLfcmZ3ILpeCe13O_upload', '.doc', '113664', 'MfokklYeqit6DecvrOu', '2023-08-25 06:08:32', '/uploads/item/2023/8/合同审核软件（委托）合同-20230323-8xd6z2ZRf0Epdf3ELZo.doc', '/uploads/item/', '2e020cd0-67eb-4d21-9e47-05661ea1256f', 'V1.3', 'm2dWB131J19naz5E4vf--__7NuSLfcmZ3ILpeCe13O', '合同名称：技术开发（委托）合同');
INSERT INTO `t_upload` VALUES ('cbdc0cca-29b2-4970-9121-cec7e7825dce', '合同审核软件（委托）合同-20230323.doc', null, 'm2dWB131J19naz5E4vf--__7NuSLfcmZ3ILpeCe13O_upload', '.doc', '113664', 'RzJ3os3MIzsnlZIH8O9', '2023-08-25 04:47:37', '/uploads/item/2023/8/cbdc0cca-29b2-4970-9121-cec7e7825dce.doc', '/uploads/item/', '', 'V1.0', 'm2dWB131J19naz5E4vf--__7NuSLfcmZ3ILpeCe13O', '合同名称：技术开发（委托）合同');
INSERT INTO `t_upload` VALUES ('8554c037-94c8-421f-a450-fd1190e8ddf0', '合同审核软件（委托）合同-20230323.doc', null, 'm2dWB131J19naz5E4vf--__7NuSLfcmZ3ILpeCe13O_upload', '.doc', '113664', '9VfzB8y5m9O1LPk5MSV', '2023-08-25 04:51:23', '/uploads/item/2023/8/8554c037-94c8-421f-a450-fd1190e8ddf0.doc', '/uploads/item/', 'cbdc0cca-29b2-4970-9121-cec7e7825dce', 'V1.1', 'm2dWB131J19naz5E4vf--__7NuSLfcmZ3ILpeCe13O', '合同名称：技术开发（委托）合同');
INSERT INTO `t_upload` VALUES ('2e020cd0-67eb-4d21-9e47-05661ea1256f', '合同审核软件（委托）合同-20230323.doc', null, 'm2dWB131J19naz5E4vf--__7NuSLfcmZ3ILpeCe13O_upload', '.doc', '113664', 'hnu7lc0tv0okrCNI0y0', '2023-08-25 05:39:27', '/uploads/item/2023/8/2e020cd0-67eb-4d21-9e47-05661ea1256f.doc', '/uploads/item/', '8554c037-94c8-421f-a450-fd1190e8ddf0', 'V1.2', 'm2dWB131J19naz5E4vf--__7NuSLfcmZ3ILpeCe13O', '合同名称：技术开发（委托）合同');
INSERT INTO `t_upload` VALUES ('关于“建议采购合同管理软件”的请示-20221222-TwC8yTaErEHBdnsYGqs', '关于“建议采购合同管理软件”的请示-20221222.pdf', null, 'xCYualpZ1RzgS59EORu--__76cQLPT8YFrSM4mfQ9K_stamp_file', '.pdf', '289394', 'MfokklYeqit6DecvrOu', '2023-08-25 06:38:41', '/uploads/item/2023/8/关于“建议采购合同管理软件”的请示-20221222-TwC8yTaErEHBdnsYGqs.pdf', '/uploads/item/', '', 'V1.0', 'xCYualpZ1RzgS59EORu--__76cQLPT8YFrSM4mfQ9K', '签署中心');
INSERT INTO `t_upload` VALUES ('关于“合同审核软件采购及部署方案”请示-jR2xFiX63UbBh3uNBMk', '关于“合同审核软件采购及部署方案”请示.pdf', null, 'xCYualpZ1RzgS59EORu--__76cQLPT8YFrSM4mfQ9K_stamp_file', '.pdf', '132887', 'MfokklYeqit6DecvrOu', '2023-08-25 06:41:45', '/uploads/item/2023/8/关于“合同审核软件采购及部署方案”请示-jR2xFiX63UbBh3uNBMk.pdf', '/uploads/item/', '', 'V1.0', 'xCYualpZ1RzgS59EORu--__76cQLPT8YFrSM4mfQ9K', '签署中心');
INSERT INTO `t_upload` VALUES ('关于“合同审核软件采购及部署方案”请示-sPXxao34s6asvEkUWrc', '关于“合同审核软件采购及部署方案”请示.pdf', null, 'xCYualpZ1RzgS59EORu--__76cQLPT8YFrSM4mfQ9K_stamp_file', '.pdf', '132887', 'MfokklYeqit6DecvrOu', '2023-08-25 06:42:34', '/uploads/item/2023/8/关于“合同审核软件采购及部署方案”请示-sPXxao34s6asvEkUWrc.pdf', '/uploads/item/', '', 'V1.0', 'xCYualpZ1RzgS59EORu--__76cQLPT8YFrSM4mfQ9K', '签署中心');
INSERT INTO `t_upload` VALUES ('关于“合同审核软件采购及部署方案”请示-gj9x5Vblfeq9UvmPXhe', '关于“合同审核软件采购及部署方案”请示.pdf', null, 'xCYualpZ1RzgS59EORu--__76cQLPT8YFrSM4mfQ9K_stamp_file', '.pdf', '132887', 'MfokklYeqit6DecvrOu', '2023-08-25 06:46:52', '/uploads/item/2023/8/关于“合同审核软件采购及部署方案”请示-gj9x5Vblfeq9UvmPXhe.pdf', '/uploads/item/', '', 'V1.0', 'xCYualpZ1RzgS59EORu--__76cQLPT8YFrSM4mfQ9K', '签署中心');
INSERT INTO `t_upload` VALUES ('合同模板 (1)-yuAAUwhcfmisJd42Jq8', '合同模板 (1).docx', null, 'xCYualpZ1RzgS59EORu--__76cQLPT8YFrSM4mfQ9K_stamp_file', '.docx', '22297', 'MfokklYeqit6DecvrOu', '2023-08-25 07:43:47', '/uploads/item/2023/8/合同模板 (1)-yuAAUwhcfmisJd42Jq8.docx', '/uploads/item/', '', 'V1.0', 'xCYualpZ1RzgS59EORu--__76cQLPT8YFrSM4mfQ9K', '签署中心');
INSERT INTO `t_upload` VALUES ('合同模板 (1)-tsfGaBW62YbBpY1h7vE', '合同模板 (1).docx', null, 'xCYualpZ1RzgS59EORu--__76cQLPT8YFrSM4mfQ9K_stamp_file', '.docx', '22297', 'MfokklYeqit6DecvrOu', '2023-08-25 07:45:20', '/uploads/item/2023/8/合同模板 (1)-tsfGaBW62YbBpY1h7vE.docx', '/uploads/item/', '', 'V1.0', 'xCYualpZ1RzgS59EORu--__76cQLPT8YFrSM4mfQ9K', '签署中心');
INSERT INTO `t_upload` VALUES ('myapps启动步骤文档-yz0JqBYKDXlQ4Ent9HH', 'myapps启动步骤文档.docx', null, 'yMOiqgzjPlL1KYxyUdR--__7NuSLfcmZ3ILpeCe13O_annex', '.docx', '12024', '4ZTy2kciL50s5wZdnEV', '2023-08-25 07:51:42', '/uploads/item/2023/8/myapps启动步骤文档-yz0JqBYKDXlQ4Ent9HH.docx', '/uploads/item/', '', 'V1.0', 'yMOiqgzjPlL1KYxyUdR--__7NuSLfcmZ3ILpeCe13O', '合同名称：');
INSERT INTO `t_upload` VALUES ('项目导入-eHcTU1QjFq2NmTVEvKt', '项目导入.xlsx', null, 'yMOiqgzjPlL1KYxyUdR--__7NuSLfcmZ3ILpeCe13O_annex', '.xlsx', '9385', '4ZTy2kciL50s5wZdnEV', '2023-08-25 07:51:42', '/uploads/item/2023/8/项目导入-eHcTU1QjFq2NmTVEvKt.xlsx', '/uploads/item/', '', 'V1.0', 'yMOiqgzjPlL1KYxyUdR--__7NuSLfcmZ3ILpeCe13O', '合同名称：');
INSERT INTO `t_upload` VALUES ('旧系统数据量统计-XggtsuPet4k2hdzHQir', '旧系统数据量统计.xlsx', null, 'yMOiqgzjPlL1KYxyUdR--__7NuSLfcmZ3ILpeCe13O_annex', '.xlsx', '112836', '4ZTy2kciL50s5wZdnEV', '2023-08-25 07:51:42', '/uploads/item/2023/8/旧系统数据量统计-XggtsuPet4k2hdzHQir.xlsx', '/uploads/item/', '', 'V1.0', 'yMOiqgzjPlL1KYxyUdR--__7NuSLfcmZ3ILpeCe13O', '合同名称：');
INSERT INTO `t_upload` VALUES ('秦越志远方案书-ELFoRTdl0F2zbAv4ciG', '秦越志远方案书.docx', null, 'yMOiqgzjPlL1KYxyUdR--__7NuSLfcmZ3ILpeCe13O_annex', '.docx', '222046', '4ZTy2kciL50s5wZdnEV', '2023-08-25 07:51:42', '/uploads/item/2023/8/秦越志远方案书-ELFoRTdl0F2zbAv4ciG.docx', '/uploads/item/', '', 'V1.0', 'yMOiqgzjPlL1KYxyUdR--__7NuSLfcmZ3ILpeCe13O', '合同名称：');
INSERT INTO `t_upload` VALUES ('合同模板 (1)-bSeNoSG2TYAm5F6Wjuq', '合同模板 (1).docx', null, 'yMOiqgzjPlL1KYxyUdR--__7NuSLfcmZ3ILpeCe13O_annex', '.docx', '22297', '4ZTy2kciL50s5wZdnEV', '2023-08-25 07:51:42', '/uploads/item/2023/8/合同模板 (1)-bSeNoSG2TYAm5F6Wjuq.docx', '/uploads/item/', '', 'V1.0', 'yMOiqgzjPlL1KYxyUdR--__7NuSLfcmZ3ILpeCe13O', '合同名称：');
INSERT INTO `t_upload` VALUES ('activity流程引擎数据表结构-JxCrz0g60EJko395HB9', 'activity流程引擎数据表结构.docx', null, 'yMOiqgzjPlL1KYxyUdR--__7NuSLfcmZ3ILpeCe13O_annex', '.docx', '0', '4ZTy2kciL50s5wZdnEV', '2023-08-25 07:51:42', '/uploads/item/2023/8/activity流程引擎数据表结构-JxCrz0g60EJko395HB9.docx', '/uploads/item/', '', 'V1.0', 'yMOiqgzjPlL1KYxyUdR--__7NuSLfcmZ3ILpeCe13O', '合同名称：');
INSERT INTO `t_upload` VALUES ('合同模板 (1)-BvWmAysjgxd8Vyh0JSs', '合同模板 (1).docx', null, 'yMOiqgzjPlL1KYxyUdR--__7NuSLfcmZ3ILpeCe13O_upload', '.docx', '22297', '4ZTy2kciL50s5wZdnEV', '2023-08-25 07:52:39', '/uploads/item/2023/8/合同模板 (1)-BvWmAysjgxd8Vyh0JSs.docx', '/uploads/item/', '', 'V1.0', 'yMOiqgzjPlL1KYxyUdR--__7NuSLfcmZ3ILpeCe13O', '合同名称：测试');
INSERT INTO `t_upload` VALUES ('秦越志远表单流程-7ra5yyc6Iszc2KM9FMY', '秦越志远表单流程.docx', null, 'yMOiqgzjPlL1KYxyUdR--__7NuSLfcmZ3ILpeCe13O_annex', '.docx', '26341758', '4ZTy2kciL50s5wZdnEV', '2023-08-25 08:00:47', '/uploads/item/2023/8/秦越志远表单流程-7ra5yyc6Iszc2KM9FMY.docx', '/uploads/item/', '', 'V1.0', 'yMOiqgzjPlL1KYxyUdR--__7NuSLfcmZ3ILpeCe13O', '合同名称：测试');
INSERT INTO `t_upload` VALUES ('48795512-88bb-48db-ac28-c03025aa9255', '招标代理委托协议.docx', null, 'J66gFjAYgKN0Ye2yNvD--__mSryjVglOxq2vz1msZZ_template', '.docx', '21837', 'hnu7lc0tv0okrCNI0y0', '2023-08-25 08:33:21', '/uploads/item/2023/8/48795512-88bb-48db-ac28-c03025aa9255.docx', '/uploads/item/', '', 'V1.0', 'J66gFjAYgKN0Ye2yNvD--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('新建 DOCX 文档-PhEJv0FJE6U0m9HMcUS', '新建 DOCX 文档.docx', null, 'sPFlXaXsFNS1N8qvqyo--__7NuSLfcmZ3ILpeCe13O_upload', '.docx', '0', '4ZTy2kciL50s5wZdnEV', '2023-08-25 08:52:16', '/uploads/item/2023/8/新建 DOCX 文档-PhEJv0FJE6U0m9HMcUS.docx', '/uploads/item/', 'c7d4a14e-8307-49e8-95c2-abf5e88a150d', 'V1.1', 'sPFlXaXsFNS1N8qvqyo--__7NuSLfcmZ3ILpeCe13O', '合同名称：测试1');
INSERT INTO `t_upload` VALUES ('c7d4a14e-8307-49e8-95c2-abf5e88a150d', '新建 DOCX 文档.docx', null, 'sPFlXaXsFNS1N8qvqyo--__7NuSLfcmZ3ILpeCe13O_upload', '.docx', '0', '4ZTy2kciL50s5wZdnEV', '2023-08-25 08:51:50', '/uploads/item/2023/8/c7d4a14e-8307-49e8-95c2-abf5e88a150d.docx', '/uploads/item/', '', 'V1.0', 'sPFlXaXsFNS1N8qvqyo--__7NuSLfcmZ3ILpeCe13O', '合同名称：测试1');
INSERT INTO `t_upload` VALUES ('新建 DOCX 文档-rsBAWyBKgD24mZOcIwF', '新建 DOCX 文档.docx', null, 'y5GlnI0LTiHm1r1xgVI--__mSryjVglOxq2vz1msZZ_template', '.docx', '0', '4ZTy2kciL50s5wZdnEV', '2023-08-25 09:07:39', '/uploads/item/2023/8/新建 DOCX 文档-rsBAWyBKgD24mZOcIwF.docx', '/uploads/item/', 'ab2e7277-3683-4b67-b3fd-bfd896775d12', 'V1.1', 'y5GlnI0LTiHm1r1xgVI--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('ab2e7277-3683-4b67-b3fd-bfd896775d12', '新建 DOCX 文档.docx', null, 'y5GlnI0LTiHm1r1xgVI--__mSryjVglOxq2vz1msZZ_template', '.docx', '0', '4ZTy2kciL50s5wZdnEV', '2023-08-25 09:07:14', '/uploads/item/2023/8/ab2e7277-3683-4b67-b3fd-bfd896775d12.docx', '/uploads/item/', '', 'V1.0', 'y5GlnI0LTiHm1r1xgVI--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('新建 DOCX 文档-P3sLcYmmee76vXvwIln', '新建 DOCX 文档.docx', null, 'iyN43ZUtFsswESHNJ0D--__mSryjVglOxq2vz1msZZ_template', '.docx', '0', 'bCEHt1D0J8lg3TC7p9h', '2023-08-25 09:50:30', '/uploads/item/2023/8/新建 DOCX 文档-P3sLcYmmee76vXvwIln.docx', '/uploads/item/', '860d571c-206e-4594-95e2-7cc4cf5639db', 'V1.1', 'iyN43ZUtFsswESHNJ0D--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('860d571c-206e-4594-95e2-7cc4cf5639db', '新建 DOCX 文档.docx', null, 'iyN43ZUtFsswESHNJ0D--__mSryjVglOxq2vz1msZZ_template', '.docx', '0', 'bCEHt1D0J8lg3TC7p9h', '2023-08-25 09:50:13', '/uploads/item/2023/8/860d571c-206e-4594-95e2-7cc4cf5639db.docx', '/uploads/item/', '', 'V1.0', 'iyN43ZUtFsswESHNJ0D--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('新建 XLSX 工作表-UUIMCJkFNSTyzeeuEk3', '新建 XLSX 工作表.xlsx', null, 'lqSbthvb768tWYr5pwc--__mSryjVglOxq2vz1msZZ_template', '.xlsx', '0', 'bCEHt1D0J8lg3TC7p9h', '2023-08-25 09:52:24', '/uploads/item/2023/8/新建 XLSX 工作表-UUIMCJkFNSTyzeeuEk3.xlsx', '/uploads/item/', 'a136a1fb-3766-4dec-a1d1-342cffa82225', 'V1.1', 'lqSbthvb768tWYr5pwc--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('a136a1fb-3766-4dec-a1d1-342cffa82225', '新建 XLSX 工作表.xlsx', null, 'lqSbthvb768tWYr5pwc--__mSryjVglOxq2vz1msZZ_template', '.xlsx', '0', 'bCEHt1D0J8lg3TC7p9h', '2023-08-25 09:52:05', '/uploads/item/2023/8/a136a1fb-3766-4dec-a1d1-342cffa82225.xlsx', '/uploads/item/', '', 'V1.0', 'lqSbthvb768tWYr5pwc--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('新建 DOCX 文档-OX59rLfUNinSdsMiMRm', '新建 DOCX 文档.docx', null, 'ggKIwClvRuZxi3DV1qI--__mSryjVglOxq2vz1msZZ_template', '.docx', '0', 'bCEHt1D0J8lg3TC7p9h', '2023-08-25 09:56:00', '/uploads/item/2023/8/新建 DOCX 文档-OX59rLfUNinSdsMiMRm.docx', '/uploads/item/', '00605009-f7ad-4354-964a-95bde104a7c6', 'V1.1', 'ggKIwClvRuZxi3DV1qI--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('00605009-f7ad-4354-964a-95bde104a7c6', '新建 DOCX 文档.docx', null, 'ggKIwClvRuZxi3DV1qI--__mSryjVglOxq2vz1msZZ_template', '.docx', '0', 'bCEHt1D0J8lg3TC7p9h', '2023-08-25 09:55:36', '/uploads/item/2023/8/00605009-f7ad-4354-964a-95bde104a7c6.docx', '/uploads/item/', '', 'V1.0', 'ggKIwClvRuZxi3DV1qI--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('新建 DOCX 文档-LIy6EUjqgnBtPOj9OIB', '新建 DOCX 文档.docx', null, 'xLcKUjce0O0YEtDCa33--__mSryjVglOxq2vz1msZZ_template', '.docx', '0', 'bCEHt1D0J8lg3TC7p9h', '2023-08-25 09:56:47', '/uploads/item/2023/8/新建 DOCX 文档-LIy6EUjqgnBtPOj9OIB.docx', '/uploads/item/', '269d212d-0666-4968-ba80-30b8104a66e3', 'V1.1', 'xLcKUjce0O0YEtDCa33--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('269d212d-0666-4968-ba80-30b8104a66e3', '新建 DOCX 文档.docx', null, 'xLcKUjce0O0YEtDCa33--__mSryjVglOxq2vz1msZZ_template', '.docx', '0', 'bCEHt1D0J8lg3TC7p9h', '2023-08-25 09:56:34', '/uploads/item/2023/8/269d212d-0666-4968-ba80-30b8104a66e3.docx', '/uploads/item/', '', 'V1.0', 'xLcKUjce0O0YEtDCa33--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('253a01bc-b5df-4cf3-92f6-50ac5913dce2', '新建 DOCX 文档.docx', null, 'hcHjxDcu3atkoHaydJh--__mSryjVglOxq2vz1msZZ_template', '.docx', '0', 'bCEHt1D0J8lg3TC7p9h', '2023-08-25 09:57:13', '/uploads/item/2023/8/253a01bc-b5df-4cf3-92f6-50ac5913dce2.docx', '/uploads/item/', '', 'V1.0', 'hcHjxDcu3atkoHaydJh--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('用户导入模板-adVF0ggOyHokBSFpV1o', '用户导入模板.xlsx', null, 'GoD7l27E8CgQUz24Q7M--__PX2ukhQA9L2styARWJj_file', '.xlsx', '9979', '__oP0irhWXGA2oZRusW1d', '2023-08-28 01:47:13', '/uploads/item/2023/8/用户导入模板-adVF0ggOyHokBSFpV1o.xlsx', '/uploads/item/', '', 'V1.0', 'GoD7l27E8CgQUz24Q7M--__PX2ukhQA9L2styARWJj', '常规诉讼');
INSERT INTO `t_upload` VALUES ('属性 (1)-kEiljrfry2IFqw25dOr', '属性 (1).xlsx', null, 'WBZAsCBlCvcr1VYgtvR--__7NuSLfcmZ3ILpeCe13O_annex', '.xlsx', '9699', 'gWsc9qqUYEnVavXC5dM', '2023-08-28 02:29:42', '/uploads/item/2023/8/属性 (1)-kEiljrfry2IFqw25dOr.xlsx', '/uploads/item/', '', 'V1.0', 'WBZAsCBlCvcr1VYgtvR--__7NuSLfcmZ3ILpeCe13O', '合同名称：1');
INSERT INTO `t_upload` VALUES ('新建 DOCX 文档-XzfgBXRLDQsZJQ8hNAS', '新建 DOCX 文档.docx', null, 'CZHBmKn81cHEm1zMSBL--__1HnnpsDdzfkNvwYDmRk_upload', '.docx', '0', 'gWsc9qqUYEnVavXC5dM', '2023-08-28 02:31:32', '/uploads/item/2023/8/新建 DOCX 文档-XzfgBXRLDQsZJQ8hNAS.docx', '/uploads/item/', '6f4836e0-aece-4472-b6c6-0d11e98e05de', 'V1.1', 'CZHBmKn81cHEm1zMSBL--__1HnnpsDdzfkNvwYDmRk', '合同名称：111');
INSERT INTO `t_upload` VALUES ('6f4836e0-aece-4472-b6c6-0d11e98e05de', '新建 DOCX 文档.docx', null, 'CZHBmKn81cHEm1zMSBL--__1HnnpsDdzfkNvwYDmRk_upload', '.docx', '0', 'gWsc9qqUYEnVavXC5dM', '2023-08-28 02:31:14', '/uploads/item/2023/8/6f4836e0-aece-4472-b6c6-0d11e98e05de.docx', '/uploads/item/', '', 'V1.0', 'CZHBmKn81cHEm1zMSBL--__1HnnpsDdzfkNvwYDmRk', '合同名称：111');
INSERT INTO `t_upload` VALUES ('常规诉讼-7jBXgRXX2nwtQSynpA2', '常规诉讼.xlsx', null, 'ebJGjPJAePSzOUFjlIP--__76cQLPT8YFrSM4mfQ9K_stamp_file', '.xlsx', '4023', 'gWsc9qqUYEnVavXC5dM', '2023-08-28 06:17:51', '/uploads/item/2023/8/常规诉讼-7jBXgRXX2nwtQSynpA2.xlsx', '/uploads/item/', '', 'V1.0', 'ebJGjPJAePSzOUFjlIP--__76cQLPT8YFrSM4mfQ9K', '签署中心');
INSERT INTO `t_upload` VALUES ('【3.10】“开机锁屏”流量服务合作协议-优购物20230307-vx9NeosvF1HaaV3nSlg', '【3.10】“开机锁屏”流量服务合作协议-优购物20230307.docx', null, 'KvwsHgmInj94S1GJfKb--__1HnnpsDdzfkNvwYDmRk_upload', '.docx', '33673', 'MfokklYeqit6DecvrOu', '2023-08-28 07:07:09', '/uploads/item/2023/8/【3.10】“开机锁屏”流量服务合作协议-优购物20230307-vx9NeosvF1HaaV3nSlg.docx', '/uploads/item/', '', 'V1.0', 'KvwsHgmInj94S1GJfKb--__1HnnpsDdzfkNvwYDmRk', '合同名称：开机锁屏');
INSERT INTO `t_upload` VALUES ('版权维权类诉讼-sKHol5WeJs7qUbE04QW', '版权维权类诉讼.xlsx', null, 'PvuBzgZ82m44vCwEr2n--__qwo0CY05xSdyRB6uI3n_annex', '.xlsx', '4031', '__oP0irhWXGA2oZRusW1d', '2023-08-29 02:32:28', '/uploads/item/2023/8/版权维权类诉讼-sKHol5WeJs7qUbE04QW.xlsx', '/uploads/item/', '', 'V1.0', 'PvuBzgZ82m44vCwEr2n--__qwo0CY05xSdyRB6uI3n', '其他纠纷');
INSERT INTO `t_upload` VALUES ('新建 DOCX 文档-EqpSIhDvHpztDVJCsaY', '新建 DOCX 文档.docx', null, 'UysMQCBDDt53spKGtas--__mSryjVglOxq2vz1msZZ_template', '.docx', '0', '__oP0irhWXGA2oZRusW1d', '2023-08-29 02:35:03', '/uploads/item/2023/8/新建 DOCX 文档-EqpSIhDvHpztDVJCsaY.docx', '/uploads/item/', 'f609f6ad-a774-471f-9d2b-cbfc1ae4299b', 'V1.1', 'UysMQCBDDt53spKGtas--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('f609f6ad-a774-471f-9d2b-cbfc1ae4299b', '新建 DOCX 文档.docx', null, 'UysMQCBDDt53spKGtas--__mSryjVglOxq2vz1msZZ_template', '.docx', '0', '__oP0irhWXGA2oZRusW1d', '2023-08-29 02:34:30', '/uploads/item/2023/8/f609f6ad-a774-471f-9d2b-cbfc1ae4299b.docx', '/uploads/item/', '', 'V1.0', 'UysMQCBDDt53spKGtas--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('p79sV6sQ7loiwbfe3GE', '项目导入.xlsx', null, '', '.xlsx', '9221', '__oP0irhWXGA2oZRusW1d', '2023-08-29 05:45:46', '/uploads/excel/2023/p79sV6sQ7loiwbfe3GE.xlsx', '/uploads/excel/', null, 'V1.0', null, null);
INSERT INTO `t_upload` VALUES ('【6.19定版】顺义张堪文化节活动执行服务合同-MLr26GtprWLWsjNIesy', '【6.19定版】顺义张堪文化节活动执行服务合同.docx', null, 'VwVENM2qRhqqL0gxOkE--__1HnnpsDdzfkNvwYDmRk_annex', '.docx', '26402', 'UXzo63pIOMHnkEn9wGt', '2023-08-30 07:00:53', '/uploads/item/2023/8/【6.19定版】顺义张堪文化节活动执行服务合同-MLr26GtprWLWsjNIesy.docx', '/uploads/item/', '', 'V1.0', 'VwVENM2qRhqqL0gxOkE--__1HnnpsDdzfkNvwYDmRk', '合同名称：顺义区2023年端午文化节暨第二届张堪文化节活动执行服务合同');
INSERT INTO `t_upload` VALUES ('法律咨询H5改造开发服务合同-终稿-eCLvrj8xSSISDAchCBD', '法律咨询H5改造开发服务合同-终稿.docx', null, 'VgXeXSIFhrNL3F4HYpT--__7NuSLfcmZ3ILpeCe13O_annex', '.docx', '27974', '9cagMGQba7zhagaFV8r', '2023-08-30 07:44:01', '/uploads/item/2023/8/法律咨询H5改造开发服务合同-终稿-eCLvrj8xSSISDAchCBD.docx', '/uploads/item/', '', 'V1.0', 'VgXeXSIFhrNL3F4HYpT--__7NuSLfcmZ3ILpeCe13O', '合同名称：法律咨询H5改造 开发服务合同');
INSERT INTO `t_upload` VALUES ('【6.19定版】顺义张堪文化节活动执行服务合同-Ii8AJyinZAATZYKZwjZ', '【6.19定版】顺义张堪文化节活动执行服务合同.docx', null, 'cHh2PiEQoafBYrjwXw4--__1HnnpsDdzfkNvwYDmRk_upload', '.docx', '26402', 'UXzo63pIOMHnkEn9wGt', '2023-08-30 08:04:27', '/uploads/item/2023/8/【6.19定版】顺义张堪文化节活动执行服务合同-Ii8AJyinZAATZYKZwjZ.docx', '/uploads/item/', '', 'V1.0', 'cHh2PiEQoafBYrjwXw4--__1HnnpsDdzfkNvwYDmRk', '合同名称：顺义区2023年端午文化节暨第二届张堪文化节活动执行服务合同');
INSERT INTO `t_upload` VALUES ('OA-P1mtWQDFRoNhjYkJN8p', 'OA.png', null, 'cHh2PiEQoafBYrjwXw4--__1HnnpsDdzfkNvwYDmRk_annex', '.png', '609249', 'UXzo63pIOMHnkEn9wGt', '2023-08-30 08:04:39', '/uploads/item/2023/8/OA-P1mtWQDFRoNhjYkJN8p.png', '/uploads/item/', '', 'V1.0', 'cHh2PiEQoafBYrjwXw4--__1HnnpsDdzfkNvwYDmRk', '合同名称：顺义区2023年端午文化节暨第二届张堪文化节活动执行服务合同');
INSERT INTO `t_upload` VALUES ('【6.19定版】顺义张堪文化节活动执行服务合同-JugUg3PmenPYQAhoPIp', '【6.19定版】顺义张堪文化节活动执行服务合同.docx', null, 'YSdzXHuh7uXJPVWRwc5--__1HnnpsDdzfkNvwYDmRk_upload', '.docx', '26402', 'UXzo63pIOMHnkEn9wGt', '2023-08-31 01:47:23', '/uploads/item/2023/8/【6.19定版】顺义张堪文化节活动执行服务合同-JugUg3PmenPYQAhoPIp.docx', '/uploads/item/', '95805669-3232-4926-974c-5802241277a9', 'V1.1', 'YSdzXHuh7uXJPVWRwc5--__1HnnpsDdzfkNvwYDmRk', '合同名称：顺义区2023年端午文化节暨第二届张堪文化节活动执行服务合同');
INSERT INTO `t_upload` VALUES ('2023年大数据集成服务合同-3vux7Ue3iYXJsElnoxA', '2023年大数据集成服务合同.doc', null, 'vtQRtt8e4MKNBhX4KHX--__7NuSLfcmZ3ILpeCe13O_annex', '.doc', '115200', 'WlrAo0DK6YyyoGCLzBr', '2023-08-30 08:27:04', '/uploads/item/2023/8/2023年大数据集成服务合同-3vux7Ue3iYXJsElnoxA.doc', '/uploads/item/', '', 'V1.0', 'vtQRtt8e4MKNBhX4KHX--__7NuSLfcmZ3ILpeCe13O', '合同名称：大数据集成服务合同');
INSERT INTO `t_upload` VALUES ('95805669-3232-4926-974c-5802241277a9', '【6.19定版】顺义张堪文化节活动执行服务合同.docx', null, 'YSdzXHuh7uXJPVWRwc5--__1HnnpsDdzfkNvwYDmRk_upload', '.docx', '26402', '37wipIeJTuR7qrkH3Nf', '2023-08-30 08:10:41', '/uploads/item/2023/8/95805669-3232-4926-974c-5802241277a9.docx', '/uploads/item/', '', 'V1.0', 'YSdzXHuh7uXJPVWRwc5--__1HnnpsDdzfkNvwYDmRk', '合同名称：顺义区2023年端午文化节暨第二届张堪文化节活动执行服务合同');
INSERT INTO `t_upload` VALUES ('M-2023067-1ejDE6UoVGppQo3aqlu', 'M-2023067.PDF', null, '2ZMjyHNd2wskb2X9uPK--__76cQLPT8YFrSM4mfQ9K_stamp_file', '.pdf', '4850611', 'UXzo63pIOMHnkEn9wGt', '2023-08-31 02:24:07', '/uploads/item/2023/8/M-2023067-1ejDE6UoVGppQo3aqlu.pdf', '/uploads/item/', '', 'V1.0', '2ZMjyHNd2wskb2X9uPK--__76cQLPT8YFrSM4mfQ9K', '签署中心');
INSERT INTO `t_upload` VALUES ('合同模板 (1)-kEskVhL1VgjGbIbmA18', '合同模板 (1).docx', null, 'SM6qvdCkM7NkkroRK4o--__7NuSLfcmZ3ILpeCe13O_upload', '.docx', '22297', 'MfokklYeqit6DecvrOu', '2023-09-05 09:18:10', '/uploads/item/2023/9/合同模板 (1)-kEskVhL1VgjGbIbmA18.docx', '/uploads/item/', '', 'V1.0', 'SM6qvdCkM7NkkroRK4o--__7NuSLfcmZ3ILpeCe13O', '合同名称：');
INSERT INTO `t_upload` VALUES ('合同模板-36GZeUky8yGyHUBwsjt', '合同模板.docx', null, 'VQ2T4R1eNNmYM6CjA2k--__7NuSLfcmZ3ILpeCe13O_annex', '.docx', '10239', 'MfokklYeqit6DecvrOu', '2023-09-05 09:21:30', '/uploads/item/2023/9/合同模板-36GZeUky8yGyHUBwsjt.docx', '/uploads/item/', '', 'V1.0', 'VQ2T4R1eNNmYM6CjA2k--__7NuSLfcmZ3ILpeCe13O', '合同名称：');
INSERT INTO `t_upload` VALUES ('合同模板 (1)-hmXopYMkycyveO6If3P', '合同模板 (1).docx', null, 'hVlswA59WS7Zuo1uhDu--__7NuSLfcmZ3ILpeCe13O_annex', '.docx', '22297', 'MfokklYeqit6DecvrOu', '2023-09-05 09:22:20', '/uploads/item/2023/9/合同模板 (1)-hmXopYMkycyveO6If3P.docx', '/uploads/item/', '', 'V1.0', 'hVlswA59WS7Zuo1uhDu--__7NuSLfcmZ3ILpeCe13O', '合同名称：');
INSERT INTO `t_upload` VALUES ('合同模板 (1)-bdjWrAPrDEfTc8A34vt', '合同模板 (1).docx', null, '6lC4mnajYEpcrrFgwDE--__7NuSLfcmZ3ILpeCe13O_annex', '.docx', '22297', 'MfokklYeqit6DecvrOu', '2023-09-05 09:23:46', '/uploads/item/2023/9/合同模板 (1)-bdjWrAPrDEfTc8A34vt.docx', '/uploads/item/', '', 'V1.0', '6lC4mnajYEpcrrFgwDE--__7NuSLfcmZ3ILpeCe13O', '合同名称：');
INSERT INTO `t_upload` VALUES ('合同模板 (1.1)-MdgFhQQNpGC29nskSGN', '合同模板 (1.1).docx', null, 'k7nSEkQRcT69VG9CTAl--__qKVklNXbUpNGc2yOtwK_annex', '.docx', '22297', 'MfokklYeqit6DecvrOu', '2023-09-14 03:05:57', '/uploads/item/2023/9/合同模板 (1.1)-MdgFhQQNpGC29nskSGN.docx', '/uploads/item/', '', 'V1.0', 'k7nSEkQRcT69VG9CTAl--__qKVklNXbUpNGc2yOtwK', '合同名称：借款合同');
INSERT INTO `t_upload` VALUES ('北京人艺2023短视频制作协议最终版-RK5Ko3TtR1cVeaOMqzl', '北京人艺2023短视频制作协议最终版.doc', null, 'oqeyaCCvEOjNNspyyq8--__1HnnpsDdzfkNvwYDmRk_annex', '.doc', '250880', 'UXzo63pIOMHnkEn9wGt', '2023-10-07 07:04:16', '/uploads/item/2023/10/北京人艺2023短视频制作协议最终版-RK5Ko3TtR1cVeaOMqzl.doc', '/uploads/item/', '', 'V1.0', 'oqeyaCCvEOjNNspyyq8--__1HnnpsDdzfkNvwYDmRk', '合同名称：2023年北京人艺短视频拍摄制作委托协议');
INSERT INTO `t_upload` VALUES ('2023年度合作协议-北京时间&汉华易美0619-raPjEm0tcA5fuClwF39', '2023年度合作协议-北京时间&汉华易美0619.docx', null, 'v039RmYZMgCWfRld8uv--__7NuSLfcmZ3ILpeCe13O_upload', '.docx', '51530', 'to1jk20TgizISD5GXN4', '2023-10-07 08:00:55', '/uploads/item/2023/10/2023年度合作协议-北京时间&汉华易美0619-raPjEm0tcA5fuClwF39.docx', '/uploads/item/', '', 'V1.0', 'v039RmYZMgCWfRld8uv--__7NuSLfcmZ3ILpeCe13O', '合同名称：版权采购');
INSERT INTO `t_upload` VALUES ('附件2：倒签说明-7cEJ9gzsBZ2uiUeQOTB', '附件2：倒签说明.docx', null, 'v039RmYZMgCWfRld8uv--__7NuSLfcmZ3ILpeCe13O_annex', '.docx', '14177', 'to1jk20TgizISD5GXN4', '2023-10-07 08:01:17', '/uploads/item/2023/10/附件2：倒签说明-7cEJ9gzsBZ2uiUeQOTB.docx', '/uploads/item/', '', 'V1.0', 'v039RmYZMgCWfRld8uv--__7NuSLfcmZ3ILpeCe13O', '合同名称：版权采购');
INSERT INTO `t_upload` VALUES ('附件5：2022年下载转载数据-klOqzOMwyf0WrFjEbZj', '附件5：2022年下载转载数据.xlsx', null, 'v039RmYZMgCWfRld8uv--__7NuSLfcmZ3ILpeCe13O_annex', '.xlsx', '13142', 'to1jk20TgizISD5GXN4', '2023-10-07 08:01:17', '/uploads/item/2023/10/附件5：2022年下载转载数据-klOqzOMwyf0WrFjEbZj.xlsx', '/uploads/item/', '', 'V1.0', 'v039RmYZMgCWfRld8uv--__7NuSLfcmZ3ILpeCe13O', '合同名称：版权采购');
INSERT INTO `t_upload` VALUES ('附件6：2023年1-4月下载转载数据-uBJq1Ufmha9bmCcIMBu', '附件6：2023年1-4月下载转载数据.xlsx', null, 'v039RmYZMgCWfRld8uv--__7NuSLfcmZ3ILpeCe13O_annex', '.xlsx', '12034', 'to1jk20TgizISD5GXN4', '2023-10-07 08:01:18', '/uploads/item/2023/10/附件6：2023年1-4月下载转载数据-uBJq1Ufmha9bmCcIMBu.xlsx', '/uploads/item/', '', 'V1.0', 'v039RmYZMgCWfRld8uv--__7NuSLfcmZ3ILpeCe13O', '合同名称：版权采购');
INSERT INTO `t_upload` VALUES ('附件4：2021年下载和转载数据-QrJXzNsjjmFgGCPtkzg', '附件4：2021年下载和转载数据.pdf', null, 'v039RmYZMgCWfRld8uv--__7NuSLfcmZ3ILpeCe13O_annex', '.pdf', '131727', 'to1jk20TgizISD5GXN4', '2023-10-07 08:01:18', '/uploads/item/2023/10/附件4：2021年下载和转载数据-QrJXzNsjjmFgGCPtkzg.pdf', '/uploads/item/', '', 'V1.0', 'v039RmYZMgCWfRld8uv--__7NuSLfcmZ3ILpeCe13O', '合同名称：版权采购');
INSERT INTO `t_upload` VALUES ('附件1：情况说明-4m9yH3QYTtDKNptZ5q1', '附件1：情况说明.pdf', null, 'v039RmYZMgCWfRld8uv--__7NuSLfcmZ3ILpeCe13O_annex', '.pdf', '247385', 'to1jk20TgizISD5GXN4', '2023-10-07 08:01:19', '/uploads/item/2023/10/附件1：情况说明-4m9yH3QYTtDKNptZ5q1.pdf', '/uploads/item/', '', 'V1.0', 'v039RmYZMgCWfRld8uv--__7NuSLfcmZ3ILpeCe13O', '合同名称：版权采购');
INSERT INTO `t_upload` VALUES ('附件4：2023年1-4月下载转载数据-AyVb4zzWmc03bpMczhH', '附件4：2023年1-4月下载转载数据.pdf', null, 'v039RmYZMgCWfRld8uv--__7NuSLfcmZ3ILpeCe13O_annex', '.pdf', '122323', 'to1jk20TgizISD5GXN4', '2023-10-07 08:01:18', '/uploads/item/2023/10/附件4：2023年1-4月下载转载数据-AyVb4zzWmc03bpMczhH.pdf', '/uploads/item/', '', 'V1.0', 'v039RmYZMgCWfRld8uv--__7NuSLfcmZ3ILpeCe13O', '合同名称：版权采购');
INSERT INTO `t_upload` VALUES ('附件5：2022年下载和转载数据-0zHEzufPyJCLY95H7SU', '附件5：2022年下载和转载数据.pdf', null, 'v039RmYZMgCWfRld8uv--__7NuSLfcmZ3ILpeCe13O_annex', '.pdf', '133092', 'to1jk20TgizISD5GXN4', '2023-10-07 08:01:19', '/uploads/item/2023/10/附件5：2022年下载和转载数据-0zHEzufPyJCLY95H7SU.pdf', '/uploads/item/', '', 'V1.0', 'v039RmYZMgCWfRld8uv--__7NuSLfcmZ3ILpeCe13O', '合同名称：版权采购');
INSERT INTO `t_upload` VALUES ('附件3：2023第20期会议纪要-MSI8304n0tyrkUAY1KJ', '附件3：2023第20期会议纪要.jpg', null, 'v039RmYZMgCWfRld8uv--__7NuSLfcmZ3ILpeCe13O_annex', '.jpg', '1064291', 'to1jk20TgizISD5GXN4', '2023-10-07 08:01:22', '/uploads/item/2023/10/附件3：2023第20期会议纪要-MSI8304n0tyrkUAY1KJ.jpg', '/uploads/item/', '', 'V1.0', 'v039RmYZMgCWfRld8uv--__7NuSLfcmZ3ILpeCe13O', '合同名称：版权采购');
INSERT INTO `t_upload` VALUES ('附件7：独家供应商证明文件-cDK3EW6RANA1QVxRRlw', '附件7：独家供应商证明文件.pdf', null, 'v039RmYZMgCWfRld8uv--__7NuSLfcmZ3ILpeCe13O_annex', '.pdf', '1057779', 'to1jk20TgizISD5GXN4', '2023-10-07 08:01:22', '/uploads/item/2023/10/附件7：独家供应商证明文件-cDK3EW6RANA1QVxRRlw.pdf', '/uploads/item/', '', 'V1.0', 'v039RmYZMgCWfRld8uv--__7NuSLfcmZ3ILpeCe13O', '合同名称：版权采购');
INSERT INTO `t_upload` VALUES ('“开机锁屏”流量服务合作协议-优购物20230629-9Cig99tMuv2x6C4SAWM', '“开机锁屏”流量服务合作协议-优购物20230629.docx', null, 'mN9k0QSIm1lbrmMF3HA--__1HnnpsDdzfkNvwYDmRk_upload', '.docx', '33175', 'OzdSAQBjeXp51fcn9wB', '2023-10-07 08:05:04', '/uploads/item/2023/10/“开机锁屏”流量服务合作协议-优购物20230629-9Cig99tMuv2x6C4SAWM.docx', '/uploads/item/', '', 'V1.0', 'mN9k0QSIm1lbrmMF3HA--__1HnnpsDdzfkNvwYDmRk', '合同名称：“开机锁屏”流量服务合作协议');
INSERT INTO `t_upload` VALUES ('【华视网聚】终版北京IPTV内容合作协议2023年版20230814-3aXtIMulmPJ1BsCvb34', '【华视网聚】终版北京IPTV内容合作协议2023年版20230814.docx', null, 'tWuXz85NPY8s8pi105P--__hB08FiQYT6fsHmKbhVQ_annex', '.docx', '59752', '9MnvWeuJ3hdu3yrkyT7', '2023-10-08 03:09:16', '/uploads/item/2023/10/【华视网聚】终版北京IPTV内容合作协议2023年版20230814-3aXtIMulmPJ1BsCvb34.docx', '/uploads/item/', '', 'V1.0', 'tWuXz85NPY8s8pi105P--__hB08FiQYT6fsHmKbhVQ', '合同名称：北京新媒体集团与捷成华视网聚内容合作协议');
INSERT INTO `t_upload` VALUES ('【华视网聚】终版北京IPTV内容合作协议2023年版20230814-lC9ftn2rgYn4PRUpMeI', '【华视网聚】终版北京IPTV内容合作协议2023年版20230814.docx', null, 'QZFu9fcaoaLKM1cLGpt--__hB08FiQYT6fsHmKbhVQ_upload', '.docx', '59752', '9MnvWeuJ3hdu3yrkyT7', '2023-10-09 02:18:35', '/uploads/item/2023/10/【华视网聚】终版北京IPTV内容合作协议2023年版20230814-lC9ftn2rgYn4PRUpMeI.docx', '/uploads/item/', '', 'V1.0', 'QZFu9fcaoaLKM1cLGpt--__hB08FiQYT6fsHmKbhVQ', '合同名称：北京新媒体（集团）有限公司与捷成华视网聚（常州）文化有限公司之北京IPTV内容合作协议');
INSERT INTO `t_upload` VALUES ('【华视网聚】终版北京IPTV内容合作协议2023年版20230814-Dm8cW0n73KdrmyhCqkO', '【华视网聚】终版北京IPTV内容合作协议2023年版20230814.docx', null, 'z1NZYXNNz6Vy6EDr90Y--__7NuSLfcmZ3ILpeCe13O_annex', '.docx', '59752', '9MnvWeuJ3hdu3yrkyT7', '2023-10-09 03:03:45', '/uploads/item/2023/10/【华视网聚】终版北京IPTV内容合作协议2023年版20230814-Dm8cW0n73KdrmyhCqkO.docx', '/uploads/item/', '', 'V1.0', 'z1NZYXNNz6Vy6EDr90Y--__7NuSLfcmZ3ILpeCe13O', '合同名称：北京新媒体（集团）有限公司与捷成华视网聚（常州）文化传媒有限公司之北京IPTV内容合作协议');
INSERT INTO `t_upload` VALUES ('【华视网聚】终版北京IPTV内容合作协议2023年版20230814-SOM7AaACV1lP0Q4YqwA', '【华视网聚】终版北京IPTV内容合作协议2023年版20230814.docx', null, 'z1NZYXNNz6Vy6EDr90Y--__7NuSLfcmZ3ILpeCe13O_upload', '.docx', '59752', '9MnvWeuJ3hdu3yrkyT7', '2023-10-09 03:04:45', '/uploads/item/2023/10/【华视网聚】终版北京IPTV内容合作协议2023年版20230814-SOM7AaACV1lP0Q4YqwA.docx', '/uploads/item/', '', 'V1.0', 'z1NZYXNNz6Vy6EDr90Y--__7NuSLfcmZ3ILpeCe13O', '合同名称：北京新媒体（集团）有限公司与捷成华视网聚（常州）文化传媒有限公司之北京IPTV内容合作协议');
INSERT INTO `t_upload` VALUES ('Logo抬头模板-DZxDYDiT8R1hyjfsROI', 'Logo抬头模板.docx', null, 'YEK86inVWAGRhoCfPr9--__7NuSLfcmZ3ILpeCe13O_upload', '.docx', '43670', '0FRa9PPu6GzctvcFxIX', '2023-10-10 07:01:04', '/uploads/item/2023/10/Logo抬头模板-DZxDYDiT8R1hyjfsROI.docx', '/uploads/item/', '', 'V1.0', 'YEK86inVWAGRhoCfPr9--__7NuSLfcmZ3ILpeCe13O', '合同名称：123');
INSERT INTO `t_upload` VALUES ('Logo抬头模板-JO7K7Hlpw8LwF5xVyDS', 'Logo抬头模板.docx', null, 'YEK86inVWAGRhoCfPr9--__7NuSLfcmZ3ILpeCe13O_annex', '.docx', '43670', '0FRa9PPu6GzctvcFxIX', '2023-10-10 07:01:36', '/uploads/item/2023/10/Logo抬头模板-JO7K7Hlpw8LwF5xVyDS.docx', '/uploads/item/', '', 'V1.0', 'YEK86inVWAGRhoCfPr9--__7NuSLfcmZ3ILpeCe13O', '合同名称：123');
INSERT INTO `t_upload` VALUES ('设备（货物）采购合同模版-1a2XoDD1G0nno6zbBwz', '设备（货物）采购合同模版.doc', null, 'SckyOHvu1LcFVEBbZGU--__mSryjVglOxq2vz1msZZ_template', '.doc', '49664', 'hnu7lc0tv0okrCNI0y0', '2023-10-16 05:32:11', '/uploads/item/2023/10/设备（货物）采购合同模版-1a2XoDD1G0nno6zbBwz.doc', '/uploads/item/', '', 'V1.0', 'SckyOHvu1LcFVEBbZGU--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('“开机锁屏”流量服务合作协议-优购物20230629-iYB23wYiobm89GM4azk', '“开机锁屏”流量服务合作协议-优购物20230629.docx', null, 'uAaIKZOuiqVbTKBnTZU--__1HnnpsDdzfkNvwYDmRk_upload', '.docx', '33175', 'OzdSAQBjeXp51fcn9wB', '2023-10-16 07:58:52', '/uploads/item/2023/10/“开机锁屏”流量服务合作协议-优购物20230629-iYB23wYiobm89GM4azk.docx', '/uploads/item/', '', 'V1.0', 'uAaIKZOuiqVbTKBnTZU--__1HnnpsDdzfkNvwYDmRk', '合同名称：“开机锁屏”流量服务合作协议');
INSERT INTO `t_upload` VALUES ('myapps启动步骤文档-2qbDlyaVjfNp3tYM7uD', 'myapps启动步骤文档.docx', null, 'pn6HShEsY3XRzRhRFv6--__mSryjVglOxq2vz1msZZ_template', '.docx', '12293', 'hnu7lc0tv0okrCNI0y0', '2023-10-20 07:01:27', '/uploads/item/2023/10/myapps启动步骤文档-2qbDlyaVjfNp3tYM7uD.docx', '/uploads/item/', '', 'V1.0', 'pn6HShEsY3XRzRhRFv6--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('微鳄品牌策划V4.0-OSrijFOTAFCvpetBcOU', '微鳄品牌策划V4.0.pptx', null, 'rR4CDLdRmW00fkfR2Ev--__76cQLPT8YFrSM4mfQ9K_stamp_file', '.pptx', '4765934', 'MfokklYeqit6DecvrOu', '2023-10-23 07:23:34', '/uploads/item/2023/10/微鳄品牌策划V4.0-OSrijFOTAFCvpetBcOU.pptx', '/uploads/item/', '', 'V1.0', 'rR4CDLdRmW00fkfR2Ev--__76cQLPT8YFrSM4mfQ9K', '签署中心');
INSERT INTO `t_upload` VALUES ('北京人艺2023短视频制作协议最终版-06hgFVqeK7DTcR9ryAm', '北京人艺2023短视频制作协议最终版.doc', null, 'nFlwi5bOtq4slRDVU28--__1HnnpsDdzfkNvwYDmRk_upload', '.doc', '250880', 'UXzo63pIOMHnkEn9wGt', '2023-10-26 07:09:33', '/uploads/item/2023/10/北京人艺2023短视频制作协议最终版-06hgFVqeK7DTcR9ryAm.doc', '/uploads/item/', '', 'V1.0', 'nFlwi5bOtq4slRDVU28--__1HnnpsDdzfkNvwYDmRk', '合同名称：2023年北京人艺短视频拍摄制作委托协议');
INSERT INTO `t_upload` VALUES ('1-广告类-一般广告发布合同（我方受托）-修订-bghlf0LZVtuNCpKhDrm', '1-广告类-一般广告发布合同（我方受托）-修订.doc', null, 'jEVJNUHmyLxrVqPnE1Y--__mSryjVglOxq2vz1msZZ_template', '.doc', '75776', '37wipIeJTuR7qrkH3Nf', '2023-10-31 01:50:20', '/uploads/item/2023/10/1-广告类-一般广告发布合同（我方受托）-修订-bghlf0LZVtuNCpKhDrm.doc', '/uploads/item/', '', 'V1.0', 'jEVJNUHmyLxrVqPnE1Y--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('1-广告类-一般广告发布合同（我方受托）-V1.0-AEefSIqmMSFUkM3R0OU', '1-广告类-一般广告发布合同（我方受托）-V1.0.doc', null, 'qq9pSO4IYraIpxY0nMs--__mSryjVglOxq2vz1msZZ_template', '.doc', '75776', '37wipIeJTuR7qrkH3Nf', '2023-10-31 02:10:47', '/uploads/item/2023/10/1-广告类-一般广告发布合同（我方受托）-V1.0-AEefSIqmMSFUkM3R0OU.doc', '/uploads/item/', '', 'V1.0', 'qq9pSO4IYraIpxY0nMs--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('1-广告类-一般广告发布合同（我方受托）-V1.0-ffpd6tI8lRdU4PJx8hp', '1-广告类-一般广告发布合同（我方受托）-V1.0.doc', null, '5j3hpofiCNgIOMJvkHI--__mSryjVglOxq2vz1msZZ_template', '.doc', '75776', '37wipIeJTuR7qrkH3Nf', '2023-10-31 02:13:07', '/uploads/item/2023/10/1-广告类-一般广告发布合同（我方受托）-V1.0-ffpd6tI8lRdU4PJx8hp.doc', '/uploads/item/', '', 'V1.0', '5j3hpofiCNgIOMJvkHI--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('2-直播类-直播宣传推广服务合同（我方受托）-V1.0-y62C1l0zgEYq0ApBetN', '2-直播类-直播宣传推广服务合同（我方受托）-V1.0.docx', null, 'C7JhbFUbFjpbRKIrm6D--__mSryjVglOxq2vz1msZZ_template', '.docx', '49413', '37wipIeJTuR7qrkH3Nf', '2023-10-31 02:18:33', '/uploads/item/2023/10/2-直播类-直播宣传推广服务合同（我方受托）-V1.0-y62C1l0zgEYq0ApBetN.docx', '/uploads/item/', '', 'V1.0', 'C7JhbFUbFjpbRKIrm6D--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('3-视频类-视频制作及宣传（我方受托）-V1.0-GRxo3TiJ8Cyr0XPaiad', '3-视频类-视频制作及宣传（我方受托）-V1.0.docx', null, '5pI332ix71tuHb1tGmA--__mSryjVglOxq2vz1msZZ_template', '.docx', '47177', '37wipIeJTuR7qrkH3Nf', '2023-10-31 02:21:29', '/uploads/item/2023/10/3-视频类-视频制作及宣传（我方受托）-V1.0-GRxo3TiJ8Cyr0XPaiad.docx', '/uploads/item/', '', 'V1.0', '5pI332ix71tuHb1tGmA--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('4-采购服务类-技术服务合作协议（…注意替换相应技术服务名称）-V1.0-uFINI4GkdkPBHtnFbKf', '4-采购服务类-技术服务合作协议（…注意替换相应技术服务名称）-V1.0.doc', null, 'QyOkiXmWcfWnJzeXrvq--__mSryjVglOxq2vz1msZZ_template', '.doc', '67584', '37wipIeJTuR7qrkH3Nf', '2023-10-31 02:23:47', '/uploads/item/2023/10/4-采购服务类-技术服务合作协议（…注意替换相应技术服务名称）-V1.0-uFINI4GkdkPBHtnFbKf.doc', '/uploads/item/', '', 'V1.0', 'QyOkiXmWcfWnJzeXrvq--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('5-采购服务类-视频后期制作委托协议-V1.0-e6kOjZmVkUst8gHk2v6', '5-采购服务类-视频后期制作委托协议-V1.0.doc', null, 'sHFy2goeDosxXVqTg1Q--__mSryjVglOxq2vz1msZZ_template', '.doc', '401408', '37wipIeJTuR7qrkH3Nf', '2023-10-31 02:29:34', '/uploads/item/2023/10/5-采购服务类-视频后期制作委托协议-V1.0-e6kOjZmVkUst8gHk2v6.doc', '/uploads/item/', '', 'V1.0', 'sHFy2goeDosxXVqTg1Q--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('6-采购服务类-导播摄像服务合作协议-V1.0-vdOjjGWetJv2cDbU15N', '6-采购服务类-导播摄像服务合作协议-V1.0.docx', null, 'spfSZmql8S7tWO6bvcT--__mSryjVglOxq2vz1msZZ_template', '.docx', '43924', '37wipIeJTuR7qrkH3Nf', '2023-10-31 02:30:04', '/uploads/item/2023/10/6-采购服务类-导播摄像服务合作协议-V1.0-vdOjjGWetJv2cDbU15N.docx', '/uploads/item/', '', 'V1.0', 'spfSZmql8S7tWO6bvcT--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('7-采购服务类-演员演出统筹服务协议-V1.0-ji3hhH2UEDTHhxg0S4V', '7-采购服务类-演员演出统筹服务协议-V1.0.doc', null, 'fKn49moRZsNVOfMwNkU--__mSryjVglOxq2vz1msZZ_template', '.doc', '411648', '37wipIeJTuR7qrkH3Nf', '2023-10-31 02:30:38', '/uploads/item/2023/10/7-采购服务类-演员演出统筹服务协议-V1.0-ji3hhH2UEDTHhxg0S4V.doc', '/uploads/item/', '', 'V1.0', 'fKn49moRZsNVOfMwNkU--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('8-采购服务类-演出服务协议-V1.0-mfP8h7vmiTmVTRvA5XG', '8-采购服务类-演出服务协议-V1.0.docx', null, 'w41wRtwma0NOmOnmOkD--__mSryjVglOxq2vz1msZZ_template', '.docx', '67433', '37wipIeJTuR7qrkH3Nf', '2023-10-31 02:31:39', '/uploads/item/2023/10/8-采购服务类-演出服务协议-V1.0-mfP8h7vmiTmVTRvA5XG.docx', '/uploads/item/', '', 'V1.0', 'w41wRtwma0NOmOnmOkD--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('9-采购服务类-口译服务协议-V1.0-EdRqbfmMbgmI2jih1d4', '9-采购服务类-口译服务协议-V1.0.doc', null, 'LlDoWUoRPsmV3KIQMpi--__mSryjVglOxq2vz1msZZ_template', '.doc', '71680', '37wipIeJTuR7qrkH3Nf', '2023-10-31 02:32:10', '/uploads/item/2023/10/9-采购服务类-口译服务协议-V1.0-EdRqbfmMbgmI2jih1d4.doc', '/uploads/item/', '', 'V1.0', 'LlDoWUoRPsmV3KIQMpi--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('6-采购服务类-导播摄像服务合作协议-V1.0-Z3Bd6SNB0RUk2BcNndH', '6-采购服务类-导播摄像服务合作协议-V1.0.docx', null, 'Q24zJAckexKxKh2WZVc--__mSryjVglOxq2vz1msZZ_template', '.docx', '49541', '37wipIeJTuR7qrkH3Nf', '2023-10-31 04:08:21', '/uploads/item/2023/10/6-采购服务类-导播摄像服务合作协议-V1.0-Z3Bd6SNB0RUk2BcNndH.docx', '/uploads/item/', '', 'V1.0', 'Q24zJAckexKxKh2WZVc--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('7-采购服务类-演员演出统筹服务协议-V1.0-0y8h1EF6bBGz7Ob8haj', '7-采购服务类-演员演出统筹服务协议-V1.0.doc', null, '6VqW5hsilWAtkA7Am8O--__mSryjVglOxq2vz1msZZ_template', '.doc', '398848', '37wipIeJTuR7qrkH3Nf', '2023-10-31 04:09:15', '/uploads/item/2023/10/7-采购服务类-演员演出统筹服务协议-V1.0-0y8h1EF6bBGz7Ob8haj.doc', '/uploads/item/', '', 'V1.0', '6VqW5hsilWAtkA7Am8O--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('8-采购服务类-演出服务协议-V1.0-vr3t6vjrVK4gKE6l87U', '8-采购服务类-演出服务协议-V1.0.docx', null, 'YY7fsheJyTWzc6glL0W--__mSryjVglOxq2vz1msZZ_template', '.docx', '58373', '37wipIeJTuR7qrkH3Nf', '2023-10-31 04:09:47', '/uploads/item/2023/10/8-采购服务类-演出服务协议-V1.0-vr3t6vjrVK4gKE6l87U.docx', '/uploads/item/', '', 'V1.0', 'YY7fsheJyTWzc6glL0W--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('演播室技术服务协议-b0FZULuS3bPHfIMaCfD', '演播室技术服务协议.docx', null, '6qXVdvsuoj3UzvAB5xt--__mSryjVglOxq2vz1msZZ_template', '.docx', '18079', 'WlrAo0DK6YyyoGCLzBr', '2023-10-16 05:30:29', '/uploads/item/2023/10/演播室技术服务协议-b0FZULuS3bPHfIMaCfD.docx', '/uploads/item/', '', 'V1.0', '6qXVdvsuoj3UzvAB5xt--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('1-广告类-一般广告发布合同（我方受托）-V1.0-AYmby5CKiABT9kjrGT9', '1-广告类-一般广告发布合同（我方受托）-V1.0.doc', null, 'HEoXi9eOhSvvM8G2Z1f--__mSryjVglOxq2vz1msZZ_template', '.doc', '76800', '37wipIeJTuR7qrkH3Nf', '2023-11-08 07:53:39', '/uploads/item/2023/11/1-广告类-一般广告发布合同（我方受托）-V1.0-AYmby5CKiABT9kjrGT9.doc', '/uploads/item/', '', 'V1.0', 'HEoXi9eOhSvvM8G2Z1f--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('3-视频类-视频制作及宣传（我方受托）-V1.0-uu345KbfUyAddbiA18I', '3-视频类-视频制作及宣传（我方受托）-V1.0.docx', null, '2aIEKDlkbTXEJZGkKJL--__mSryjVglOxq2vz1msZZ_template', '.docx', '47106', '37wipIeJTuR7qrkH3Nf', '2023-11-08 07:57:47', '/uploads/item/2023/11/3-视频类-视频制作及宣传（我方受托）-V1.0-uu345KbfUyAddbiA18I.docx', '/uploads/item/', '', 'V1.0', '2aIEKDlkbTXEJZGkKJL--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('2-直播类-直播宣传推广服务合同（我方受托）-V1.0-MZCDtGR2BTkAeF3EzAI', '2-直播类-直播宣传推广服务合同（我方受托）-V1.0.docx', null, 'Eq3o61RIFbPTBUOEEqh--__mSryjVglOxq2vz1msZZ_template', '.docx', '49537', '37wipIeJTuR7qrkH3Nf', '2023-11-08 07:58:09', '/uploads/item/2023/11/2-直播类-直播宣传推广服务合同（我方受托）-V1.0-MZCDtGR2BTkAeF3EzAI.docx', '/uploads/item/', '', 'V1.0', 'Eq3o61RIFbPTBUOEEqh--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('4-采购服务类-技术服务合作协议（…注意替换相应技术服务名称）-V1.0-sNsjYhgQHiXyX5zt89i', '4-采购服务类-技术服务合作协议（…注意替换相应技术服务名称）-V1.0.doc', null, 'SfARSGYsfPGqYzGkOMc--__mSryjVglOxq2vz1msZZ_template', '.doc', '68096', '37wipIeJTuR7qrkH3Nf', '2023-11-08 07:58:45', '/uploads/item/2023/11/4-采购服务类-技术服务合作协议（…注意替换相应技术服务名称）-V1.0-sNsjYhgQHiXyX5zt89i.doc', '/uploads/item/', '', 'V1.0', 'SfARSGYsfPGqYzGkOMc--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('5-采购服务类-视频后期制作委托协议-V1.0-CgF0y9wOqATGQxr1g6E', '5-采购服务类-视频后期制作委托协议-V1.0.doc', null, 'y6RZkSsUwFqyyVPKAj2--__mSryjVglOxq2vz1msZZ_template', '.doc', '570368', '37wipIeJTuR7qrkH3Nf', '2023-11-08 07:59:10', '/uploads/item/2023/11/5-采购服务类-视频后期制作委托协议-V1.0-CgF0y9wOqATGQxr1g6E.doc', '/uploads/item/', '', 'V1.0', 'y6RZkSsUwFqyyVPKAj2--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('6-采购服务类-导播摄像服务合作协议-V1.0-jwBQi7ycIbqzJgsxIV5', '6-采购服务类-导播摄像服务合作协议-V1.0.docx', null, 'TQKnO2gYfiqWe2eKay3--__mSryjVglOxq2vz1msZZ_template', '.docx', '45801', '37wipIeJTuR7qrkH3Nf', '2023-11-08 07:59:44', '/uploads/item/2023/11/6-采购服务类-导播摄像服务合作协议-V1.0-jwBQi7ycIbqzJgsxIV5.docx', '/uploads/item/', '', 'V1.0', 'TQKnO2gYfiqWe2eKay3--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('7-采购服务类-演员演出统筹服务协议-V1.0-PZ0cfG0KOfYbdAARyJn', '7-采购服务类-演员演出统筹服务协议-V1.0.doc', null, 'jzhSyxOt0A0sPTz4HxO--__mSryjVglOxq2vz1msZZ_template', '.doc', '567808', '37wipIeJTuR7qrkH3Nf', '2023-11-08 08:00:10', '/uploads/item/2023/11/7-采购服务类-演员演出统筹服务协议-V1.0-PZ0cfG0KOfYbdAARyJn.doc', '/uploads/item/', '', 'V1.0', 'jzhSyxOt0A0sPTz4HxO--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('8-采购服务类-演出服务协议-V1.0-pmdXW27oK0lUy825YGj', '8-采购服务类-演出服务协议-V1.0.docx', null, '9zsR4FPHKHMsXZ6gLys--__mSryjVglOxq2vz1msZZ_template', '.docx', '51363', '37wipIeJTuR7qrkH3Nf', '2023-11-08 08:00:35', '/uploads/item/2023/11/8-采购服务类-演出服务协议-V1.0-pmdXW27oK0lUy825YGj.docx', '/uploads/item/', '', 'V1.0', '9zsR4FPHKHMsXZ6gLys--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('9-采购服务类-口译服务协议-V1.0-06ChxEfQ0xosmKmfy7v', '9-采购服务类-口译服务协议-V1.0.doc', null, '2u3edowFLDp04JAMAl4--__mSryjVglOxq2vz1msZZ_template', '.doc', '72704', '37wipIeJTuR7qrkH3Nf', '2023-11-08 08:01:19', '/uploads/item/2023/11/9-采购服务类-口译服务协议-V1.0-06ChxEfQ0xosmKmfy7v.doc', '/uploads/item/', '', 'V1.0', '2u3edowFLDp04JAMAl4--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('7-采购服务类-演员演出统筹服务协议-V1.0-JPd7jPh0jLm5qyGgILm', '7-采购服务类-演员演出统筹服务协议-V1.0.doc', null, 'aFmYumAYnTPntQBFI9n--__7NuSLfcmZ3ILpeCe13O_upload', '.doc', '567808', '9cagMGQba7zhagaFV8r', '2023-11-09 07:07:38', '/uploads/item/2023/11/7-采购服务类-演员演出统筹服务协议-V1.0-JPd7jPh0jLm5qyGgILm.doc', '/uploads/item/', '', 'V1.0', 'aFmYumAYnTPntQBFI9n--__7NuSLfcmZ3ILpeCe13O', '合同名称：支出合同测试');
INSERT INTO `t_upload` VALUES ('4-采购服务类-技术服务合作协议（…注意替换相应技术服务名称）-V1.0-Kf7J1YfMVibzqnsaGLY', '4-采购服务类-技术服务合作协议（…注意替换相应技术服务名称）-V1.0.doc', null, 'GNJgfHlTXdRBr5mZIuK--__1HnnpsDdzfkNvwYDmRk_upload', '.doc', '68096', '9cagMGQba7zhagaFV8r', '2023-11-09 07:12:57', '/uploads/item/2023/11/4-采购服务类-技术服务合作协议（…注意替换相应技术服务名称）-V1.0-Kf7J1YfMVibzqnsaGLY.doc', '/uploads/item/', '', 'V1.0', 'GNJgfHlTXdRBr5mZIuK--__1HnnpsDdzfkNvwYDmRk', '合同名称：收入类合同');
INSERT INTO `t_upload` VALUES ('新建 DOCX 文档-uifv7gokCLL7TIkJ6B5', '新建 DOCX 文档.docx', null, 'GDBGScj0MAEhWD0osx9--__mSryjVglOxq2vz1msZZ_template', '.docx', '0', 'Ro6tebUrjHw0LKx5ceU', '2023-11-15 01:20:29', '/uploads/item/2023/11/新建 DOCX 文档-uifv7gokCLL7TIkJ6B5.docx', '/uploads/item/', '新建 DOCX 文档-RCHwUVetNU1aDcS5Tnm', 'V1.1', 'GDBGScj0MAEhWD0osx9--__mSryjVglOxq2vz1msZZ', '合同模板');
INSERT INTO `t_upload` VALUES ('新建 DOCX 文档-RCHwUVetNU1aDcS5Tnm', '新建 DOCX 文档.docx', null, 'GDBGScj0MAEhWD0osx9--__mSryjVglOxq2vz1msZZ_template', '.docx', '0', 'Ro6tebUrjHw0LKx5ceU', '2023-11-15 01:19:50', '/uploads/item/2023/11/新建 DOCX 文档-RCHwUVetNU1aDcS5Tnm.docx', '/uploads/item/', '', 'V1.0', 'GDBGScj0MAEhWD0osx9--__mSryjVglOxq2vz1msZZ', '合同模板');

-- ----------------------------
-- Table structure for t_workflow_reminder_history
-- ----------------------------
DROP TABLE IF EXISTS `t_workflow_reminder_history`;
CREATE TABLE `t_workflow_reminder_history` (
  `ID` varchar(100) NOT NULL,
  `REMINDER_CONTENT` varchar(100) DEFAULT NULL,
  `USER_ID` varchar(100) DEFAULT NULL,
  `USER_NAME` varchar(100) DEFAULT NULL,
  `NODE_NAME` varchar(100) DEFAULT NULL,
  `DOC_ID` varchar(100) DEFAULT NULL,
  `FLOW_INSTANCE_ID` varchar(100) DEFAULT NULL,
  `DOMAINID` varchar(100) DEFAULT NULL,
  `APPLICATIONID` varchar(100) DEFAULT NULL,
  `PROCESS_TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_workflow_reminder_history
-- ----------------------------
